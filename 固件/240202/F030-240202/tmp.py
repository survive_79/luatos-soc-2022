# encoding=utf-8

# 读取文件
fd_r= open("APM32F003.bin","rb")
fd_w= open("app_pic_updata.c",'w')

tmpr = fd_r.read()
tmpw = '#include "os_task.h"\r\n'
tmpw +='#include "our_tools.h"\r\n'

tmpw +='const char updata[] = {\r'
j = 0
for i in range(len(tmpr)):
    tmpw += str("0x%02x,"% tmpr[i])
    j =j+1
    if j == 100:
        j= 0
        tmpw +='\r'
tmpw +='\r};\r\n'

tmpw +='INT16U  pic_getUPdataSize(void)\r'
tmpw +='{\r'
tmpw +='    return sizeof(updata);\r'
tmpw +='}\r'

tmpw +='void pic_readdata(INT16U ptr,INT8U *buf,INT8U len)\r'
tmpw +='{\r'
tmpw +='    bk_memcpy(buf,&updata[ptr],len);\r'
tmpw +='}\r'
# 故意少个分号，提醒要更新版本号
 
tmpw +='// char ver[]="BKLD-210813"; 固定pic完整版本号\r'
tmpw +='char* pic_getUpdataVer(void)\r'
tmpw +='{\r'
tmpw +='    static char ver[]="BKLD-220526"\r'
tmpw +='    return ver;\r'
tmpw +='}\r'


fd_w.write(tmpw)
#写文件


fd_r.close()
fd_w.close()