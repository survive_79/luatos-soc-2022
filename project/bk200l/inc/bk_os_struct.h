/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : our_struct.h
  版 本 号   : 初稿
  作    者   : rocky
  生成日期   : 2018年3月4日
  最近修改   :
  功能描述   : 数据格式定义
  函数列表   :
  修改历史   :
  1.日    期   : 2018年3月4日
    作    者   : rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __OUR_STRUCT_H__
#define __OUR_STRUCT_H__

#include "bk_msg_def.h"

/*----------------------------------------------*
 * 数据格式定义                                 *
 *----------------------------------------------*/
typedef union {
	INT16U     hword;
	struct {
		INT8U  low;
		INT8U  high;
	} bytes;
} HWORD_UNION;

typedef union {
    INT32U ulong;
    struct {
		INT8U  byte4;
		INT8U  byte3;
        INT8U  byte2;
		INT8U  byte1;
	} bytes;
} LONG_UNION;

/*
********************************************************************************
*                  DEFINE DATE STRUCT
********************************************************************************
*/
typedef struct {
    INT8U           year;
    INT8U           month;
    INT8U           day;
} DATE_T;

/*
********************************************************************************
*                  DEFINE TIME STRUCT
********************************************************************************
*/
typedef struct {
    INT8U           hour;
    INT8U           minute;
    INT8U           second;
} TIME_T;

/*
********************************************************************************
*                  DEFINE SYSTIME STRUCT
********************************************************************************
*/
typedef struct {
    DATE_T     date;
    TIME_T     time;
} SYSTIME_T;

typedef struct {
    SYSTIME_T   last_tm;        // 上次唤醒时的时间点
    INT16S      cali_minute;    // 需要补偿的分钟数，负数表示要减
} CALI_PARA_T;

/*
********************************************************************************
*                  唤醒时间设置的数据结构
********************************************************************************
*/
typedef struct {
    INT8U           day;
    INT8U           hour;
    INT8U           minute;
} AWAKE_TIME_T;

// 工作流程顺序: 先wifi，扫描一遍wifi热点后，再根据模式开GPS和LBS
#define MODE_POS_LBS    0x00    // LBS定位模式，不开GPS，LBS定位就报
#define MODE_POS_GPS    0x01    // GPS定位模式，上报完毕就休眠，但定位精度有要求，GPS定位为主
#define MODE_POS_WLBS   0x04    // wifi+LBS模式,开机先扫wifi，如果有热点则直接上报，不再开LBS；否则开LBS定位。不开GPS
#define MODE_POS_WGPS   0x05    // wifi+GPS模式，开机先开wifi，没热点则开GPS和LBS，GPS定位则报GPS位置，否则报LBS信息
//#define BLIND_SPOT_EN   0x08    // 盲区补传
#define MODE_YUNDAN     0x07    // 运单模式改到这边
#define MODE_ONLINE     0x09    // 实时在线模式

/*
typedef union {
    struct {
        INT8U dd;
        INT8U hh;
        INT8U mm;
        INT8U reserve[7];
    } DHM_1;
    struct {
        INT8U HM[10];
    }HMN5_2;  // 5时段闹铃
    struct {
        INT8U ww;
        INT8U hh;
        INT8U mm;
        INT8U reserve[7];
    } WHM_3;
} ALARM_DATA_U;
*/
// EC600E open好像不支持union赋初值，编译过不去
typedef struct {
    INT8U HM[10];   // mode1时，前3字节是dd，hh，mm，mode3时，前3字节是ww、hh、mm
} ALARM_DATA_U;

typedef struct {
    INT8U   mode;   // 工作模式。目前仅支持MODE_POS_GPS和MODE_POS_WGPS
    INT8U   sec;    // 间隔，有效范围15~240秒，再长请用普通的休眠模式
} SHORT_PARA_T;

#define ALM_MASK_ALS        0X01    // 防拆报警开关, 光感
#define ALM_MASK_FLIP_TAP   0X02    // 碰撞侧翻震动报警
#define ALM_SHAKE_WAKE      0X04    // 震动唤醒开关

typedef struct {   
    INT8U           mode;       // bit0~2=MODE_POS_LBS等, bit2表示盲区补传开关
    INT8U           alm_type;   // 闹铃类型
    HWORD_UNION     max_worktime;
    INT8U           eint_en;    // 报警使能位,1表示使能,0禁止. bit0=防拆，bit1=碰撞侧翻，bit2=震动唤醒
    ALARM_DATA_U    alm;        // 闹铃数据
} UP_PARA_NEW_T;

#define MAX_OURAPN_LEN  31  
typedef struct {
    BOOLEAN force;  // 是否强制使用，如果为true，则强制使用此apn，否则只在plmn没有匹配的apn时才使用此apn
    char    apn[MAX_OURAPN_LEN];
    char    user[MAX_OURAPN_LEN];   
    char    pswd[MAX_OURAPN_LEN];   
} APN_PARA_T;

typedef struct {
    INT8U       data;
} GENERAL_8_T;

typedef struct {
    INT16U      data;
} GENERAL_16_T;

typedef union {
    INT16U pic_alarm;
    struct {
        INT16U als_alarm:1;         /* bit0-光感报警(防拆报警) */
        INT16U t_alarm:1;           /* bit1-温度报警 */
        INT16U rh_alarm:1;          /* bit2-湿度报警 */
        INT16U monitor_alarm:1;     /* bit3-多时段监控开车报警 */       
        INT16U Reserved2:12;        /* 预留 */
    }alarm_bits;
}PIC_ALARM_U;

typedef struct {
    INT8U  cnt;                 // 总需报的次数
    INT8U  cur;                 // 当前已报的次数
    INT16U period;              // 每次的间隔,单位分钟
    PIC_ALARM_U alm_event;      // 告警事件
    INT8U  interval;            // 事件上报间隔与休眠间隔的次数
} ALARM_HDL_T;                  // 用于报警处理流程

// 震动和光感的报警灵敏度
typedef struct {
    INT8U   als_filter;     // 光感有效性过滤次数
    INT16U  als_silence;    // 光感报警静默时长
    INT8U   vibrate_filter; // 震动有效性过滤次数
    INT16U  no_vib_timeout; // 停车判断时长
} ALM_SENSE_PARA_T;

typedef struct {
    INT8U     rst_retry;  // 复位重新传输计数，表示需要短时重启几次
} RESET_RETRY_T;
// 202012224 add，秒定功能
#define EPO_CHAR_LEN    14
// 固定小数点后6位
#define DEFAULT_LAT     "24.539946"
#define DEFAULT_LONG    "118.148506"
typedef struct {
    char epo_lat[EPO_CHAR_LEN];
    char epo_long[EPO_CHAR_LEN];
} EPO_POS_T;

#define MAX_DOMAINNAME_LEN   32

typedef struct {
    BOOLEAN isvalid;                             /* 该组参数是否有效 */
    INT16U port;                                 /* 端口 */
    char   ip[MAX_DOMAINNAME_LEN];               /* ip或域名 */
} GPRS_GROUP_T;

typedef struct {        // 151211 add sms命令密码
    char ps[3];
} SMS_PSWD_T;

typedef struct {
    INT8U cnt;      // 失败次数
    INT8U cur;      // 当前次数
    INT16U reserve;
} FAIL_DEFAULT_T;   // 160615 add, 用于在频繁报点的时候判断，超过cnt后，把周期改为默认23:59。

#define MAX_SHAKE_CNT   5   // 最大只能设置5个时段
typedef struct {
    INT8U hour;
    INT8U minute;
    INT8U period;
} SHAKE_PERIOD_T;

typedef struct {
    INT8U           mode;   // 目前就是cnt。范围0~5
    SHAKE_PERIOD_T  shake[MAX_SHAKE_CNT];
} MULTI_SHAKE_PARA_T;

#define SYS_TELLEN      15
typedef struct {
	INT8U      len;
	INT8U      tel[SYS_TELLEN];
} TEL_T;

#endif /* __OUR_STRUCT_H__ */




