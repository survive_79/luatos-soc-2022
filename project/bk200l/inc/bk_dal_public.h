/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : dal_public.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2020年8月5日
  最近修改   :
  功能描述   : dal_public.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2020年8月5日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __DAL_PUBLIC_H__
#define __DAL_PUBLIC_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_struct.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define RESET_DEFAULT       5       // 190313 change为5次。 2次短时重启。
#if AGPS_ON > 0
#if USE_PRIVATE_APN > 0
#define GPS_CHEATER_DEFAULT 120
#else
#define GPS_CHEATER_DEFAULT 90      // 180603 change, 关闭AGPS，则默认时长为120秒
#endif
#else
#define GPS_CHEATER_DEFAULT 120
#endif

/*
********************************************************************************
*                  DEFINE PUBLIC PARAMETERS TYPE
********************************************************************************
*/
#define SYSTIME_                        0
#define UPLOAD_PARA_                    1
#define APN_                            2
#define IP_DOM_MAIN_                    3
#define IP_DOM_BACK_                    4
#define IP_DOM_OTA_                     5
#define PREV_POS_                       6   // 本地的上一点位置信息
#define SMSC_TEL_                       7   // 短信服务号
#define SHORT_PERIOD_                   8
#define RESET_RETRY_                    9
#define SMS_PSWD_                       10  // 短信密码
#define ALARM_HDL_                      11  // 报警处理流程的参数
#define GSM_FAIL_DEF_                   12  // 找网失败复位闹铃参数
#define SIM_ICCID_                      13  // iccid读取
#define MULTI_SHAKE_                    14  // 多时段震动监测
#define ALM_SENSITIVITY_                15  // 报警灵敏度
#define GPS_CHEATER_                    16  // GPS定位时长可改
#define CALI_MINUTE_                    17  // 休眠间隔校准值

#ifdef PUBLIC_C
/*
********************************************************************************
*                  DEFINE PUBLIC INTIALIZE PARAMETERS
********************************************************************************
*/
//static SYSTIME_T        i_SYSTIME       = {15, 6, 14, 0, 0, 0};  // 15年6月14日, 实际无效，都在DAL_InitSysTime赋初值

static UP_PARA_NEW_T    i_UPLOAD_PARA   = {MODE_POS_GPS,       	/* LBS定位模式, 默认没盲区补传 BLIND_SPOT_EN */
                                           01,                  /* 经典闹铃模式*/
                                           {240},               /* 最大工作时长240秒*/
                                           0, /* ALM_SHAKE_WAKE,      默认打开震动报警 */
                                           {{0, 23, 59, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
                                           };

static CALI_PARA_T      i_cali_minute   = {{{15, 1, 1}, {0, 0, 0}}, 0};   // 休眠间隔校准值

//static APN_PARA_T       i_apn           = {true, 12, "wap.cingular",   
//static APN_PARA_T       i_apn           = {true, 11, "ip.primetel",
//static APN_PARA_T       i_apn           = {true,    "movistar.es",     
//                                                    "movistar",
//                                                    "movistar"};
#if USE_PRIVATE_APN > 0
// 新疆接入细信息 电信卡
//static APN_PARA_T       i_apn           = {TRUE,   "public.vpdn", "555@xbyt3.vpdn.xj", "123456"};
//static DOMAIN_IP_T   i_main_ip          = {7117, "172.21.1.128"};
// 大庆测试卡，联通
static APN_PARA_T       i_apn           = {TRUE,   "slof.sdapn", "14573667693@slof.sdapn", "12345678"};
static GPRS_GROUP_T   i_main_ip         = {TRUE, 8101, "10.67.41.18"};
static GPRS_GROUP_T   i_back_ip         = {FALSE, 0, ""};
#else
#if FOREIGN_APN == APN_CMIOT
static APN_PARA_T       i_apn           = {false,   "CMIOT", "", ""};
#elif FOREIGN_APN == APN_Indonesia
static APN_PARA_T       i_apn           = {false,   "m2minternet", "", ""};
#elif FOREIGN_APN == APN_Kazakhstan
static APN_PARA_T       i_apn           = {false,   "internet.beeline.kz", "internet.beeline", ""};
#elif FOREIGN_APN == APN_Laos
static APN_PARA_T       i_apn           = {false,   "unitel3g", "", ""};
#endif
//static GPRS_GROUP_T   i_main_ip         = {TRUE, 9999, "ydzc.borcom.net"};
//static GPRS_GROUP_T   i_back_ip         = {FALSE, 0, ""};
// 231213 改为双IP默认主IP是新平台，副IP是老平台
static GPRS_GROUP_T   i_main_ip         = {TRUE, 9999, "ydzc.borcom.net"};
static GPRS_GROUP_T   i_back_ip         = {TRUE, 9399, "ydzc.tbox-svr.com"};
//static GPRS_GROUP_T   i_main_ip         = {TRUE, 8066, "61.155.106.98"};
//static GPRS_GROUP_T   i_back_ip         = {TRUE, 8068, "61.155.106.98"};


#endif
// static DOMAIN_IP_T   i_ota_ip           = {10001, "61.155.106.98"}; // 目前没用

//static EPO_POS_T        i_epo_pos       = {DEFAULT_LAT, DEFAULT_LONG};

static SHORT_PARA_T     i_short_para    = {0xFF, 0x00};// 180613 {MODE_POS_WGPS, 20};  //FF和00时，视为无效，设备从UP_PARA_NEW_T取值

static RESET_RETRY_T    i_reset_retry   = {RESET_DEFAULT};  
static GENERAL_8_T      i_gps_cheater   = {GPS_CHEATER_DEFAULT};

//static SMS_PSWD_T       i_sms_pswd      = {'0', '0', '0'};    // 151211 add, 默认短信密码
// 默认半小时，10次
static ALARM_HDL_T      i_alm_hdl       = {5, 5, 30, {0}, 3};   // 160131 add, 默认参数
static FAIL_DEFAULT_T   i_fail_def      = {0, 0, 0};       	    // 找网失败复位闹铃参数. 默认关闭
static ALM_SENSE_PARA_T i_alm_sense     = {3, 3600, 6, 180};    // 231109 change, 默认1小时


//static YUNDAN_PARA_T    i_yd_para       = {0, 0, 0};
/*
********************************************************************************
* define PUBTBL_STRUCT
********************************************************************************
*/
typedef struct {
    void   *i_ptr;                      /* 指向初始化区 */
    INT8U   len;                        /* 长度 */
} PUBTBL_STRUCT;

static const PUBTBL_STRUCT PubTbl[] = {
            {NULL,                      sizeof(SYSTIME_T)},         // 0    系统时间
            {&i_UPLOAD_PARA,            sizeof(UP_PARA_NEW_T)},     // 1    上传参数
            {&i_apn,                    sizeof(APN_PARA_T)},        // 2    APN
            {&i_main_ip,                sizeof(GPRS_GROUP_T)},       // 3    主IP
            {&i_back_ip,                sizeof(GPRS_GROUP_T)},       // 4    副IP
            {NULL,                      sizeof(GPRS_GROUP_T)},       // 5    OTA-IP
            {NULL,                      sizeof(EPO_POS_T)},         // 6    本地位置点存储
            {NULL,                      sizeof(TEL_T)},             // 7    SMSC短信服务号
            {&i_short_para,             sizeof(SHORT_PARA_T)},      // 8    短周期参数
            {&i_reset_retry,            sizeof(RESET_RETRY_T)},     // 8    复位重新传输计数，表示需要短时重启几次
            {NULL,                      sizeof(SMS_PSWD_T)},        // 10   短信密码
            {&i_alm_hdl,                sizeof(ALARM_HDL_T)},       // 11   报警处理参数
            {&i_fail_def,               sizeof(FAIL_DEFAULT_T)},    // 12   找网失败复位闹铃参数
            {NULL,                      20},                        // 13   iccid读取
            {NULL,                      sizeof(MULTI_SHAKE_PARA_T)},// 14   多时段震动监测
            {&i_alm_sense,              sizeof(ALM_SENSE_PARA_T)},  // 15   报警灵敏度
            {&i_gps_cheater,            sizeof(GENERAL_8_T)},       // 16   GPS定位时长
            {&i_cali_minute,            sizeof(CALI_PARA_T)},       // 17   休眠间隔校准值
          };
#endif          /* end of PUBLIC_C */


/*****************************************************************************
 函 数 名  : DAL_PP_Valid
 功能描述  : 判断校验和来确定para是否有效
 输入参数  : INT8U ParaID  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN DAL_PP_Valid(INT8U ParaID);
/*****************************************************************************
 函 数 名  : DAL_PP_Read
 功能描述  : 读取PP
 输入参数  : INT8U ParaID  
             void *ptr     
 输出参数  : 无
 返 回 值  : 长度
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
INT8U DAL_PP_Read(INT8U ParaID, void *ptr);
/*****************************************************************************
 函 数 名  : DAL_PP_Store_RAM
 功能描述  : 存储某个pp，但不立即写到NVRAM
 输入参数  : INT8U ParaID  
             void *ptr     
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Store_RAM(INT8U ParaID, void *ptr);
/*****************************************************************************
 函 数 名  : DAL_PP_Update_NVRAM
 功能描述  : 立即更新到NVRAM
 输入参数  : immi, true 表示立即更新，false表示走定时器  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Update_NVRAM(BOOLEAN immi);
/*****************************************************************************
 函 数 名  : DAL_PP_Store
 功能描述  : 存储某个PP，并立即更新到NVRAM
 输入参数  : INT8U ParaID  
             void *ptr     
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Store(INT8U ParaID, void *ptr);
/*****************************************************************************
 函 数 名  : DAL_PP_RestoreDefault
 功能描述  : 恢复所有pp默认值，并更新到NVRAM
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_RestoreDefault(BOOLEAN update);// 160413 add 增加不强制写文件的接口
/*****************************************************************************
 函 数 名  : DAL_PP_Init
 功能描述  : pp的初始化入口, 隐含对PP的nvram写操作(在判断pp无效时)
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Init(void);
BOOLEAN DAL_PP_Ready(void); // 160605 add

void MsgHdl_PP_save_back(void);
void DAL_PP_init_delay_hdl(void);
#endif /* __DAL_PUBLIC_H__ */


