/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : dal_gpsdrv.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年2月7日, 星期二
  最近修改   :
  功能描述   : dal_gpsdrv.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2023年2月7日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __DAL_GPSDRV_H__
#define __DAL_GPSDRV_H__
/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#ifdef GLOBAL_DAL_GPS
#define DAL_GPS_EXT 
#else
#define DAL_GPS_EXT extern
#endif

#define GPS_SLEEP       0x00    // 省电模式
#define GPS_TYPE        0x40    // GPS模式
//#define BDS_TYPE        0x80    // BDS模式
//#define MIX_TYPE        0xc0    // 混合模式

#define GPS_VALID       0x20    // 已定位

#define LAT_SOUTH       0x10    // 南纬
#define LONG_WEST       0x08    // 西经

#define EM_FLAG         0x04    //偏移位置
// GPS天线状态
typedef enum {
    ANT_OK = 0,         // 天线OK
    ANT_SHORT = 1,      // 天线短路
    ANT_OPEN  = 2,      // 天线开路
} ANT_STATE;

typedef struct {
    BOOLEAN     t_valid;        // time有效标志
    SYSTIME_T   t_local;        // 当地时间
    INT8U       status_flag;    // S2	S1	D	LA	LO	Em	R	R
                                // 定位模块工作状态SS：00b=省电；01b=GPS模式；10b=BDS模式；11b=混合定位
                                // 定位状态D：0=不定位； 1=已定位
                                // 纬度状态位LA：    0b=北纬；     1b=南纬
                                // 经度状态位LO：    0b=东经；     1b=西经
                                // 偏移加密位Em：    0b=不经过偏移加密；   1b=GPS数据已经过偏移加密
                                
    INT8U       c_lat[4];       // 纬度，4字节=度、分、小数分1、小数分2
    INT8U       c_long[4];      // 经度，4字节=度、分、小数分1、小数分2
    INT32U      D_lat;          // 140708 度×1000000格式纬度
    INT32U      D_long;         // 140708 度×1000000格式经度
    INT16S      alt;            // 海拔高度，单位米
    INT16U       spd;           // 与300m 的改过0715 // 速度，单位0.1 km/h   
    INT16U       direction;     // 与300m 的改过0715 // 方向，正北为0°，顺时针，真实方向角
    BOOLEAN     is_3d;          // 150325 GPS fix status，1表示3d定位

	INT8U       inuse;
	INT8U       inview;    
    INT16U      hdop;        // 水平精度因子
    INT8U       gps_err;          /* GPS模块是否正常 0:正常,1异常 ：串口是否正常接收数据*/
    BOOLEAN     angle;    // 拐点
    INT32U      mileage;  // 里程计，重设备开机后累计和
    ANT_STATE   ant_state;      // 210901 add, 天线状态
    
} GPS_DATA_T;

DAL_GPS_EXT GPS_DATA_T  g_gps_data;

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/


INT16S ConvertAltitude(INT8U *ptr);
INT16S ConvertHdop(INT8U *ptr);
void ConvertLatitude(INT8U *dptr, INT8U *sptr);
void ConvertLongitude(INT8U *dptr, INT8U *sptr);
INT16S DAL_GetGpsHeight(void);
void DAL_getGpsSrcData(DATE_T *date, TIME_T *time);
void DAL_GetLatitude(INT8U *ptr);
void DAL_GetLongitude(INT8U *ptr);
void DAL_GPSProtocolInit(void);
void DAL_GPSRecvProc(INT8U ch);
INT32U DAL_GPS_DegreeToSec(INT8U *ptr);
void DAL_GPS_GetWeekAndTow(INT16U *week, INT32U *tow);
BOOLEAN DAL_GPS_POS_IsOK(void);
BOOLEAN DAL_GPS_POS_Is_3DFIX(void);
void DAL_GPS_SecToDegree(INT8U *ptr, INT32U sec);
BOOLEAN DAL_GPS_time_valid(void);
INT8U   DAL_GSVMsgGet(char *ptr,INT8U maxLen);
INT32U _GetD_Latitude(INT8U* c_lat);
INT32U _GetD_Longitude(INT8U* c_long);

INT8U get_lastGPGGAmsg(INT8U *ptr);
INT32U get_lat_average_value(void);
INT32U get_lat_average_value(void);
INT32U get_long_average_value(void);
INT32U get_long_average_value(void);



#endif /* __DAL_GPSDRV_H__ */

