/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_port_socket.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2024年1月9日, 星期二
  最近修改   :
  功能描述   : bk_port_socket.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2024年1月9日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __PORT_SOCKET_H__
#define __PORT_SOCKET_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define URL_LEN             60
#define SOCKET_FD_INVALID   -1
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef enum {
    SOCK_FD_MAIN = 0,
    SOCK_FD_OTA,
    SOCK_FD_BACK,
    //SOCK_FD_UDP,
    SOCK_FD_MAX
} SOCK_INDEX_E;
    
typedef enum {
    SOCK_NONE,              // 无用状态，处于这个状态，不予连接
    SOCK_IDLE,              // 空闲状态, 准备链接
    SOCK_CONNECTING,        // 连接状态
    SOCK_OK,                // 连接成功
    SOCK_STAT_MAX
} SOCK_STATE_E;


typedef void(*sock_cb)(SOCK_INDEX_E index, s32 errCode, void* customParam);

typedef struct {
    SOCK_STATE_E state;     // 状态
    s32     sock_fd;        // socket句柄
    u16     port;
    char    server_str[URL_LEN]; // 域名或者ip字符串
//    bool    is_linked;      // 是否已经连接成功
    bool    is_tcp;
    sock_cb link_cb;
    sock_cb sent_cb;
    sock_cb recv_cb;
    sock_cb close_cb;
    bool    need_run_sentCB;// 临时加个变量，用于发送通知的异步操作. 合宙无用
} SOCKET_FD_T;



/*----------------------------------------------*
 * 合宙所需的                                   *
 *----------------------------------------------*/
typedef enum SOCKET_EVENT
{
	SOCKET_EVENT_CONNECT,
    SOCKET_EVENT_SEND,
    SOCKET_EVENT_RECEIVE,
}SOCKET_EVENT_E;

typedef union socket_event_param
{	
	int connect_result; //>=0成功；其余值失败
	struct
    {
        int result;//>0成功；其余值失败
        int user_param;
    } send_cnf_t;
    struct
    {
        int len;
        char *data;
    } recv_ind_t;
    
} socket_event_param_t;

typedef void (*socket_service_event_callback_t)(int app_id, SOCKET_EVENT_E event, socket_event_param_t param);

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

SOCK_STATE_E PORT_GPRS_sock_state(SOCK_INDEX_E index);

int PORT_GPRS_sock_close(SOCK_INDEX_E index);
int PORT_GPRS_sock_new(SOCK_INDEX_E index, bool is_tcp, char* url, INT8U u_len, INT16U port,
                                 sock_cb l_cb, sock_cb s_cb, sock_cb r_cb, sock_cb c_cb);
int PORT_GPRS_sock_recv(SOCK_INDEX_E index, INT8U *dbuf, int len);
int PORT_GPRS_sock_send(SOCK_INDEX_E index, const INT8U *sbuf, int len);

void PORT_Socket_MspInit(void);
// 用不上了 BOOLEAN PORT_GPRS_Sock_Init(SOCK_INDEX_E index);

#endif /* __PORT_SOCKET_H__ */

