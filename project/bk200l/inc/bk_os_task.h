
#ifndef __OS_TASK_H__
#define __OS_TASK_H__

#include "common_api.h"
#include "luat_rtos.h"
#include "luat_mem.h"
#include "luat_debug.h"
#include "platform_define.h"
#include <string.h>



#define BOOLEAN     bool
#define INT8U       uint8_t
#define INT8S       int8_t
#define INT16U      uint16_t
#define INT16S      int16_t
#define INT32U      uint32_t
#define INT32S      int32_t

#define u8          uint8_t
#define s8          int8_t
#define u16         uint16_t
#define s16         int16_t
#define u32         uint32_t
#define s32         int32_t

#ifndef  OFF 
#define  OFF            0
#endif

#ifndef  ON
#define  ON             1
#endif

#ifndef TRUE
#define TRUE true
#endif

#ifndef FALSE
#define FALSE false
#endif

#ifndef	 CR
#define  CR                      0x0D
#endif

#ifndef  LF
#define  LF                      0x0A
#endif

#ifndef	 CTRL_Z
#define  CTRL_Z                  0x1A
#endif

#ifndef  ESC
#define  ESC                     0x1B
#endif

#ifndef	 _SUCCESS
#define  _SUCCESS                0
#endif

#ifndef	 _FAILURE
#define  _FAILURE                1
#endif

#ifndef  _OVERTIME
#define  _OVERTIME               2
#endif

#ifndef  _OPEN
#define  _OPEN                   0xAA
#endif

#ifndef  _CLOSE
#define  _CLOSE                  0x55
#endif

//#define TEST_M20_SLEEP    0
//#define DEBUG_AT        0   // 模块AT命令测试

#define MAX_MODE_SLEEP_SECOND   300   // 模块支持最长休眠时间（保持链路不掉）


#define WIFI_EN         1
#define BL_EN           1   // 盲区补传
#define APP_OTA_EN      1   // 支持OTA
#define ALMFAIL_HDL_EN  0
#define MULTI_SHAKE_EN  0
/*#define USE_RIL_GPS     1   // 201224 ADD, 使用RIL接口
#if USE_RIL_GPS > 0
#define EN_GNSS_EPO     0
#else
#define EN_GNSS_EPO     0   // 使能辅助定位
#define EN_SECOND_GNSS    0   // 秒定位功能（运动状态下）  ，取到位置后在下发，目前测试要8秒，没啥意义
#define EN_SECOND_GNSS2   0   // 秒定位功能  ,测试也要10秒
#endif*/

//#define DEBUG_WIFI_BUG    1  // wifi 晶振测试
#define SEND_LAST_GPGGA   0    // 上报最后一天GPGGA到平台
#define FIX_GPS_TIME      0    // 固定运动状态下GPS最长搜索时间
#define SEND_GPS_SING     0   // 发送GPS 可视卫星数，与对应的信号强读

#define MAIN_SVR_MAX_WAIT  120  // 主中心异常，最大等待时间（超时，如果副中心正常，可直接发数据）

//#define GPS_FAIL_NO_CLOSE_POW  1  // 运动状态GPS失败，不断电
#define GPS_ANGLE_UPLOAD        0   //// 222026 ADD, 关闭拐点上传

#define APN_CMIOT           0
#define APN_Indonesia       1   // 印尼APN
#define APN_Kazakhstan      2   // 哈萨克斯坦
#define APN_Laos            3   // 老挝

#define FOREIGN_APN         APN_Laos   

#define FORCE_NO_GSENSOR        1   // 240202, 禁止使用GSENSOR，合宙目前的板子没贴
/*
调试功能
1.日志固定打开
*/
#define DEBUG_FUN       0

// 调试开关
#define OUR_DEBUG       1

#if OUR_DEBUG > 0
#define PIC_DEBUG       0   // pic的调试开关
#define PP_DEBUG        1   // PUBPARA的调试开关
#define GPS_DEBUG       0
#define GPRS_DEBUG      1   // GPRS的调试开关
#define OTA_DEBUG       1   // OTA调试开关
#define BK_DEBUG        1   // APP_MAIN的调试开关
#define SYSTIME_DEBUG   1   // SYSTIME
#define ALMHDL_DEBUG    0   // ALM-HDL的调试开关
#define BL_DEBUG        1
#else
#define PIC_DEBUG       0   // pic的调试开关
#define PP_DEBUG        0   // PUBPARA的调试开关
#define GPS_DEBUG       0   // gps的调试开关
#define GPRS_DEBUG      0   // GPRS的调试开关
#define OTA_DEBUG       0   // OTA调试开关
#define BK_DEBUG        0   // APP_MAIN的调试开关
#define SYSTIME_DEBUG   0   // SYSTIME
#define ALMHDL_DEBUG    0
#define BL_DEBUG        0
#endif

#include "luat_gpio.h"
#include "luat_adc.h"
#include "luat_flash.h"
#include "luat_fs.h"
#include "luat_fota.h"
#include "luat_kv.h"
#include "luat_uart.h"

#include "bk_os_struct.h"
#include "bk_port_uart.h"


#endif /* __OS_TASK_H__ */




