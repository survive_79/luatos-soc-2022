/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_dal_sock.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2024年1月9日, 星期二
  最近修改   :
  功能描述   : bk_dal_sock.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2024年1月9日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __BK_DAL_SOCK_H__
#define __BK_DAL_SOCK_H__
/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_port_socket.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/


/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



void DAL_GPRSInit(void);

void Dal_SockDisable(SOCK_INDEX_E index);
void Dal_SockEnable(SOCK_INDEX_E index, GPRS_GROUP_T* para);
SOCK_STATE_E Dal_SockGetState(SOCK_INDEX_E index);

INT16S DAL_SockReadByte(SOCK_INDEX_E index);
BOOLEAN Dal_SockSend(SOCK_INDEX_E index, INT8U* sptr, INT16U slen);


#ifdef EN_UDP
bool dal_connectUdpLog(void);
void dal_udpLogClose(void);
void dal_udpLogSend(char *buf,int len);
void  factory_addOrsend_msg(INT8U  msgId,INT8U result,BOOLEAN sendFlag);
void _udpSendLogin(void);
void _udpSendLogin_ex(INT8U      B2 ,INT8U      B3 ,INT8U B4, INT8U B5 );
void udp_sock_cb(s32 socketId, s32 errCode, void* customParam);

#endif



#endif /* __BK_DAL_SOCK_H__ */
