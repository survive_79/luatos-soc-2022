/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : our_msg_def.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年3月9日
  最近修改   :
  功能描述   : mc20自定义任务所需的msg id的定义在此。
  函数列表   :
  修改历史   :
  1.日    期   : 2018年3月9日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

#define OUR_MSG_BASE                    100

#define MSG_ID_RIL_IS_OK                (OUR_MSG_BASE + 0) // 这个消息在main task收到ril注册OK时调用, 发给全体任务，看看初始化最好等这个消息

// MAIN TASK任务所处理的MSG
#define MSG_ID_GET_VOLTAGE              (OUR_MSG_BASE + 0x01)
#define MSG_ID_POWEROFF                 (OUR_MSG_BASE + 0x02)
#define MSG_ID_GPRS_CONN                (OUR_MSG_BASE + 0x03)
#define MSG_ID_GPRS_SEND_BEGIN          (OUR_MSG_BASE + 0x04)   // 准备连GPRS。鉴于调用可能在bk_task里，因此用msg来开启timer
#define MSG_ID_GPRS_SEND_DATA           (OUR_MSG_BASE + 0x05)
#define MSG_ID_ENTERSLEEP               (OUR_MSG_BASE + 0x06)

// app_bk_task任务所处理的msg
#define MSG_ID_FS_PP_INIT               (OUR_MSG_BASE + 0x11)
#define MSG_ID_FS_PP_SAVE_BACK          (OUR_MSG_BASE + 0x12)
#define MSG_ID_OTA_BEGIN                (OUR_MSG_BASE + 0x13)   // OTA在app_bk_task里，得用这种方式调用
#define MSG_ID_UPLOAD_GO                (OUR_MSG_BASE + 0x14)   // GPRS已经附着成功了，可以上报了

// app_etc_task任务所处理的MSG
#define MSG_ID_GPS_ON                   (OUR_MSG_BASE + 0x21)
#define MSG_ID_GPS_OFF                  (OUR_MSG_BASE + 0x22)
#define MSG_ID_WIFI_CLOSE               (OUR_MSG_BASE + 0x23)
#define MSG_ID_WIFI_OPEN                (OUR_MSG_BASE + 0x24)
#define MSG_ID_PIC_BEGIN_SEND           (OUR_MSG_BASE + 0x25)   // 调用picsend接口要务必确保start send timer是在etc task内
#define MSG_ID_SYSTIMER_CLOSE           (OUR_MSG_BASE + 0x26)
#define MSG_ID_SYSTIMER_REOPEN          (OUR_MSG_BASE + 0x27)
#define MSG_ID_SYS_ENTERSLEEP           (OUR_MSG_BASE + 0x28)
#define MSG_ID_PIC_SEND_DATA            (OUR_MSG_BASE + 0x29)

#define MSG_ID_SAVE_CNT                 (OUR_MSG_BASE + 0x50)



