/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : port_uart.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年3月2日
  最近修改   :
  功能描述   : port_uart.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2018年3月2日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __PORT_UART_H__
#define __PORT_UART_H__

//#include "quec_uart.h"

#ifdef GLOBAL_PORT_UART
#define UART_EXT
#else
#define UART_EXT extern 
#endif
#define DBG_PORT          1  // PIC那个
#if 0
#define MAX_DBG_BUF_LEN     512         // 定义最大trace 缓冲长度
UART_EXT char s_trace_buf[MAX_DBG_BUF_LEN];      // strings 缓冲

// 好像这个m20工程不直接支持C99模式的方式，得用宏来替代
#define OURAPP_trace(FORMAT,...) {\
    if(g_flag_dbg) { \
    memset(s_trace_buf, 0, MAX_DBG_BUF_LEN);\
    sprintf(s_trace_buf,FORMAT,##__VA_ARGS__); \
    strcat(s_trace_buf, "!\r\n\0");   \
    luat_uart_write(DBG_PORT, (void*)(s_trace_buf), strlen((const char *)(s_trace_buf)));} \
    }

#else
//void OURAPP_trace(const char *fmt, ...);
#define OURAPP_trace LUAT_DEBUG_PRINT
#endif

UART_EXT INT8U g_flag_dbg;


UART_EXT INT8U g_GPRS_dbg_switch;   // GPRS调试开关
UART_EXT INT8U g_BK_dbg_switch;     // BK基础调试开关
UART_EXT INT8U g_pic_dbg_switch;    // PIC调试开关
UART_EXT INT8U g_gps_dbg_switch;    // GPS试开关


#define GPRS_TRACE(fmt, ... ) {\
        if (g_GPRS_dbg_switch > 0) {\
            OURAPP_trace(fmt, ##__VA_ARGS__); }\
        }

#define BK_LOG(fmt, ... ) {\
        if (g_BK_dbg_switch > 0) {\
            OURAPP_trace(fmt, ##__VA_ARGS__); }\
        }

#define PIC_LOG(fmt, ... ) {\
        if (g_pic_dbg_switch > 0) {\
            OURAPP_trace(fmt, ##__VA_ARGS__); }\
        }
#define GPS_LOG(fmt, ... ) {\
        if (g_gps_dbg_switch > 0) {\
            OURAPP_trace(fmt, ##__VA_ARGS__); }\
        }


#define BK_ASSERT( cond )               \
    do                                  \
    {                                   \
        if( ! ( cond ) )                \
        {                               \
            OURAPP_trace("BK_ASSERT!%s,%s", __FUNCTION__, __LINE__);    \
            BK_Reset();                 \
        }                               \
    }                                   \
    while( 0 )

#if BK_DEBUG > 0
#define BK_LOG_H        OURAPP_printhexbuf
#else
#define BK_LOG_H(...)        
#endif

/* 串口配置参数 */
typedef struct {
    INT8U port;       /* 串口编号 */
    INT32U baud;       /* 波特率, 1200~115200 */
//    INT32U parity;     /* 奇偶校验位 */
//    INT32U databit;    /* 数据位 */
//    INT32U stopbit;    /* 停止位 */
    
    INT16U rx_len;     /* 配置接收缓存长度 */
    INT16U tx_len;     /* 配置发送缓存长度 */
    INT8U  *rx_ptr;    /* 配置接收缓存指针 */
    INT8U  *tx_ptr;    /* 配置发送缓存指针 */
} UART_CFG_T;

// 对应UART_No数组的下标

#define PIC_UART        0
#define GPS_UART        1
// #define WIFI_UART       2   // 这个最好别用

#define MAX_UART_NUM    2   // 3


void OURAPP_InitTrace(BOOLEAN flag);
void OURAPP_printhexbuf(INT8U *hex, INT16U len);
//void OURAPP_trace(char *fmt, ...);

BOOLEAN PORT_InitUart(UART_CFG_T *cfg);

void PORT_CloseUart(INT8U port);

INT16U PORT_LeftOfUartWriteBuf(INT8U port);
INT16S PORT_UartRead(INT8U port);
BOOLEAN PORT_UartWriteBlock(INT8U port, INT8U *sdata, INT16U slen);

#endif /* __PORT_UART_H__ */
