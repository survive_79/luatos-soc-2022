/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_our_tools.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年12月28日, 星期四
  最近修改   :
  功能描述   : bk_our_tools.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2023年12月28日, 星期四
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __OUR_TOOLS_H__
#define __OUR_TOOLS_H__

#ifdef GLOBAL_OUR_TOOLS
#define TOOLS_EXT
#else
#define TOOLS_EXT extern
#endif

typedef struct {
    INT16U bufsize;          /* round buffer size */
    INT16U used;             /* used bytes */
    INT8U  *bptr;            /* begin position */
    INT8U  *eptr;            /* end position */
    INT8U  *wptr;            /* write position */
    INT8U  *rptr;            /* read position */
} ROUND_T;

typedef struct {
    INT8U       c_flags;                /* c_convert0 + c_convert1 = c_flags */
    INT8U       c_convert0;             /* c_convert0 + c_convert2 = c_convert0 */
    INT8U       c_convert1;
    INT8U       c_convert2;
} ASMRULE_T;

typedef struct {
    INT16U index;
    void (*proc_null)(void);
    void (*proc_para)(INT8U*, INT16U);
} FUNC_ENTRY_T;

// 141116 新增: 开机统计时长等参数用
typedef struct {
    INT32U  work_time;      // PIC工作时长
    INT32U  car_on_time;    // 开车时长
    INT16U  start_cnt;      // 模块打开次数
    INT8U   creg_time;      // 开机到找到网络的时长
    INT8U   attach_time;    // 开机到附着gprs成功的时长

    INT8U   connect_satau;  // 连接状态：与pic是否通信成功
    INT8U   acc_statu;      // 开车状态:  0 off  ; 1 on
    INT8U   pow_statu;      // 主电状态
    INT8U   shake_statu;    // 震动状态:有用于判断运动静止
    INT8U   destroy_statu;  // 拆卸状态
    INT8U   crash_statu;    // 碰撞（该bit在收到心跳应答后清空）
    INT8U   rollOver_statu; // 侧翻状态
    
    INT8U   car_statu;      // 开车状态
    INT32U  car_stop_time;  // 停车时长    
    INT8U   stop_cnt;       // 停车次数  
    INT16U  temperature;    // 温度
    INT16U  humidity;       // 湿度
    INT8U   charge_sta;     // （充电状态 1:充电状态 0:未充电状态 
    INT8U   factoryFlag;    // 生产检测模式
    INT16U  volt;   // 电压 mv
    INT8U   XYZ[6];
} DEV_STATUS_T;
TOOLS_EXT DEV_STATUS_T g_our_status;

/////////////////////wifi热点定义//////////////////////////
#define MAX_WIFI_AP     10 // 最多一帧10个热点

typedef struct {
    INT8U mac[6];   // 热点的mac地址
    INT8U rssi;     // 热点的信号强度, 目前是用100+负值的RSSI换算得来
} WIFI_AP_T;

typedef struct {
    INT8U cnt;
    WIFI_AP_T ap[MAX_WIFI_AP];
} WIFI_AP_RESULT_T;

//////////////////////基站ID定义///////////////////////////
#define MAX_CELL_IN_APP 6   // 应用层最多6个基站就是最好的了
#define MAX_CELL_CNT    7   // 底层限制死了最多7个

typedef struct {
    INT16U  lac;
    INT32U  cellid;
    INT8U   signal;
} CELL_INFO_T;

typedef struct {
    INT16U  mcc;                    // 国家编码
    INT16U  mnc;                    // 运营编码
    INT8U   cell_num;               // 基站id数
    CELL_INFO_T cell[MAX_CELL_CNT];
} CELL_ID_T;

TOOLS_EXT CELL_ID_T g_cell; // 当前的瞬时基站信息

typedef struct {
    INT16U TAlarmDown;   // 最低温度，偏移量 -60℃
    INT16U TAlarmUp;     // 最高温度，偏移量 -60℃
    INT16U RHAlarmDown;  // 最低湿度，%
    INT16U RHAlarmUp;    // 最高湿度，%
}SHT21_ALARM_T;

typedef struct {
    INT8U last_3dfix_sta;   // 上次3D定位情况,faile 失败,true 成功
    INT8U gpsfail_trytimes; // gps定位失败,尝试次数
}GPS_3DFIX_INFO_T;

/*******************************************************************
** 函数名:     _Round_Init
** 函数描述:   初始化循环缓冲区
** 参数:       [in]  round:           循环缓冲区
**             [in]  mem:             循环缓冲区所管理的内存地址
**             [in]  memsize:         循环缓冲区所管理的内存字节数
** 返回:       无
********************************************************************/
void _Round_Init(ROUND_T *round, INT8U *mem, INT32U memsize);
/*******************************************************************
** 函数名:     _Round_Left
** 函数描述:   获取循环缓冲区中剩余的可用字节数
** 参数:       [in]  round:           循环缓冲区
** 返回:       剩余的可用字节数
********************************************************************/
INT32U _Round_Left(ROUND_T *round);
/*******************************************************************
** 函数名:     _Round_GetReadData
** 函数描述:   读取但不减掉指针
** 参数:       [in]  round:         循环缓冲区
**             [out] optr:          读取的目标内存
**             [in]  maxlen:        目标内存的最大长度
** 返回:       真实读取到的长度
********************************************************************/
INT32U _Round_GetReadData(ROUND_T *round, INT8U* optr, INT32U maxlen);
/*******************************************************************
** 函数名:     _Round_ReleaseRead
** 函数描述:   释放一定的读空间.配合_Round_GetReadData使用
** 参数:       [in]  round:         循环缓冲区
**             [in]  jumplen:       欲释放的长度
** 返回:       真实释放的长度
********************************************************************/
INT32U _Round_ReleaseRead(ROUND_T *round, INT32U jumplen);
/*******************************************************************
** 函数名:     _Round_ReadByte
** 函数描述:   从循环缓冲区中读取一个字节的数据
** 参数:       [in]  round:           循环缓冲区
** 返回:       如读之前循环缓冲区中的使用字节数为0, 则返回-1;
**             否则, 返回读取到的字节
********************************************************************/
INT16S _Round_ReadByte(ROUND_T *round);
/*******************************************************************
** 函数名:     _Round_WriteBlock
** 函数描述:   往循环缓冲区中写入一块数据单元
** 参数:       [in]  round:           循环缓冲区
**             [in]  bptr:            待写入数据块的指针
**             [in]  blksize:         待写入数据块的字节数
** 返回:       成功写入循环缓冲区的字节数
********************************************************************/
BOOLEAN _Round_WriteBlock(ROUND_T *round, INT8U *bptr, INT32U blksize);


INT8U ChkSum_XOR(INT8U* msg, INT16U len);
/* 返回：累加和 */
INT8U ChkSum(INT8U *dptr, INT16U len);
/* 累加和的反码 */
INT8U ChkSum_N(INT8U *dptr, INT16U len);
/*******************************************************************
**  函数名:     CRC_32
**  函数描述:   计算 32 位 CRC-32 值
**  参数:       none
**  返回:       none
********************************************************************/
INT32U CRC_32(INT8U * aData, INT32U aSize);
void CRC_32_StepInit(void);
INT32U CRC_32_step(INT8U* aData, INT32U aSize);

INT16U Deassemble(INT8U *dptr, INT8U *sptr, INT16U len, INT16U maxlen);
INT16U OUR_AssembleByRules(INT8U *dptr, INT8U *sptr, INT16U len, ASMRULE_T *rule);

BOOLEAN FindProc_Entry(INT16U index, const FUNC_ENTRY_T *funcentry, INT16U num, INT8U *data, INT16U dlen);

INT8U HexToChar(INT8U sbyte);
INT8U CharToHex(INT8U schar);
INT16U HexToDec(INT8U *dptr, INT8U *sptr, INT16U len);
INT32U AsciiToDec(INT8U *sptr, INT8U len);
/*******************************************************************
**  函数名:     Char2_ToHex
**  函数描述:   将两个可见字符转换为hex_u8
**  参数:       [in] sch: 源可见字符指针
**  返回:       转换后的hex u8
********************************************************************/
INT8U Char2_ToHex(INT8U* sch);
/*******************************************************************
**  函数名:     Char2_ToDec
**  函数描述:   将两个可见字符转换为十进制u8
**  参数:       [in] sch: 源可见字符指针
**  返回:       转换后的dec u8
********************************************************************/
INT8U Char2_ToDec(INT8U* sch);
/*******************************************************************
**  函数名:     Dec_ToChar2
**  函数描述:   将十进制u8转换为两个可见字符
**  参数:       [out] dptr: 目标指针
**              [in]  sch:  源dec数
**  返回:       none
********************************************************************/
void Dec_ToChar2(INT8U* dptr, INT8U sch);
/*******************************************************************
**  函数名:     FFChar
**  函数描述:   从开头查找sptr中最近的ch下标
**  参数:       [in] sptr:  源指针
**              [in] ch:    匹配字符
**  返回:       第一个ch的下标
********************************************************************/
INT8U FFChar(INT8U *sptr, INT8U ch);
/*******************************************************************
**  函数名:     FindCharPos
**  函数描述:   FFChar的增强版, 找到第n个匹配字符
**  参数:       [in] sptr:  源指针
**              [in] ch:    匹配字符
**              [in] numchar: 个数
**              [in] maxlen:数据长度
**  返回:       第numchar个ch的下标
********************************************************************/
INT16U FindCharPos(INT8U *sptr, char ch, INT8U numchar, INT16U maxlen);
// 16 00 10 -> 10 00 0a
INT16U BCD2Hex(INT8U *dptr, INT8U *sptr, INT16U len);
INT16U DataHex2BCD(INT8U *dptr, INT8U *sptr, INT16U len);

BOOLEAN SearchKeyWordFromHead(INT8U *ptr, INT16U maxlen, char *kw);

/*******************************************************************
** 函数名:     OUR_GPS_UtcToWeekAndTow
** 函数描述:   根据输入的格林尼志日期和时间, 计算对应的GPS周和周秒
** 参数:       [in]  date:     输入格林尼志日期
**             [in]  time:     输入格林尼志时间
**             [out] week:     GPS周数, 自1980年1月6日零时起算, 至当前时间累计的周数，也即GPS周(七天为一周)
**             [out] tow:      GPS秒数, 以GPS周为准, 从一周起始时间累积至当前时间的秒数, 该值每周清零
** 返回:       无
********************************************************************/
void OUR_GPS_UtcToWeekAndTow(DATE_T *date, TIME_T *time, INT16U *week, INT32U *tow);
/*********************************************************************************
** 函数名:     OUR_ConvertGmtToLocaltime
** 函数描述:   将格林尼治时间转换成本地时间
** 参数:       [in/out]  date:     日期
**             [in/out]  time:     时间
**             [in]      diftime:  时差
** 返回:       无
*********************************************************************************/
void OUR_ConvertGmtToLocaltime(DATE_T *date, TIME_T *time, INT8U diftime);
/*********************************************************************************
** 函数名:     OUR_ConvertGmtToLocaltime
** 函数描述:   将本地时间换成格林尼治时间转
** 参数:       [in/out]  date:     日期
**             [in/out]  time:     时间
**             [in]      diftime:  时差
** 返回:       无
*********************************************************************************/
void OUR_ConvertLocaltimeToGmt(DATE_T *date, TIME_T *time, INT8U diftime);
BOOLEAN DateValid(DATE_T *date);
BOOLEAN TimeValid(TIME_T *time);

BOOLEAN LongitudeValid(INT8U *longitude);
BOOLEAN LatitudeValid(INT8U *latitude);

#endif /* __OUR_TOOLS_H__ */


