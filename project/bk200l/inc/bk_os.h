/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_os.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年12月28日, 星期四
  最近修改   :
  功能描述   : bk_os.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2023年12月28日, 星期四
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __BK_OS_H__
#define __BK_OS_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_task.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef enum {
    QUEUE_PIC = 0,
    MAX_QUEUE_NUM       // 最多支持队列个数
} MSG_QUEUE_E;

// 固定的格式
typedef struct {
    u32  message;
    u32  param1;
    u32  param2;
    u32  srcTaskId;
} BK_OS_FIX_MSG;
/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

char* PORT_GetVersion(void);

void* BK_MEM_Alloc(u32 size);
void BK_MEM_Free(void *ptr);
/*
BOOLEAN BK_OS_CreateMessage(MSG_QUEUE_E index);
BOOLEAN BK_OS_CreateMutex(ql_mutex_t* mutex);
BOOLEAN BK_OS_CreateTask(ql_task_t* taskRef, INT32U stackSize, INT8U priority, char *taskName, void (*taskStart)(void*), void* argv);
BOOLEAN BK_OS_GetMessage(MSG_QUEUE_E index, BK_OS_FIX_MSG* msg, BOOLEAN suspend);
void BK_OS_GiveMutex(ql_mutex_t* mutex);
BOOLEAN BK_OS_SendMessage(MSG_QUEUE_E index, BK_OS_FIX_MSG* msg);
void BK_OS_TakeMutex(ql_mutex_t* mutex, UINT32 outTime);
*/
void BK_Reset(void);
void BK_Sleep(u32 msec);
void BK_Poweroff(void);
void Error_Handler(const char* func_name, int line);



#endif /* __BK_OS_H__ */

