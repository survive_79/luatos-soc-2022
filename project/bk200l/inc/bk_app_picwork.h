/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_app_picwork.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2024年1月10日, 星期三
  最近修改   :
  功能描述   : bk_app_picwork.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2024年1月10日, 星期三
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __BK_APP_PICWORK_H__
#define __BK_APP_PICWORK_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef struct {
    SYSTIME_T       cur_time;       // PIC当前时间。依次为年月日时分秒
    INT8U           week;           // 当前日期对应的星期，0~6对应周日到周六
    INT16U          max_worktime;   // 最大工作时间，单位：秒
    INT8U           work_type;      // 01H--正常工作状态,02H--临时供电状态
    INT16U          rest_time;      // 距离休眠的剩余时间。单位秒
    INT16U          eint_mask;      // 报警MASK. bit1=停车报警,bit0=拆卸报警
    INT8U           ver[20];        // pic版本号
}PIC_BASE_PARA_T;
/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



INT8U* app_get_wsd_value(void);
void App_Pic_PollFunc_100ms(void);
void App_Pic_PreInit(void);
INT8U GetPicAlarmStatus(void);
INT16U GetPicRestSec(void);
INT8U *GetPicVer(void);

void InitPic_work(void);
void Request_PIC_Sleep(INT16U t);
void Request_PIC_TmpWork(INT16U t);
void Requset_PIC_AlarmParaGet(void);
void Requset_PIC_AlarmParaSet(UP_PARA_NEW_T* su);
void Requset_PIC_BaseGet(void);
void Requset_PIC_BaseSet(SYSTIME_T* st, INT16U work_t, INT8U alm_en);
void Requset_PIC_WorkTime(void);
// 升级pic程序请求
void Request_PIC_OTAReq(void);

void app_set_pic_time(void);

#endif /* __BK_APP_PICWORK_H__ */

