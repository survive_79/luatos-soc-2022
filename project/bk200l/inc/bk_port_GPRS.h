/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_port_GPRS.h
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2024年1月5日, 星期五
  最近修改   :
  功能描述   : bk_port_GPRS.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2024年1月5日, 星期五
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#ifndef __PORT_GPRS_H__
#define __PORT_GPRS_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_our_tools.h"
/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#ifdef GLOBAL_PORT_GPRS
#define PORT_GPRS_EXT 
#else
#define PORT_GPRS_EXT extern
#endif

//#define IV_VER4     QL_DATA_TYPE_IP // IPV4版本
#define BK_NW_IDX   1               // 就1个连接
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/


/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

BOOLEAN PORT_GPRS_IS_ON(void);
void PORT_GPRS_Attach(void);
void PORT_GPRS_Deattach(void);

void PORT_GPRS_Init(void);

BOOLEAN PORT_GPRS_IsPowerOn(void);
char* PORT_GPRS_GetIMEI(void);
// 获取信号强度 OK
INT8U PORT_GSM_Get_SNR(void);
// 确认GSM网络是否注册成功  OK
BOOLEAN PORT_GSM_IS_REG(void);
char* PORT_Get_iccid(void);

void PORT_GPRS_Suspend(void);
void PORT_GPRS_Resume(void);


// 启动wifi扫描，结果就等待回调
void PORT_wifi_ap_enable(BOOLEAN en);
WIFI_AP_RESULT_T* PORT_get_cur_ap(void);
WIFI_AP_RESULT_T* PORT_get_wifi_ap(void);
INT8U PORT_has_wifi_ap(void);

#endif /* __PORT_GPRS_H__ */


