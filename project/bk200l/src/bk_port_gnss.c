/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : port_gnss.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年2月7日, 星期二
  最近修改   :
  功能描述   : GNSS串口驱动
  函数列表   :
              _gnss_task
              PORT_Gnss_close
              PORT_Gnss_IsPowerOn
              PORT_Gnss_MspInit
              PORT_Gnss_open
  修改历史   :
  1.日    期   : 2023年2月7日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#include "bk_our_tools.h"
#include "bk_dal_gpsdrv.h"
#include "bk_port_hardware.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
#define GNSS_RLEN       512
#define GNSS_SENDL      64      // 目前用不上，故开小点
/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static luat_rtos_task_handle s_gnss_task_ref;
static BOOLEAN s_gnss_is_open;

static INT8U s_recv_round[GNSS_RLEN];
static INT8U s_send_round[GNSS_SENDL];

static UART_CFG_T s_gnss_uart = {
    GPS_UART,
    9600,
    GNSS_RLEN,
    GNSS_SENDL,
    s_recv_round,
    s_send_round
};
/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/




/*****************************************************************************
 函 数 名  : PORT_Gnss_close
 功能描述  : 关闭GNSS模块
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年2月7日, 星期二
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void PORT_Gnss_close(void)
{
    PORT_CloseUart(GPS_UART);

    PORT_GPS_power(FALSE);    
    s_gnss_is_open = FALSE;
    
    GPS_LOG("GPS:[关]");

    DAL_GPSProtocolInit();
}

/*****************************************************************************
 函 数 名  : PORT_Gnss_open
 功能描述  : 开启GPS模块
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年2月7日, 星期二
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void PORT_Gnss_open(void)
{
    if (!s_gnss_is_open) {
        DAL_GPSProtocolInit();
        // 串口通信
        if (!PORT_InitUart(&s_gnss_uart)) {
            Error_Handler(__FUNCTION__, __LINE__);
        }
        
        PORT_GPS_power(TRUE);
        s_gnss_is_open = TRUE;
        BK_LOG("开GPS");
    } else {
        BK_LOG("GPS已开");
    }
}

BOOLEAN PORT_Gnss_IsPowerOn(void)
{
    return s_gnss_is_open;
}

static void _gnss_task(void* para)
{
    INT16S ret = -1;
    INT8U ch;
    //if (!s_gnss_task_ref) return;

    luat_rtos_task_sleep(1000);
    PORT_Gnss_open();   // for test
    
    while (1) {
        if (!PORT_Gnss_IsPowerOn()) {
            luat_rtos_task_sleep(1000);
            continue;
        }

        ret = PORT_UartRead(GPS_UART);
        if (ret == -1) {
            luat_rtos_task_sleep(100);
        } else {
            ch = ret & 0xff;
            DAL_GPSRecvProc(ch);
        }
        
    }
}

/*****************************************************************************
 函 数 名  : PORT_Gnss_MspInit
 功能描述  : GNSS驱动开机首次初始化资源
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年2月7日, 星期二
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void PORT_Gnss_MspInit(void)
{
    s_gnss_is_open = FALSE;
    
    luat_rtos_task_create(&s_gnss_task_ref, 2*1024, 50, "GPS", _gnss_task, NULL, 0);
}


