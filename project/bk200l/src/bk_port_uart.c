/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : port_uart.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年3月4日
  最近修改   :
  功能描述   : UART串口的port层实现。
  函数列表   :
              OURAPP_InitTrace
              OURAPP_printhexbuf
              PORT_CloseUart
              PORT_InitUart
              PORT_LeftOfUartWriteBuf
              PORT_UartRead
              PORT_UartWriteBlock
              _UART_CallBack
              _uart_rx_cb
              _uart_tx_cb
  修改历史   :
  1.日    期   : 2018年3月4日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
#define GLOBAL_PORT_UART    1
/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_task.h"

#include "bk_our_tools.h"



/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

const int UART_No[MAX_UART_NUM] = {1, 0};   // pic串口对应uart1，gps串口对应uart0


/*
********************************************************************************
* 打印调试接口，暂放于此
********************************************************************************
*/
#if OUR_DEBUG > 0
#if 1
// 临时测试写法
static INT8U _recv_round[400];
static INT8U _send_round[400];

static UART_CFG_T s_dbg_uart = {
    PIC_UART,
    9600,
    400,
    400,
    _recv_round,
    _send_round
};
#endif

void OURAPP_InitTrace(BOOLEAN flag)
{
    if (flag) {
        g_flag_dbg = 1;
        PORT_InitUart(&s_dbg_uart); // for test
    } else {
        g_flag_dbg = 0;
    }
    
    g_GPRS_dbg_switch   = 1;
    g_BK_dbg_switch     = 1;
    g_pic_dbg_switch    = 1;
    g_gps_dbg_switch    = 0;
}
/*
void OURAPP_trace(char *fmt, ...)
{
    if(g_flag_dbg == 0) return;

    ////memset(buf, 0, MAX_DBG_BUF_LEN);
    //strcpy(ptr, "[ourapp_trace]: ");

    memset(s_trace_buf, 0, MAX_DBG_BUF_LEN);
    sprintf(s_trace_buf, fmt, ##__VA_ARGS__);
    strcat(s_trace_buf, "\r\n\0");
    ql_uart_write((ql_uart_port_number_e)(DBG_PORT), (u8*)(s_trace_buf), strlen((const char *)(s_trace_buf)));
}*/

#if 0
void OURAPP_trace(const char *fmt, ...)
{
    if (!g_flag_dbg) {
        return;
    }
    va_list ap;
	va_start(ap, fmt);

    memset(gb_hexbuf, 0, MAX_DBG_HEXBUF_LEN);
    sprintf(gb_hexbuf,fmt); 
    strcat(gb_hexbuf, "?\r\n\0");
    luat_uart_write(DBG_PORT, (void*)(gb_hexbuf), strlen((const char *)(gb_hexbuf)));
	va_end(ap);
    
}
#endif

//#define MAX_DBG_HEXBUF_LEN  1024        // 定义最大trace 缓冲长度
//char gb_hexbuf[MAX_DBG_HEXBUF_LEN];     // hex 输出缓冲
void OURAPP_printhexbuf(INT8U *hex, INT16U len)
{
    if (g_flag_dbg == 0 || g_BK_dbg_switch == 0) return;
	/*INT16U i=0;

    char *hexbuf=gb_hexbuf;

    if (g_flag_dbg == 0 || g_BK_dbg_switch == 0) return;

    if (len > (MAX_DBG_HEXBUF_LEN / 3 - 1)) {
        len = MAX_DBG_HEXBUF_LEN / 3 - 1;
    }
    for (i=0; i<len; i++) {
        hexbuf[3*i]   = HexToChar(hex[i] >> 4);
        hexbuf[3*i+1] = HexToChar(hex[i] & 0x0f);
        hexbuf[3*i+2] = ' ';
    }
    hexbuf[3*i]   = 0x0d;
    hexbuf[3*i+1] = 0x0a;
    //hexbuf[3*i+2] = 0x00;
    //eat_sleep(5);

    luat_uart_write(DBG_PORT, (void*)(hexbuf), len*3+2);*/
    
    luat_debug_dump((uint8_t*)hex, (uint32_t)len);
}
#endif  // #if OUR_DEBUG > 0

/*
********************************************************************************
* 定义模块配置参数
********************************************************************************
*/
#define MAX_UART_SIZE            1024

/*
********************************************************************************
* 定义模块数据结构
********************************************************************************
*/
typedef struct {
    BOOLEAN inited;          /* 是否已初始化的 */
    
    INT16U  rx_len;          /* 配置接收缓存长度 */
    INT16U  tx_len;          /* 配置发送缓存长度 */
    BOOLEAN tx_busy;         // 180302 add, 用于做发送繁忙的判断
    
    INT8U   *tx_ptr;         /* 发送指针 */
    INT8U   *rx_ptr;         /* 接收指针 */
    
    ROUND_T w_round;         /* 发送环形缓冲 */
    ROUND_T r_round;         /* 接收环形缓冲 */
} USART_T;

/*
********************************************************************************
* 定义模块变量
********************************************************************************
*/
static USART_T s_uart[MAX_UART_NUM];  // !!! 必须保证uart_port2要等于1才行
//static INT16U s_restlen[MAX_UART_NUM];  // 150628 add 未完全发送的字节
//static INT8U s_buf[MAX_UART_NUM][MAX_UART_SIZE]; // 14010 rocky 发送缓冲
static INT8U s_tbuf1[MAX_UART_SIZE]; // uart1发送缓冲

static INT8U s_rbuf1[MAX_UART_SIZE]; // uart1接收缓冲
//static INT8U s_rbuf2[MAX_UART_SIZE]; // uart2接收缓冲
//static INT8U s_rbuf3[MAX_UART_SIZE]; // uart3接收缓冲

#define BK_MIN(a,b) ((a) < (b) ? (a) : (b))
// TODO: 读取数据回调。可能由于mtk特性，回调不确定可否共用，待观察

static void _uart_rx_cb(int ch, uint32_t size)
{
    unsigned int readSize = 0;
    int rlen = 0;
    INT16U roundleft;
    INT8U  port;   // = ch - UART_No[0];
    if (ch == UART_No[0]) {
        port = 0;
    } else if (ch == UART_No[1]) {
        port = 1;
    } else {
        return;
    }

    //LUAT_DEBUG_PRINT("uart%d rlen=%d", ch, size);
    
    if (port >= MAX_UART_NUM) return;

    
    while (size > 0) {
        readSize = BK_MIN(size, MAX_UART_SIZE);
        rlen = luat_uart_read(ch, s_rbuf1, readSize);// 这个接口返回值是形参data_len,根本不会是读取到的缓存
        //LUAT_DEBUG_PRINT("u%d got:%x,ret=%d", ch, s_rbuf1[0], rlen);
        if ((rlen <= 0) || (size < (uint32_t)rlen)) return;
        if (!s_uart[port].inited) {
            return;// 丢弃
        } 
        size -= rlen;
        roundleft = _Round_Left(&s_uart[port].r_round);
        //OURAPP_trace("%d,%d!", rlen, roundleft);
        if (rlen > roundleft) {
            rlen = roundleft;
        }
        if (rlen) _Round_WriteBlock(&s_uart[port].r_round, s_rbuf1, rlen);
    }
    // TODO:这边最好要通知解析
}

// 发送完成
static void _uart_tx_cb(int ch, void *param)
{
    INT16U slen;
    int ct_send;    // 实际的发送长度
    INT8U  port;   // = ch - UART_No[0];
    if (ch == UART_No[0]) {
        port = 0;
    } else if (ch == UART_No[1]) {
        port = 1;
    } else {
        return;
    }
    
    slen = _Round_GetReadData(&s_uart[port].w_round, s_tbuf1, MAX_UART_SIZE);
    
    if (slen == 0) {
        s_uart[port].tx_busy = FALSE;    // 无数据需要发送
        return;
    }
    ct_send = luat_uart_write(UART_No[port], s_tbuf1, slen);
    if (ct_send > 0) {
        _Round_ReleaseRead(&s_uart[port].w_round, ct_send);
    }

    // if (!_Round_Left(&s_uart[port].w_round)) {// 刚发完也做个判断。
    if (0 == s_uart[port].w_round.used) {   // 发送完毕
        s_uart[port].tx_busy = FALSE;
    }
}

BOOLEAN PORT_InitUart(UART_CFG_T *cfg)
{
    int ret;
    luat_uart_t uart = {
        .id = 1,        // 默认串口1
        .baud_rate = 9600,
        .data_bits = 8,
        .stop_bits = 1,
        .parity    = 0
    };
        
    
    if (cfg->port >= MAX_UART_NUM) return FALSE;                           /* 串口号错误 */

    if (s_uart[cfg->port].inited != 0) {                                /* 已初始化，需要先关闭串口 */
        s_uart[cfg->port].inited = FALSE;
        luat_uart_close(UART_No[cfg->port]);
    }
    uart.id = UART_No[cfg->port];
    if (uart.id == 0) {
        luat_uart_pre_setup(0, 1);// 貌似需要复用
    }
	uart.baud_rate = cfg->baud;

    s_uart[cfg->port].tx_ptr = cfg->tx_ptr;                                    /* 设置收发缓存指针和长度 */
    s_uart[cfg->port].rx_ptr = cfg->rx_ptr;
    s_uart[cfg->port].tx_len = cfg->tx_len;
    s_uart[cfg->port].rx_len = cfg->rx_len;
    _Round_Init(&s_uart[cfg->port].w_round, s_uart[cfg->port].tx_ptr, s_uart[cfg->port].tx_len);
    _Round_Init(&s_uart[cfg->port].r_round, s_uart[cfg->port].rx_ptr, s_uart[cfg->port].rx_len);

    ret = luat_uart_setup(&uart);

	if(ret >= 0) {
        luat_uart_ctrl(UART_No[cfg->port], LUAT_UART_SET_RECV_CALLBACK, _uart_rx_cb);
        luat_uart_ctrl(UART_No[cfg->port], LUAT_UART_SET_SENT_CALLBACK, _uart_tx_cb);
    } else {
        LUAT_DEBUG_PRINT("uart_open %d err,%d", UART_No[cfg->port], ret);
        return FALSE;
	}

    s_uart[cfg->port].inited = TRUE;
    s_uart[cfg->port].tx_busy= FALSE;

    return TRUE;
}

void PORT_CloseUart(INT8U port)
{
    if (port >= MAX_UART_NUM) return;
    
    s_uart[port].inited = FALSE;
    s_uart[port].tx_busy= FALSE;

    luat_uart_close(UART_No[port]);
}

/*******************************************************************
**  函数名:     PORT_UartRead
**  函数描述:   从串口读取一个字节
**  参数:       [in] port: 串口编号
**  返回:       读取到的字节, -1则无效
********************************************************************/
INT16S PORT_UartRead(INT8U port)
{
    if (port >= MAX_UART_NUM) return -1;
    if (!s_uart[port].inited) return -1;

    return _Round_ReadByte(&s_uart[port].r_round);
}


/*******************************************************************
**  函数名:     PORT_LeftOfUartWriteBuf
**  函数描述:   读取发送缓存剩余空间
**  参数:       [in] port: 串口编号
**  返回:       剩余空间，单位：字节
********************************************************************/
INT16U PORT_LeftOfUartWriteBuf(INT8U port)
{
    if (port >= MAX_UART_NUM) return 0;
    if (!s_uart[port].inited) return 0;
    
    return _Round_Left(&s_uart[port].w_round);
}

/*******************************************************************
**  函数名:     PORT_UartWriteBlock
**  函数描述:   向串口写入一串数据
**  参数:       [in] port: 串口编号
**              [in] sptr: 待发送的数据指针
**              [in] slen: 待发送的数据长度
**  返回:       发送结果
********************************************************************/
BOOLEAN PORT_UartWriteBlock(INT8U port, INT8U *sdata, INT16U slen)
{
    int ct_send;
    
    if (port >= MAX_UART_NUM) return FALSE;
    if (!s_uart[port].inited) return FALSE;

    // TODO: ql_uart_write的返回值到底是不是int，待商榷
    if (s_uart[port].tx_busy == FALSE) { // 串口空闲
        ct_send = luat_uart_write(UART_No[port], sdata, slen);
    } else {
        ct_send = 0;
    }
    if (ct_send < 0) ct_send = 0;
    ////s_uart[port].tx_busy = TRUE;//// 加入干扰，可测试通讯异常而超时复位
    if (ct_send < slen) {// 多余的写入roundbuf
        if (_Round_WriteBlock(&s_uart[port].w_round, sdata+ct_send, slen - ct_send)) {
            s_uart[port].tx_busy = TRUE;
        } else {
            return FALSE;
        }
    }
    return TRUE;
}



