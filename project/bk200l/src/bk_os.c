
#include "bk_os.h"
#include "bk_port_hardware.h"
#include "bk_app_picwork.h"
#include "luat_pm.h"

#include <stdio.h>
#include <string.h>


#if 0
void set_delayRestSys(INT16U delaySec);



static ql_queue_t s_msg_queue[MAX_QUEUE_NUM];
//static sTaskRef s_taskRef[MAX_TASK_NUM];




BOOLEAN BK_OS_CreateMessage(MSG_QUEUE_E index)
{
    QlOSStatus ret;
    if (index >= MAX_QUEUE_NUM) return FALSE;
    
    ret = ql_rtos_queue_create(&s_msg_queue[index], sizeof(BK_OS_FIX_MSG), 20); //// 目前先给每个队列20个节点
    if (ret != QL_OSI_SUCCESS) {
        return FALSE;
        Error_Handler("创建Queue出错", index);
    } else {
        return TRUE;
    }
}

BOOLEAN BK_OS_GetMessage(MSG_QUEUE_E index, BK_OS_FIX_MSG* msg, BOOLEAN suspend)
{
    QlOSStatus ret;
    uint32 timeOut;

    if (index >= MAX_QUEUE_NUM) return FALSE;
    if (suspend) {
        timeOut = QL_WAIT_FOREVER;
    } else {
        timeOut = QL_NO_WAIT;
    }
    ret = ql_rtos_queue_wait(s_msg_queue[index], (uint8*)msg, sizeof(BK_OS_FIX_MSG), timeOut);
    if (ret != QL_OSI_SUCCESS) {
        return FALSE;
    } else {
        return TRUE;
    }
}

BOOLEAN BK_OS_SendMessage(MSG_QUEUE_E index, BK_OS_FIX_MSG* msg)
{
    QlOSStatus ret;
    if (index >= MAX_QUEUE_NUM) return FALSE;

    ret = ql_rtos_queue_release(s_msg_queue[index], sizeof(BK_OS_FIX_MSG), (uint8*)msg, 0);

    if (ret != QL_OSI_SUCCESS) {
        return FALSE;
    } else {
        return TRUE;
    }
}


BOOLEAN BK_OS_CreateMutex(ql_mutex_t* mutex)
{
    QlOSStatus ret = ql_rtos_mutex_create(mutex);
    if (ret != QL_OSI_SUCCESS) {
        return FALSE;
    } else {
        return TRUE;
    }
}

void BK_OS_TakeMutex(ql_mutex_t* mutex, UINT32 outTime)
{
    ql_rtos_mutex_lock(mutex, outTime);
}

void BK_OS_GiveMutex(ql_mutex_t* mutex)
{
    ql_rtos_mutex_unlock(mutex);
}
/*
// 获取当前任务的ID
s32 BK_OS_GetActiveTaskId(void)
{
    sTaskRef taskRef;
    SC_STATUS status;
    INT8U  i;
    status = sAPI_TaskGetCurrentRef(&taskRef);
    if (SC_SUCCESS ==  status) {
        for (i = 0; i <MAX_TASK_NUM; i++ ) {
           // OURAPP_trace("debug msg: %s i[%d] curtaskRef[%d],ref[%d]\r\n",__FUNCTION__,i,taskRef,s_taskRef[i]);
            if (taskRef ==s_taskRef[i] ) {
                return i;
            }
        }
    }
    return BK_RET_ERR;
}*/

BOOLEAN BK_OS_CreateTask(ql_task_t* taskRef, INT32U stackSize, INT8U priority, char *taskName, void (*taskStart)(void*), void* argv)
{
    QlOSStatus ret;

    ret = ql_rtos_task_create(taskRef, stackSize, priority, taskName, taskStart, argv);
    if (ret != QL_OSI_SUCCESS) {
        return FALSE;
    } else {
        return TRUE;
    }

}

#endif
void* BK_MEM_Alloc(u32 size)
{
    return malloc(size);
}

void BK_MEM_Free(void *ptr)
{   
    free(ptr);
}

void BK_Sleep(u32 msec)
{
    if (msec == 0) msec = 1;
    luat_rtos_task_sleep(msec);
}


void BK_Reset(void)
{
    /*static BOOLEAN hasHdl = FALSE;
    if (hasHdl == TRUE) return;  // 禁止重复调用
    hasHdl = TRUE;*/
    BK_Sleep(1000);
    luat_pm_reboot();
}

void BK_Poweroff(void)
{
    BK_Sleep(500);
    luat_pm_poweroff();
}

void Error_Handler(const char* func_name, int line)
{
    #if OUR_DEBUG > 0
        PORT_LED_GREEN(TRUE);
        OURAPP_trace("Err:[%s] L=%d", func_name, line);
        BK_Sleep(1000);
    #endif
    BK_Reset();
}

// 格式 Apr 23 2017
// Example of __DATE__ string: "Jul 27 2012"
// 01234567890

#define BUILD_YEAR_CH0 (__DATE__[ 7])
#define BUILD_YEAR_CH1 (__DATE__[ 8])
#define BUILD_YEAR_CH2 (__DATE__[ 9])
#define BUILD_YEAR_CH3 (__DATE__[10])


#define BUILD_MONTH_IS_JAN (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_FEB (__DATE__[0] == 'F')
#define BUILD_MONTH_IS_MAR (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define BUILD_MONTH_IS_APR (__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define BUILD_MONTH_IS_MAY (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define BUILD_MONTH_IS_JUN (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_JUL (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define BUILD_MONTH_IS_AUG (__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define BUILD_MONTH_IS_SEP (__DATE__[0] == 'S')
#define BUILD_MONTH_IS_OCT (__DATE__[0] == 'O')
#define BUILD_MONTH_IS_NOV (__DATE__[0] == 'N')
#define BUILD_MONTH_IS_DEC (__DATE__[0] == 'D')


#define BUILD_MONTH_CH0 \
((BUILD_MONTH_IS_OCT || BUILD_MONTH_IS_NOV || BUILD_MONTH_IS_DEC) ? '1' : '0')

#define BUILD_MONTH_CH1 \
( \
(BUILD_MONTH_IS_JAN) ? '1' : \
(BUILD_MONTH_IS_FEB) ? '2' : \
(BUILD_MONTH_IS_MAR) ? '3' : \
(BUILD_MONTH_IS_APR) ? '4' : \
(BUILD_MONTH_IS_MAY) ? '5' : \
(BUILD_MONTH_IS_JUN) ? '6' : \
(BUILD_MONTH_IS_JUL) ? '7' : \
(BUILD_MONTH_IS_AUG) ? '8' : \
(BUILD_MONTH_IS_SEP) ? '9' : \
(BUILD_MONTH_IS_OCT) ? '0' : \
(BUILD_MONTH_IS_NOV) ? '1' : \
(BUILD_MONTH_IS_DEC) ? '2' : \
/* error default */ '?' \
)

#define BUILD_DAY_CH0 ((__DATE__[4] >= '0') ? (__DATE__[4]) : '0')
#define BUILD_DAY_CH1 (__DATE__[ 5])

// 前面11字节固定

#if FOREIGN_APN == APN_CMIOT
static char s_ver[17] = "BKM000000-180610";
#elif FOREIGN_APN == APN_Indonesia
static char s_ver[17] = "YNH000000-180610";
#elif FOREIGN_APN == APN_Kazakhstan
static char s_ver[17] = "KAZ000000-180610";
#elif FOREIGN_APN == APN_Laos
static char s_ver[17] = "LAO000000-180610";
#endif
char* PORT_GetVersion(void)
{
    INT8U* picVer = GetPicVer();

    s_ver[3] = picVer[5];
    s_ver[4] = picVer[6];
    s_ver[5] = picVer[7];
    s_ver[6] = picVer[8];
    s_ver[7] = picVer[9];
    s_ver[8] = picVer[10];
    s_ver[10] = BUILD_YEAR_CH2;
    s_ver[11] = BUILD_YEAR_CH3;
    s_ver[12] = BUILD_MONTH_CH0;
    s_ver[13] = BUILD_MONTH_CH1;
    s_ver[14] = BUILD_DAY_CH0;
    s_ver[15] = BUILD_DAY_CH1;
    s_ver[16] = 0x00;
    
    return s_ver;
}
/*
DATE_T *PORT_GetVersionDate(void)
{
    static DATE_T s_dt;
    s_dt.year   = ((BUILD_YEAR_CH2 - '0')*10) + (BUILD_YEAR_CH3-'0');
    s_dt.month  = ((BUILD_MONTH_CH0 - '0')*10) + (BUILD_MONTH_CH1-'0');
    s_dt.day    = ((BUILD_DAY_CH0 - '0')*10) + (BUILD_DAY_CH1-'0');
    return &s_dt;
}*/


