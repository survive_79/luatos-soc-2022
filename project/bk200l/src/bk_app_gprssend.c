/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : app_gprs_send.c
  版 本 号   : 初稿
  作    者   : rocky
  生成日期   : 2015年5月10日
  最近修改   :
  功能描述   : GPRS数据发送接口。包括了重传处理、发送结果通知等
  函数列表   :
              APP_GPRS_InitSend
              APP_GPRS_ListAck
              APP_GPRS_ListSend
              _DelCell
              GPRSSendTmrProc
  修改历史   :
  1.日    期   : 2015年5月10日
    作    者   : rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#include "bk_our_tools.h"
#include "bk_our_list.h"
#include "bk_app_gprssend.h"
#include "bk_port_uart.h"
#include "bk_port_gprs.h"
#include "bk_port_hardware.h"
#include "bk_dal_sock.h"



/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define GSS_DEBUG   0

#if GSS_DEBUG > 0
#define GSS_TRACE OURAPP_trace
#define GSS_LOGH  OURAPP_printhexbuf
#else
#define GSS_TRACE(FORMAT,...) 
#define GSS_LOGH(A, B) 
#endif

#define NUM_MEM              6
#define MAX_SIZE             400       /* 数据长度限制 */

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef struct {
    INT8U type;                        /* 协议类型 */
    INT8U ct_send;                     /* 重发次数 */
    INT8U ct_time;                     /* 重发等待时间计数 */
    INT8U flowtime;                    /* 重发等待时间 */
    INT16U slen;                       /* 数据长度 */
    BOOLEAN is_main;                   // 191128 add, 决定发送到哪个通道
    INT8U sptr[MAX_SIZE];              /* 数据空间 */
    ACK_FUNC fp;                       /* 发送结果通知回调 */
} CELL_T;
/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static struct {
    NODE   reserve;
    CELL_T cell;
} s_memory[NUM_MEM];

// static INT8U s_gprs_sendtmr;
static LIST_T s_gprs_waitlist, s_gprs_readylist, s_gprs_freelist;
static ASMRULE_T s_rule = {0x7e, 0x7d, 0x02, 0x01};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



/*******************************************************************
** 函数名:     _DelCell
** 函数描述:   删除节点
** 参数:       [in] cell:链表节点
** 参数:       [in] result:结果
** 返回:       无
********************************************************************/
static void _DelCell(CELL_T *cell, INT8U result)
{
    ACK_FUNC fp;
    
    fp = cell->fp;
    cell->slen = 0;
    GSS_TRACE("_Del[%d]:%d", cell->is_main, cell->type);
    OUR_AppendListEle(&s_gprs_freelist, (LISTMEM *)cell);
    if (fp != 0) { 
        fp(cell->is_main, result);
    }
}

void APP_GPRSSendTmrProc(void)
{
    CELL_T *cell, *next;
    
    if (OUR_ListItem(&s_gprs_waitlist) + OUR_ListItem(&s_gprs_readylist) == 0) {
        return;
    }
    // if  (!PORT_GPRS_IS_ON()) return;    //// 140706, 用于在没上GPRS时让发送超时
    if ((SOCK_OK != Dal_SockGetState(SOCK_FD_MAIN)) /*&& 暂时屏蔽
        (OTA_ATTACH_SUCC > DAL_OTA_GetState()) && 
        AGPS判断*/) {
        return;
    }
        
    cell = (CELL_T *)OUR_GetListHead(&s_gprs_waitlist);                          /* 扫描等待链表，检测重传时间是否超时 */
    for (;;) {
        if (cell == 0) break;
        if (++cell->ct_time > cell->flowtime) {                             /* 重传时间超时 */
            cell->ct_time = 0;
            next = (CELL_T *)OUR_DelListEle(&s_gprs_waitlist,(LISTMEM *)cell);
            if (--cell->ct_send == 0) {                                     /* 超过最大重传次数 */
                _DelCell(cell, _OVERTIME);
            } else {
                OUR_AppendListEle(&s_gprs_readylist, (LISTMEM *)cell);
            }
            cell = next;
        } else { 
            cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);
        }
    }
    
    cell = (CELL_T *)OUR_GetListHead(&s_gprs_readylist);                         /* 扫描准备就绪链表，检查等待时间是否超时 */
    for (;;) {
        if (cell == 0) break;
        if (++cell->ct_time > cell->flowtime) {                             /* 等待时间超时 */
            GSS_TRACE("##flowtime");
            cell->ct_time = 0;
            if (cell->ct_send == 0) {   // TODO: 3次都发不成功要考虑延时60分钟重新登录再发
                next = (CELL_T *)OUR_DelListEle(&s_gprs_readylist, (LISTMEM *)cell); /* 从就绪链表中删除节点 */
                _DelCell(cell, _OVERTIME);
                cell = next;
                continue;
            } else {
                cell->ct_send--;
            }
        }
        cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);
    }
    
    if (OUR_ListItem(&s_gprs_readylist) > 0) {                                   /* 就绪链表中存在待发送节点 */
        GSS_TRACE("##2222!");
        cell = (CELL_T *)OUR_GetListHead(&s_gprs_readylist);
        while (cell) {
            #if GSS_DEBUG > 0
            BOOLEAN ret;
            #endif
            // 这里发送到socket
            
            if (cell->is_main) {
#if GSS_DEBUG > 0
                ret = Dal_SockSend(SOCK_FD_MAIN, cell->sptr, cell->slen);
#else
                Dal_SockSend(SOCK_FD_MAIN, cell->sptr, cell->slen);
#endif
            } else {
#if GSS_DEBUG > 0
                ret = Dal_SockSend(SOCK_FD_BACK, cell->sptr, cell->slen);
#else
                Dal_SockSend(SOCK_FD_BACK, cell->sptr, cell->slen);
#endif
            }
            GSS_TRACE("%s发:%d", cell->is_main?"主":"副", ret);

            next = (CELL_T *)OUR_DelListEle(&s_gprs_readylist, (LISTMEM *)cell);
            if (cell->ct_send > 0) {                                           /* 需要重传的数据帧 */
                cell->ct_time = 0;
                OUR_AppendListEle(&s_gprs_waitlist,(LISTMEM *)cell);
            } else {                                                           /* 无需继续重传的数据帧 */
                _DelCell(cell, _SUCCESS);
            }
            cell = next;
        }
    }
}

/*******************************************************************
** 函数名:     APP_GPRS_InitSend
** 函数描述:   GPRS发送模块初始化
** 参数:       无
** 返回:       无
********************************************************************/
void APP_GPRS_InitSend(void)
{
    OUR_InitList(&s_gprs_waitlist);
    OUR_InitList(&s_gprs_readylist);
    OUR_InitMemList(&s_gprs_freelist, (LISTMEM *)s_memory, NUM_MEM, sizeof(s_memory[0]));
    /*
    s_gprs_sendtmr = CreateTmr(GPRSSendTmrProc);

    if (s_gprs_sendtmr == INVALID_TMR) {
        GSS_TRACE("[%s]err! sendtmr=ff", __FUNCTION__);
    }*/
}

/*******************************************************************
** 函数名:     APP_GPRS_ListSend
** 函数描述:   GPRS数据链表发送
** 参数:       [in] type: 协议类型
**             [in] body: 协议体指针, 必须是末尾已有校验和
**             [in] len:  协议长度
**             [in] ct_send:  发送次数
**             [in] ct_time:  重发等待时间, 单位秒
**             [in] fp:   发送结果通知
** 返回:       成功返回true，失败返回false
********************************************************************/
BOOLEAN APP_GPRS_ListSend(BOOLEAN is_main, INT8U type, INT8U *body, INT16U len, INT8U ct_send, INT16U ct_time, ACK_FUNC fp)
{
    CELL_T *cell;
    BOOLEAN ret;
   
	if (len > MAX_SIZE) return false;                                       /* 数据帧长度过长 */
	
    if ((cell = (CELL_T *)OUR_DelListHead(&s_gprs_freelist)) != 0) {         /* 申请链表节点 */
        cell->type     = type;
        cell->ct_send  = ct_send;
        cell->flowtime = ct_time * 10 + 1;                                  /* 等待重传时间 */
        cell->ct_time  = 0;
        cell->fp       = fp;
        cell->is_main  = is_main;
        cell->slen = OUR_AssembleByRules(cell->sptr, body, len, &s_rule);

        if (cell->slen < 13) {
            OUR_AppendListEle(&s_gprs_freelist, (LISTMEM *)cell);             /* 重新放回已获取的空闲链表 */
            return false;
        }
        ret = OUR_AppendListEle(&s_gprs_readylist, (LISTMEM *)cell);                /* 将新增节点放入就绪链表 */
        GSS_TRACE("#append[%d]:%x,ret%d,%d", is_main, cell->type, ret, OUR_ListItem(&s_gprs_readylist));
        /*if (!TmrIsRun(s_gprs_sendtmr)) {
            StartTmr(s_gprs_sendtmr, _100MS,  TRUE);
            GSS_TRACE("##StartTmr");
        }*/
        
        return ret;
    } else {
        return false;
    }
}

/*******************************************************************
** 函数名:     APP_GPRS_ListAck
** 函数描述:   GPRS发送链表确认
** 参数:       [in] type: 协议类型
** 返回:       成功返回true，失败返回false
********************************************************************/
BOOLEAN APP_GPRS_ListAck(BOOLEAN is_main, INT8U type)
{
    CELL_T  *cell;
    
    cell = (CELL_T *)OUR_GetListHead(&s_gprs_waitlist);
    for (;;) {                                                          /* 查找等待链表 */
        if (cell == 0) break;
        if ((cell->type == type) && (cell->is_main == is_main)) {     /* 查找到匹配的节点 */
            OUR_DelListEle(&s_gprs_waitlist, (LISTMEM *)cell);
            _DelCell(cell, _SUCCESS);
            return true;
        } else {
            cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);          /* 扫描下个节点 */
        }
    }

    cell = (CELL_T *)OUR_GetListHead(&s_gprs_readylist);                 /* 查找就绪链表 */
    for (;;) {
        if (cell == 0) break;
        if ((cell->type == type) && (cell->is_main == is_main)) {       /* 查找到匹配节点 */
            OUR_DelListEle(&s_gprs_readylist, (LISTMEM *)cell);
            _DelCell(cell, _SUCCESS);
            return true;
        } else {
            cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);          /* 扫描下个节点 */
        }
    }
    return false;        
}





