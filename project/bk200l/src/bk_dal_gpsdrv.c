
#define GLOBAL_DAL_GPS
#include "bk_os_task.h"

#include <math.h>


#include <string.h>
#include "bk_our_tools.h"

#include "bk_dal_gpsdrv.h"
#include "bk_dal_gpstool.h"
#include "bk_dal_systime.h"
#include "bk_port_gnss.h"

#define SIMPLE_FILTER   0

#define COMMA   ','
// UBX协议的SYNC CHAR
#define SYNC_CHAR_1 0xB5
#define SYNC_CHAR_2 0x62

#define PTYPE_NONE  0x00    // 当前并未处理任何数据
#define PTYPE_NMEA  0x01    // 正在处理NMEA协议
#define PTYPE_UBX_1 0x02    // 处理UBX 协议1--刚收到SYNC_CHAR_1
#define PTYPE_UBX   0x03    // 当前正在处理UBX 协议

static INT8U s_ptype;       // 当前处理的协议类型

#define MAX_RECV    512

///////////////////////////////////////////////////////
typedef struct {// NMEA消息处理结构体
    char *kword;
    void (*handler)(INT8U *ptr, INT16U len);
} NMEA_TBL_T;


static INT8U    s_rbuf[MAX_RECV];   // 接收缓冲
static INT16U   s_rlen;             // 接收缓冲收到的数据长度指示器


#define VALID_FIX_CNT           1   // 有了AGPS，就只过滤1个点，省电//3
static INT8U    s_fix_cnt;          // 定位过滤累加器，当大于VALID_FIX_CNT才认为定位

#define VALID_FIX3D_CNT         2   // 150325 新增
static INT8U    s_fix3d_cnt;        // 3d定位过滤累加器，当大于VALID_FIX3D_CNT才认为定位

#if SIMPLE_FILTER > 0
#define VALID_LAT_LONG_CNT  3
static INT8U  s_lat_cnt,s_long_cnt;
static INT32U s_lat[VALID_LAT_LONG_CNT];
static INT32U s_long[VALID_LAT_LONG_CNT];
#endif
//GPS_DATA_T  g_gps_data;

/*******************************************************************
**  函数名:     NMEAIsValid
**  函数描述:   检查校验和判断NMEA帧是否合法。
**  参数:       [in] ptr: 数据指针。$之后的数据
**              [in] len: 数据长度
**  返回:       true为合法
********************************************************************/
static BOOLEAN NMEAIsValid(INT8U* ptr, INT16U len)
{
    INT8U i, recvsum, chksum = 0;

    if (len < 5) return false;
    if (ptr[len-1] != 0x0a || ptr[len - 2] != 0x0d || ptr[len - 5] != '*') return false;

    recvsum = Char2_ToHex(&ptr[len - 4]);

    for (i = 0; i < (len-5); i++) {
        chksum ^= ptr[i];
    }

    if (chksum == recvsum) return true;
    else return false;
}

/***************************************************************
*   函数名:    _cvt_lat
*   功能描述:  转换为度数据格式(度×1000000格式)接口
*　 参数:      dptr(out):目标数据指针,sptr(in):原始数据指针
*   返回值:    无
***************************************************************/
static void _cvt_lat(INT32U *dptr, INT8U *sptr)
{  
    if(sptr[1] >= 60){
       sptr[0] += sptr[1]/60;
       sptr[1] %= 60;
    }
    *dptr = (sptr[1]*1000000L + sptr[2]*10000L + sptr[3]*100L)/60;        //将整数分和小数分转换成度并乘10^6
    *dptr = sptr[0]*1000000L + *dptr;                                     //将整数度与小数度(由整数分＆小数分转换而来)相加并乘10^6
}

/***************************************************************
*   函数名:    _cvt_long
*   功能描述:  转换经度数据格式(度×1000000格式)接口
*　 参数:      dptr(out):目标数据指针,sptr(in):原始数据指针
*   返回值:    无
***************************************************************/
static void _cvt_long(INT32U *dptr, INT8U *sptr)
{
    if(sptr[1] >= 60){
       sptr[0] += sptr[1] / 60;
       sptr[1] %= 60;
    }
    *dptr = (sptr[1]*1000000L + sptr[2]*10000L + sptr[3]*100L)/60;        //将整数分和小数分转换成度并乘10^6
    *dptr = sptr[0]*1000000L + *dptr;                                     //将整数度与小数度(由整数分＆小数分转换而来)相加并乘10^6
}

/***************************************************************
*   函数名:    YX_GetD_Latitude
*   功能描述:  获取度×1000000格式纬度
*　 参数:      无
*   返回值:    度×1000000格式纬度
***************************************************************/
INT32U _GetD_Latitude(INT8U* c_lat)                                            
{
    INT32U D_Latitude;
   
    _cvt_lat(&D_Latitude, c_lat);
    return D_Latitude;   	
}

/***************************************************************
*   函数名:    YX_GetD_Longitude
*   功能描述:  获取度×1000000格式经度
*　 参数:      无
*   返回值:    度×1000000格式经度
***************************************************************/
INT32U _GetD_Longitude(INT8U* c_long)                                           
{
    INT32U D_Longitude;
   
    _cvt_long(&D_Longitude, c_long);
    return D_Longitude;   	
}


/***************************************************************
*   函数名:    ConvertLatitude
*   功能描述:  转换纬度数据格式(10进制表示)接口
*　 参数:      dptr(out):目标数据指针,sptr(in):原始数据指针
*   返回值:    无
***************************************************************/
void ConvertLatitude(INT8U *dptr, INT8U *sptr)             
{
    dptr[0]  = Char2_ToDec(&sptr[0]);
    dptr[1]  = Char2_ToDec(&sptr[2]);
    dptr[2]  = Char2_ToDec(&sptr[5]);
    dptr[3]  = Char2_ToDec(&sptr[7]);
 }

/***************************************************************
*   函数名:    ConvertLongitude
*   功能描述:  转换经度数据格式(10进制表示)接口
*　 参数:      dptr(out):目标数据指针,sptr(in):原始数据指针
*   返回值:    无
***************************************************************/
void ConvertLongitude(INT8U *dptr, INT8U *sptr)
{
    dptr[0] = AsciiToDec(&sptr[0], 3);
    dptr[1] = Char2_ToDec(&sptr[3]);
    dptr[2] = Char2_ToDec(&sptr[6]);
    dptr[3] = Char2_ToDec(&sptr[8]);
}


/* 解析水平精度因子,返回值*10 */
INT16S ConvertHdop(INT8U *ptr)
{
    INT8U len;
    INT16S ret;
    
    len = FFChar(ptr, '.');
    if (len < 5) {
        ret = AsciiToDec(ptr, len);
        ret *= 10;
        ret += ptr[len+1] - '0';
    } else {
        len = FFChar(ptr, ',');
        ret = AsciiToDec(ptr+1, len-1);
        ret *= 10;        
    }
    return ret;
}


/* 解析海拔高度 */
INT16S ConvertAltitude(INT8U *ptr)
{
    INT8U len;
    INT16S ret;
    
    len = FFChar(ptr, '.');
        
    if (ptr[0] == '-') {// 海平面以下
        ret = AsciiToDec(ptr+1, len-1);
        return (0-ret);
    } else {
        return AsciiToDec(ptr, len);
    }
}

/***************************************************************
*   函数名:    DAL_GetLatitude
*   功能描述:  获取度分秒格式纬度
*　 参数:      ptr(in):目标地址指针
*   返回值:    无
***************************************************************/
void DAL_GetLatitude(INT8U *ptr)
{
    memcpy(ptr, g_gps_data.c_lat, sizeof(g_gps_data.c_lat));
}

/***************************************************************
*   函数名:    DAL_GetLongitude
*   功能描述:  获取度分秒格式经度
*　 参数:      ptr(in):目标地址指针
*   返回值:    无
***************************************************************/
void DAL_GetLongitude(INT8U *ptr)
{
    memcpy(ptr, g_gps_data.c_long, sizeof(g_gps_data.c_long));
}

/***************************************************************
*   函数名:    DAL_GetGpsHeight
*   功能描述:  用于获取高程数据
*　 参数:      无
*   返回值:    高程
***************************************************************/
INT16S DAL_GetGpsHeight(void)
{
    return g_gps_data.alt;
}

#if SEND_LAST_GPGGA == 1
static  INT8U  s_lastGPGGAmsg[256];
static  INT8U  s_lastGpggaLen;

INT8U get_lastGPGGAmsg(INT8U *ptr)
{
	
	memcpy(ptr,s_lastGPGGAmsg,s_lastGpggaLen);
	return s_lastGpggaLen;
}

#endif
#define GPS_GET_MAX_START_MSG   8  // 获取最多卫星信息(目前设置最多只输出2条GSV命令)
static  INT8U  s_GpsUseStartNum;  // 最新可视星历个数
static  INT8U  s_gpsStartSerial[GPS_GET_MAX_START_MSG];   // 卫星编号
static  INT8U  s_gpsStartSignel[GPS_GET_MAX_START_MSG];   // 信号强度

/**
 ***********************************************
 * 获取信息
 * @param[inout] char *ptr:  
 * @return 
 * @see 
 * @note
 ptr 格式
 个数 第一个星编号 第一个星信号 ....
 ***********************************************
*/
INT8U   DAL_GSVMsgGet(char *ptr,INT8U maxLen)
{
	INT8U i = 0;
	if (maxLen < (1+GPS_GET_MAX_START_MSG*2)) return 0;
	// 数据读走 前s_GpsUseStartNum个数信息
    
	//信息清除
	ptr[0] = s_GpsUseStartNum ;
	for (i = 0; i < GPS_GET_MAX_START_MSG;i++) {
		ptr[1+i*2] = s_gpsStartSerial[i] ;
		ptr[1+i*2 +1] = s_gpsStartSignel[i];
	}
	return (1+GPS_GET_MAX_START_MSG*2);
}

static  INT16U  getPosVal(INT8U *ptr, INT16U len,INT8U locat)
{
	INT8U pos;
	INT8U pos2;
	INT16U  retVal;

    pos = FindCharPos(ptr, COMMA, locat-1, len)+1;

    pos2= FindCharPos(ptr, COMMA, locat, len);
	if(pos2 == (len)) {
		pos2= FindCharPos(ptr, '*', 0, len);		
	}
	if (pos ==0 || pos2 == 0) return 0;
	
    retVal = AsciiToDec(ptr+pos, pos2-pos);// TODO: 待验证
	return retVal;

}

/*******************************************************************
**  函数名:     HdlNMEA_GSV
**  函数描述:   GPGGA的程序处理. 只处理高程
**  参数:       none
**  返回:       none

$GPGSV, <1>,<2>,<3>,<4>,<5>,<6>,<7>,?<4>,<5>,<6>,<7>,<8><CR><LF>

<1> GSV语句的总数 
<2> 本句GSV的编号 
<3> 可见卫星的总数，00 至 12。 
<4> 卫星编号， 01 至 32。 
<5>卫星仰角， 00 至 90 度。 
<6>卫星方位角， 000 至 359 度。实际值。 
<7>讯号噪声比（C/No）， 00 至 99 dB；无表未接收到讯号。 
<8>Checksum.(检查位).

第<4>,<5>,<6>,<7>项个别卫星会重复出现，每行最多有四颗卫星。其余卫星信息会于次一行出现，若未使用，这些字段会空白。

********************************************************************/
static void HdlNMEA_GSV(INT8U *ptr, INT16U len)
{
    INT8U cnt = 0;
	INT8U total = 0;
	INT8U curCnt = 4;  // 每条固定4个卫星，除最后一条
	INT8U i = 0;
#if GPS_DEBUG > 0    
		OURAPP_trace("GSV:alt=%s", ptr);
#endif

	total= getPosVal(ptr,len,1);

	cnt = getPosVal(ptr,len,2);

	s_GpsUseStartNum = getPosVal(ptr,len,3);
	g_gps_data.inview = s_GpsUseStartNum;
	if (total == cnt) {  
		curCnt =s_GpsUseStartNum - (total -1)*4;
		if (curCnt > 4) return; // 异常
	}
	cnt-=1;  // 改为0开始计数
	for (i = 0; i < curCnt; i++) {
		if(cnt*4+i >= GPS_GET_MAX_START_MSG) break;
		s_gpsStartSerial[cnt*4+i] = getPosVal(ptr,len,4+i*4);
		s_gpsStartSignel[cnt*4+i] = getPosVal(ptr,len,7+i*4);		
	}	
	
#if 0   //GPS_DEBUG > 0    
	OURAPP_trace("start NUm[%d]",s_GpsUseStartNum);
	for (i = 0; i < GPS_GET_MAX_START_MSG;i++) {
		OURAPP_trace("%d:ser[%d],sing[%d]",i,s_gpsStartSerial[i],s_gpsStartSignel[i]);	
	}
#endif

	
}

/*******************************************************************
**  函数名:     HdlNMEA_GGA
**  函数描述:   GPGGA的程序处理. 只处理高程
**  参数:       none
**  返回:       none
********************************************************************/
static void HdlNMEA_GGA(INT8U *ptr, INT16U len)
{
    INT8U pos;
//#if GPS_DEBUG > 0
    // NUMBER SVs
    INT8U pos2;
//    INT8U sv;
//#endif
   // OURAPP_trace("GGA:%s", ptr);

    pos = FindCharPos(ptr, COMMA, 6, len)+1;
    pos2= FindCharPos(ptr, COMMA, 7, len);
    g_gps_data.inuse = AsciiToDec(ptr+pos, pos2-pos);// TODO: 待验证
   // OURAPP_trace("GGA:inuse=%d", g_gps_data.inuse);

    pos = FindCharPos(ptr, COMMA, 7, len)+1;
    g_gps_data.hdop =  ConvertHdop(ptr+pos);
    //OURAPP_trace("GGA:hdop=%d", g_gps_data.hdop);

    pos = FindCharPos(ptr, COMMA, 8, len)+1;
    if ((g_gps_data.status_flag & GPS_VALID) && pos) {
        g_gps_data.alt = ConvertAltitude(ptr+pos);
    }
    
   // OURAPP_trace("GGA:alt=%d",g_gps_data.alt);
}
// 模块送出的天线状态
static void HdlNMEA_TXT(INT8U *ptr, INT16U len)
{
    ptr[len-1] = '\0';
    if (strstr((const char*)ptr, "ANTENNA OK")) {
        g_gps_data.ant_state = ANT_OK;
    } else if (strstr((const char*)ptr, "ANTENNA SHORT")) {
        g_gps_data.ant_state = ANT_SHORT;
    } else if (strstr((const char*)ptr, "ANTENNA OPEN")) {
        g_gps_data.ant_state = ANT_OPEN;
    }
}

/*******************************************************************
** 函数名:     DAL_GPS_DegreeToSec
** 函数描述:   将度分格式转换成1/1024秒为单位的数据格式
** 参数:       [in]  ptr:      待转换的度分秒格式数据缓冲区
** 返回:       转换成1/1024秒为单位的数据
********************************************************************/
INT32U DAL_GPS_DegreeToSec(INT8U *ptr)
{
    INT32U tmp1;
	
	tmp1  = ((INT16U)ptr[2] * 100 + ptr[3]) * 60L;
	tmp1  = (tmp1 * 1024) / 10000L;
	tmp1 += (ptr[0] * 3600L + (INT16U)ptr[1] * 60) * 1024L;
	
	return tmp1;
}

/*******************************************************************
** 函数名:     DAL_GPS_SecToDegree
** 函数描述:   将1/1024秒为单位的数据格式转换成度分秒格式
** 参数:       [out] ptr:      转换后的度分秒格式数据缓冲区
**             [in]  sec:      以1/1024秒为单位的数据
** 返回:       无
********************************************************************/
void DAL_GPS_SecToDegree(INT8U *ptr, INT32U sec)
{
	INT16U tmp;

	ptr[0] = (INT8U)(sec / (1024 * 3600L));
	sec = sec - ptr[0] * (3600L * 1024);
	ptr[1] = (INT8U)(sec / (1024 * 60L));
	sec = sec - ptr[1] * (60L * 1024);
	tmp = (INT16U)((sec * 10000) / (60L * 1024));
	ptr[2] = (INT8U)(tmp / 100);
	ptr[3] = (INT8U)(tmp % 100);
}


/*******************************************************************
** 函数名:     DAL_GPS_GetWeekAndTow
** 函数描述:   获取GPS周数
** 参数:       [out] week:     GPS周数, 自1980年1月6日零时起算, 至当前时间累计的周数，也即GPS周(七天为一周)
**             [out] tow:      GPS秒数, 以GPS周为准, 从一周起始时间累积至当前时间的秒数, 该值每周清零
** 返回:       无
********************************************************************/
static TIME_T s_c_time;
static DATE_T s_c_date;
void DAL_GPS_GetWeekAndTow(INT16U *week, INT32U *tow)
{
    OUR_GPS_UtcToWeekAndTow(&s_c_date, &s_c_time, week, tow);
}

/*******************************************************************
**  函数名:     HdlNMEA_RMC
**  函数描述:   GPRMC的程序处理. 取日期、时间、速度和方向
**  参数:       none
**  返回:       none
********************************************************************/
static void HdlNMEA_RMC(INT8U *ptr, INT16U len)
{
    INT8U pos;
    INT16U tmpData;
    
    //BK_memset(g_gps_data, 0, sizeof(GPS_DATA_T));  // TODO:是否要清空待确定
    g_gps_data.status_flag = GPS_TYPE;
    g_gps_data.t_valid = false;
#if GPS_DEBUG > 0
    ptr[len] = '\0';  // 字符串结束符，用于打印
    OURAPP_trace("RMC=%s", ptr);    // 201224 add 
#endif
    
    /////////////////// GPRMC,
    ptr += 6;
    /////////////////// hhmmss.sss
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA)) < 9) return;
        s_c_time.hour   = Char2_ToDec(ptr);
        s_c_time.minute = Char2_ToDec(ptr+2);
        s_c_time.second = Char2_ToDec(ptr+4);
        ptr += pos;
    }
    ptr++; // ,
    /////////////////// A or V
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA))!= 1) return;
        if (*ptr == 'A' || *ptr == 'a') {
            //OURAPP_trace("RMC定");  //// 201224 add for test
            if (s_fix_cnt < VALID_FIX_CNT) {    // 过滤前几个点
                s_fix_cnt++;
            } else {
                g_gps_data.status_flag |= GPS_VALID;
#if GPS_DEBUG > 0
                ptr[len] = '\0';  // 字符串结束符，用于打印
                OURAPP_trace("RMC定位=%s", ptr);    // 221215
#endif
            }
        } else {    // 不定位
            g_gps_data.status_flag &= ~GPS_VALID;
        }
        ptr++;
    }
    ptr++; // ,
    /////////////////// Latitude. ddmm.mmmmm
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA)) < 9) return;
        ConvertLatitude(g_gps_data.c_lat, ptr);
        g_gps_data.D_lat = _GetD_Latitude(g_gps_data.c_lat);
        ptr += pos;
        #if SIMPLE_FILTER > 0
        if (s_fix3d_cnt >= VALID_FIX3D_CNT) {
            s_lat[s_lat_cnt] = g_gps_data.D_lat;
            s_lat_cnt++;
            if (s_lat_cnt >= VALID_LAT_LONG_CNT) {
                s_lat_cnt = 0;
            }
        }
        #endif
    }
    ptr++; // ,
    /////////////////// N/S
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA))!= 1) return;
        if (*ptr == 'S' || *ptr == 's') {
            g_gps_data.status_flag |= LAT_SOUTH;
        }
        ptr++;
    }
    ptr++; // ,
    /////////////////// Longitude. dddmm.mmmmm
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA)) < 10) return;
        ConvertLongitude(g_gps_data.c_long, ptr);
        g_gps_data.D_long = _GetD_Longitude(g_gps_data.c_long);
        ptr += pos;
        #if SIMPLE_FILTER > 0
        if (s_fix3d_cnt >= VALID_FIX3D_CNT) {
            s_long[s_long_cnt] = g_gps_data.D_long;
            s_long_cnt++;
            if (s_long_cnt >= VALID_LAT_LONG_CNT) {
                s_long_cnt = 0;
            }
        }    
        #endif
    }
    ptr++; // ,
    /////////////////// W/E
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA))!= 1) return;
        if (*ptr == 'w' || *ptr == 'W') {
            g_gps_data.status_flag |= LONG_WEST;
        }
        ptr++;
    }
    ptr++; // ,
    /////////////////// SOG, knots
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA)) < 3) return; // 起码是0.0
        len = FFChar(ptr, '.');

        tmpData = 0;
        if (len < 4) {
            tmpData = AsciiToDec(ptr, len);
        }
#if GPS_DEBUG > 0
        OURAPP_trace("RMC:spd=%d", tmpData);
#endif
        g_gps_data.spd = (tmpData * 1852L*10) / 1000;  //0.1 km/h  
        ptr += pos;
    }
    ptr++; // ,
    /////////////////// COG, degree
    if (*ptr != COMMA) {
        if ((pos = FFChar(ptr, COMMA)) < 3) return; // 起码是0.0
        len = FFChar(ptr, '.');
        tmpData = 0;
        if (len < 4) {
            tmpData = AsciiToDec(ptr, len);
        }
#if GPS_DEBUG > 0
        OURAPP_trace("RMC:dir=%d", tmpData);
#endif
        if (tmpData > 360) tmpData = 0;
        g_gps_data.direction = tmpData;
        ptr += pos;
    }
    /////////////////// ,DD,MM,YYYY
    if (*(ptr+1) != COMMA) {
        /*s_c_date.day    = Char2_ToDec(ptr+1);
        s_c_date.month  = Char2_ToDec(ptr+4);
        s_c_date.year   = Char2_ToDec(ptr+9);*/
        // 170712 change, 发现现在用u7的模块，日期信息都是ddmmyy了
        s_c_date.day = Char2_ToDec(ptr+1);
        s_c_date.month  = Char2_ToDec(ptr+3);
        s_c_date.year   = Char2_ToDec(ptr+5);
    }
    
    
    // 转换时间和日期
    memcpy(&g_gps_data.t_local.time, &s_c_time, sizeof(TIME_T));
    //170712 change, 笔误。memcpy(&g_gps_data.t_local.time, &s_c_date, sizeof(DATE_T));
    memcpy(&g_gps_data.t_local.date, &s_c_date, sizeof(DATE_T));
    OUR_ConvertGmtToLocaltime(&g_gps_data.t_local.date, &g_gps_data.t_local.time, 8);
#if GPS_DEBUG > 0
    OURAPP_trace("RMC:tm=%d-%d,%d:%d", g_gps_data.t_local.date.month, g_gps_data.t_local.date.day,
                                         g_gps_data.t_local.time.hour, g_gps_data.t_local.time.minute);
#endif
    if (DateValid(&g_gps_data.t_local.date) && TimeValid(&g_gps_data.t_local.time)) {
        if (!g_gps_data.t_valid) {
            DAL_SetSysTime(&g_gps_data.t_local, TRUE);// 换这个接口校时
            app_set_pic_time();    // 这里判断1次是否是未按时唤醒，如果是，则需要休眠
        } else {
            DAL_SetSysTime(&g_gps_data.t_local, FALSE);// 换这个接口校时
        }
        g_gps_data.t_valid = true;
    }

#if 0
    g_gps_data.angle = GPS_angle_judge(&g_gps_data);
    tmpData =  GPS_mileage_estimate(&g_gps_data);
#else// for test
    tmpData = g_gps_data.angle;
#endif
    
    //if ((g_our_status.shake_statu == 0) && (g_our_status.acc_statu == 1)) {  // 静止的时候强制处理
    if ((g_our_status.shake_statu == 0)) {  // 静止的时候强制处理
        g_gps_data.angle = 0;
        tmpData = 0;
    }

    /*if ((g_gps_data.angle == 1 ) || (tmpData > 0)) {
        OURAPP_trace("angle[%d]:mileage[%d]", g_gps_data.angle, g_gps_data.mileage);
    }*/
    g_gps_data.mileage  +=tmpData ;
#if GPS_ANGLE_UPLOAD > 0
    extern     void PROWZ2_putGpsBuf(GPS_DATA_T *gpsData);
    PROWZ2_putGpsBuf(&g_gps_data);
#endif
  
}
// 获取GPS未转换过的时间：三合一协议用
void DAL_getGpsSrcData(DATE_T *date, TIME_T *time)
{
    memcpy(date,&s_c_date,sizeof(s_c_date));
    memcpy(time,&s_c_time,sizeof(s_c_time));
}


/*******************************************************************
**  函数名:     HdlNMEA_GSA
**  函数描述:   GPGSA的程序处理. 判断2D/3D定位状态，及HDOP因子
**  参数:       
**  返回:       none
********************************************************************/
static void HdlNMEA_GSA(INT8U *ptr, INT16U len)
{
    INT8U pos;
    #if SIMPLE_FILTER > 0
    INT8U i;
    #endif

    pos = FindCharPos(ptr, COMMA, 1, len)+1;// fix status,2=2D,3=3D

    // OURAPP_trace("3D=%c%c%c%c", ptr[pos],ptr[pos+1],ptr[pos+2],ptr[pos+3]); //// add for test 201224
    if (ptr[pos] == '3') {
        if (s_fix3d_cnt < VALID_FIX3D_CNT) {    // 过滤前几个点
            s_fix3d_cnt++;
            #if SIMPLE_FILTER > 0
            s_lat_cnt = 0;
            s_long_cnt = 0;
            for (i = 0; i < VALID_LAT_LONG_CNT; i++) {
                s_lat[i] = 0;
                s_long[i] = 0;
            }
            #endif
        } else {
            #if SIMPLE_FILTER > 0
            if ((s_lat[0] != 0) && (s_lat[VALID_LAT_LONG_CNT - 1] != 0) && (s_long[0] != 0) && (s_long[VALID_LAT_LONG_CNT - 1] != 0)) 
            #endif
            {
                
                g_gps_data.is_3d = TRUE;
            }
        }
    } else {
        g_gps_data.is_3d = FALSE;
    }

/*#if GPS_DEBUG > 0
    OURAPP_trace("GSA:3D=%d", g_gps_data.is_3d);
#endif*/
//    pos = FindCharPos(ptr, COMMA, 16, len)+1;
//    pos2 = FindCharPos(ptr, COMMA, 17, len);
//    pos2 = pos2 - pos;
}

static const NMEA_TBL_T NMEA_TBL[] = {
                            {"GPRMC",       HdlNMEA_RMC},
                            {"GNRMC",       HdlNMEA_RMC},
                            {"BDRMC",       HdlNMEA_RMC},
                            {"GPGGA",       HdlNMEA_GGA},
                            {"BDGGA",       HdlNMEA_GGA},
                            {"GNGGA",       HdlNMEA_GGA},

                            {"GPGSA",       HdlNMEA_GSA},
                            // 201224del, 虽然没证据，但是去掉这一条，可能对3d定位有干扰的处理。 "BDGSA",       HdlNMEA_GSA},
                            {"GNGSA",       HdlNMEA_GSA},
                            {"GPGSV",       HdlNMEA_GSV},
                            {"GPTXT",       HdlNMEA_TXT},    
                            ////{"GPZDA",       HdlNMEA_ZDA},
                            ////{"GPVTG",       HdlNMEA_VTG},
                           };

/*******************************************************************
**  函数名:     HdlNMEA
**  函数描述:   收到完整一帧NMEA数据
**  参数:       none
**  返回:       none
********************************************************************/
static void HdlNMEA(void)
{
    INT8U i;
    
    if (NMEAIsValid(s_rbuf, s_rlen)) {
        //ResetChkCnt();  // feed the GPS's monitor
#if 0   // GPS_DEBUG > 0
        s_rbuf[s_rlen] = '\0';  // 字符串结束符，用于打印
        OURAPP_trace("->GPS:$%s", s_rbuf);
#endif
        // 解析NMEA数据
        for (i=0; i<sizeof(NMEA_TBL)/sizeof(NMEA_TBL[0]); i++) {
            if (SearchKeyWordFromHead(s_rbuf, s_rlen, NMEA_TBL[i].kword)) {
                NMEA_TBL[i].handler(s_rbuf, s_rlen);
                return;
            }
        }
    }
}

/*******************************************************************
**  函数名:     DAL_GPSRecvProc
**  函数描述:   串口收到字节的处理
**  参数:       [in] ch: 收到的字符
**  返回:       none
********************************************************************/
void DAL_GPSRecvProc(INT8U ch)
{
#if 1
    g_gps_data.gps_err = 1;
    switch (s_ptype)
    {
    case PTYPE_NONE:
        if (ch == '$') {        // 当前未处理任何协议
            s_ptype = PTYPE_NMEA;
            s_rlen  = 0;
        }/* else if (ch == SYNC_CHAR_1) {
            s_ptype = PTYPE_UBX_1;
        } */
        break;
    case PTYPE_NMEA:            // 当前处理的是NMEA
        if (s_rlen < MAX_RECV) {
            s_rbuf[s_rlen++] = ch;
            if (ch != 0x0a) break;
            // 到这里说明收齐一帧NMEA数据
            HdlNMEA();
        }
        // 长度异常或收齐一帧NMEA则到这里
        s_ptype = PTYPE_NONE;
        break;
    /*case PTYPE_UBX_1:           // 已收到UBX开头
        if (ch == SYNC_CHAR_2) {
            s_ptype = PTYPE_UBX;
            s_rlen  = 0;
            s_ubxlen= 0;
        } else {
            s_ptype = PTYPE_NONE;
        }
        break;
    case PTYPE_UBX:             // 当前处理的是UBX数据
        if (s_rlen < MAX_RECV) {
            s_rbuf[s_rlen++] = ch;
            if (s_rlen == 4) {                  // 已收了4个字节
                s_ubxlen = s_rbuf[3];
                s_ubxlen = (s_ubxlen << 8) + s_rbuf[2];
                if (s_ubxlen > MAX_RECV - 4) {  // 长度字段出错
                    s_ptype = PTYPE_NONE;
                }
            } else if (s_rlen == s_ubxlen + 6) {// 收齐了
                HdlUBX();
                s_ptype = PTYPE_NONE;
            }
            break;
        }
        s_ptype = PTYPE_NONE;
        break;*/
    default:
        s_ptype = PTYPE_NONE;
        break;
    }
#else// fot test
    double aa  = sin(ch);    // for test
    if (aa > 1) {
        aa *= 2;
        HdlNMEA();
    } else {
        aa *= 5;
    }
#endif
}


/*******************************************************************
**  函数名:     DAL_GPSProtocolInit
**  函数描述:   NEO-6M协议处理初始化. 可反复调用
**  参数:       none
**  返回:       none
********************************************************************/
void DAL_GPSProtocolInit(void)
{
    s_ptype = PTYPE_NONE;
    s_rlen  = 0;

    s_fix_cnt = 0;
    s_fix3d_cnt = 0;// 150325
    g_gps_data.status_flag &= ~GPS_VALID;   //// 清成不定位
}

/*******************************************************************
**  函数名:     DAL_GPS_POS_IsOK
**  函数描述:   GPS定位否
**  参数:       none
**  返回:       TRUE表示正常，否则异常
********************************************************************/
BOOLEAN DAL_GPS_POS_IsOK(void)
{
    if (/*(s_status == _NORMAL) &&  201224 DEL, 可能状态有误*/
        (g_gps_data.status_flag & GPS_VALID)) 
        return TRUE;
    else 
        return FALSE;
}

/*******************************************************************
**  函数名:     DAL_GPS_POS_Is_3DFIX 150325新增
**  函数描述:   GPS是否3D定位
**  参数:       none
**  返回:       TRUE表示是
********************************************************************/
BOOLEAN DAL_GPS_POS_Is_3DFIX(void)
{
    if (g_gps_data.is_3d == TRUE) return TRUE;
    return FALSE;
}


BOOLEAN DAL_GPS_time_valid(void)
{
	return g_gps_data.t_valid;
}


#if SIMPLE_FILTER > 0
//冒泡排序算法
void bubbleSort(INT32U *arr, INT8U n) 
{
    INT8U i,j;
    INT32U temp;

    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - i - 1; j++) {
            //如果前面的数比后面大，进行交换
            if (arr[j] > arr[j + 1]) {
                temp = arr[j]; 
                arr[j] = arr[j + 1]; 
                arr[j + 1] = temp;
            }
        }
    }
}

INT32U get_lat_average_value(void)
{
    INT8U i, flag;

    flag = 0;
    
    for (i = 0; i < VALID_LAT_LONG_CNT; i++) {
        if (s_lat[i] == 0) {
            flag = 1;
        }
    }
    
    if (flag == 0) {
        bubbleSort(s_lat, VALID_LAT_LONG_CNT);
        
        return s_lat[1];
    } else {
        return g_gps_data.D_lat;
    }
}

INT32U get_long_average_value(void)
{
    INT8U i, flag;

    flag = 0;
    
    for (i = 0; i < VALID_LAT_LONG_CNT; i++) {
        if (s_long[i] == 0) {
            flag = 1;
        }
    }
    
    if (flag == 0) {
        bubbleSort(s_long, VALID_LAT_LONG_CNT);

        return s_long[1];
    } else {
        return g_gps_data.D_long;
    }
}
#else
INT32U get_lat_average_value(void)
{
    return g_gps_data.D_lat;
}
INT32U get_long_average_value(void)
{
    return g_gps_data.D_long;
}

#endif

