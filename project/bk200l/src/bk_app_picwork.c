
#include "bk_os.h"
#include "bk_our_tools.h"
#include "bk_our_list.h"
#include "bk_our_stream.h"
#include "bk_app_picwork.h"
#include "bk_app_picsend.h"
#include "bk_port_uart.h"
#include "bk_port_public.h"
#include "bk_port_hardware.h"
#include "bk_port_gprs.h"
#include "bk_dal_public.h"
#include "bk_dal_systime.h"
#include "bk_app_bkwork.h"
#if BL_EN > 0
#include "bk_dal_blind.h"
#endif
//static ql_task_t s_pic_task_ref;

//static BOOLEAN s_pictime_synced = FALSE;
static void _judge_sleep_mode1(void);

INT16U  pic_getUPdataSize(void);
void pic_readdata(INT16U ptr,INT8U *buf,INT16U len);
char* pic_getUpdataVer(void);

/*
********************************************************************************
* 定义模块变量
********************************************************************************
*/
// 标准重发配置，3秒重发，重发3次
#define PERIOD_NEEDACK      3, 3, NULL

#define PIC_ROUND_LEN       400
#define R_SIZE  64
#define S_SIZE 64


static struct {
    INT8U  rbuf[R_SIZE];
    INT16U len;
} s_recv_hdl;

static INT8U s_send_round[PIC_ROUND_LEN];
static INT8U s_recv_round[PIC_ROUND_LEN];

static UART_CFG_T s_pic_uart = {
    PIC_UART,
    9600,
    PIC_ROUND_LEN,
    PIC_ROUND_LEN,
    s_recv_round,
    s_send_round
};

HWORD_UNION             s_pic_life_time;        // 从pic开机到当前的时长。没啥用，就看看
static PIC_BASE_PARA_T  s_pic_base_para;        // pic基础状态
static STREAM_T         s_pwstrm;
 
static INT8U            s_pic_alm_status;       // 报警状态位.bit1=停车报警,bit0=拆卸报警
static BOOLEAN          s_pic_linked;           // pic连接状态



/* PIC 心跳探询 */
static INT8U heart_cnt = 0; // 160605 add, 怕dal死循环
static void hdl_pic_heart(INT8U* body, INT16U len)
{
    INT8U sbuf[10];
    
    UP_PARA_NEW_T *para;
// 160605 add，在pp还未稳定的时候，别发配置到pic
    if (!DAL_PP_Ready()) {
        if (++heart_cnt < 5) return;
        else heart_cnt = 10;
    }
// 160605 add end
    if (len >= 2) {
        s_pic_life_time.bytes.high = body[0];
        s_pic_life_time.bytes.low  = body[1];
    }

    PIC_LOG("PIC时=%d,s=%x,ALM=%x:%x", s_pic_life_time.hword, body[2], body[3], body[4]);
    //// 140715 APP_ReviseWorkSec(s_pic_life_time.hword);   // 校准系统的lifetime
    if (body[2] == 0x05) {          // 防拆报警，优先级最高
        PIC_LOG("防拆");
        s_pic_alm_status = 0x01;
    } else if (body[2] == 0x04) {   // 停车报警
        s_pic_alm_status = 0x02;
    } else if ((body[2] == 0x06) 
            || (body[2] == 0x07)) {   // 210112 开车报警，都视为时段开车报警
        PIC_LOG("开车");
    } else {
        s_pic_alm_status = 0x00;
    }
    s_pic_alm_status |= body[4];
    
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x01);
    OUR_WriteHWORD_Strm(&s_pwstrm, 0x00);   // 2个保留字段
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x01);    // chksum

    OUR_PIC_ListSend(0x01, s_pwstrm.StartPtr, s_pwstrm.Len, 0, 0, NULL);
    
    ////if (g_our_status.work_time == 0) {// 161114 获取开机次数换这种写法，防止超时
        Requset_PIC_WorkTime();
    ////}
    
    if (!s_pic_linked) {
        #if ALMFAIL_HDL_EN > 0  // 160605 add
        ALARM_HDL_T almHdl; // 160131 add
        #endif
        Requset_PIC_AlarmParaGet();
        
        // 150420 这里要把配置发给pic
        para = APP_GetUploadPara();
#if ALMFAIL_HDL_EN > 0  // 160605 add
        // 160131 add 检测alm hdl是否还存在, 在则替换周期参数，改到pic去
        DAL_PP_Read(ALARM_HDL_, (void*)&almHdl);
        
        if ((almHdl.cur < almHdl.cnt) && (almHdl.cnt <= 100) && 
            (almHdl.period < 721) && (almHdl.period > 4)) {
            SYSTIME_T tm;
            #if NO_RESTORE_SHT == 0 // 不去恢复默认参数, 连报警流程都不考虑
            para->mode = MODE_POS_GPS;// 这里同时也把系统的模式改了
            para->alm_type = 1;
            para->alm.HM[0] = 0;
            para->alm.HM[1] = almHdl.period/60;
            para->alm.HM[2] = almHdl.period%60;
            para->max_worktime.hword = 210;
            #endif
            if (IS_ALS_ALM & GetPicAlarmStatus()) {// 170614
                para->eint_en = 0x00;   // 禁止光敏防拆
            }
            DAL_GetSysTime(&tm);
            Requset_PIC_BaseSet(&tm, para->max_worktime.hword, para->eint_en);
            #if ALMHDL_DEBUG > 0
            OURAPP_trace("ALMHDL改周期:%d", almHdl.period);
            #endif
            #if MULTI_SHAKE_EN > 0  // 161221 add ,多时段的报警策略
            if (IS_EINT_ALM & GetPicAlarmStatus()) {
                MULTI_SHAKE_PARA_T shk;
                shk.mode = 0;
                Request_PIC_MultiShake(&shk);
            }
            #endif
        }// 160131 add end
#endif
#if MULTI_SHAKE_EN > 0 // 170614 理解:中心在处理almhdl的时候，会下发18帧关闭的。所以，这里这么放，也没问题
        {
            MULTI_SHAKE_PARA_T ssptr;
            DAL_PP_Read(MULTI_SHAKE_, (void*)&ssptr);
            // TODO: 调试用，默认就开启振动多时段监测
            /*ssptr.mode = 1;
            ssptr.shake[0].hour     = 3;
            ssptr.shake[0].minute   = 0;
            ssptr.shake[0].period   = 255;  // 3~24点*/
            //ssptr.shake[1].hour     = 17;
            //ssptr.shake[1].minute   = 0;
            //ssptr.shake[1].period   = 24;  // 2小时，到19点
            //ssptr.shake[2].hour     = 20;
            //ssptr.shake[2].minute   = 0;
            //ssptr.shake[2].period   = 48;  // 4小时，到24点
            //
            if (ssptr.mode <= MAX_SHAKE_CNT) {// 简单判断合法性
                Request_PIC_MultiShake(&ssptr);
            }
        }
#endif
        Requset_PIC_AlarmParaSet(para);//Requset_PIC_PeriodSet(&para->period_awake, para->max_worktime.hword);
    }
    // 放在这里做基础参数查询，不另开定时器了
    Requset_PIC_BaseGet();
}

/* 获取PIC 复位记录 
static void hdl_pic_get_rst_record(INT8U* body, INT16U len)
{
    // TODO: 待完善
    OUR_PIC_ListAck(0x02);
}*/

/* PIC 供电请求的应答 */
static void hdl_pic_tmpwork_ans(INT8U* body, INT16U len)
{
    PIC_LOG("PIC临供ack=%d", body[0]);

    OUR_PIC_ListAck(0x03);
}
/*void _ShutDown(void)
{
    // TODO: 这里执行关机操作
    //StopTimer(OUR_TIMER2);
    //srv_shutdown_normal_start(0);   // 140722 关机
}

 立即休眠请求的应答 */
static void hdl_pic_sleep_ans(void)
{
    PIC_LOG("PIC休眠");

    // TODO: 这里执行关机操作
    OUR_PIC_ListAck(0x04);
    BK_Poweroff();
}

/* 14H-获取PIC工作时长等参数 */
static void hdl_get_pic_worktime(INT8U* body, INT16U len)
{
    STREAM_T rstrm;
    OUR_InitStrm(&rstrm, body, len);
    g_our_status.work_time  = OUR_ReadLONG_Strm(&rstrm);
    g_our_status.car_on_time= OUR_ReadLONG_Strm(&rstrm);
    g_our_status.start_cnt  = OUR_ReadHWORD_Strm(&rstrm);

    OUR_PIC_ListAck(0x14);
}
/* 21H-获取基础参数 */
static void hdl_get_base_para_ans(INT8U* body, INT16U len)
{
    STREAM_T rstrm;
    
    OUR_InitStrm(&rstrm, body, len);
    OUR_ReadDATA_Strm(&rstrm, &s_pic_base_para.cur_time.date.year, 7);

    s_pic_base_para.max_worktime = OUR_ReadHWORD_Strm(&rstrm);
    s_pic_base_para.work_type    = OUR_ReadBYTE_Strm(&rstrm);
    s_pic_base_para.rest_time    = OUR_ReadHWORD_Strm(&rstrm);
    s_pic_base_para.eint_mask    = OUR_ReadBYTE_Strm(&rstrm);
    OUR_ReadLONG_Strm(&rstrm);
    OUR_ReadHWORD_Strm(&rstrm);
    memset(s_pic_base_para.ver, 0, sizeof (s_pic_base_para.ver));
    OUR_ReadDATA_Strm(&rstrm,s_pic_base_para.ver,sizeof(s_pic_base_para.ver));  // 一般pic后面版本号是没那么长的
    
    // PIC_LOG("-pic基础参数查询-");
    PIC_LOG("PIC_time:%d-%d-%d,%02d:%02d,%s", s_pic_base_para.cur_time.date.year,
        s_pic_base_para.cur_time.date.month, s_pic_base_para.cur_time.date.day, s_pic_base_para.cur_time.time.hour, 
        s_pic_base_para.cur_time.time.minute, s_pic_base_para.ver);
////    PIC_LOG("-max_worktime:%d", s_pic_base_para.max_worktime);
////    PIC_LOG("-work_type:%d-%s", s_pic_base_para.work_type, (s_pic_base_para.work_type == 1) ? "正常":"临时");
////    PIC_LOG("-rest_sec:%d", s_pic_base_para.rest_time);
////    PIC_LOG("-alm msk=%x", s_pic_base_para.eint_mask);
////    PIC_LOG("PIC-V:%s", s_pic_base_para.ver);
    if (!s_pic_linked) {
        s_pic_linked = true;

        DAL_SetSysTime(&s_pic_base_para.cur_time, TRUE);
    /*} else {
        DAL_SetSysTime(&s_pic_base_para.cur_time, false);   */
    }
    OUR_PIC_ListAck(0x21);
}

/* 22H-设置PIC基础参数 */
static void hdl_set_base_para_ans(INT8U* body, INT16U len)
{
    PIC_LOG("PIC基参set=%d", body[0]);

    //s_pictime_synced = TRUE;
    
    OUR_PIC_ListAck(0x22);
    Requset_PIC_BaseGet();  // 230606 add, 用于确保版本号获得
    _judge_sleep_mode1();
}

/* 23H-获取闹铃参数 */
static void hdl_get_alarm_para_ans(INT8U* body, INT16U len)
{
    UP_PARA_NEW_T up;
    
    PIC_LOG("-DHM=%x,%x,%x\r\n,HM5:", body[0], body[1], body[2]);
    BK_LOG_H(&body[3], 10);
    PIC_LOG("-WHM=%x,%x,%x", body[13], body[14], body[15]);

    OUR_PIC_ListAck(0x23);
#if ALMFAIL_HDL_EN == 0// 161225: 只能针对无报警处理的来搞，不然会出现报警处理的周期被改为默认值
    // 161114 这里比较pic和public，不一致再用public覆盖，同时判断一下public有效性
    DAL_PP_Read(UPLOAD_PARA_, (void*)&up);
    if (memcmp(body, up.alm.HM, 3)) {
        Requset_PIC_AlarmParaSet(&up);
    }
    // 161114 end
#endif
}

/* 24H-设置PIC闹铃参数 */
static void hdl_set_alarm_para_ans(INT8U* body, INT16U len)
{
    PIC_LOG("闹铃设置ack:%d", body[0]);

    OUR_PIC_ListAck(0x24);
}

#if MULTI_SHAKE_EN > 0
/* 25H-多时段震动监测应答 */
static void hdl_set_25_ans(INT8U* body, INT16U len)
{
    PIC_LOG("pic多震动设置=%d", body[0]);

    OUR_PIC_ListAck(0x25);
}
#endif


#if OUR_DEBUG > 0
static BOOLEAN s_test_en = FALSE;
static void hdl_pic_dbg_off(void)
{
    //OURAPP_trace("-debug-off-");
    OURAPP_InitTrace(0);
    s_test_en = 0;
}

static void hdl_pic_dbg_on(void)
{
    OURAPP_InitTrace(1);
    OURAPP_trace("[调试开]\r\n%s", PORT_GetVersion());
    PORT_UartWriteBlock(PIC_UART, "[调试开]\r\n\0", 11);//// 给合宙用的
    s_test_en = 1;
}

//#include "dal_public.h"
static void hdl_test1(void)
{
    if (g_flag_dbg) {
        OURAPP_trace("[测试关机]");
        BK_Poweroff();
    }
}

static void hdl_test2(void)
{
    if (g_flag_dbg) {
        OURAPP_trace("[恢复默认并关机]");
        DAL_PP_RestoreDefault(TRUE);
        #if BL_EN > 0
        DAL_BlindReset();
        #endif
        BK_Poweroff();
    }
}

static void hdl_changeSN(INT8U* body, INT16U len)
{
    if (g_flag_dbg) {
        if (PORT_SetIMEI(body, len)) {
            OURAPP_trace("[SN存OK]");
            PORT_UartWriteBlock(PIC_UART, "[SN存OK]\r\n\0", 11);//// 给合宙用的
        } else {
            OURAPP_trace("[SN存错]");
            PORT_UartWriteBlock(PIC_UART, "[SN存错]\r\n\0", 11);//// 给合宙用的
        }
    }
}


static void hdl_getSN(void)
{
    if (g_flag_dbg) {
        char sn[16];
        INT8U* ptr = PORT_GetIMEI();
        if (ptr) {
            memcpy(sn, ptr, 15);
            sn[15] = '\0';
            OURAPP_trace("[SN=%s]", sn);
            PORT_UartWriteBlock(PIC_UART, "[SN=", 4);
            PORT_UartWriteBlock(PIC_UART, sn, 15);
            PORT_UartWriteBlock(PIC_UART, "]\r\n\0", 4);
        } else {
            OURAPP_trace("[SN=NULL]");
        }
    }
}
#endif
#if (BL_EN > 0) && (BL_DEBUG > 0)

extern BOOLEAN g_fake_fail;
static void hdl_bl(void)
{
    INT8U* ptr;
    INT8U len = 0;
    if (g_flag_dbg) {
        OURAPP_trace("BL强制");
        g_fake_fail = TRUE;
        ptr = DAL_BlindGetData(&len);
        if (ptr && len) {
            OURAPP_printhexbuf(ptr, len);
        } else {
            OURAPP_trace("空BL");
        }
    }
}

#endif
/*
extern sMsgQRef s_sms_msgQ;
BOOLEAN _sms_read_parse(char* sptr);
static void hdl_sms_tes(void)
{
    SC_SMSReturnCode ret;
    SIM_MSG_T msg = {0};
    SC_STATUS rret;
    
    ret = sAPI_SmsReadMsg(1, 17, s_sms_msgQ);    // 写死1,text mode
    OURAPP_trace("读短信=%d", ret);
    if(ret == SC_SMS_SUCESS) {
        memset(&msg,0,sizeof(msg));
        rret = sAPI_MsgQRecv(s_sms_msgQ, &msg, 1000);
        OURAPP_trace("SMSR=%d,%s", rret, (char*)msg.arg3);

        if (rret == SC_SUCCESS) 
            _sms_read_parse((char*)msg.arg3);
        
        sAPI_Free(msg.arg3);
    } else {
        OURAPP_trace("SMSR败%d", ret);
    }
}
*/
///////////////////////////pic升级START/////////////////////////////////
static INT8U s_updataTotalPack;  // 总包数  
static INT16U s_updataOnePackSize; // 一包大小
static INT8U  s_updataSer;  // 包序列号
#define PIC_UPDATA_VER  pic_getUpdataVer()
#define PIC_UPDATA_SIZE   pic_getUPdataSize()   // 升级包大小


// pic OTA数据包
void Request_PIC_OTAData(void)
{
    INT8U *sbuf = NULL;
    INT16U sendLen = 0;
    INT16U readPtr = 0;

    readPtr = s_updataSer * s_updataOnePackSize;
    sendLen = s_updataOnePackSize;
    if ((readPtr + s_updataOnePackSize) > PIC_UPDATA_SIZE) {
        sendLen = PIC_UPDATA_SIZE - readPtr;
    }
    if (sendLen > s_updataOnePackSize) {
        BK_LOG("POTA l-err:%d", sendLen);
        return;
    }
    sbuf = BK_MEM_Alloc(sendLen + 20);
    BK_ASSERT(sbuf);

    PIC_LOG("POTA包%d,%d",s_updataSer, sendLen);
    OUR_InitStrm(&s_pwstrm, sbuf, sendLen + 20);  
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x32);
    OUR_WriteBYTE_Strm(&s_pwstrm, s_updataTotalPack);
    OUR_WriteBYTE_Strm(&s_pwstrm, s_updataSer);
    pic_readdata(readPtr,s_pwstrm.CurPtr,sendLen);
    OUR_MovStrmPtr(&s_pwstrm,sendLen);
    
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));

    OUR_PIC_ListSend(0x32, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
    // OURAPP_trace("s_pwstrm.len[%d]",s_pwstrm.Len);
    
    BK_MEM_Free(sbuf);
}
// 升级pic程序请求
void Request_PIC_OTAReq(void)
{
    INT8U sbuf[30] = {0};
    
    PIC_LOG("P升级REQ:%s",PIC_UPDATA_VER);
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));  
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x31);
    OUR_WriteHWORD_Strm(&s_pwstrm, PIC_UPDATA_SIZE);
    OUR_WriteSTR_Strm(&s_pwstrm, PIC_UPDATA_VER);
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));
    OUR_PIC_ListSend(0x31, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

// 7e 31 01 00 ff cc 7e
// pic OTA请求应答处理
void hdl_pic_OTAReq(INT8U* body, INT16U len)
{
    INT16U size;
    if (!body || len < 3 || len > 8) {
        goto _ERR_OTA_REQ;
    }
    if (body[0] != 0x01) {
        PIC_LOG("POTA-ACK:%d",body[0]);
        OUR_PIC_ListAck(0x31);
        if (body[0] == 2 || body[0] == 3) {
_ERR_OTA_REQ:
            Request_PIC_Sleep(0);   // 要确保pic OTA请求是最后调用的
        }
        return;
    }
    // 启动升级
    s_updataSer = 0;
    s_updataOnePackSize = (body[1]<<8) | body[2];
    if (s_updataOnePackSize > 300 || s_updataOnePackSize < 128) {   // 容错
        goto _ERR_OTA_REQ;
    }
    size = PIC_UPDATA_SIZE;
    s_updataTotalPack = size / s_updataOnePackSize;
    if (size % s_updataOnePackSize) s_updataTotalPack += 1;
    
    PIC_LOG("POTA go!%d,共%d包", s_updataOnePackSize, s_updataTotalPack);
    
    Request_PIC_OTAData();
    OUR_PIC_ListAck(0x31);
}
// 7e 32 00 01 cc 7e
// 7e 32 01 01 cc 7e
// 7e 32 02 01 cc 7e

// OTA数据绝收应答
void hdl_pic_OTAData(INT8U* body, INT16U len)
{
    OUR_PIC_ListAck(0x32);
    if (body[1] != 0x01) {
        BK_LOG("POTA ERR");   
        return;
    }

    // if (body[0] < s_updataSer) return; //重传的包应答
    s_updataSer = body[0] + 1;  // 直接重应答的包序号开始处理

    Request_PIC_OTAData();
}


///////////////////////////pic升级 END /////////////////////////////////

static const FUNC_ENTRY_T pic_entry[] = {
    {0x01,      NULL,                   hdl_pic_heart},                 /* PIC 心跳探询 */
    {0x03,      NULL,                   hdl_pic_tmpwork_ans},           /* PIC 供电请求的应答 */
    {0x04,      hdl_pic_sleep_ans,      NULL},                          /* 立即休眠请求的应答 */
//    {0x02,      NULL,                   hdl_pic_get_rst_record},        /* 获取PIC 复位记录 */
    //{0x11,      NULL,                   hdl_pic_query_ans},             /* 查询PIC参数的应答 */
    //{0x12,      NULL,                   hdl_set_pictime_ans},           /* 设置PIC时间的应答 */
    //{0x13,      NULL,                   hdl_set_pic_period_ans},        /* 13H-设置PIC休眠/工作周期 */
    {0x14,      NULL,                   hdl_get_pic_worktime},          /* 14H-获取工作时长等141116 */
    {0x21,      NULL,                   hdl_get_base_para_ans},         /* 21H-获取基础参数 */
    {0x22,      NULL,                   hdl_set_base_para_ans},         /* 22H-设置PIC基础参数 */
    {0x23,      NULL,                   hdl_get_alarm_para_ans},        /* 23H-获取闹铃参数 */
    {0x24,      NULL,                   hdl_set_alarm_para_ans},        /* 24H-设置PIC闹铃参数 */
#if MULTI_SHAKE_EN > 0
    {0x25,      NULL,                   hdl_set_25_ans},
#endif
    {0x31,      NULL,                   hdl_pic_OTAReq},                /* PIC升级请求 */  
    {0x32,      NULL,                   hdl_pic_OTAData},               /* PIC升级数据包发送 */  
        
#if OUR_DEBUG > 0
    {0xaa,      hdl_pic_dbg_off,        NULL},                /*PIC 关闭调试*/
    {0x55,      hdl_pic_dbg_on,         NULL},                /*PIC 打开调试*/
    {0x50,      hdl_test1,              NULL},
    {0x51,      hdl_test2,              NULL},
    {0x52,      NULL,                   hdl_changeSN},          // 烧写SN码
    {0x53,      hdl_getSN,              NULL},                  // 读取SN码
    #if (BL_EN > 0) && (BL_DEBUG > 0)
    {0x54,      hdl_bl, NULL},
    #endif
#endif
};


static void _hdl_frame(INT8U cmd, INT8U* body, INT16U len)
{
    FindProc_Entry(cmd, pic_entry, sizeof(pic_entry)/sizeof(pic_entry[0]), body, len);
}

static void _pic_recv_proc(void)
{
    INT16S ch;
    INT8U len;

    while ((ch = PORT_UartRead(s_pic_uart.port)) != -1) {          
        if (ch == 0x7e) {
            if (!s_recv_hdl.len) {  // 长度为0说明还未收到过头
                s_recv_hdl.rbuf[0] = ch;
                s_recv_hdl.len = 1;
            } else {                // 说明是帧尾,一帧数据收齐
                s_recv_hdl.rbuf[s_recv_hdl.len++] = ch;
                len = Deassemble(s_recv_hdl.rbuf, s_recv_hdl.rbuf + 1, s_recv_hdl.len-2, R_SIZE);
                if (len < 2) {// 说明解析有误
                    PIC_LOG("PIC err:%02x",len);
                    goto _RECV_7E_ERR;//_RECVBUF_RST;
                }
               // if (1 != g_flag_dbg) {// 180410 调试打开的时候，不去计算校验和
                    if (s_recv_hdl.rbuf[len-1] != ChkSum_XOR(&s_recv_hdl.rbuf[0], len - 1)) {
                        #if  0 //(OUR_DEBUG > 0) && (PIC_DEBUG > 0)
                        OURAPP_trace("Get PIC data crc err:%02x",ChkSum_XOR(&s_recv_hdl.rbuf[0], len - 1));
                        OURAPP_printhexbuf(&s_recv_hdl.rbuf[1], len - 2);
                        #endif
                        goto _RECV_7E_ERR;//_RECVBUF_RST;
                    }
                //}
                #if PIC_DEBUG > 0
                PIC_LOG("PIC get:%02x",s_recv_hdl.rbuf[0]);
                OURAPP_printhexbuf(&s_recv_hdl.rbuf[1], len - 2);
                #endif
                _hdl_frame(s_recv_hdl.rbuf[0], &s_recv_hdl.rbuf[1], len - 2);
                goto _RECVBUF_RST;                
            }
        } else {
            if (s_recv_hdl.len) {  // 这说明是数据
                s_recv_hdl.rbuf[s_recv_hdl.len++] = ch;
                if (s_recv_hdl.len == R_SIZE) {// 缓冲区满，则把收到的数据舍弃
                    goto _RECVBUF_RST;
                }
                // 到这里则说明是垃圾数据，过滤
            }
        }
        continue;
_RECV_7E_ERR:   // 已经收到所谓2个7E，但不是想要的数据，因此要认为上一个收到的7E不是尾而是头
        s_recv_hdl.len = 1;
        continue;
_RECVBUF_RST:
        s_recv_hdl.len = 0;
    }
}

/*****************************************************************************
 函 数 名  : Request_PIC_TmpWork
 功能描述  : 临时供电请求
 输入参数  : INT16U t  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年5月27日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void Request_PIC_TmpWork(INT16U t)
{
    INT8U sbuf[10];
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x03);
    OUR_WriteHWORD_Strm(&s_pwstrm, t);
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));

    OUR_PIC_ListSend(0x03, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

// 大时间减小时间的分钟差
static INT32U _get_sub_minute(SYSTIME_T* big, SYSTIME_T* sml)
{
    INT16U nouse;
    INT32U secBig, secSmall;
    INT32U result;
    
    OUR_GPS_UtcToWeekAndTow(&big->date, &big->time, &nouse, &secBig);
    OUR_GPS_UtcToWeekAndTow(&sml->date, &sml->time, &nouse, &secSmall);
    if (secBig < secSmall) {// 240402 add, 兼容性写法，防止大小不对。
        result = secBig;
        secBig  = secSmall;
        secSmall = result;
    }
    
    result = (secBig - secSmall) / 60;
    if (((secBig - secSmall) % 60) > 30) result++;// 四舍五入
    return result;
}
#if 0
static INT16U _calibrate_sleep(INT16U t)
{
    INT32U workMin;
    INT32U oldMin;  // 唤醒所需的分钟数
    INT32U actMin;  // 距离上一次唤醒所过去的分钟数
    INT32S tmpMin;  // 差额
    SYSTIME_T tm;
    CALI_PARA_T  cali = {{15, 1, 1}, {0, 0, 0}, 0};
    UP_PARA_NEW_T para;

    if (t > 5) return t;// 一次性时间，无需计算补时
    if (!DAL_PP_Read(UPLOAD_PARA_, (void*)&para)) return t;
    if (para.alm_type != 1) return t;       // 非间隔模式就不补偿了
    if (!DAL_PP_Read(CALI_MINUTE_, (void*)&cali)) return t;
    if (!DAL_GetSysTime(&tm)) return t;
    if (!DateValid(&tm.date) || !TimeValid(&tm.time)) {
        return t; 
    }
    PIC_LOG("tm=%d/%d/%d %d:%d:%d", tm.date.year,tm.date.month,tm.date.day,
    tm.time.hour,tm.time.minute,tm.time.second);
    PIC_LOG("RDcali:%d-%d-%d %d:%d:%d %d", 
    cali.last_tm.date.year, cali.last_tm.date.month, cali.last_tm.date.day, 
    cali.last_tm.time.hour, cali.last_tm.time.minute, cali.last_tm.time.second, cali.cali_minute);
    
    if (cali.cali_minute > 0xffffff) cali.cali_minute = 0;  // 容错，防止读出来全FF的异常情况
// 计算补时时间, 单位是分钟
    tmpMin = app_get_WorkSec();//借用变量
    if (tmpMin > 240) {
        workMin = 4;
    } else {
        workMin = tmpMin / 60;
        if ((tmpMin % 60) > 30) workMin++; // 四舍五入
    }
    workMin = 0;    // TODO: 感觉不需要把工作时间补偿进去，去掉看看
// step2：根据休眠周期，计算下一个唤醒的时间点
    if (!DateValid(&cali.last_tm.date) || !TimeValid(&cali.last_tm.time)) {// 存储的时间无效，则不补偿，直接更新
        t = 0;
        cali.cali_minute = 0;
        goto _UPDATE_DATE;
    }
    //if (para.alm_type != 1) {// 经典闹铃模式
    if (para.alm.HM[0]==0) {   // 短间隔模式
        oldMin = para.alm.HM[1]*60+para.alm.HM[2]; // 休眠唤醒的间隔
        if (oldMin < 60) return t;  // 间隔低于60分钟的，不补偿

        if (memcmp(&cali.last_tm.time, &tm.time, 2) >= 0) {// 比对小时和分钟
            actMin = (tm.time.hour+24)*60+tm.time.minute - cali.last_tm.time.hour*60 - cali.last_tm.time.minute;
        } else {
            actMin = tm.time.hour*60+tm.time.minute - cali.last_tm.time.hour*60 - cali.last_tm.time.minute;
        }
    } else {
        if (memcmp(&tm.time, &para.alm.HM[1], 2) >= 0) {// 当前时间点晚于需要唤醒的时间点
            oldMin = (para.alm.HM[1]+para.alm.HM[0]*24)*60+para.alm.HM[2] - 
                tm.time.hour*60 - tm.time.minute;
        } else {
            oldMin = (para.alm.HM[1]+(para.alm.HM[0]-1)*24)*60+para.alm.HM[2] - 
                tm.time.hour*60 - tm.time.minute;
        }
        // 上个时间点到当前所度过的分钟数
        
        if (memcmp(&cali.last_tm, &tm, 5) >= 0) {// 容错，上一次时间不应大于当前时间
            t = oldMin - workMin;
            cali.cali_minute = 0;
            goto _UPDATE_DATE;
        } else {
            actMin = _get_sub_minute(&tm, &cali.last_tm);
        }
    }
    //}
    tmpMin = actMin - oldMin;
    PIC_LOG("到这里old=%d act=%d min=%d", oldMin, actMin, workMin);
    if ((abs(tmpMin) > oldMin / 2) || (abs(tmpMin)> 80)) {// 容错，如果误差太大则不校时了
        t = oldMin - workMin;
        cali.cali_minute = 0;
        goto _UPDATE_DATE;
    }
    // tmpMin > 0 说明 偏慢，需要减去时间
    //cali.cali_minute = cali.cali_minute - tmpMin / 2;  // 折半补偿
    cali.cali_minute = cali.cali_minute - tmpMin;
    t = oldMin - workMin + cali.cali_minute;
    PIC_LOG("cali=%d, t=%d", cali.cali_minute, t);
_UPDATE_DATE:
    memcpy(&cali.last_tm, &tm, sizeof(SYSTIME_T));
    DAL_PP_Store(CALI_MINUTE_, (void*)&cali);
    
    if (t > 1440*5) t = 0;  // 最后一步来容错，时间太离谱，就不发了
    PIC_LOG("要睡%d分", t);
    return t;
}
#else

static BOOLEAN s_realtime_syncd = FALSE;    // 网络或GPS已校时

// 当网络或GPS能校时时，调用此接口
void app_set_pic_time(void)
{
    SYSTIME_T tm;
    UP_PARA_NEW_T *para;
    
    if (!DAL_GetSysTime(&tm)) return;
    if (!DateValid(&tm.date) || !TimeValid(&tm.time)) {
        return; 
    }
    s_realtime_syncd = TRUE;
    //s_pictime_synced = FALSE;   // 强制一下，等应答收到置TRUE

    BK_LOG("时间OK 校时P");

    para = APP_GetUploadPara();

    Requset_PIC_BaseSet(&tm, para->max_worktime.hword, para->eint_en);
}

// 在模式1下判断是否需要睡眠。即是否是被提前唤醒的
static void _judge_sleep_mode1(void)
{
    SYSTIME_T tm;       // 当前时间
    SYSTIME_T planTm;   // 计划闹铃
    UP_PARA_NEW_T para;
    INT32U subMin;

    if (!s_realtime_syncd/* || !s_pictime_synced*/) return;
    if (APP_IsInTrack()) return;

    if (!DAL_GetSysTime(&tm)) return;
    if (!DateValid(&tm.date) || !TimeValid(&tm.time)) {
        BK_LOG("时err退1");
        return; 
    }

    if (!DAL_PP_Read(UPLOAD_PARA_, (void*)&para)) return;
    if (para.alm_type != 1) return;
    if (para.alm.HM[0]!= 1) return;   // 只针对1天的闹铃模式, 其他则无需判断
    // 本判断只针对模式1、闹铃模式去判断。本质上就是判断当前是否正常周期的唤醒，如果是则返回，不是则直接睡眠
    
    if (g_our_status.work_time <= para.max_worktime.hword) {
        PIC_LOG("刚开机,不判断%d", g_our_status.work_time);
        return;
    }
    
    planTm.date.year = tm.date.year;
    planTm.date.month= tm.date.month;
    planTm.date.day  = tm.date.day;
    planTm.time.hour = para.alm.HM[1];
    planTm.time.minute = para.alm.HM[2];
    planTm.time.second = tm.time.second;
    subMin = _get_sub_minute(&tm, &planTm);
    if (subMin > 10) {
        Request_PIC_Sleep(0);
        return;
    }
    
}

static INT16U _calibrate_sleep(INT16U t)
{
    INT32U workMin;
    INT32U oldMin;  // 唤醒所需的分钟数
    INT32U actMin;  // 距离上一次唤醒所过去的分钟数
    INT32S tmpMin;  // 差额
    SYSTIME_T tm;   // 当前时间
    CALI_PARA_T  cali = {{15, 1, 1}, {0, 0, 0}, 0};
    UP_PARA_NEW_T para;

    if (t >= 5) return t;// 一次性时间，无需计算补时
    if (!DAL_PP_Read(UPLOAD_PARA_, (void*)&para)) return t;

    if (!DAL_PP_Read(CALI_MINUTE_, (void*)&cali)) return t;
    if (!DAL_GetSysTime(&tm)) return t;
    if (!DateValid(&tm.date) || !TimeValid(&tm.time)) {
        return t; 
    }
    PIC_LOG("tm=%d/%d/%d %d:%d:%d", tm.date.year,tm.date.month,tm.date.day,
    tm.time.hour,tm.time.minute,tm.time.second);
    PIC_LOG("RDcali:%d-%d-%d %d:%d:%d %d", 
    cali.last_tm.date.year, cali.last_tm.date.month, cali.last_tm.date.day, 
    cali.last_tm.time.hour, cali.last_tm.time.minute, cali.last_tm.time.second, cali.cali_minute);
    if (g_our_status.work_time <= para.max_worktime.hword) {
        PIC_LOG("刚开机,先mcu估算%d", g_our_status.work_time);
        cali.cali_minute = 0;
        goto _UPDATE_DATE;
    }
    if (cali.cali_minute > 0xffffff) cali.cali_minute = 0;  // 容错，防止读出来全FF的异常情况
// 计算补时时间, 单位是分钟
    tmpMin = app_get_WorkSec();//借用变量
    if (tmpMin > 240) {
        workMin = 4;
    } else {
        workMin = tmpMin / 60;
        if ((tmpMin % 60) > 30) workMin++; // 四舍五入
    }
    // workMin = 0;    // TODO: 感觉不需要把工作时间补偿进去，去掉看看
// step2：根据休眠周期，计算下一个唤醒的时间点
    if (!DateValid(&cali.last_tm.date) || !TimeValid(&cali.last_tm.time)) {// 存储的时间无效，则不补偿，直接更新
        t = 0;
        cali.cali_minute = 0;
        goto _UPDATE_DATE;
    }
    switch (para.alm_type) {
    case 1:// 经典闹铃模式
        if (para.alm.HM[0]==0) {   // 短间隔模式
            oldMin = para.alm.HM[1]*60+para.alm.HM[2]; // 休眠唤醒的间隔
            if (oldMin < 60) return t;  // 间隔低于60分钟的，不补偿

            if (memcmp(&cali.last_tm.time, &tm.time, 2) >= 0) {// 比对小时和分钟
                actMin = (tm.time.hour+24)*60+tm.time.minute - cali.last_tm.time.hour*60 - cali.last_tm.time.minute;
            } else {
                actMin = tm.time.hour*60+tm.time.minute - cali.last_tm.time.hour*60 - cali.last_tm.time.minute;
            }
        } else {
        #if 0
            if (memcmp(&tm.time, &para.alm.HM[1], 2) >= 0) {// 当前时间点晚于需要唤醒的时间点
                oldMin = (para.alm.HM[1]+para.alm.HM[0]*24)*60+para.alm.HM[2] - 
                    tm.time.hour*60 - tm.time.minute;
            } else {
                oldMin = (para.alm.HM[1]+(para.alm.HM[0]-1)*24)*60+para.alm.HM[2] - 
                    tm.time.hour*60 - tm.time.minute;
            }
            // 上个时间点到当前所度过的分钟数
            if (memcmp(&cali.last_tm, &tm, 5) >= 0) {// 容错，上一次时间不应大于当前时间
                t = 0;//// 240314 容错就不计算了 oldMin - workMin;
                cali.cali_minute = 0;
                goto _UPDATE_DATE;
            } else {
                actMin = _get_sub_minute(&tm, &cali.last_tm);
            }
        #else// 换个算法，每n天的闹铃模式,按理都是1440h的倍数，所以是当前时间和上一个时间点的对比
            /*INT32U minuteA, minuteB;
            SYSTIME_T wakeTm;
            oldMin = para.alm.HM[0]*1440;
            // step1,先根据上次睡眠时间记录算出来时间A，
            if (memcmp(&cali.last_tm, &tm, 5) >= 0) {// 容错，上一次时间不应大于当前时间
                t = 0;//// 240314 容错就不计算了 oldMin - workMin;
                cali.cali_minute = 0;
                goto _UPDATE_DATE;
            } else {
                minuteA = _get_sub_minute(&tm, &cali.last_tm);
            }
            // step2,再用当前时间减配置间隔算出时间B，B有个缺陷，不知道间隔了几天，就按1天算
            minuteB = tm.time.hour*60 + tm.time.minute + 1440 - para.alm.HM[1]*60-para.alm.HM[2];
            // step3,取最大值
            if (minuteA > minuteB) {
                actMin = minuteA;
            } else {
                actMin = minuteB;
            }
            PIC_LOG("A=%d B=%d 实际%d", minuteA, minuteB, actMin);*/
            if (para.alm.HM[0]==1) {
                t = 0;//// 240314 容错就不计算了 oldMin - workMin;
                cali.cali_minute = 0;
                goto _UPDATE_DATE;
            }
            // step1,先根据上次睡眠时间记录算出来时间A，
            if (memcmp(&cali.last_tm, &tm, 5) >= 0) {// 容错，上一次时间不应大于当前时间
                t = 0;//// 240314 容错就不计算了 oldMin - workMin;
                cali.cali_minute = 0;
                goto _UPDATE_DATE;
            }
            oldMin = para.alm.HM[0]*1440;
            actMin = tm.time.hour*60 + tm.time.minute + oldMin - para.alm.HM[1]*60-para.alm.HM[2];
        #endif
        }
        break;
    case 2:// 每天时段模式
    default:
        return 0;
    }
    tmpMin = actMin - oldMin;   // 此次需要补偿的分钟数，正数表示延后，负数表示提前
    PIC_LOG("本应=%d 实际=%d 误差%d 工作%d 原=%d", oldMin, actMin, tmpMin, workMin, cali.cali_minute);
    if ((abs(tmpMin) > oldMin / 3) || (abs(tmpMin) > 120)) {// 容错，如果误差太大则不校时了
        t = 0;  ////oldMin - workMin; 算不准就依赖MCU的近似估算
        cali.cali_minute = 0;
        goto _UPDATE_DATE;
    }
    // tmpMin > 0 说明 偏慢，需要减去时间
    //cali.cali_minute = cali.cali_minute - tmpMin / 2;  // 折半补偿
    cali.cali_minute = cali.cali_minute - tmpMin; // 
    t = oldMin - workMin + cali.cali_minute;
    PIC_LOG("cali=%d,t=%d",cali.cali_minute, t);
_UPDATE_DATE:
    memcpy(&cali.last_tm, &tm, sizeof(SYSTIME_T));
    DAL_PP_Store(CALI_MINUTE_, (void*)&cali);
    
    if (t > 1440*7) t = 0;  // 最后一步来容错，时间太离谱，就不发了
    PIC_LOG("要睡%d分", t);
    return t;
}
#endif
/*****************************************************************************
 函 数 名  : Request_PIC_Sleep
 功能描述  : 立即休眠请求
 输入参数  : INT16U t  t:单位min  如果t > 4,则改变休眠周期,这时候补时要自己计算
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年5月27日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void Request_PIC_Sleep(INT16U t)
{
    INT8U sbuf[10];

    t = _calibrate_sleep(t);
    PIC_LOG("休眠[%d]",t);
    ////t = 6;
    ////PIC_LOG("测试[%d]",t);
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x04);
    OUR_WriteHWORD_Strm(&s_pwstrm, t);
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));

    OUR_PIC_ListSend(0x04, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
    PORT_GPRS_Suspend();    // TODO: 待确认，这样会不会影响串口发送
}

/*****************************************************************************
 函 数 名  : Requset_PIC_Para
 功能描述  : 读取pic参数请求
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年5月27日
    作    者   : rocky
    修改内容   : 新生成函数

****************************************************************************
void Requset_PIC_Para(void)
{
    OUR_InitStrm(&s_pwstrm, s_sbuf, S_SIZE);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x11);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x11);

    OUR_PIC_ListSend(0x11, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}
*/
/*****************************************************************************
 函 数 名  : Requset_PIC_TimeSet
 功能描述  : 设置时间请求
 输入参数  : TIME_T* t  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年5月27日
    作    者   : rocky
    修改内容   : 新生成函数

****************************************************************************
void Requset_PIC_TimeSet(TIME_T* t)
{
    OUR_InitStrm(&s_pwstrm, s_sbuf, S_SIZE);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x12);
    OUR_WriteBYTE_Strm(&s_pwstrm, t->hour);
    OUR_WriteBYTE_Strm(&s_pwstrm, t->minute);
    OUR_WriteBYTE_Strm(&s_pwstrm, t->second);
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));

    OUR_PIC_ListSend(0x12, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}
*/
/*****************************************************************************
 函 数 名  : Requset_PIC_PeriodSet
 功能描述  : 设置工作、休眠周期请求
 输入参数  : AWAKE_TIME_T* awake  
             INT16U work_t        
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年5月27日
    作    者   : rocky
    修改内容   : 新生成函数

****************************************************************************
void Requset_PIC_PeriodSet(AWAKE_TIME_T* awake, INT16U work_t)
{
    OUR_InitStrm(&s_pwstrm, s_sbuf, S_SIZE);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x13);
    OUR_WriteBYTE_Strm(&s_pwstrm, awake->day);
    OUR_WriteBYTE_Strm(&s_pwstrm, awake->hour);
    OUR_WriteBYTE_Strm(&s_pwstrm, awake->minute);
    OUR_WriteHWORD_Strm(&s_pwstrm, work_t);
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));

    OUR_PIC_ListSend(0x13, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}
*/

/*****************************************************************************
 函 数 名  : Requset_PIC_WorkTime
 功能描述  : 请求工作时长等数据
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年11月21日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void Requset_PIC_WorkTime(void)
{
    INT8U sbuf[20];
        
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x14);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x14);

    OUR_PIC_ListSend(0x14, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

// 请求基础参数
void Requset_PIC_BaseGet(void)
{
    INT8U sbuf[20];
        
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x21);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x21);
    OUR_PIC_ListSend(0x21, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

// 设置基础参数
/*****************************************************************************
 函 数 名  : Requset_PIC_BaseSet
 功能描述  : 设置MCU基础参数
 输入参数  : SYSTIME_T* st  当前时间，年月日时分秒周
             INT16U work_t  最大工作时长
             INT8U alm_en   报警使能位
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年5月23日, 星期二
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void Requset_PIC_BaseSet(SYSTIME_T* st, INT16U work_t, INT8U alm_en)
{
   INT8U sbuf[32];
    ALM_SENSE_PARA_T sensePara;

	DAL_PP_Read(ALM_SENSITIVITY_, (void*)&sensePara);
	////if ((sensePara.als_silence > MAX_MODE_SLEEP_SECOND)) alm_en = 0;  // 强制用pic休眠

    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x22);
    OUR_WriteBYTE_Strm(&s_pwstrm, st->date.year);
    OUR_WriteBYTE_Strm(&s_pwstrm, st->date.month);
    OUR_WriteBYTE_Strm(&s_pwstrm, st->date.day);
    OUR_WriteBYTE_Strm(&s_pwstrm, st->time.hour);
    OUR_WriteBYTE_Strm(&s_pwstrm, st->time.minute);
    OUR_WriteBYTE_Strm(&s_pwstrm, st->time.second);
	OUR_WriteBYTE_Strm(&s_pwstrm, DAL_GetDayOfWeek(&(st->date))); 
    OUR_WriteHWORD_Strm(&s_pwstrm,work_t);
    OUR_WriteBYTE_Strm(&s_pwstrm, alm_en);
    OUR_WriteBYTE_Strm(&s_pwstrm, sensePara.als_filter);    // light-filter, 默认3s
    OUR_WriteHWORD_Strm(&s_pwstrm,sensePara.als_silence);   // light-silence,默认0s
    OUR_WriteBYTE_Strm(&s_pwstrm, sensePara.vibrate_filter);// vibrate-filter, 默认4次
    OUR_WriteHWORD_Strm(&s_pwstrm,sensePara.no_vib_timeout);// no-vibrate-timeout,默认300s
    PIC_LOG("alm=%x,T=%d", alm_en, sensePara.als_silence);// 231110
#if 0
    OURAPP_trace("als para:%d-%d-%d-%d", sensePara.als_filter,sensePara.als_silence,\
                                         sensePara.vibrate_filter,sensePara.no_vib_timeout);
    DAL_PP_Read(ALARM_SHT21_HDL_, (void*)&sht21_data);
    OUR_WriteHWORD_Strm(&s_pwstrm, sht21_data.TAlarmDown);  // 最低温度
    OUR_WriteHWORD_Strm(&s_pwstrm, sht21_data.TAlarmUp);    // 最高温度
    OUR_WriteHWORD_Strm(&s_pwstrm, sht21_data.RHAlarmDown); // 最低湿度
    OUR_WriteHWORD_Strm(&s_pwstrm, sht21_data.RHAlarmUp);   // 最高湿度*/
#else
    OUR_WriteLONG_Strm(&s_pwstrm, 0);
    OUR_WriteLONG_Strm(&s_pwstrm, 0);// 8字节预留，填充0
#endif
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));
    OUR_PIC_ListSend(0x22, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

// 请求闹铃参数
void Requset_PIC_AlarmParaGet(void)
{
    INT8U sbuf[10];
        
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x23);
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x23);

    OUR_PIC_ListSend(0x23, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

// 设置闹铃
void Requset_PIC_AlarmParaSet(UP_PARA_NEW_T* su)
{
    INT8U i;
    INT8U sbuf[20];
    
    OUR_InitStrm(&s_pwstrm, sbuf, sizeof(sbuf));
    OUR_WriteBYTE_Strm(&s_pwstrm, 0x24);
    switch (su->alm_type) {
    case 2:     // 时段闹铃
        for (i=0; i<3; i++) {
            OUR_WriteBYTE_Strm(&s_pwstrm, 0xff);
        }
        for (i=0; i<10; i++) {
            OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[i]);
        }
        for (i=0; i<3; i++) {
            OUR_WriteBYTE_Strm(&s_pwstrm, 0xff);
        }
        break;
    case 3:     // 星期闹铃
        for (i=0; i<13; i++) {
            OUR_WriteBYTE_Strm(&s_pwstrm, 0xff);
        }
        OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[0]);
        OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[1]);
        OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[2]);
        break;
    default:
        OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[0]);
        OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[1]);
        OUR_WriteBYTE_Strm(&s_pwstrm, su->alm.HM[2]);
        for (i=0; i<13; i++) {
            OUR_WriteBYTE_Strm(&s_pwstrm, 0xff);
        }
        break;
    }
    OUR_WriteBYTE_Strm(&s_pwstrm, ChkSum_XOR(s_pwstrm.StartPtr, s_pwstrm.Len));
    OUR_PIC_ListSend(0x24, s_pwstrm.StartPtr, s_pwstrm.Len, PERIOD_NEEDACK);
}

void App_Pic_PreInit(void)
{
    if (!PORT_InitUart(&s_pic_uart)) {
        Error_Handler(__FUNCTION__, __LINE__);
    }
}

/*
static void _pic_task(void* para)
{
    
}*/

// 简化的轮询处理，目前放在bk_main里轮询算了
void App_Pic_PollFunc_100ms(void)
{
    _pic_recv_proc();       // 接收扫描
    OUR_PIC_SendScanProc(); // 发送队列扫描
}


void InitPic_work(void)
{
//    PIC_LOG("pic GO");
    s_pic_linked = 0;
    s_recv_hdl.len = 0;
    s_pic_life_time.hword = 0;
    s_pic_alm_status = 0;
    memset(&s_pic_base_para, 0, sizeof(s_pic_base_para));

/*    if (BK_OS_CreateTask(&s_pic_task_ref, 4096, APP_PRIORITY_BELOW_NORMAL, "PITsk", 
                        150,
                        "picTask",
                        PIC_task,
                        (void *)0) != SC_SUCCESS){    
        PIC_LOG("pic TASK ERR!");
        Error_Handler(__FUNCTION__, __LINE__);
    }*/
    OUR_PIC_InitSend(); //// 发送配置
}


// 获取PIC的剩余工作时长
INT16U GetPicRestSec(void)
{
    if (s_pic_linked)
        return s_pic_base_para.rest_time;
    else
        return 0xffff;
}

// bit1=移动静止,bit0=拆卸报警
// bit2=侧翻状态
// bit3=多时段监控开车报警
INT8U GetPicAlarmStatus(void)
{
    return s_pic_alm_status;
}

INT8U *GetPicVer(void)
{
    if (s_pic_linked)
        return s_pic_base_para.ver;
    else
        return (INT8U*)"bk20-230101";   // 换成小写，这样在没有读到pic版本号的时候，不至于去升级
}



