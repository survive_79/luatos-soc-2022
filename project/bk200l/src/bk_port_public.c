/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : port_public.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年3月7日
  最近修改   :
  功能描述   : 公共参数存取接口. 怀疑security data可能有问题（因为用的是at命
               令），因此暂时用文件实现
  函数列表   :
              PORT_PP_Get_RamPtr
              PORT_PP_Init
              PORT_PP_RamValid
              PORT_PP_Read
              PORT_PP_Write
  修改历史   :
  1.日    期   : 2018年3月7日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_task.h"
#include "bk_port_public.h"
#include "bk_our_tools.h"
#include "luat_kv.h"
#include "luat_mobile.h"



/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define BLOCK_IMEI      "bk_imei"
#define BLOCK_PP        "bk_pp"

#define BLK_IMEI_SIZE   15
#define BLK_PP_SIZE     500

#define SIZE_FLASHID    8
#define NVRAM_EF_OURAPP_SIZE BLK_PP_SIZE


/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static INT8U s_deviceID[BLK_IMEI_SIZE+1];

static INT8U s_nvram_data[NVRAM_EF_OURAPP_SIZE];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
//static const u8 FLASHID[SIZE_FLASHID] = {'B', 'K', '2', '0', '0', 'L', 0x2, 0x03};   // nvram头部
#if 1
static const u8 FLASHID[SIZE_FLASHID] = {'B', 'K', '2', '0', '0', 'L', FOREIGN_APN, FOREIGN_APN};   // 根据不同默认APN，头部不同，便于恢复默认参数
#else
static const u8 FLASHID[SIZE_FLASHID] = {'B', 'K', '2', '0', '0', 'T', 0x1, 0x03};   // 测试用头部，以后测试统一用此头部
#endif

/*****************************************************************************
 函 数 名  : PORT_GetIMEI
 功能描述  : 获取IMEI号，char字符串。如果获取不到则返回NULL
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2015年4月27日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
INT8U* PORT_GetIMEI(void)
{
    int ret;
    //int ret = luat_kv_get(BLOCK_IMEI, (void*)s_deviceID, BLK_IMEI_SIZE);
    ret = luat_mobile_get_sn((char*)s_deviceID, BLK_IMEI_SIZE);
    // OURAPP_printhexbuf(s_deviceID, 15);
    if (ret > 0) {// 大于等于0x21小于等于0x7e
        if ((s_deviceID[0] < 0x21) || (s_deviceID[0] > 0x7e)) {
            goto _ERR_SN;
        }
        s_deviceID[BLK_IMEI_SIZE] = 0x00;   // 字符串结束符
        return s_deviceID;
    } else {
_ERR_SN:
        return (INT8U*)"aaaaaaa-bbbbbbb";
    }
}

BOOLEAN PORT_SetIMEI(INT8U* sptr, INT16U slen)
{
    int ret;
    if ((sptr == NULL) || (slen != BLK_IMEI_SIZE)) return FALSE;

    memcpy(s_deviceID, sptr, slen);
    s_deviceID[BLK_IMEI_SIZE] = 0x00;   // 字符串结束符
    ret = luat_mobile_set_sn((char*)s_deviceID, BLK_IMEI_SIZE);
    // ret = luat_kv_set(BLOCK_IMEI, (void*)sptr, slen);
    if (ret >= 0) return TRUE;
    else return FALSE;
}

/*****************************************************************************
 函 数 名  : _SecurePP_Read
 功能描述  : 读取指定slen长度的内容到dptr中。如果读取成功，则返回true，否则
             返回false
 输入参数  : INT8U* dptr  
             INT8U slen   这里slen必须为500，作为一个可靠性判断
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月3日
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
static BOOLEAN _SecurePP_Read(INT8U* dptr, INT16U slen)
{
    int ret;
    if (slen != BLK_PP_SIZE) return FALSE;// 参数合法性判断，防止接口被异常调用
    
    ret = luat_kv_get(BLOCK_PP, (void*)dptr, slen);
    #if PP_DEBUG > 0
    OURAPP_trace("PPrd:%d", ret);
    #endif
    if (ret >= 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

static BOOLEAN _SecurePP_Write(INT8U* sptr, INT16U slen)
{
    int ret;
    
    if (slen != BLK_PP_SIZE) return FALSE;

    ret = luat_kv_set(BLOCK_PP, (void*)sptr, slen);
    #if PP_DEBUG > 0
    OURAPP_trace("PPwr:%d", ret);
    #endif
    if (ret >= 0) return TRUE;
    else return FALSE;
}


void PORT_PP_Init(void)
{
    luat_kv_init();
    memset(s_nvram_data, 0, NVRAM_EF_OURAPP_SIZE);
}

BOOLEAN PORT_PP_Read(void)
{
    BOOLEAN result;
	INT8U  failCnt = 0 ;  // 最大读3次
	for (failCnt = 0; failCnt < 3; failCnt++) {
    	result = _SecurePP_Read(s_nvram_data, NVRAM_EF_OURAPP_SIZE);		
	    #if PP_DEBUG > 0
	        OURAPP_trace("PP读:%s", (result==TRUE)?"OK":"Fail");
	    #endif
		if (result == TRUE) {
			 return result;
		}
	}
	
    return result;
}

INT8U* PORT_PP_Get_RamPtr(void)
{
    return s_nvram_data + SIZE_FLASHID;
}

BOOLEAN PORT_PP_RamValid(void)
{
    if (0 == memcmp(s_nvram_data, FLASHID, SIZE_FLASHID)) {
        #if PP_DEBUG > 0
            OURAPP_trace("PP_ram有效");
        #endif
        return TRUE;
    } else {
        #if PP_DEBUG > 0
            OURAPP_trace("PP_ram无效");
        #endif
        return FALSE;
    }
}

/*****************************************************************************
 函 数 名  : PORT_PP_Write
 功能描述  : 写文件接口。与以前的区别是增加了一个is_main的接口
 输入参数  : BOOLEAN is_main  true则表示写入main，否则为写入back文件
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2015年5月12日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN PORT_PP_Write(BOOLEAN is_main)
{
    BOOLEAN result;

    if (!is_main) return FALSE; // 使用SecureDATA，则只需存1次即可
    
    memcpy((void *)s_nvram_data, FLASHID, SIZE_FLASHID);    // 写入头部
    result = _SecurePP_Write(s_nvram_data, NVRAM_EF_OURAPP_SIZE);

    #if PP_DEBUG > 0
    OURAPP_trace("PP存:%s", (result==TRUE)?"OK":"ERR");
    #endif
    return result;
}





