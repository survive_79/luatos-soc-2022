/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : app_ota_work.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年5月30日, 星期二
  最近修改   :
  功能描述   : 升级流程处理。先拿OTA文件，同时判断pic升级。
  函数列表   :
  修改历史   :
  1.日    期   : 2023年5月30日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#if APP_OTA_EN > 0
#include "bk_port_hardware.h"
#include "bk_port_gprs.h"
#include "bk_dal_ota.h"
#include "bk_app_picwork.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/
extern char* pic_getUpdataVer(void);
/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#if OTA_DEBUG > 0
#define OTA_TRACE OURAPP_trace
#define OTA_LOGH  OURAPP_printhexbuf
#else
#define OTA_TRACE(FORMAT,...) 
#define BTA_LOGH(A, B) 
#endif
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
// static INT8U s_ota_tmr;
static INT16U s_ota_overtime_cnt;
//static INT8U s_ota_retry_cnt;  重连可能有问题，不要重连
static BOOLEAN s_ota_mutex = 0;
/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

static BOOLEAN s_pic_ota_go = FALSE;
static void _check_pic_ota(void)
{
    char* picVer;
    char* upVer;

    if (s_pic_ota_go) return;
    s_pic_ota_go = TRUE;
    upVer = pic_getUpdataVer();
    picVer = (char*)GetPicVer();

    OTA_TRACE("检查POTA,%s %s", upVer, picVer);
    
    if (memcmp(upVer, picVer, 11) > 0) {    // pic需要升级
        Request_PIC_OTAReq();
    } else {
        Request_PIC_Sleep(0);
    }
}

void OTA_tmr_func(void)
{
    if (s_ota_mutex != 1) return;
    switch (DAL_OTA_GetState())
    {
    case OTA_ATTACH_IDLE:
        if (PORT_GPRS_IS_ON()) {
            //if (++s_ota_overtime_cnt > 1) {// 稍作延迟
                OTA_TRACE("OTA_ATTACH");
                DAL_OTA_Connect();
                s_ota_overtime_cnt = 0;
            //}
        }
        break;
    case OTA_ATTACH_ING:
        if (++s_ota_overtime_cnt > 60) {
            OTA_TRACE("OTA_OVERTIME");
            DAL_OTA_DeInit();
            //if (s_ota_retry_cnt++ < 3) {
                //s_ota_overtime_cnt = 0;
                //// PORT_Force_Reattach();   // 140729 暂不把模块整个gprs断掉
            //} else {
                //#if OTA_DEBUG > 0
                //OTA_TRACE("abort OTA");
                //#endif
                s_ota_mutex = 2;//// StopTmr(s_ota_tmr);
                Request_PIC_Sleep(0);   // 放弃并关机
            //}
        }
        break;
    /* //// no need for dns
    case OTA_ATTACH_DNS_OK:
        s_ota_overtime_cnt = 0;
        #if OTA_DEBUG > 0
        OTA_TRACE("OTA DNS OK");
        #endif
        //PORT_AGPS_CONNECT();
        break;
    */
    case OTA_ATTACH_SUCC:
        if (PORT_GPRS_IS_ON()) {
            OTA_TRACE("OTA_REQ");
            DAL_OTA_FsInit();
            DAL_OTA_SendRequest();
            DAL_OTA_SetState(OTA_ATTACH_REQUESTING);
            s_ota_overtime_cnt = 0;
        }
        break;
    case OTA_ATTACH_REQUESTING:
        if (++s_ota_overtime_cnt > 100) {// 100s 内完不成就放弃
            OTA_TRACE("OTA_TIMEOUT");
            s_ota_mutex = 2;////StopTmr(s_ota_tmr);
            Request_PIC_Sleep(0);   // 放弃并关机
        }
        break;
    case OTA_GOT_DATA:
        OTA_TRACE("OTA-D OK!");
        //s_agps_ok_cnt = 1;
        s_ota_mutex = 2;////StopTmr(s_ota_tmr);
        // DAL_OTA_DownloadData();
        DAL_OTA_Check();
        break;
    case OTA_NONEED:
        _check_pic_ota();
        break;
    default:
        break;
    }
}

static BOOLEAN _OTA_Need_update(void)
{
    return TRUE;
}
// 初始化接口, 建议在后面一些调用
void OTA_Init(void)
{
    s_ota_overtime_cnt  = 0;
    //s_ota_retry_cnt     = 0;
    s_pic_ota_go    = FALSE;
    s_ota_mutex     = FALSE;
}

void OTA_works(void)
{
    if (s_ota_mutex) return;    // 防止重复调用
    
    //s_ota_tmr = CreateTmr(_ota_tmr_func, TRUE);
    DAL_OTA_DeInit();

    if (_OTA_Need_update()) {
        s_ota_mutex = 1;    ////StartTmr(s_ota_tmr, _SECOND);
    } else {    // PIC检查版本号，并判断是否需要升级和休眠
        _check_pic_ota();
    }
}

#endif

