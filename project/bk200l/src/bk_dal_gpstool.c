
/******************************************************************************

 
                   版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司
 
  ******************************************************************************
   文 件 名   : DAL_Gps_tool.c
   版 本 号   : 初稿
   作    者   : Rocky
   生成日期   : 2021年8月4日, 星期三
   最近修改   :
   功能描述   : GPS工具函数，主要是拐点计算、2点间距离计算
   函数列表   :
               ConvertDegreesToRadians
               Distance_Haversine
               GPS_angle_judge
               GPS_mileage_estimate
               GPS_mileage_init
               HaverSin
   修改历史   :
   1.日    期   : 2021年8月4日, 星期三
     作    者   : Rocky
     修改内容   : 创建文件
 
 ******************************************************************************/
 
 /*----------------------------------------------*
  * 包含头文件                                   *
  *----------------------------------------------*/
#include "bk_os_task.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "bk_our_tools.h"
#include "bk_dal_gpsdrv.h"

 
 /*----------------------------------------------*
  * 外部变量说明                                 *
  *----------------------------------------------*/
 
 /*----------------------------------------------*
  * 外部函数原型说明                             *
  *----------------------------------------------*/
 
 /*----------------------------------------------*
  * 内部函数原型说明                             *
  *----------------------------------------------*/
 
 /*----------------------------------------------*
  * 宏定义                                       *
  *----------------------------------------------*/
#define PI                          3.14159265358979f           /* 圆周率 */
#define EARTH_R                     6371004.0000                /* 地球半径 */
 
#define MAX_ANGLE_CNT               10  // 必须偶数
#define ANGLE_THRESHOLD             20//45  // 前后5S的角度差绝对值如果大于45度，视为拐点
 /*----------------------------------------------*
  * type/enum定义                                *
  *----------------------------------------------*/
 typedef struct {
     INT16U angle[MAX_ANGLE_CNT]; //存储10个角度
     INT8U cnt;
 } ANGLE_ARRAY_T;
 /*----------------------------------------------*
  * 全局变量                                     *
  *----------------------------------------------*/
 static ANGLE_ARRAY_T s_angle_array;
 static GPS_DATA_T s_prev_gps;
 
 /*----------------------------------------------*
  * 常量定义                                     *
  *----------------------------------------------*/
 
 
 
 
 // degrees的格式: 度×1000000格式
 static double ConvertDegreesToRadians(INT32U degrees)
 {
     return (double)(degrees * PI) / 180000000;
 }
 
 static  double HaverSin(double theta)
 {
     double v = sin(theta / 2);
     
     return v * v;
 }
 // 使用Haversine公式计算的两点间距离
 static double Distance_Haversine(INT32U d_lat1, INT32U d_lon1, INT32U d_lat2, INT32U d_lon2)
 {
     double latRadins1, longRadins1, latRadins2, longRadins2;
     double vLon, vLat, h;
     //用haversine公式计算球面两点间的距离。
     // 经纬度转换成弧度
     latRadins1  = ConvertDegreesToRadians(d_lat1);
     longRadins1 = ConvertDegreesToRadians(d_lon1);
     latRadins2  = ConvertDegreesToRadians(d_lat2);
     longRadins2 = ConvertDegreesToRadians(d_lon2);
 
     //差值绝对值
     if (longRadins1 > longRadins2) {
         vLon = longRadins1 - longRadins2;
     } else {
         vLon = longRadins2 - longRadins1;
     }
 
     if (latRadins1 > latRadins2) {
         vLat = latRadins1 - latRadins2;
     } else {
         vLat = latRadins2 - latRadins1;
     }
 
     // h is the great circle distance in radians, great circle就是一个球体上的切面，它的圆心即是球心的一个周长最大的圆。
     h = HaverSin(vLat) + cos(latRadins1) * cos(latRadins2) * HaverSin(vLon);
     //OURAPP_trace("===%3.7f, %3.7f;%3.7f,%3.7f", latRadins1, longRadins1, latRadins2, longRadins2);
 
     h = 2 * EARTH_R * asin(sqrt(h));
     //OURAPP_trace("ABS:%3.8f, %3.8f,H=%3.8f", vLon, vLat, h);
     return h;
 }
 
 /*****************************************************************************
  函 数 名  : GPS_mileage_init
  功能描述  : GPS拐点和两点间距离算法的初始化函数
  输入参数  : void  
  输出参数  : 无
  返 回 值  : 
  调用函数  : 
  被调函数  : 
  
  修改历史      :
   1.日    期   : 2021年8月4日, 星期三
     作    者   : Rocky
     修改内容   : 新生成函数
 
 *****************************************************************************/
 void GPS_mileage_init(void)
 {
     s_prev_gps.status_flag = 0; // 清掉定位状态即可
     s_angle_array.cnt = 0;
 }

INT32U _abs_sub(INT32U sub1, INT32U sub2)
 {
     if (sub1 > sub2) {
         return (sub1-sub2);
     } else {
         return (sub2-sub1);
     }
 }
 /*****************************************************************************
  函 数 名  : GPS_angle_judge
  功能描述  : 拐点判断，输入每秒GPS信息，当前10s和前5s的角度合之差绝对值大于45
              度视为拐点。返回true，否则返回false
  输入参数  : GPS_DATA_T* gps  
  输出参数  : 无
  返 回 值  : 是否为拐点
  调用函数  : 
  被调函数  : 
  
  修改历史      :
   1.日    期   : 2021年8月4日, 星期三
     作    者   : Rocky
     修改内容   : 新生成函数
 
 *****************************************************************************/
 BOOLEAN GPS_angle_judge(GPS_DATA_T* gps)
 {
     INT8U i;
     INT32U cnt1=0, cnt2=0;
 // step1: 判断gps点是否定位，如果不定位，则重跑算法
     if (!(gps->status_flag & GPS_VALID)) {
         s_angle_array.cnt = 0;
         return FALSE;
     }
 // step2: 把当前的角度加入到队列中
     for (i=(MAX_ANGLE_CNT-1); i>0;i--) {
         s_angle_array.angle[i] = s_angle_array.angle[i-1];
     }
     //memmove(s_angle_array.angle+1, s_angle_array.angle, MAX_ANGLE_CNT-1);
     s_angle_array.angle[0] = gps->direction;
     s_angle_array.cnt++;
     if (s_angle_array.cnt >= MAX_ANGLE_CNT) {
         //cnt1 = s_angle_array.angle[0]+s_angle_array.angle[1]+s_angle_array.angle[2]+s_angle_array.angle[3]+s_angle_array.angle[4];
         //cnt2 = s_angle_array.angle[5]+s_angle_array.angle[6]+s_angle_array.angle[7]+s_angle_array.angle[8]+s_angle_array.angle[9];
         
         for (i=0; i<(MAX_ANGLE_CNT/2); i++) {
             cnt1 += s_angle_array.angle[i];
             cnt2 += s_angle_array.angle[MAX_ANGLE_CNT/2+i];
         }
         if (_abs_sub(cnt1, cnt2) >= ANGLE_THRESHOLD)////if (abs(cnt1-cnt2) >= ANGLE_THRESHOLD) 
         {
             s_angle_array.cnt = 0;
             return TRUE;
         } else {
             s_angle_array.cnt = MAX_ANGLE_CNT;  // 防止溢出
             // 到末尾一起返回 return FALSE;
         }
     }
     
     return FALSE;
 }
 
 
 /*****************************************************************************
  函 数 名  : GPS_mileage_estimate
  功能描述  : 根据gps点来计算两点间距离。
  输入参数  : GPS_DATA_T* gps  
  输出参数  : 无
  返 回 值  : 该点与上个点的距离，单位为米
  调用函数  : 
  被调函数  : 
  
  修改历史      :
   1.日    期   : 2021年8月4日, 星期三
     作    者   : Rocky
     修改内容   : 新生成函数
 
 *****************************************************************************/
 INT32U GPS_mileage_estimate(GPS_DATA_T* gps)
 {
     double distance;
     INT32U result;
 // step1: 不定位，不做处理
     if (!(s_prev_gps.status_flag & GPS_VALID) || !(gps->status_flag & GPS_VALID)) {
         result = 0;
         goto _QUIT_ESTIMATE;
     }
 // step2: 计算2点间距离，单位为米
     distance = Distance_Haversine(s_prev_gps.D_lat, s_prev_gps.D_long, gps->D_lat, gps->D_long);
     result = round(distance) / 1;   // 取四舍五入后的整数部分
//     result = distance / 1;
 _QUIT_ESTIMATE:
     memcpy(&s_prev_gps, gps, sizeof(GPS_DATA_T));   // 更新
     return result;
 }
 
 
 
 





