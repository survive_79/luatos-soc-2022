/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : app_gprs_work.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年5月25日, 星期四
  最近修改   :
  功能描述   : 主网络收发接口
  函数列表   :
              APP_2nd_server_valid
              APP_UploadPos
              _PrepareFrame
              _SendFrame
              _upload_pos_ack
  修改历史   :
  1.日    期   : 2023年5月25日, 星期四
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#include "bk_our_stream.h"
#include "bk_dal_public.h"
#include "bk_port_uart.h"
#include "bk_port_public.h"
#include "bk_port_gprs.h"
#include "bk_port_socket.h"
#include "bk_port_hardware.h"
#include "bk_dal_systime.h"
#include "bk_dal_sock.h"
#include "bk_dal_gpsdrv.h"

#include "bk_app_bkwork.h"
#include "bk_app_gprswork.h"
#include "bk_app_gprssend.h"
#include "bk_app_picwork.h"
#if APP_OTA_EN > 0
#include "bk_app_otawork.h"
#endif
#if BL_EN > 0
#include "bk_dal_blind.h"
#endif

#if GPRS_DEBUG > 0
#define GS_TRACE OURAPP_trace
#define GS_LOGH  OURAPP_printhexbuf
#else
#define GS_TRACE(FORMAT,...) 
#define GS_LOGH(A, B) 
#endif
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/
BOOLEAN APP_SendBlind_Pack(void);
/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define CMD_UPLOAD_POS          0x01    // 主动上报位置
#define CMD_BLIND               0x03    // 盲区补传命令
#define CMD_SET_PARA            0x12    // 设置命令


#define R_SOCK_LEN      300
#define S_SOCK_LEN      300

#define NO_NEED_ACK         0, 0, NULL
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef struct {
    INT8U rbuf[R_SOCK_LEN];
    INT16U len;
} SOCKR_T;

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static INT8U s_gprs_sbuf[S_SOCK_LEN];
static STREAM_T s_gwstrm;
// TODO: 需要考虑的几个地方:正常接收、盲区收到、设置收到这3个地方
static BOOLEAN  s_main_got_ans;
static BOOLEAN  s_back_got_ans;

static SOCKR_T s_rbuf_main;
static SOCKR_T s_rbuf_back;
#if BL_EN > 0
static BOOLEAN s_bl_prepared = FALSE;
static BLIND_STRUCT_UNION s_cur_bl;

/* timer 目前没用
static INT8U s_delay_sleep_tmr; // 保险起见，要加个定时器，不能一味死等

static void _delay_sleep_tmr_func(void)
{
    GS_TRACE("强睡主%d副%d", s_main_got_ans,s_back_got_ans);
    ////StopTmr(s_delay_sleep_tmr);

    #if APP_OTA_EN > 0
    OTA_works();
    #else
    Request_PIC_Sleep(0);
    #endif
}*/
#endif


// TODO: 目前最大长度搞成50，后续如果涉及折线的偏航报警等，就不能用这个了
#define TLV_MAX_LEN     50  // 一个TLV的数据长度不要超过50字节  // 211206 从40改为50
#define TLV_MAX_NUM     8  // 假设单条协议最多不要超过10个TLV

#define TLV_ACK_OK      0x00    // TLV的成功应答
#define TLV_ACK_ERR     0x10    // TLV的数据有误应答
typedef struct {
    INT8U tag;
    INT8U len;
    INT8U value[TLV_MAX_LEN];
    INT8U ack;  // 对应的应答,00为成功，10为数据有误
} TLV_T;
static TLV_T s_tlv_data[TLV_MAX_NUM];
static INT8U _Convert_TLV_Data(INT8U* bptr, INT8U len)
{
    INT8U num = 0;
    while (len) {
        s_tlv_data[num].tag = *bptr++;
        s_tlv_data[num].len = *bptr++;
        GS_TRACE("T=%x,L=%d,%d",s_tlv_data[num].tag,s_tlv_data[num].len, len);
        if (len < s_tlv_data[num].len+2) {// 说明当前TLV有误，剩余buf长度都不够了
            return num;
        } else {
            len -= s_tlv_data[num].len+2;
        }
        
        if (s_tlv_data[num].len > TLV_MAX_LEN) s_tlv_data[num].len = TLV_MAX_LEN;   // 保护,防止溢出
        memcpy(s_tlv_data[num].value, bptr, s_tlv_data[num].len);
        bptr += s_tlv_data[num].len;
        num++;
    }
    return num;
}


BOOLEAN APP_2nd_server_valid(void)
{
    GPRS_GROUP_T para;
    if (DAL_PP_Read(IP_DOM_BACK_, (void *)&para)) {
        if (para.isvalid && para.port && (para.ip[0] > 0x20)) {
            return TRUE;
        }
    }

    return FALSE;
}


static STREAM_T* _PrepareFrame(INT8U cmd_id)
{
    INT8U* imei;
    INT8U i;
    OUR_InitStrm(&s_gwstrm, s_gprs_sbuf, S_SOCK_LEN);
    imei = (INT8U*)PORT_GetIMEI();
    GS_TRACE("SN=%s", (char*)imei);

    for (i = 0; i < 7; i++) {
        OUR_WriteBYTE_Strm(&s_gwstrm, Char2_ToHex(imei+i*2));
    }
    OUR_WriteBYTE_Strm(&s_gwstrm, (CharToHex(imei[14])) << 4);
    OUR_WriteBYTE_Strm(&s_gwstrm, cmd_id);
    OUR_WriteHWORD_Strm(&s_gwstrm, 0);  // 预留，长度字段
    return &s_gwstrm;
}

// 封装IMEI等，并发送数据
static BOOLEAN _SendFrame(BOOLEAN is_main, INT16U type, INT8U *body, INT16U len, INT8U ct_send, INT16U ct_time, ACK_FUNC fp)
{
    INT16U tmpLen;

    if (len < FRAME_HEAD_LEN) return FALSE;
    
    tmpLen = len - FRAME_HEAD_LEN;
    body[POS_CMD_ATTR]  = (tmpLen >> 8) & 0x07; // 填充长度字段. 加密字段暂为00
    body[POS_CMD_ATTR+1]= tmpLen & 0xff;
    body[len] = ChkSum_XOR(body, len);          // 140715 校验和

    GS_TRACE("_SF[%d]:%d", is_main, type);

// TODO: 网络监控这一块，还需要查一下流程    if (PORT_GPRS_IS_ON()) s_gprs_mon_tick = 0;  // 140715 清空计数器
    return APP_GPRS_ListSend(is_main, type, body, len+1, ct_send, ct_time, fp);  // 140715
}

/* 主动上报的应答 */
static void hdl_pos_ans(BOOLEAN is_main, INT8U* body, INT16U len)
{
    SYSTIME_T t;
    
    GS_TRACE("[%d]POS ack", is_main);
    GS_LOGH(body, len);

    // 140813 rocky 兼容性，中心误传为hex了，无需做bcd转码
    ////BCD2Hex(&t.date.year, body+1, 6);   // 时间转换一下
    if (!is_main) goto _SKIP_SET_TIME;

    if (len > 6) {
        t.date.year   = body[1];
        t.date.month  = body[2];
        t.date.day    = body[3];
        t.time.hour   = body[4];
        t.time.minute = body[5];
        t.time.second = body[6];
        DAL_SetSysTime(&t, TRUE); // 暂时也视为网络时间，系统时间也不太准
    }

_SKIP_SET_TIME:
    switch (body[0]) {
    case 0x00:  // 可以直接睡眠
        APP_GPRS_ListAck(is_main, CMD_UPLOAD_POS);
#if 1   // TODO: 待完善
        if (FALSE == APP_IsInTrack()) {
            #if OUR_DEBUG > 0
            OURAPP_trace("@sleep");
            #endif

#if ALMFAIL_HDL_EN > 0  // 160605 add
            // 160131 add 这里加个二次判断ALMHDL，写入默认参数
            if (ALM_CHECK_MASK & GetPicAlarmStatus()) {
                ALARM_HDL_T almHdl;
                UP_PARA_NEW_T *para;

                para = APP_GetUploadPara();
                
                #if MULTI_SHAKE_EN > 0  // 161221 add ,多时段的报警策略
                if (IS_EINT_ALM & GetPicAlarmStatus()) {
                    MULTI_SHAKE_PARA_T shk;
                    #if ALMHDL_DEBUG > 0
                    OURAPP_trace("SHK2");
                    #endif
                    shk.mode = 0;
                    Request_PIC_MultiShake(&shk);
                    DAL_PP_Store(MULTI_SHAKE_, (void*)para);
                }
                #endif
                if (IS_ALS_ALM & GetPicAlarmStatus()) {// 170614 add, 光感报警和震动报警分开
                    #if ALMHDL_DEBUG > 0
                    OURAPP_trace("ALS2");
                    #endif
                    Requset_PIC_BaseSet(&t, 210, 0);// 禁止光敏报警, 先设置这个，再设置alarmset
                    para->eint_en = 0;
                    DAL_PP_Store(UPLOAD_PARA_, (void*)para);
                }
                
                #if ALMHDL_DEBUG > 0
                OURAPP_trace("ALMHDL-2,睡");
                #endif
#if NO_RESTORE_SHT == 0
                DAL_PP_Read(ALARM_HDL_, (void*)&almHdl);
                almHdl.cur = 0;
                DAL_PP_Store(ALARM_HDL_, (void*)&almHdl);
                
                para->mode = MODE_POS_GPS;
                para->alm_type = 1;
                para->alm.DHM_1.dd = 0;
                para->alm.DHM_1.hh = almHdl.period/60;
                para->alm.DHM_1.mm = almHdl.period%60;
                Requset_PIC_AlarmParaSet(para);
#endif
            }
            // 160131 add end
#endif
            #if BL_EN > 0
            // 180603 改为探询是否需要发送盲区点
            if (!APP_SendBlind_Pack()) {
                if (is_main) s_main_got_ans = TRUE;
                else s_back_got_ans = TRUE;

                if (APP_2nd_server_valid()) {// 231211 改写，收全就马上睡眠
                    if (!s_main_got_ans || !s_back_got_ans) {
                        return;
                    }
                }
                //#if DUAL_IP > 0 // 这里刚好仅处理非追踪状态下的报送
                /* 因为目前没双ip，加上timer没启用，就不开延时了，后续考虑用状态机
                
                if ((!s_main_got_ans) || (!s_back_got_ans)) {
                    StartTmr(s_delay_sleep_tmr, _SECOND*15);
                    return; // 没有都收全，不能往下走
                } else {
                    StopTmr(s_delay_sleep_tmr);
                }*/
                //#endif
                #if APP_OTA_EN > 0
                OTA_works();
                #else
                Request_PIC_Sleep(0);
                #endif
            }
            #else
                // TODO: 200828 待完善，还没做双中心判断
                #if APP_OTA_EN > 0
                OTA_works();
                #else
                Request_PIC_Sleep(0);
                #endif
            #endif
        }
#endif
        break;
    case 0x01:  // 等待休眠指令
        APP_GPRS_ListAck(is_main, CMD_UPLOAD_POS);
        break;
    case 0x10:
        break;
    default:
        break;
    }
}

/* 参数设置请求 */
static void hdl_set_para(BOOLEAN is_main, INT8U *body, INT16U len)
{
    INT8U ccid[20];
    INT8U i, num, j;
    STREAM_T* strm;
    SYSTIME_T tm;
    GPRS_GROUP_T ip;
    
    DAL_GetSysTime(&tm);
    
    #if GPRS_DEBUG > 0
    OURAPP_printhexbuf(body, len);
    #endif
    num = _Convert_TLV_Data(body, len);
    if (num == 0) return;
    
    strm =_PrepareFrame(CMD_SET_PARA);

    GS_TRACE("CMD_SET_PARA.%d",num);

    for (i = 0; i < num; i++) {
        OUR_WriteBYTE_Strm(strm, s_tlv_data[i].tag);// tag
        OUR_WriteBYTE_Strm(strm, 1);                // tag
        switch (s_tlv_data[i].tag) {
        case 0x01:  // 上报模式设置(老,兼容)
            if (s_tlv_data[i].len != 6) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                UP_PARA_NEW_T para;
                para.mode = s_tlv_data[i].value[0];
                para.alm_type = 1;
                para.alm.HM[0]= s_tlv_data[i].value[1];
                para.alm.HM[1]= s_tlv_data[i].value[2];
                para.alm.HM[2]= s_tlv_data[i].value[3];
                para.max_worktime.bytes.high= s_tlv_data[i].value[4];
                para.max_worktime.bytes.low = s_tlv_data[i].value[5];
                para.eint_en = 0;   // 兼容，不开启
                // 判断参数合法性 160807 change, 天数限制改为185天
                if ((para.mode > MODE_POS_GPS) ||
                    (para.alm.HM[0] > 185) || (para.alm.HM[1] > 23) || (para.alm.HM[2] > 59) ||
                    (para.alm.HM[0] == 0 && para.alm.HM[1] == 0 && para.alm.HM[2] < 5) || 
                    (para.max_worktime.bytes.high > 7) || 
                    (para.max_worktime.bytes.high == 0 && para.max_worktime.bytes.low < 180)) {
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                } else {
                    //para.mode &= 0x0f;              // 表示中心已经设置过
                    APP_Store_UploadPara(&para);    // 保存参数
                    Requset_PIC_AlarmParaSet(&para);//Requset_PIC_PeriodSet(&para.period_awake, para.max_worktime.hword);// 还需要更新PIC
                    s_tlv_data[i].ack = TLV_ACK_OK;
                }
            }
            break;
        case 0x02:  // 终端上报数据配置，暂时不动作
            s_tlv_data[i].ack = TLV_ACK_OK;
            break;
        case 0x03:  // APN设置
            {
            APN_PARA_T apn; //// 140915
            apn.force = true;
            //apn.len   = s_tlv_data[i].len;
            memcpy(apn.apn, s_tlv_data[i].value, s_tlv_data[i].len);
            apn.apn[s_tlv_data[i].len] = '\0';
            apn.user[0] = 0;    // 141116 暂时搞成没密码的
            apn.pswd[0] = 0;
            DAL_PP_Store(APN_, (void*)&apn);
            s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;
        case 0x13:  // 专网apn设置
            {
                APN_PARA_T apn; //// 140915
                INT8U tmpPtr = 1;
                
                apn.force = true;
                j = s_tlv_data[i].value[0]; // j作为临时变量，表示字段长度
                if ((j < 3) || (j >= MAX_OURAPN_LEN)) {
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                    break;
                }
                memcpy(apn.apn, &s_tlv_data[i].value[tmpPtr], j);
                apn.apn[j] = '\0';
                tmpPtr += j;
                
                j = s_tlv_data[i].value[tmpPtr];
                if (j >= MAX_OURAPN_LEN) {
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                    break;
                } else if (j == 0) {
                    apn.user[0] = 0;
                    apn.pswd[0] = 0;
                    goto _SAVE_APN;
                }
                tmpPtr++;
                memcpy(apn.user, &s_tlv_data[i].value[tmpPtr], j);
                apn.user[j] = '\0';
                tmpPtr += j;

                j = s_tlv_data[i].value[tmpPtr];
                if (j >= MAX_OURAPN_LEN) {
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                    break;
                } else if (j == 0) {
                    apn.pswd[0] = 0;
                    goto _SAVE_APN;
                }
                tmpPtr++;
                memcpy(apn.pswd, &s_tlv_data[i].value[tmpPtr], j);
                apn.pswd[j] = '\0';
_SAVE_APN:
                DAL_PP_Store(APN_, (void*)&apn);
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;
        case 0x04:  // 主ip设置
            if (s_tlv_data[i].len != 6) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                ip.isvalid = TRUE;
                sprintf(ip.ip, "%d.%d.%d.%d", 
                    s_tlv_data[i].value[0], s_tlv_data[i].value[1], s_tlv_data[i].value[2], s_tlv_data[i].value[3]);
                ip.port = s_tlv_data[i].value[4];
                ip.port = (ip.port << 8) + s_tlv_data[i].value[5];
                DAL_PP_Store(IP_DOM_MAIN_, (void*)&ip);
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;
        case 0xAE:  //// 231115 add, 新增的支持域名
            if (s_tlv_data[i].len < 5) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                ip.isvalid = TRUE;
                ip.port = s_tlv_data[i].value[0];
                ip.port = (ip.port << 8) + s_tlv_data[i].value[1];
                memset(ip.ip, 0, sizeof(ip.ip));
                memcpy(ip.ip, &s_tlv_data[i].value[2], s_tlv_data[i].len-2);
                DAL_PP_Store(IP_DOM_MAIN_, (void*)&ip);
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;
        case 0xAF:  //// 231211 add, 副IP支持域名
            if (s_tlv_data[i].len < 5) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                memset(ip.ip, 0, sizeof(ip.ip));
                ip.port = s_tlv_data[i].value[0];
                ip.port = (ip.port << 8) + s_tlv_data[i].value[1];
                if (ip.port == 0) {
                    ip.isvalid = FALSE;
                } else {
                    ip.isvalid = TRUE;
                    memcpy(ip.ip, &s_tlv_data[i].value[2], s_tlv_data[i].len-2);
                }
                DAL_PP_Store(IP_DOM_BACK_, (void*)&ip);
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;
#if ALMFAIL_HDL_EN > 0  // 160605 add
        case 0x0d:  // TODO: 160131 add, 用新协议帧
#endif
        case 0x09:  // 紧急追踪请求
            if (s_tlv_data[i].len != 4) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                HWORD_UNION tmp_time, tmp_period;
                tmp_time.bytes.high  = s_tlv_data[i].value[0];
                tmp_time.bytes.low   = s_tlv_data[i].value[1];
                tmp_period.bytes.high= s_tlv_data[i].value[2];
                tmp_period.bytes.low = s_tlv_data[i].value[3];
                if ((tmp_time.hword != 0) && (tmp_time.hword <= 100) && 
                    (tmp_period.hword > 4) && (tmp_period.hword < 721)) {  // 160131 add,增加报警处理。
#if ALMFAIL_HDL_EN > 0  // 160605 add
                    ALARM_HDL_T almHdl;
       11             // 161127 del, 统一处理 SYSTIME_T tm;
                    UP_PARA_NEW_T pppp, *para;
                    // 这里关闭pic的光感等，并修改到pic的参数
                    // 161127 del, 统一处理 YX_GetSysTime(&tm);
                    // TODO: 170614 change，修改为根据报警类型，做特定的报警关闭
                    #if ALMHDL_DEBUG > 0
                    OURAPP_trace("设置ALMHDL-睡");
                    #endif
                    if (IS_ALS_ALM & GetPicAlarmStatus()) {// 170614 ADD
                        #if ALMHDL_DEBUG > 0
                        OURAPP_trace("关ALS");
                        #endif
                        Requset_PIC_BaseSet(&tm, 210, 0);
                        // TODO: 这里把UPLOAD_PARA里的报警开关关闭
                        para = APP_GetUploadPara();
                        para->eint_en = 0;
                        DAL_PP_Store(UPLOAD_PARA_, (void*)para);
                    }
                    
                    #if MULTI_SHAKE_EN > 0  // 161221 add ,多时段的报警策略。这里不做MULTI_SHAKE_的pubpara存储是因为中心会下发18tag，那边存
                    if (IS_EINT_ALM & GetPicAlarmStatus()) {
                        MULTI_SHAKE_PARA_T shk;
                        #if ALMHDL_DEBUG > 0
                        OURAPP_trace("关SHK");
                        #endif
                        shk.mode = 0;
                        Request_PIC_MultiShake(&shk);
                    }
                    #endif
#if NO_RESTORE_SHT == 0
                    almHdl.cnt = tmp_time.hword;
                    almHdl.cur = 0;// 从0开始
                    almHdl.period = tmp_period.hword;
                    DAL_PP_Store(ALARM_HDL_, (void*)&almHdl);
                    
                    pppp.mode = MODE_POS_GPS;
                    pppp.alm_type = 1;
                    pppp.alm.DHM_1.dd = 0;
                    pppp.alm.DHM_1.hh = almHdl.period/60;
                    pppp.alm.DHM_1.mm = almHdl.period%60;
                    Requset_PIC_AlarmParaSet(&pppp);
#endif
                    s_tlv_data[i].ack = TLV_ACK_OK;
                    //Request_PIC_Sleep(0);
                    // 160131 add end
#else
                    s_tlv_data[i].ack = TLV_ACK_ERR;
#endif
                } else if ((tmp_time.hword >= 0xffff) || (tmp_time.hword < 300) || 
                    (tmp_period.hword < 30) || (tmp_period.hword > tmp_time.hword / 3)) {
                    
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                } else {

                    #if 1
                    APP_TrackStart(tmp_time.hword+60, tmp_period.hword);    // 140715 增加60秒，以减缓误差
                    //StopTmr(s_delay_sleep_tmr);
                    s_tlv_data[i].ack = TLV_ACK_OK;
                    #else
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                    #endif
                }
            }
            break;
        case 0x0a:  // 停止追踪，以及立即休眠
            if (s_tlv_data[i].value[0] == 0x01) {
                APP_TrackStop(FALSE);// TODO: 停止追踪
                s_tlv_data[i].ack = TLV_ACK_OK;
                ////StartTmr(s_delay_sleep_tmr, _SECOND*10);
            } else if (s_tlv_data[i].value[0] == 0x02) {
                APP_TrackStop(TRUE);// TODO: 立即休眠，是不是还有其他事情要做?
                s_tlv_data[i].ack = TLV_ACK_OK;
            } else {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            }
            break;
        case 0x0b:
            if (s_tlv_data[i].len != 20) {
                goto _ERR_OCCUR;//s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                UP_PARA_NEW_T para, *pp;
                // 161127 del, 统一处理 SYSTIME_T tm;
#if ALMFAIL_HDL_EN > 0  // 160605 add
                ALARM_HDL_T almHdl;
#endif
                // 161127 del, 统一处理 YX_GetSysTime(&tm);
                pp = APP_GetUploadPara();
                memcpy(&para, pp, sizeof(UP_PARA_NEW_T));
#if ALMFAIL_HDL_EN > 0  // 160605 add
                // 160131 add 一旦中心有设置，则清掉alm-hdl
                DAL_PP_Read(ALARM_HDL_, (void*)&almHdl);
                if ((almHdl.cur < almHdl.cnt) && (almHdl.cnt <= 100) && 
                    (almHdl.period < 721) && (almHdl.period > 4)) {
                    #if ALMHDL_DEBUG > 0
                    OURAPP_trace("清ALMHDL");
                    #endif
                    // 只要有设置周期，处于报警处理状态的，则需清掉该状态。
                    almHdl.cur = almHdl.cnt;
                    DAL_PP_Store(ALARM_HDL_, (void*)&almHdl);
                }
                // 160131 add end
#endif
                if (s_tlv_data[i].value[0] != 0xff) para.mode = s_tlv_data[i].value[0];
                if (s_tlv_data[i].value[1] != 0xff) para.eint_en = s_tlv_data[i].value[1];
                if (s_tlv_data[i].value[2] != 0xff) {       // 这样判断为模式1
                    para.alm_type   = 1;
                    para.alm.HM[0]  = s_tlv_data[i].value[2];
                    para.alm.HM[1]  = s_tlv_data[i].value[3];
                    para.alm.HM[2]  = s_tlv_data[i].value[4];
                } else if (s_tlv_data[i].value[5] != 0xff) {// 判断为模式2
                    para.alm_type = 2;
                    memcpy(&para.alm.HM[0], &s_tlv_data[i].value[5], 10);
                } else if (s_tlv_data[i].value[15] != 0xff) {// 判断为模式3
                    para.alm_type = 3;
                    para.alm.HM[0]  = s_tlv_data[i].value[15];
                    if (para.alm.HM[0] == 0) {
                        para.alm.HM[0]  = 0x7f;   // 151211 add 兼容性处理，与协议匹配
                    }
                    para.alm.HM[1]  = s_tlv_data[i].value[16];
                    para.alm.HM[2]  = s_tlv_data[i].value[17];
                }
                if (s_tlv_data[i].value[18] != 0xff) {
                    para.max_worktime.bytes.high= s_tlv_data[i].value[18];
                    para.max_worktime.bytes.low = s_tlv_data[i].value[19];
                }
                
                if ((para.max_worktime.bytes.high > 7) || 
                    (para.max_worktime.bytes.high == 0 && para.max_worktime.bytes.low < 180)) {
                    goto _ERR_OCCUR;//s_tlv_data[i].ack = TLV_ACK_ERR;
                }
                switch (para.alm_type) {
                case 1: // 160807 change, 天数限制改为185天
                    if ((para.alm.HM[0] > 185) || (para.alm.HM[1] > 23) || (para.alm.HM[2] > 59) ||
                        (para.alm.HM[0] == 0 && para.alm.HM[1] == 0 && para.alm.HM[2] < 5)) {
                        goto _ERR_OCCUR;//s_tlv_data[i].ack = TLV_ACK_ERR;
                    }
                    break;
                case 2:
                    for (j=0; j<5; j++) {
                        if (para.alm.HM[2*j] == 0xff) break; // 不必再往下判断了
                        if ((para.alm.HM[2*j] > 23) || (para.alm.HM[2*j+1] > 59)) {
                            goto _ERR_OCCUR;//s_tlv_data[i].ack = TLV_ACK_ERR;
                        }
                    }
                    break;
                case 3:
                    if ((para.alm.HM[0]==0) || (para.alm.HM[1] > 23) || (para.alm.HM[2] > 59)) {
                        goto _ERR_OCCUR;//s_tlv_data[i].ack = TLV_ACK_ERR;
                    }
                    break;
                }
                //para.mode &= 0x0f;              // 表示中心已经设置过. 这个去掉，中心没处理
                BK_LOG("模式%d:%d:%d:%d", para.alm_type, para.alm.HM[0], para.alm.HM[1], para.alm.HM[2]);
                Requset_PIC_BaseSet(&tm, para.max_worktime.hword, para.eint_en);
                APP_Store_UploadPara(&para);    // 保存参数
                Requset_PIC_AlarmParaSet(&para);//Requset_PIC_PeriodSet(&para.period_awake, para.max_worktime.hword);// 还需要更新PIC
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;
        /*case 0x0c:  // AGPSOTA-ip设置 201206 del, 后续需要升级再OTA升掉
            if (s_tlv_data[i].len != 7) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                IP_PORT_PARA_T ip;
                memcpy(ip.ip, &s_tlv_data[i].value[1], 4);
                ip.port.bytes.high = s_tlv_data[i].value[5];
                ip.port.bytes.low  = s_tlv_data[i].value[6];
                if (s_tlv_data[i].value[0] == 1) {      // 1==AGPS
                    // DAL_PP_Store(AGPS_IPPORT_, (void*)&ip);
                } else if (s_tlv_data[i].value[0] == 2) {// 2==OTA
                    DAL_PP_Store(OTA_IPPORT_, (void*)&ip);
                } else {
                    goto _ERR_OCCUR;
                }
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            break;*/
#if MULTI_SHAKE_EN > 0
        case 0x18:  // 多时段震动报警监测
        {
            MULTI_SHAKE_PARA_T ssptr = {0};
            
            ssptr.mode = s_tlv_data[i].value[0];
            if (ssptr.mode > 5) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                INT8U m;
                //INT16U pre = 0;// 用来做时段间隔的合法性判断 暂不打开，不做1小时限制
                for (m=0; m<ssptr.mode; m++) {
                    ssptr.shake[m].hour    = s_tlv_data[i].value[1+m*3];
                    ssptr.shake[m].minute  = s_tlv_data[i].value[2+m*3];
                    ssptr.shake[m].period  = s_tlv_data[i].value[3+m*3];
                    if ((ssptr.shake[m].hour>23) || 
                        (ssptr.shake[m].minute>59) || 
                        //((pre+60) > (ssptr.shake[m].hour*60+ssptr.shake[m].minute)) || 
                        (ssptr.shake[m].period<5)) {// 合法性判断
                        s_tlv_data[i].ack = TLV_ACK_ERR;
                        goto _ERR_18H_BREAK;
                    //} else {
                    //    pre = ssptr.shake[m].hour*60+ssptr.shake[m].minute;
                    }
                }
                #if OUR_DEBUG > 0
                OURAPP_trace("多时段%d", ssptr.mode);
                for (m=0; m<ssptr.mode; m++) {
                    OURAPP_trace("<%d,%d,%d>", ssptr.shake[m].hour, ssptr.shake[m].minute, ssptr.shake[m].period);
                }
                #endif
                DAL_PP_Store(MULTI_SHAKE_, (void*)&ssptr);
                Request_PIC_MultiShake(&ssptr);// 这里通知pic进行参数配置
                s_tlv_data[i].ack = TLV_ACK_OK;
            }
            
        }
_ERR_18H_BREAK:
            break;
#endif
#if GPS_CHEATER > 0
        case 0xf0:  // 160706 add, gps时长可改
            {
                RESET_RETRY_T gt;
                gt.rst_retry = s_tlv_data[i].value[0];
                if (gt.rst_retry < 5 || gt.rst_retry > 180) {
                    s_tlv_data[i].ack = TLV_ACK_ERR;
                } else {
                    DAL_PP_Store(GPS_CHEATER_, (void *)&gt);
                    s_tlv_data[i].ack = TLV_ACK_OK;
                }
                
            }
            break;
#endif
#if 1   //// 231109 open it MULTI_SHAKE_EN > 0
        case 0Xf1:
            if (s_tlv_data[i].len != 6) {
                s_tlv_data[i].ack = TLV_ACK_ERR;
            } else {
                ALM_SENSE_PARA_T senseP;
                senseP.als_filter   = s_tlv_data[i].value[0];
                senseP.als_silence  = s_tlv_data[i].value[1];
                senseP.als_silence  = (senseP.als_silence<<8)+s_tlv_data[i].value[2];
                senseP.vibrate_filter=s_tlv_data[i].value[3];
                senseP.no_vib_timeout=s_tlv_data[i].value[4];
                senseP.no_vib_timeout=(senseP.no_vib_timeout<<8)+s_tlv_data[i].value[5];

                if ((senseP.als_filter < 3) || /* 170416 del, 去掉此字段的上限限制，可以从3~255  (senseP.als_filter > 120) ||*/
                    (senseP.als_silence>65530) ||
                    (senseP.vibrate_filter==0) || (senseP.vibrate_filter > 20) ||
                    (senseP.no_vib_timeout <30 ||(senseP.no_vib_timeout > 3600))) {
                        s_tlv_data[i].ack = TLV_ACK_ERR;
                    } else {
                        UP_PARA_NEW_T para, *pp;
                        #if OUR_DEBUG > 0
                        OURAPP_trace("sense:%d,%d,%d,%d", senseP.als_filter, senseP.als_silence, senseP.vibrate_filter, senseP.no_vib_timeout);
                        #endif
                        DAL_PP_Store(ALM_SENSITIVITY_, (void *)&senseP);
#if FORCE_NO_GSENSOR == 0   // 240202 不打开gsensor
                        pp = APP_GetUploadPara();
                        memcpy(&para, pp, sizeof(UP_PARA_NEW_T));
                        
                        if (senseP.als_filter == 3) {   //231116 利用光感过滤配置移动静止，非3是开启，3是关闭
                            para.eint_en &= (~0x04);
                        } else {
                            para.eint_en |= 0x04;
                        }
                        APP_Store_UploadPara(&para);    // 保存参数
                        Requset_PIC_BaseSet(&tm, para.max_worktime.hword, para.eint_en);// 这里隐含了读取ALM_SENSITIVITY_
#endif
                        s_tlv_data[i].ack = TLV_ACK_OK;
                    }
            }
            break;
#endif
        case 0x0f:  // 测试数据上报开关
        case 0x05:  // 主域名，暂不支持
        case 0x06:  // 副ip，暂不支持
        case 0x07:  // 副域名，暂不支持
        case 0x08:  // 短信服务号，暂不支持
        default:    // 默认解析不了的就失败
_ERR_OCCUR:
            s_tlv_data[i].ack = TLV_ACK_ERR;
            break;
        }
        OUR_WriteBYTE_Strm(strm, s_tlv_data[i].ack);// tag
    }
    _SendFrame(is_main, CMD_SET_PARA, strm->StartPtr, strm->Len, NO_NEED_ACK);
        
    DAL_PP_Read(SIM_ICCID_, (void*)ccid);
    if (memcmp(PORT_Get_iccid(), ccid, 20)==0) {// 一致才清空
        #if OUR_DEBUG > 0
        OURAPP_trace("CID擦");
        #endif
        ccid[0] = 0;
        DAL_PP_Store(SIM_ICCID_, (void*)ccid);
    }
}

#if BL_EN > 0
// 准备好一个盲区点。如果不定位，则扔掉. 
BOOLEAN _PrepareBlindNode(GPS_DATA_T* gps, CELL_ID_T* cell, BLIND_STRUCT_UNION* bl_ptr)
{
    INT8U tmpByte, ex_bit;
    INT16U tmpWord;
    SYSTIME_T tm;

    if (DAL_GetSysTime(&tm) == 0) return FALSE;

    if (!bl_ptr) return FALSE;

    tmpWord = GetVolt_Max();
    tmpByte = (tmpWord * 5) / 180;
    if (tmpByte > 100) tmpByte = 100;

    ex_bit = GetPicAlarmStatus();   // 报警状态

    if (gps) {
        bl_ptr->gps_info.t_local.date.year = tm.date.year;
        bl_ptr->gps_info.t_local.date.month= tm.date.month;
        bl_ptr->gps_info.t_local.date.day  = tm.date.day;
        bl_ptr->gps_info.t_local.time.hour = tm.time.hour;
        bl_ptr->gps_info.t_local.time.minute= tm.time.minute;
        bl_ptr->gps_info.t_local.time.second= tm.time.second;
        bl_ptr->gps_info.voltage = tmpByte;     // 电量

        tmpByte = BL_FLAG_GPS; // flag
        if (gps->status_flag & GPS_VALID) tmpByte |= 0x40;
        if (gps->status_flag & LAT_SOUTH) tmpByte |= 0x20;
        if (gps->status_flag & LONG_WEST) tmpByte |= 0x10;
        if (ex_bit & 0x02) tmpByte |= 0x08;     // 移动静止
        if (ex_bit & 0x01) tmpByte |= 0x01;     // 光感防拆
        bl_ptr->gps_info.status_flag = tmpByte;
        
        bl_ptr->gps_info.D_lat  = gps->D_lat;
        bl_ptr->gps_info.D_long = gps->D_long;
        bl_ptr->gps_info.spd    = gps->spd/10;      // 速度，单位kmh
        bl_ptr->gps_info.dir    = gps->direction / 2;
    } else if (cell) {
        bl_ptr->lbs_info.t_local.date.year = tm.date.year;
        bl_ptr->lbs_info.t_local.date.month= tm.date.month;
        bl_ptr->lbs_info.t_local.date.day  = tm.date.day;
        bl_ptr->lbs_info.t_local.time.hour = tm.time.hour;
        bl_ptr->lbs_info.t_local.time.minute= tm.time.minute;
        bl_ptr->lbs_info.t_local.time.second= tm.time.second;
        bl_ptr->lbs_info.voltage = tmpByte;     // 电量

        tmpByte = 0x00; // flag
        if (ex_bit & 0x02) tmpByte |= 0x08;     // 移动静止
        if (ex_bit & 0x01) tmpByte |= 0x01;     // 光感防拆
        bl_ptr->lbs_info.status_flag = tmpByte;

        bl_ptr->lbs_info.mcc = cell->mcc;
        bl_ptr->lbs_info.mnc = cell->mnc & 0xff;
        bl_ptr->lbs_info.lac = cell->cell[0].lac;
        bl_ptr->lbs_info.cellid = cell->cell[0].cellid;
        bl_ptr->lbs_info.signal = cell->cell[0].signal;
    } else {
        return FALSE;
    }
    return TRUE;
}

/* 盲区补传的应答 */
static void hdl_bl_ans(BOOLEAN is_main, INT8U* body, INT16U len)
{
    // TODO: 要不要判断应答?
    
    APP_GPRS_ListAck(is_main, CMD_BLIND);
    
    DAL_BlindErase();   // 暂时不去管主副应答的处理，靠eat_fs本身去容错。

    if (is_main) s_main_got_ans = TRUE;
    else s_back_got_ans = TRUE;
    /*if (APP_IsInTrack()) {
        StopTmr(s_delay_sleep_tmr);
        return;    // 某个平台提前处于追踪状态，则不予处理休眠
    }*/
/* 231111 TIMER没启用，加上没双ip，就不用延时sleep了 
    if (!APP_2nd_server_valid()) goto _NO_2ND_SLEEP;
    
    if ((!s_main_got_ans) || (!s_back_got_ans)) {
        StartTmr(s_delay_sleep_tmr, _SECOND*10, FALSE);
        return; // 没有都收全，不能往下休眠
    } else {
_NO_2ND_SLEEP:
        StopTmr(s_delay_sleep_tmr);
    }
*/
    /* 目前就发1次
    if(s_bl_send_cnt < BL_SEND_MAX) {
        if (APP_SendBlind_Pack()) { // 还有盲区要发
            StartTmr(s_delay_sleep_tmr, _SECOND*10, FALSE);
            return;
        } else {
            StopTmr(s_delay_sleep_tmr);
        }
    }*/

    if (APP_2nd_server_valid()) {// 231211 改写，收全就马上睡眠
        if (!s_main_got_ans || !s_back_got_ans) {
            return;
        }
    }

    GS_TRACE("BL-slp");

#if APP_OTA_EN > 0
    OTA_works();
#else
    Request_PIC_Sleep(0);
#endif

}

static void _sendBlind_ack(BOOLEAN is_main, INT8U result)
{
    // 副中心不处理bl的应答
    if (!is_main) {
        GS_TRACE("副bl-ACK%d", result);
        return;
    }
    
    if (result == _SUCCESS) {
        GS_TRACE("bl发成");
        return;
    } else {
        // 盲区发送失败，直接睡眠
        GS_TRACE("bl发败");
        // TODO: 这里可以加个OTA的请求。180426
        #if APP_OTA_EN > 0
        OTA_works();
        #else
        Request_PIC_Sleep(0);
        #endif
    }
}

// 发送一包盲区补传点
BOOLEAN APP_SendBlind_Pack(void)
{
    STREAM_T* strm;
    INT8U* dataPtr;
    INT8U dataLen, i;
    BLIND_STRUCT_UNION* bl;


    dataPtr = DAL_BlindGetData(&dataLen);
    if ((dataPtr == NULL) || (dataLen < BLIND_NODE_SIZE)) {
        GS_TRACE("BL读空");
        return FALSE;
    }

    GS_TRACE("发bl=%d", dataLen);

    strm = _PrepareFrame(CMD_BLIND);
    OUR_WriteBYTE_Strm(strm, dataLen/BLIND_NODE_SIZE);  // 本包个数
    OUR_WriteBYTE_Strm(strm, 18);                       // 一个点的len
    for (i=0; i<dataLen/BLIND_NODE_SIZE; i++) {
        bl = (BLIND_STRUCT_UNION*)&dataPtr[i*BLIND_NODE_SIZE];
        OUR_WriteDATA_Strm(strm, bl->data, 8);  // 前8字节不必考虑大小端问题
        if (bl->data[7] & BL_FLAG_GPS) {   // 是GPS盲区
            OUR_WriteLONG_Strm(strm, bl->gps_info.D_lat);
            OUR_WriteLONG_Strm(strm, bl->gps_info.D_long);
            OUR_WriteBYTE_Strm(strm, bl->gps_info.spd);
            OUR_WriteBYTE_Strm(strm, bl->gps_info.dir);
        } else {
            OUR_WriteHWORD_Strm(strm, bl->lbs_info.mcc);
            OUR_WriteBYTE_Strm(strm, bl->lbs_info.mnc);
            OUR_WriteLONG_Strm(strm, bl->lbs_info.cellid);
            OUR_WriteBYTE_Strm(strm, bl->lbs_info.signal);
        }
    }
    _SendFrame(TRUE, CMD_BLIND, strm->StartPtr, strm->Len, 3, 10, _sendBlind_ack);
    s_main_got_ans = FALSE;
    
    if (APP_2nd_server_valid()) {
        _SendFrame(FALSE, CMD_BLIND, strm->StartPtr, strm->Len, 3, 10, _sendBlind_ack);
        s_back_got_ans = FALSE;
    } else {
        s_back_got_ans = TRUE;
    }
    
    return TRUE;

}
#endif


static const PROTOCOL_ENTRY_T PROTOCOL_ENTRY[] = {
    {CMD_UPLOAD_POS,    NULL,           hdl_pos_ans},
    {CMD_SET_PARA,      NULL,           hdl_set_para},
    #if BL_EN > 0
    {CMD_BLIND,         NULL,           hdl_bl_ans},
    #endif
};
static BOOLEAN FindGPRSProc_Entry(BOOLEAN is_main, INT16U index, const PROTOCOL_ENTRY_T *funcentry, INT16U num, INT8U *data, INT16U dlen)
{
    for (; num > 0; num--, funcentry++) {
        if (index == funcentry->index) {
            if (funcentry->proc_null) {
                funcentry->proc_null();
                return TRUE;
            }
            if (funcentry->proc_para) {
                funcentry->proc_para(is_main, data, dlen);
                return TRUE;
            }
        }
    }
    return FALSE;
}
void APP_hdl_gprs_frame(BOOLEAN is_main, INT8U cmd, INT8U* body, INT16U len)
{
    FindGPRSProc_Entry(is_main, cmd, PROTOCOL_ENTRY, sizeof(PROTOCOL_ENTRY)/sizeof(PROTOCOL_ENTRY[0]), body, len);
}

/*****************************************************************************
 函 数 名  : APP_socket_scan_recv_func
 功能描述  : SOCKET进程的扫描程序，这里处理sock收发。
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2020年10月22日
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void APP_socket_scan_recv_func(void)
{
    INT16S ch;
    INT16U len, tmpLen;

    if (SOCK_OK != Dal_SockGetState(SOCK_FD_MAIN)) {        // 状态不正确，不予处理
        return;
    }

    while ((ch = DAL_SockReadByte(SOCK_FD_MAIN)) != -1) {
        if (ch == 0x7e) {
            if (!s_rbuf_main.len) {  // 长度为0说明还未收到过头
                s_rbuf_main.rbuf[0] = ch;
                s_rbuf_main.len = 1;
            } else {                // 说明是帧尾,一帧数据收齐
                s_rbuf_main.rbuf[s_rbuf_main.len++] = ch;
                len = Deassemble(s_rbuf_main.rbuf, s_rbuf_main.rbuf + 1, s_rbuf_main.len-2, R_SOCK_LEN);

                //GS_TRACE("###Read sock:");
                //GS_LOGH(s_rbuf_main.rbuf, len);
                if (len < FRAME_HEAD_LEN) {// 说明解析有误
                    GS_TRACE("len<FRAME_HEAD_LEN");
                    goto _RECV_7E_ERR;//_RECVBUF_RST;
                }
                if (s_rbuf_main.rbuf[len-1] != ChkSum_XOR(&s_rbuf_main.rbuf[0], len - 1)) {
                    GS_TRACE("ChkSum_XOR err:%d", ChkSum_XOR(&s_rbuf_main.rbuf[0], len - 1));
                    goto _RECV_7E_ERR;//_RECVBUF_RST;
                }
                // TODO: 这里要剥去IMEI、长度啥的才行
                tmpLen = s_rbuf_main.rbuf[POS_CMD_ATTR] & 0x07;
                tmpLen = (tmpLen << 8) + s_rbuf_main.rbuf[POS_CMD_ATTR+1];
                //GS_TRACE("###len:%d,tmpLen:%d", len, tmpLen);

                if (len == (FRAME_HEAD_LEN + 1 + tmpLen)) {
                    APP_hdl_gprs_frame(TRUE, s_rbuf_main.rbuf[POS_CMD], &s_rbuf_main.rbuf[FRAME_HEAD_LEN], tmpLen);
                }
                goto _RECVBUF_RST;                
            }
        } else {
            if (s_rbuf_main.len) {  // 这说明是数据
                s_rbuf_main.rbuf[s_rbuf_main.len++] = ch;
                if (s_rbuf_main.len == R_SOCK_LEN) {// 缓冲区满，则把收到的数据舍弃
                    goto _RECVBUF_RST;
                }
            ////} else {
                //// 到这里则说明是垃圾数据，过滤
            }
        }
        continue;
_RECV_7E_ERR:   // 已经收到所谓2个7E，但不是想要的数据，因此要认为上一个收到的7E不是尾而是头
        s_rbuf_main.len = 1;
        continue;
_RECVBUF_RST:
        s_rbuf_main.len = 0;
        return; //// 140704, 可能是打印导致来不及处理?
    }
}

void s2_scan_recv_func(void)
{
    INT16S ch;
    INT16U len, tmpLen;

    if (SOCK_OK != Dal_SockGetState(SOCK_FD_BACK)) {        // 状态不正确，不予处理
        return;
    }

    while ((ch = DAL_SockReadByte(SOCK_FD_BACK)) != -1) {
        if (ch == 0x7e) {
            if (!s_rbuf_back.len) {  // 长度为0说明还未收到过头
                s_rbuf_back.rbuf[0] = ch;
                s_rbuf_back.len = 1;
            } else {                // 说明是帧尾,一帧数据收齐
                s_rbuf_back.rbuf[s_rbuf_back.len++] = ch;
                len = Deassemble(s_rbuf_back.rbuf, s_rbuf_back.rbuf + 1, s_rbuf_back.len-2, R_SOCK_LEN);

                //GS_TRACE("###Read sock:");
                //GS_LOGH(s_rbuf_back.rbuf, len);
                if (len < FRAME_HEAD_LEN) {// 说明解析有误
                    GS_TRACE("S2太短");
                    goto _R2_7E_ERR;//_R2_RST;
                }
                if (s_rbuf_back.rbuf[len-1] != ChkSum_XOR(&s_rbuf_back.rbuf[0], len - 1)) {
                    GS_TRACE("S2 CHK err:%d", ChkSum_XOR(&s_rbuf_back.rbuf[0], len - 1));
                    goto _R2_7E_ERR;//_R2_RST;
                }
                // TODO: 这里要剥去IMEI、长度啥的才行
                tmpLen = s_rbuf_back.rbuf[POS_CMD_ATTR] & 0x07;
                tmpLen = (tmpLen << 8) + s_rbuf_back.rbuf[POS_CMD_ATTR+1];
                //GS_TRACE("###len:%d,tmpLen:%d", len, tmpLen);

                if (len == (FRAME_HEAD_LEN + 1 + tmpLen)) {
                    APP_hdl_gprs_frame(FALSE, s_rbuf_back.rbuf[POS_CMD], &s_rbuf_back.rbuf[FRAME_HEAD_LEN], tmpLen);
                }
                goto _R2_RST;                
            }
        } else {
            if (s_rbuf_back.len) {  // 这说明是数据
                s_rbuf_back.rbuf[s_rbuf_back.len++] = ch;
                if (s_rbuf_back.len == R_SOCK_LEN) {// 缓冲区满，则把收到的数据舍弃
                    goto _R2_RST;
                }
            ////} else {
                //// 到这里则说明是垃圾数据，过滤
            }
        }
        continue;
_R2_7E_ERR:   // 已经收到所谓2个7E，但不是想要的数据，因此要认为上一个收到的7E不是尾而是头
        s_rbuf_back.len = 1;
        continue;
_R2_RST:
        s_rbuf_back.len = 0;
        return; //// 140704, 可能是打印导致来不及处理?
    }
}

static void _upload_pos_ack(BOOLEAN is_main, INT8U result)
{
    UP_PARA_NEW_T* para;
    FAIL_DEFAULT_T gfd;// 160615 add.

    GS_TRACE("%sUP发=%d", is_main?"主":"副", result);
    if (!is_main) {// 仅处理主中心的，副中心给的应答，就pass
        return;
    }

    DAL_PP_Read(GSM_FAIL_DEF_, (void*)&gfd);// 160615 add
    #if BL_EN == 0
    RESET_RETRY_T rst;
    DAL_PP_Read(RESET_RETRY_, (void*)&rst);
    #endif
    if (result == _SUCCESS) {
        char ccid[20] = {0};
        char* simCid;
        DAL_PP_Read(SIM_ICCID_, (void*)ccid);
        simCid =PORT_Get_iccid();
        if (simCid[0] != 0) {// 读到iccid
            if ((ccid[0] == 0) || (memcmp(simCid, ccid, 20) != 0)) {// 存储判断，不一致才存储
                DAL_PP_Store(SIM_ICCID_, (void *)simCid);
            }
        }

        #if BL_EN == 0
        if (rst.rst_retry != RESET_DEFAULT) {   // 发送成功要重置重发标志
            rst.rst_retry = RESET_DEFAULT;
            DAL_PP_Store(RESET_RETRY_, (void*)&rst);
        }
        #endif
#if NO_RESTORE_SHT == 0 // 不去恢复默认参数
        // 160615 add, 清除cur，
        if (gfd.cur != 0) {
            gfd.cur = 0;
            gfd.cnt = 15;
            DAL_PP_Store(GSM_FAIL_DEF_, (void*)&gfd);
        }
#endif
        // 160615 add end
        //#if (OUR_DEBUG > 0)
        //Requset_PIC_BaseGet(); // 140813 查看pic的参数
        //#endif
        return;
    }
    // 到这边说明没发送成功，存盲区
    PORT_GPRS_Deattach();//APP_Stop_ConnectGPRS();
    #if BL_EN > 0
    // 180603 增加了盲区补传
    if (s_bl_prepared && is_main)////if (_PrepareBlindNode(&s_cur_bl)) 
    {
        GS_TRACE("存BL");
        GS_LOGH(s_cur_bl.data, 20);// TODO: 待测试
        DAL_Blind_AddNode(&s_cur_bl);
        s_bl_prepared = FALSE;
    }
    #endif
#if NO_RESTORE_SHT > 0
    Request_PIC_Sleep(0);
    GS_TRACE("败睡");
    return;
#endif

    para = APP_GetUploadPara();

#if ALMFAIL_HDL_EN > 0  // 160605 add
    // 160131 add 报警状态下，发送失败则进入默认报警流程
    // 161127 change 
    if (ALM_CHECK_MASK & GetPicAlarmStatus()) {
        ALARM_HDL_T almHdl;
        SYSTIME_T tm;
        
        DAL_GetSysTime(&tm);
        if (IS_ALS_ALM & GetPicAlarmStatus()) {// 170614 add, 增加判断，如果只是震动报警的处理，不应关闭光感监测
            Requset_PIC_BaseSet(&tm, 210, 0);   // 禁止光敏防拆 160605 改为写死210
        }    
        #if ALMHDL_DEBUG > 0
        OURAPP_trace("默认ALMHDL,睡");
        #endif
        DAL_PP_Read(ALARM_HDL_, (void*)&almHdl);
        almHdl.cur = 0;
        DAL_PP_Store(ALARM_HDL_, (void*)&almHdl);

        #if MULTI_SHAKE_EN > 0  // 161221 add ,多时段的报警策略
        if (IS_EINT_ALM & GetPicAlarmStatus()) {
            MULTI_SHAKE_PARA_T shk;
            shk.mode = 0;
            Request_PIC_MultiShake(&shk);
        }
        #endif
        para->mode = MODE_POS_GPS;
        para->alm_type = 1;
        para->alm.DHM_1.dd = 0;
        para->alm.DHM_1.hh = almHdl.period/60;
        para->alm.DHM_1.mm = almHdl.period%60;
        para->max_worktime.hword = 210;
        Requset_PIC_AlarmParaSet(para);
        
        Request_PIC_Sleep(0);
        return;
    }
    // 160131 add end
#endif

#if 1   // WSD_EN == 0
    // 160615 add.
    if ((para->alm_type == 1) && (para->alm.HM[0] == 0) && (para->alm.HM[1] < 4) && 
        (gfd.cnt > 0) && (gfd.cnt < 0x30)) {// 有效才用
        if (gfd.cur < gfd.cnt) {// 尚未到次数则直接睡
            gfd.cur++;
            #if OUR_DEBUG > 0
            OURAPP_trace("f+cnt%d", gfd.cur);
            #endif
            DAL_PP_Store(GSM_FAIL_DEF_, (void*)&gfd);
        } else {                // 到次数则恢复成默认的23:58, 下次生效到pic
            #if OUR_DEBUG > 0
            OURAPP_trace("f+out");
            #endif
            gfd.cur = 0;
            DAL_PP_Store_RAM(GSM_FAIL_DEF_, (void*)&gfd);
            para->mode = MODE_POS_GPS;
            para->alm_type = 1;
            para->alm.HM[0] = 0;
            para->alm.HM[1] = 11;
            para->alm.HM[2] = 58;
            DAL_PP_Store(UPLOAD_PARA_, (void *)para);
        }
    }
    // 160615 add end
#endif


#if BL_EN == 0// 盲区生效的话，关闭短周期重启
    GS_TRACE("<rty=%d>", rst.rst_retry);
    
    if (0 == rst.rst_retry) {   // 准备短时重启而重发
        rst.rst_retry = RESET_DEFAULT;  // 用完重启次数，重置并休眠
        Request_PIC_Sleep(0);
    } else {
        rst.rst_retry--;
        switch (para->alm_type) {
        case 1:
            if (para->alm.HM[0] == 0) {
                if (para->alm.HM[1] >= 12) {
                    Request_PIC_Sleep(60);  // 1小时
                } else {
                    Request_PIC_Sleep(10);  // 10分钟
                }
            } else {
                Request_PIC_Sleep(120);     // 2小时
            }
            break;
        case 2: // 时段模式，就直接按10分钟重启
            Request_PIC_Sleep(10);  // 10分钟
            break;
        case 3: // 周模式，直接按2小时重启
        default:
            Request_PIC_Sleep(120);     // 2小时
            break;
        }
    }
    DAL_PP_Store(RESET_RETRY_, (void*)&rst);
#else
        // TODO: 待完善  Request_PIC_Sleep(0);// 启用盲区的话，关闭短周期重启，直接睡眠
#endif
}

/*****************************************************************************
 函 数 名  : APP_UploadPos
 功能描述  : 上报位置信息请求
 输入参数  : BOOLEAN is_main  是否为主通道
             GPS_DATA_T* gps  位置信息，没有则放空
             CELL_ID_T* cell  基站信息，没有则放空
             BOOLEAN tracked  是否追踪
             INT8U extra_bit  状态位。bit4=盲区补传,bit1=停车报警,bit0=拆卸报警
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2020年10月14日
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void APP_UploadPos(BOOLEAN is_main, GPS_DATA_T* gps, CELL_ID_T* cell, BOOLEAN tracked, INT8U extra_bit)
{
    INT8U i, cellnum;
    WIFI_AP_RESULT_T *ap;
    UP_PARA_NEW_T *para;

    INT16U tmpWord;
    int tmpdata;
    STREAM_T* strm;

    char ccid[20] = {0};
    char* simCid;


    if (!is_main) {
        if (!APP_2nd_server_valid()) {
            GS_TRACE("2ND空");
            s_back_got_ans = TRUE;  // 如果server无效，则无需等待back ans
            return;
        }
    }

    strm = _PrepareFrame(CMD_UPLOAD_POS);
#if LOCAL_POS_EPO > 0
    if ((gps) && (!tracked)) {// 已定位且非追踪
        EPO_POS_T epoPos;
        char* tmpPos;
        // 纬度
        if (gps->status_flag & LAT_SOUTH) {
            epoPos.epo_lat[0] = '-';    // 插入负号
            tmpPos = &epoPos.epo_lat[1];
        } else {
            tmpPos = epoPos.epo_lat;
        }
        sprintf(tmpPos, "%d.%06d", gps->D_lat / 1000000, gps->D_lat % 1000000);
        // 经度
        if (gps->status_flag & LONG_WEST) {
            epoPos.epo_long[0] = '-';    // 插入负号
            tmpPos = &epoPos.epo_long[1];
        } else {
            tmpPos = epoPos.epo_long;
        }
        sprintf(tmpPos, "%d.%06d", gps->D_long/1000000, gps->D_long%1000000);

        #if OUR_DEBUG > 0
        GS_TRACE("lat:%s,lon:%s", epoPos.epo_lat, epoPos.epo_long);
        #endif
        DAL_PP_Store(PREV_POS_, (void *)&epoPos);
        // TODO: 待测试191107
    }
#endif
    if (gps) {// 填充GPS信息
        OUR_WriteBYTE_Strm(strm, 0x81); // tag
        OUR_WriteBYTE_Strm(strm, 13);   // len
        OUR_WriteBYTE_Strm(strm, gps->status_flag);
        OUR_WriteLONG_Strm(strm, gps->D_lat);
        OUR_WriteLONG_Strm(strm, gps->D_long);
        OUR_WriteHWORD_Strm(strm, gps->alt);
        OUR_WriteBYTE_Strm(strm, gps->spd/10);
        OUR_WriteBYTE_Strm(strm, gps->direction / 2);
        
    }
    if (cell) {// 填充基站信息
        //GS_TRACE("LBS[%d]:%x,%d", cell->cell_num, cell->mcc,cell->mnc);
        #if 1   // (GSM_TYPE & LET_MASK)
        OUR_WriteBYTE_Strm(strm, 0xA6); // tag, 200906按小洪的意思从A3改为A6
        cellnum = cell->cell_num;
        if (cellnum > 6) cellnum = 6;   // 最大限定
        OUR_WriteBYTE_Strm(strm, 4+7*cellnum);   // len
        if (cell->mcc == 1120) {    // TODO: 模块有bug，出来的是1120，就是hex的460
            cell->mcc = 460;
        }
        OUR_WriteHWORD_Strm(strm, cell->mcc);
        OUR_WriteBYTE_Strm(strm, cell->mnc);
        OUR_WriteBYTE_Strm(strm, cellnum);
        for(i = 0; i < cellnum; i++) {
            OUR_WriteHWORD_Strm(strm,(INT16U)cell->cell[i].lac);
            OUR_WriteLONG_Strm(strm, (INT32U)cell->cell[i].cellid);
            tmpdata = cell->cell[i].signal;
            if (tmpdata <= 0) tmpdata = 1;
            OUR_WriteBYTE_Strm(strm, (INT8U)tmpdata);
        }
        #else
        OUR_WriteBYTE_Strm(strm, 0x83); // tag
        cellnum = cell->cell_num;
        if (cellnum > 6) cellnum = 6;   // 最大限定
        OUR_WriteBYTE_Strm(strm, 4+5*cellnum);   // len
        OUR_WriteHWORD_Strm(strm, cell->mcc);
        OUR_WriteBYTE_Strm(strm, cell->mnc);
        OUR_WriteBYTE_Strm(strm, cellnum);
        for(i = 0; i < cellnum; i++) {
            OUR_WriteHWORD_Strm(strm, (INT16U)cell->cell[i].lac);
            OUR_WriteHWORD_Strm(strm, (INT16U)(cell->cell[i].cellid % 65536));
            tmpdata = cell->cell[i].signal+110;   // 暂定
            if (tmpdata <= 0) tmpdata = 1;
            OUR_WriteBYTE_Strm(strm, (INT8U)tmpdata);
        }
        "this is for build error test"
        #endif
        // wifi跟随基站强行带上去
        if (tracked) {
            ap = PORT_get_cur_ap();
        } else {
            ap = PORT_get_wifi_ap();
        }
        if (ap->cnt) {
            if (ap->cnt > MAX_WIFI_AP) {
                cellnum = MAX_WIFI_AP;
            } else {
                cellnum = ap->cnt;
            }
            OUR_WriteBYTE_Strm(strm, 0x84); // tag
            OUR_WriteBYTE_Strm(strm, 1+7*cellnum); // len
            OUR_WriteBYTE_Strm(strm, cellnum);
            for (i=0; i<cellnum; i++) {
                OUR_WriteDATA_Strm(strm, ap->ap[i].mac, 6);
                OUR_WriteBYTE_Strm(strm, ap->ap[i].rssi);
            }
        }
    }
    
    // 填充版本号
    OUR_WriteBYTE_Strm(strm, 0x85);    // tag
    OUR_WriteBYTE_Strm(strm, strlen(PORT_GetVersion()));
    OUR_WriteSTR_Strm(strm, PORT_GetVersion());
    // 状态信息
    OUR_WriteBYTE_Strm(strm, 0x86);    // tag
    OUR_WriteBYTE_Strm(strm, 10);       // 150828 增加电压
    para = APP_GetUploadPara();
    /*#if OUR_DEBUG > 0   // 160605 add
    GS_LOGH((void*)para, sizeof(UP_PARA_NEW_T));
    #endif*/
    /*if (0 == (para->mode & 0xf0)) 
        i = 0x08;  // 表示已配置
    else */
        i = 0;
    if (tracked) {
        i |= 0x02;
    } else {
        i |= (para->mode & 0x03);
    }
    
    if (extra_bit & 0x10) i |= 0x10;// 盲区补传位
    if (extra_bit & 0x02) i |= 0x40;// 231109 add 移动静止

    OUR_WriteBYTE_Strm(strm, i);
    if (para->alm_type == 1) {
        OUR_WriteBYTE_Strm(strm, para->alm.HM[0]);
        OUR_WriteBYTE_Strm(strm, para->alm.HM[1]);
        OUR_WriteBYTE_Strm(strm, para->alm.HM[2]);
    } else if (para->alm_type == 3) {
        OUR_WriteBYTE_Strm(strm, 1);
        OUR_WriteBYTE_Strm(strm, para->alm.HM[1]);
        OUR_WriteBYTE_Strm(strm, para->alm.HM[2]);
    } else {// Mode2
        OUR_WriteBYTE_Strm(strm, 0);
        OUR_WriteBYTE_Strm(strm, para->alm.HM[0]);
        OUR_WriteBYTE_Strm(strm, para->alm.HM[1]);
    }
    OUR_WriteHWORD_Strm(strm, para->max_worktime.hword);
    tmpdata = PORT_Get_temperature();
    if (tmpdata < 0) tmpdata = 0x80 | (0 - tmpdata);    // 负号
    OUR_WriteBYTE_Strm(strm, (INT8U)tmpdata);
    if (tracked)
        tmpWord = GetVolt_Cur();
    else
        tmpWord = GetVolt_Max();
    
    tmpdata = (tmpWord *5) / 180;
    GS_TRACE("batt=%d,%d\r\nalm=%x,%x", tmpdata, tmpWord, extra_bit, para->eint_en);
    if (tmpdata > 100) tmpdata = 100;
    OUR_WriteBYTE_Strm(strm, (INT8U)tmpdata);
    OUR_WriteHWORD_Strm(strm, tmpWord);

    // 141116 统计时长等信息
    OUR_WriteBYTE_Strm(strm, 0x90);    // tag
    OUR_WriteBYTE_Strm(strm, 13);
    OUR_WriteLONG_Strm(strm, g_our_status.work_time);
    OUR_WriteLONG_Strm(strm, g_our_status.car_on_time);
    OUR_WriteHWORD_Strm(strm, g_our_status.start_cnt);
    OUR_WriteBYTE_Strm(strm, g_our_status.creg_time);
    OUR_WriteBYTE_Strm(strm, g_our_status.attach_time);
    OUR_WriteBYTE_Strm(strm, PORT_GSM_Get_SNR());   // 141203 增加信号强度
    
    // 150625 盲区补传需要带时间；报警则需带状态位
    /*//// 231111 DEL, 这种盲区方式已经弃用了 if (extra_bit & 0x10) {
        INT8U tm[6] = {23, 8, 9, 20, 0 ,0};
        DAL_GetSysTime((SYSTIME_T*)tm);
        OUR_WriteBYTE_Strm(strm, 0x82);    // tag
        OUR_WriteBYTE_Strm(strm, 6);
        OUR_WriteDATA_Strm(strm, tm, 6);
    }*/
    //if (extra_bit & ALM_CHECK_MASK) { // 160131 del
        OUR_WriteBYTE_Strm(strm, 0x87);    // tag
        OUR_WriteBYTE_Strm(strm, 2);
        OUR_WriteBYTE_Strm(strm, para->eint_en);    // mask

        //OUR_WriteBYTE_Strm(strm, extra_bit);
        OUR_WriteBYTE_Strm(strm, extra_bit&0xFD);  // 231109 屏蔽02的移动静止产生的误报警
    //}
    // 210114 add, 增加GPS定位时间的发送
    tmpWord = APP_GetGPSWorkSec();
    if (tmpWord > 255) tmpWord = 255;
    if (tmpWord) {
        OUR_WriteBYTE_Strm(strm, 0xA7);    // 小韦新增的tag，上传GPS定位时间
        OUR_WriteBYTE_Strm(strm, 3);
        OUR_WriteBYTE_Strm(strm, 0);    // 通信模块与gps通信异常次数,暂写死为1
        OUR_WriteBYTE_Strm(strm, (INT8U)tmpWord);
        OUR_WriteBYTE_Strm(strm, 0);    // 星历个数，暂时给0，后续的星历编号也不给了
    }

    DAL_PP_Read(SIM_ICCID_, (void*)ccid);
    simCid =PORT_Get_iccid();
    if (simCid[0] != 0) {// 读到iccid
        if ((ccid[0] == 0) || (memcmp(simCid, ccid, 20) != 0)) {// iccid不一致，则上报, 存储放到应答里判断，这样就不会丢点了
            OUR_WriteBYTE_Strm(strm, 0xA1);    // tag
            OUR_WriteBYTE_Strm(strm, 20);
            OUR_WriteDATA_Strm(strm, (INT8U*)simCid, 20);
            //DAL_PP_Store(SIM_ICCID_, (void *)simCid);
        }
    }

    #if WSD_EN > 0
    OUR_WriteBYTE_Strm(strm, 0xA2);    // tag
    OUR_WriteBYTE_Strm(strm, 0x04);
    simCid = app_get_wsd_value();// 借用变量
    for (i = 0; i < 4; i++) {
        OUR_WriteBYTE_Strm(strm, simCid[i]);
    }
    #endif
    #if BL_EN > 0
    // 231111 盲区数据准备放这里
    if (_PrepareBlindNode(gps, cell, &s_cur_bl)) {
        s_bl_prepared = TRUE;
    }
    #endif
    if (tracked) {
        _SendFrame(is_main, CMD_UPLOAD_POS, strm->StartPtr, strm->Len, NO_NEED_ACK);  // 紧急追踪模式下，漏点也没关系
    } else {
        _SendFrame(is_main, CMD_UPLOAD_POS, strm->StartPtr, strm->Len, 3, 15, _upload_pos_ack);
    }
}

void app_gprswork_init(void)
{
    GPRS_GROUP_T para = {TRUE, 9999, "ydzc.borcom.net"};
    DAL_GPRSInit();
    
    s_main_got_ans = FALSE;
    s_back_got_ans = FALSE;

    DAL_PP_Read(IP_DOM_MAIN_, (void *)&para);

    ////231115 del, 主ip必须得有 if (para.isvalid) {
        Dal_SockEnable(SOCK_FD_MAIN, &para);
    ////}
    // TODO: 副IP的也可以放这边初始化
    if (DAL_PP_Read(IP_DOM_BACK_, (void *)&para)) {
        if (para.isvalid && para.port && (para.ip[0] > 0x20)) {
            Dal_SockEnable(SOCK_FD_BACK, &para);
        }
    }
}


