/******************************************************************************

                  版权所有 (C), 2010-2014, 厦门摩天网络股份有限公司

 ******************************************************************************
  文 件 名   : app_pic_send.c
  版 本 号   : 初稿
  作    者   : rocky
  生成日期   : 2014年5月25日
  最近修改   :
  功能描述   : pic发送接口
  函数列表   :
              DelCell
              OUR_PIC_InitSend
              OUR_PIC_ListAck
              OUR_PIC_ListSend
              SendTmrProc
  修改历史   :
  1.日    期   : 2014年5月25日
    作    者   : rocky
    修改内容   : 创建文件

******************************************************************************/
#include "bk_os.h"
#include "bk_our_tools.h"
#include "bk_our_list.h"
#include "bk_app_picwork.h"
#include "bk_app_picsend.h"
#include "bk_port_uart.h"
#include "bk_port_hardware.h"

/*
********************************************************************************
* 宏定义
********************************************************************************
*/
#define NUM_MEM              10
#define MAX_SIZE             330       /* 数据长度限制 */

/*
********************************************************************************
* 定义模块数据结构
********************************************************************************
*/
typedef struct {
    INT8U type;                        /* 协议类型 */
    INT8U ct_send;                     /* 重发次数 */
    INT8U ct_time;                     /* 重发等待时间计数 */
    INT8U flowtime;                    /* 重发等待时间 */
    INT16U slen;                        /* 数据长度 */
    INT8U sptr[MAX_SIZE];              /* 数据空间 */
    void  (*fp)(INT8U);                /* 发送结果通知回调 */
} CELL_T;
/*
********************************************************************************
* 定义模块变量
********************************************************************************
*/
static struct {
    NODE   reserve;
    CELL_T cell;
} s_memory[NUM_MEM];

//static INT8U s_sendtmr;
static LIST_T s_waitlist, s_readylist, s_freelist;
static ASMRULE_T s_pic_rule = {0x7e, 0x7d, 0x02, 0x01};



/*******************************************************************
** 函数名:     DelCell
** 函数描述:   删除节点
** 参数:       [in] cell:链表节点
** 参数:       [in] result:结果
** 返回:       无
********************************************************************/
static void DelCell(CELL_T *cell, INT8U result)
{
    void (*fp)(INT8U result);
    
    fp = cell->fp;
    cell->slen = 0;
    OUR_AppendListEle(&s_freelist, (LISTMEM *)cell);            
    if (fp != 0) { 
        fp(result);
    }
}

// 201020 change, 发送队列扫描，放在task里做
void OUR_PIC_SendScanProc(void)
{
    CELL_T *cell, *next;

    if (OUR_ListItem(&s_waitlist) + OUR_ListItem(&s_readylist) == 0) {
        //StopTmr(s_sendtmr);
        return;
    }
       
    cell = (CELL_T *)OUR_GetListHead(&s_waitlist);                          /* 扫描等待链表，检测重传时间是否超时 */
    for (;;) {
        if (cell == 0) break;
        if (++cell->ct_time > cell->flowtime) {                             /* 重传时间超时 */
            cell->ct_time = 0;
            next = (CELL_T *)OUR_DelListEle(&s_waitlist,(LISTMEM *)cell);
            if (--cell->ct_send == 0) {                                     /* 超过最大重传次数 */
                DelCell(cell, _OVERTIME);
            } else {
                OUR_AppendListEle(&s_readylist, (LISTMEM *)cell);
            }
            cell = next;
        } else { 
            cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);
        }
    }

    cell = (CELL_T *)OUR_GetListHead(&s_readylist);                         /* 扫描准备就绪链表，检查等待时间是否超时 */
    for (;;) {
        if (cell == 0) break;
        if (++cell->ct_time > cell->flowtime) {                             /* 等待时间超时 */
            cell->ct_time = 0;
            if (cell->ct_send == 0) {
                next = (CELL_T *)OUR_DelListEle(&s_readylist, (LISTMEM *)cell); /* 从就绪链表中删除节点 */
                DelCell(cell, _OVERTIME);
                cell = next;
                continue;
            } else {
                cell->ct_send--;
            }
        }
        cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);
    }
    
    if (OUR_ListItem(&s_readylist) > 0) {                                   /* 就绪链表中存在待发送节点 */
        cell = (CELL_T *)OUR_GetListHead(&s_readylist);
        while (cell) {
            #if PIC_DEBUG > 0
            PIC_LOG("PIC发%x", cell->type);
            OURAPP_printhexbuf(cell->sptr, cell->slen);
            #endif
            PORT_UartWriteBlock(PIC_UART, cell->sptr, cell->slen);  // 发送数据

            next = (CELL_T *)OUR_DelListEle(&s_readylist, (LISTMEM *)cell);
            if (cell->ct_send > 0) {                                           /* 需要重传的数据帧 */
                cell->ct_time = 0;
                OUR_AppendListEle(&s_waitlist,(LISTMEM *)cell);
            } else {                                                           /* 无需继续重传的数据帧 */
                DelCell(cell, _SUCCESS);
            }
            cell = next;
        }
    }
}

/*******************************************************************
** 函数名:     OUR_PIC_InitSend
** 函数描述:   pic发送模块初始化
** 参数:       无
** 返回:       无
********************************************************************/
void OUR_PIC_InitSend(void)
{
    OUR_InitList(&s_waitlist);
    OUR_InitList(&s_readylist);
    OUR_InitMemList(&s_freelist, (LISTMEM *)s_memory, NUM_MEM, sizeof(s_memory[0]));
/*    
    s_sendtmr = CreateTmr(SendTmrProc);
#if PIC_DEBUG > 0
    // TODO: ASSERT(s_sendtmr != 0xff);
    if (s_sendtmr == INVALID_TMR) {
        OURAPP_trace("[%s]err! sendtmr=ff", __FUNCTION__);
    }
#endif*/
}

/*******************************************************************
** 函数名:     OUR_PIC_ListSend
** 函数描述:   PIC数据链表发送
** 参数:       [in] type: 协议类型
**             [in] body: 协议体指针, 必须是末尾已有校验和
**             [in] len:  协议长度
**             [in] ct_send:  发送次数
**             [in] ct_time:  重发等待时间, 单位秒
**             [in] fp:   发送结果通知
** 返回:       成功返回true，失败返回false
********************************************************************/
BOOLEAN OUR_PIC_ListSend(INT16U type, INT8U *body, INT16U len, INT8U ct_send, INT16U ct_time, void(*fp)(INT8U))
{
    CELL_T *cell;         
   
	if (len > MAX_SIZE) return false;                                   /* 数据帧长度过长 */
	
    if ((cell = (CELL_T *)OUR_DelListHead(&s_freelist)) != 0) {         /* 申请链表节点 */
        cell->type     = type;
        cell->ct_send  = ct_send;
        cell->flowtime = ct_time * 10 + 1;                             /* 等待重传时间 */
        cell->ct_time  = 0;
        cell->fp       = fp;
        cell->slen = OUR_AssembleByRules(cell->sptr, body, len, &s_pic_rule);

        if (cell->slen < 4) {
            OUR_AppendListEle(&s_freelist, (LISTMEM *)cell);             /* 重新放回已获取的空闲链表 */
            return false;
        }
        OUR_AppendListEle(&s_readylist, (LISTMEM *)cell);                /* 将新增节点放入就绪链表 */
        
        //if (!TmrIsRun(s_sendtmr)) {
        //    StartTmr(s_sendtmr, _100MS,  TRUE);
        //}
        
        return true;
    } else {
        return false;
    }
}

/*******************************************************************
** 函数名:     OUR_PIC_ListAck
** 函数描述:   PIC发送链表确认
** 参数:       [in] type: 协议类型
** 返回:       成功返回true，失败返回false
********************************************************************/
BOOLEAN OUR_PIC_ListAck(INT16U type)
{
    CELL_T  *cell;
    
    cell = (CELL_T *)OUR_GetListHead(&s_waitlist);
    for (;;) {                                                          /* 查找等待链表 */
        if (cell == 0) break;
        if (cell->type == type) {                                       /* 查找到匹配的节点 */
            OUR_DelListEle(&s_waitlist, (LISTMEM *)cell);
            DelCell(cell, _SUCCESS);
            return true;
        } else {
            cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);          /* 扫描下个节点 */
        }
    }

    cell = (CELL_T *)OUR_GetListHead(&s_readylist);                     /* 查找就绪链表 */
    for (;;) {
        if (cell == 0) break;
        if (cell->type == type) {                                       /* 查找到匹配节点 */
            OUR_DelListEle(&s_readylist, (LISTMEM *)cell);
            DelCell(cell, _SUCCESS);
            return true;
        } else {
            cell = (CELL_T *)OUR_ListNextEle((LISTMEM *)cell);          /* 扫描下个节点 */
        }
    }
    return false;        
}





