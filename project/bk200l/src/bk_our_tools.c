
#define GLOBAL_OUR_TOOLS    1
/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_task.h"
#include "bk_our_tools.h"


/*******************************************************************
** 函数名:     _Round_Init
** 函数描述:   初始化循环缓冲区
** 参数:       [in]  round:           循环缓冲区
**             [in]  mem:             循环缓冲区所管理的内存地址
**             [in]  memsize:         循环缓冲区所管理的内存字节数
** 返回:       无
********************************************************************/
void _Round_Init(ROUND_T *round, INT8U *mem, INT32U memsize)
{
    round->bufsize = memsize;
    round->used    = 0;
    round->bptr    = mem;
    round->eptr    = mem + memsize;
    round->wptr    = round->rptr = round->bptr;
}

/*******************************************************************
** 函数名:     _Round_Left
** 函数描述:   获取循环缓冲区中剩余的可用字节数
** 参数:       [in]  round:           循环缓冲区
** 返回:       剩余的可用字节数
********************************************************************/
INT32U _Round_Left(ROUND_T *round)
{
    return (round->bufsize - round->used);
}

/*******************************************************************
** 函数名:     _Round_GetReadData
** 函数描述:   读取但不减掉指针
** 参数:       [in]  round:         循环缓冲区
**             [out] optr:          读取的目标内存
**             [in]  maxlen:        目标内存的最大长度
** 返回:       真实读取到的长度
********************************************************************/
INT32U _Round_GetReadData(ROUND_T *round, INT8U* optr, INT32U maxlen)
{
    INT32U pos;
    if (round->used == 0) return 0;
    if (round->used < maxlen) maxlen = round->used;

    pos = round->eptr - round->rptr;
    if (pos <= maxlen) {
        memcpy(optr, round->rptr, pos);
        memcpy(optr+pos, round->bptr, maxlen-pos);
    } else {
        memcpy(optr, round->rptr, maxlen);
    }
    
    return maxlen;
}

/*******************************************************************
** 函数名:     _Round_ReleaseRead
** 函数描述:   释放一定的读空间.配合_Round_GetReadData使用
** 参数:       [in]  round:         循环缓冲区
**             [in]  jumplen:       欲释放的长度
** 返回:       真实释放的长度
********************************************************************/
INT32U _Round_ReleaseRead(ROUND_T *round, INT32U jumplen)
{
    INT32U pos;
    if (round->used == 0) return 0;
/*
    if (round->used < jumplen) {// 等于是清零
        jumplen = round->used;
        round->used = 0;
        round->rptr = round->wptr = round->bptr;
        return jumplen;
    } else {
        // TODO: 好好查查
    }
*/
    if (round->used < jumplen) jumplen = round->used;

    pos = round->eptr - round->rptr;
    
    if (pos <= jumplen) {
        round->rptr = round->bptr + (jumplen-pos);
    } else {
        round->rptr += jumplen;
    }
    round->used -= jumplen;
    
    return jumplen;
}

/*******************************************************************
** 函数名:     _Round_ReadByte
** 函数描述:   从循环缓冲区中读取一个字节的数据
** 参数:       [in]  round:           循环缓冲区
** 返回:       如读之前循环缓冲区中的使用字节数为0, 则返回-1;
**             否则, 返回读取到的字节
********************************************************************/
INT16S _Round_ReadByte(ROUND_T *round)
{
    INT16S ret;
    
    if (round->used == 0) {
        return -1;
    }
    ret = *round->rptr++;
    if (round->rptr >= round->eptr) {
        round->rptr = round->bptr;
    }
    round->used--;
    return ret;
}

/*******************************************************************
** 函数名:     _Round_WriteBlock
** 函数描述:   往循环缓冲区中写入一块数据单元
** 参数:       [in]  round:           循环缓冲区
**             [in]  bptr:            待写入数据块的指针
**             [in]  blksize:         待写入数据块的字节数
** 返回:       成功写入循环缓冲区的字节数
********************************************************************/
BOOLEAN _Round_WriteBlock(ROUND_T *round, INT8U *bptr, INT32U blksize)
{
    if(round == 0) {
        return FALSE;
    }
    
    if (blksize > _Round_Left(round)) {
        return FALSE;
    }
    
    for (; blksize > 0; blksize--) {
        *round->wptr++ = *bptr++;
        if (round->wptr >= round->eptr) {
            round->wptr = round->bptr;
        }
        round->used++;
    }
    
    return TRUE;
}

INT8U ChkSum_XOR(INT8U* msg, INT16U len)
{  
    INT8U chksum = 0;   
    INT16U i;

    for (i = 0; i < len; i++) {  
          chksum ^= msg[i];
      }  
      return chksum;  
}

/* 返回：累加和 */
INT8U ChkSum(INT8U *dptr, INT16U len)
{
    INT8U i, result = 0;

    for (i=0; i<len; i++) {
        result += *(INT8U *)(dptr+i);// 160412改写
    }
    return result;
}
/* 累加和的反码 */
INT8U ChkSum_N(INT8U *dptr, INT16U len)
{
    return (~ChkSum(dptr, len));
}

// 注意: 最高位一定为1, 故略去



// CRC-32 = X32 X26 X23 X22 X16 X11 X10 X8 X7 X5 X4 X2 X1 X0 
static const INT32U cnCRC_32 = 0x04C10DB7; 
static INT32U Table_CRC[256]; // CRC 表

// 构造 32 位 CRC 表 
static void BuildTable32(INT32U aPoly) 
{ 
    INT32U i, j; 
    INT32U nData; 
    INT32U nAccum; 

    for (i = 0; i < 256; i++) { 
        nData = (INT32U)(i << 24); 
        nAccum = 0; 
        for (j = 0; j < 8; j++) { 
            if ((nData ^ nAccum) & 0x80000000) 
                nAccum = (nAccum << 1) ^ aPoly; 
            else 
                nAccum <<= 1; 
            nData <<= 1; 
        } 
        Table_CRC[i] = nAccum; 
    } 
} 

/*******************************************************************
**  函数名:     CRC_32
**  函数描述:   计算 32 位 CRC-32 值
**  参数:       none
**  返回:       none
********************************************************************/
INT32U CRC_32(INT8U * aData, INT32U aSize) 
{ 
    INT32U i; 
    INT32U nAccum = 0; 

    BuildTable32(cnCRC_32); 
    for (i = 0; i < aSize; i++) 
        nAccum = (nAccum << 8) ^ Table_CRC[(nAccum >> 24) ^ *aData++]; 
    return nAccum; 
} 

// 分步计算CRC32, 开始初始化为2步
static INT32U s_Accum = 0;
void CRC_32_StepInit(void)
{
    BuildTable32(cnCRC_32);
    s_Accum = 0;
}

INT32U CRC_32_step(INT8U* aData, INT32U aSize)
{ 
    INT32U i; 

    for (i = 0; i < aSize; i++) 
        s_Accum = (s_Accum << 8) ^ Table_CRC[(s_Accum >> 24) ^ *aData++]; 
    return s_Accum;
}


#if 0
// CRC-CCITT = X16 X12 X5 X0，据说这个 16 位 CRC 多项式比上一个要好 
//static const INT16U cnCRC_CCITT = 0x1021; 
// 构造 16 位 CRC 表 
static void BuildTable16(INT16U aPoly)
{ 
    INT16U i, j; 
    INT16U nData; 
    INT16U nAccum; 

    for (i = 0; i < 256; i++) { 
        nData = (INT16U)(i << 8); 
        nAccum = 0; 
        for (j = 0; j < 8; j++) { 
            if ((nData ^ nAccum) & 0x8000) 
                nAccum = (nAccum << 1) ^ aPoly; 
            else 
                nAccum <<= 1; 
            nData <<= 1; 
        } 
        Table_CRC[i] = (INT32U)nAccum; 
    } 
} 

/*******************************************************************
**  函数名:     CRC_16
**  函数描述:   计算 16 位 CRC 值. cnCRC_CCITT 作为多项式因子
**  参数:       [in] adata: 待计算的数据指针
**              [in] asize: 数据长度
**  返回:       该数据的CRC16
********************************************************************/
INT16U CRC_16(INT8U *aData, INT32U aSize)
{ 
    INT32U i; 
    INT16U nAccum = 0; 

    BuildTable16(cnCRC_CCITT); // cnCRC_CCITT 作为多项式因子
    for (i = 0; i < aSize; i++) 
        nAccum = (nAccum << 8) ^ (INT16U)Table_CRC[(nAccum >> 8) ^ *aData++]; 
    return nAccum; 
} 
#endif

INT16U Deassemble(INT8U *dptr, INT8U *sptr, INT16U len, INT16U maxlen)
{
    INT16U rlen;
    INT8U prechar, curchar;

    rlen = 0;
    prechar = (INT8U)(~0x7d);
    for (; len > 0; len--) {
        curchar = *sptr++;
        if (len >= maxlen) {
            return 0;
        }
        if (prechar == 0x7d) {
            if (curchar == 0x02) {            /* 7d + 02 = 7e */
                prechar = (INT8U)(~0x7d);
                *dptr++ = 0x7e;
                rlen++;
            } else if (curchar == 0x01) {     /* 7d + 01 = 7d */
                prechar = (INT8U)(~0x7d);
                *dptr++ = 0x7d;
                rlen++;
            } else {
                return 0;
            }
        } else {
            if ((prechar = curchar) != 0x7d) {
                *dptr++ = curchar;
                rlen++;
            }
        }
    }
    return rlen;
}

INT16U OUR_AssembleByRules(INT8U *dptr, INT8U *sptr, INT16U len, ASMRULE_T *rule)
{
    INT8U  temp;
    INT16U rlen;
    
    if (rule == NULL) return 0;
    
    *dptr++ = rule->c_flags;
    rlen    = 1;
    for (; len > 0; len--) {
        temp = *sptr++;
        if (temp == rule->c_flags) {            /* c_flags    = c_convert0 + c_convert1 */
            *dptr++ = rule->c_convert0;
            *dptr++ = rule->c_convert1;
            rlen += 2;
        } else if (temp == rule->c_convert0) {  /* c_convert0 = c_convert0 + c_convert2 */
            *dptr++ = rule->c_convert0;
            *dptr++ = rule->c_convert2;
            rlen += 2;
        } else {
            *dptr++ = temp;
            rlen++;
        }
    }
    *dptr = rule->c_flags;
    rlen++;
    return rlen;
}

BOOLEAN FindProc_Entry(INT16U index, const FUNC_ENTRY_T *funcentry, INT16U num, INT8U *data, INT16U dlen)
{
    for (; num > 0; num--, funcentry++) {
        if (index == funcentry->index) {
            if (funcentry->proc_null) {
                funcentry->proc_null();
                return TRUE;
            }
            if (funcentry->proc_para) {
                funcentry->proc_para(data, dlen);
                return TRUE;
            }
        }
    }
    return FALSE;
}

INT8U HexToChar(INT8U sbyte)
{
    sbyte &= 0x0F;
    if (sbyte < 0x0A) return (sbyte + '0');
    else return (sbyte - 0x0A + 'A');
}

INT8U CharToHex(INT8U schar)
{
    if (schar >= '0' && schar <= '9') return (schar - '0');
    else if (schar >= 'A' && schar <= 'F') return (schar - 'A' + 0x0A);
    else if (schar >= 'a' && schar <= 'f') return (schar - 'a' + 0x0A);
    else return 0;
}

/* SAMPLE: '7','8','9','9' -----> 7899 */
INT32U AsciiToDec(INT8U *sptr, INT8U len)
{
    INT32U result;
    
    result = 0;
    for (; len > 0; len--) {
        result = result * 10 + CharToHex(*sptr++);
    }
    return result;
}

/*******************************************************************
**  函数名:     Char2_ToHex
**  函数描述:   将两个可见字符转换为hex_u8
**  参数:       [in] sch: 源可见字符指针
**  返回:       转换后的hex u8
********************************************************************/
INT8U Char2_ToHex(INT8U* sch)
{
    return ((CharToHex(sch[0])<<4) + CharToHex(sch[1]));
}

/*******************************************************************
**  函数名:     Char2_ToDec
**  函数描述:   将两个可见字符转换为十进制u8
**  参数:       [in] sch: 源可见字符指针
**  返回:       转换后的dec u8
********************************************************************/
INT8U Char2_ToDec(INT8U* sch)
{
    if(*sch < '0' || *sch > '9' || *(sch+1) < '0' || *(sch+1) > '9') return 0;
    return (((*sch - '0')*10) + (*(sch+1)-'0'));
}
/*******************************************************************
**  函数名:     Dec_ToChar2
**  函数描述:   将十进制u8转换为两个可见字符
**  参数:       [out] dptr: 目标指针
**              [in]  sch:  源dec数
**  返回:       none
********************************************************************/
void Dec_ToChar2(INT8U* dptr, INT8U sch)
{
    *dptr       = (sch % 100) / 10 + '0';
    *(dptr+1)   = (sch % 10) + '0';
}

INT16U HexToDec(INT8U *dptr, INT8U *sptr, INT16U len)
{
    INT16U i;
    INT8U  stemp;
    
    for (i = 0; i < len; i++) {
        stemp = *sptr++;
        *dptr++ = HexToChar(stemp / 10);
        *dptr++ = HexToChar(stemp % 10);
    }
    return (2 * len);
}
// 16 00 10 -> 10 00 0a
INT16U BCD2Hex(INT8U *dptr, INT8U *sptr, INT16U len)
{
    INT16U i;
    
    for (i = 0; i < len; i++) {
        dptr[i] = (sptr[i]&0x0f) + (sptr[i] >> 4)*10;
    }
    return len;
}
//时间 转BCD 值大于255的会有问题
// 19 06 0a --> 19 06 10
INT16U DataHex2BCD(INT8U *dptr, INT8U *sptr, INT16U len)
{
    INT16U i;
    
    for (i = 0; i < len; i++) {
        dptr[i] = ((sptr[i]/10)<<4) + (sptr[i]%10);
    }
    return len;
}


/*******************************************************************
**  函数名:     FFChar
**  函数描述:   从开头查找sptr中最近的ch下标
**  参数:       [in] sptr:  源指针
**              [in] ch:    匹配字符
**  返回:       第一个ch的下标
********************************************************************/
INT8U FFChar(INT8U *sptr, INT8U ch)
{
    INT8U pos = 0;
    while (*sptr != 0x0a) {
        if (*sptr++ == ch) return pos;
        pos++;
    }
    return 0;
}


/*******************************************************************
**  函数名:     FindCharPos
**  函数描述:   FFChar的增强版, 找到第n个匹配字符
**  参数:       [in] sptr:  源指针
**              [in] ch:    匹配字符
**              [in] numchar: 个数
**              [in] maxlen:数据长度
**  返回:       第numchar个ch的下标
********************************************************************/
INT16U FindCharPos(INT8U *sptr, char ch, INT8U numchar, INT16U maxlen)
{
    INT16U pos;
    
    if (maxlen == 0) return 0;
    pos = 0;
    for (;;) {
        if (*sptr++ == ch) {
            if (numchar == 0) break;
            else numchar--;
        }
        maxlen--;
        pos++;
        if (maxlen == 0) break;
    }
    return pos;
}

BOOLEAN SearchKeyWordFromHead(INT8U *ptr, INT16U maxlen, char *kw)
{
    if (*kw == 0) return FALSE;
    
    while(maxlen--) {
        if (*ptr++ == *kw++) {
            if (*kw == 0) return TRUE;
        } else {
            return FALSE;
        }
    }
    return FALSE;
}

/*******************************************************************
** 函数名:     OUR_GPS_UtcToWeekAndTow
** 函数描述:   根据输入的格林尼志日期和时间, 计算对应的GPS周和周秒
** 参数:       [in]  date:     输入格林尼志日期
**             [in]  time:     输入格林尼志时间
**             [out] week:     GPS周数, 自1980年1月6日零时起算, 至当前时间累计的周数，也即GPS周(七天为一周)
**             [out] tow:      GPS秒数, 以GPS周为准, 从一周起始时间累积至当前时间的秒数, 该值每周清零
** 返回:       无
********************************************************************/
static const INT8U S_MONTH[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
void OUR_GPS_UtcToWeekAndTow(DATE_T *date, TIME_T *time, INT16U *week, INT32U *tow)
{
    INT8U  i;
    INT16U days, year;

    year = date->year + 2000;
    days = (year - 1980)*365 + ((year - 1980 - 1)/4 + 1) - 5;

    for (i = 1; i < date->month; i++) {
        days += S_MONTH[i - 1];
    }

    if ((date->month > 2) && (date->year % 4) == 0) {
        days++;
    }
    days += date->day - 1;

    *week = days / 7;
    *tow  = (days % 7) * (24*3600) + time->hour * 3600 + time->minute * 60 + time->second;
}

/*********************************************************************************
** 函数名:     OUR_ConvertGmtToLocaltime
** 函数描述:   将格林尼治时间转换成本地时间
** 参数:       [in/out]  date:     日期
**             [in/out]  time:     时间
**             [in]      diftime:  时差
** 返回:       无
*********************************************************************************/
void OUR_ConvertGmtToLocaltime(DATE_T *date, TIME_T *time, INT8U diftime)
{
    INT8U curday;
    
    if (date->month == 2 && (date->year % 4) == 0) {
        curday = 29;
    } else {
        curday = S_MONTH[date->month - 1];
    }

    if (time->second >= 60) {
        time->second -= 60;
        time->minute++;
    }
    if (time->minute >= 60) {
        time->minute -= 60;
        time->hour++;
    }
    time->hour += diftime;
    if (time->hour >= 24) {
        time->hour -= 24;
        date->day++;
    }
    
    if (date->day > curday) {
        date->day = 1;
        date->month++;
    }
    if (date->month > 12) {
        date->month = 1;
        date->year++;
    }
}

/*********************************************************************************
** 函数名:     OUR_ConvertGmtToLocaltime
** 函数描述:   将本地时间换成格林尼治时间转
** 参数:       [in/out]  date:     日期
**             [in/out]  time:     时间
**             [in]      diftime:  时差
** 返回:       无
*********************************************************************************/
void OUR_ConvertLocaltimeToGmt(DATE_T *date, TIME_T *time, INT8U diftime)
{
    INT8U curday;
    
    if (date->month == 2 && (date->year % 4) == 0) {
        curday = 29;
    } else {
        curday = S_MONTH[date->month - 1];
    }

    if (time->second >= 60) {
        time->second -= 60;
        time->minute++;
    }
    if (time->minute >= 60) {
        time->minute -= 60;
        time->hour++;
    }
    if (time->hour < diftime) {
        if (date->day == 1) {
            if (date->month == 1) {
                date->year -=1;
                date->month = 12;
            } else {
                date->month--;
            }
            date->day = curday;////220825发现的bug date->day = S_MONTH[date->month - 1];
        } else {
            date->day--;
        }
        time->hour = 24 - (diftime - time->hour);
    } else {
        time->hour -=diftime;
    }
}

BOOLEAN DateValid(DATE_T *date)
{
    if (date->year > 99 || date->year < 24) return FALSE;
    if ((date->month > 12) || (date->month == 0)) return FALSE;
    if ((date->day > 31) || (date->day == 0)) return FALSE;
    else return TRUE;
}

BOOLEAN TimeValid(TIME_T *time)
{
    if (time->hour > 24)   return FALSE;
    if (time->minute > 60) return FALSE;
    if (time->second > 60) return FALSE;
    else return TRUE;
}

BOOLEAN LongitudeValid(INT8U *longitude)
{
    if (longitude[0] > 180) return FALSE;
    if (longitude[1] > 60)  return FALSE;
    if (longitude[2] > 99)  return FALSE;
    if (longitude[3] > 99)  return FALSE;
    else return TRUE;
}

BOOLEAN LatitudeValid(INT8U *latitude)
{
    if (latitude[0] > 90) return FALSE;
    if (latitude[1] > 60) return FALSE;
    if (latitude[2] > 99) return FALSE;
    if (latitude[3] > 99) return FALSE;
    else return TRUE;
}


