
#include "bk_os_task.h"
#include "bk_our_stream.h"


/*******************************************************************
** 函数名:     OUR_InitStrm
** 函数描述:   初始化数据流
** 参数:       [in]  Sp:              数据流
**             [in]  Bp:              数据流所管理内存地址
**             [in]  MaxLen:          数据流所管理内存字节数
** 返回:       成功或失败
********************************************************************/
BOOLEAN OUR_InitStrm(STREAM_T *Sp, STREAMMEM *Bp, INT32U MaxLen)
{
    if (Sp == 0) return FALSE;
	
    Sp->Len      = 0;
    Sp->MaxLen   = MaxLen;
    Sp->CurPtr   = Bp;
    Sp->StartPtr = Bp;
    return TRUE;
}

/*******************************************************************
** 函数名:     OUR_GetStrmLeftLen
** 函数描述:   获取数据流中剩余的可用字节数
** 参数:       [in]  Sp:              数据流
** 返回:       数据流剩余的可用字节数
********************************************************************/
INT32U OUR_GetStrmLeftLen(STREAM_T *Sp)
{
    if (Sp->MaxLen >= Sp->Len) {
        return (Sp->MaxLen - Sp->Len);
    } else {
        return 0;
    }
}

/*******************************************************************
** 函数名:     OUR_GetStrmLen
** 函数描述:   获取数据流中已用字节数
** 参数:       [in]  Sp:              数据流
** 返回:       数据流已用字节数
********************************************************************/
INT32U OUR_GetStrmLen(STREAM_T *Sp)
{
    return (Sp->Len);
}

/*******************************************************************
** 函数名:     OUR_GetStrmMaxLen
** 函数描述:   获取数据流缓存总长度
** 参数:       [in]  Sp:              数据流
** 返回:       数据流的最大长度
********************************************************************/
INT32U OUR_GetStrmMaxLen(STREAM_T *Sp)
{
    return (Sp->MaxLen);
}
/*******************************************************************
** 函数名:     OUR_GetStrmPtr
** 函数描述:   获取数据流当前读/写指针
** 参数:       [in]  Sp:              数据流
** 返回:       当前读/写指针
********************************************************************/
STREAMMEM *OUR_GetStrmPtr(STREAM_T *Sp)
{
    return (Sp->CurPtr);
}

/*******************************************************************
** 函数名:     OUR_GetStrmStartPtr
** 函数描述:   获取数据流所管理内存的地址
** 参数:       [in]  Sp:              数据流
** 返回:       所管理内存地址
********************************************************************/
STREAMMEM *OUR_GetStrmStartPtr(STREAM_T *Sp)
{
    return (Sp->StartPtr);
}

/*******************************************************************
** 函数名:     OUR_MovStrmPtr
** 函数描述:   移动数据流中读/写指针
** 参数:       [in]  Sp:              数据流
**             [in]  Len:             移动字节数
** 返回:       无
********************************************************************/
void OUR_MovStrmPtr(STREAM_T *Sp, INT32U Len)
{
    if (Sp != 0) {
        if ((Sp->Len + Len) <= Sp->MaxLen) {
            Sp->Len    += Len;
            Sp->CurPtr += Len;
        } else {
            Sp->Len = Sp->MaxLen;
        }
    }
}

/*******************************************************************
** 函数名:     OUR_WriteBYTE_Strm
** 函数描述:   往数据流中写入一个字节数据
** 参数:       [in]  Sp:              数据流
**             [in]  writebyte:       写入的数据
** 返回:       无
********************************************************************/
void OUR_WriteBYTE_Strm(STREAM_T *Sp, INT8U writebyte)
{
    if (Sp != 0){
        if (Sp->Len < Sp->MaxLen) {
            *Sp->CurPtr++ = writebyte;
            Sp->Len++;
        }
    }
}

/*******************************************************************
** 函数名:     OUR_WriteHWORD_Strm
** 函数描述:   往数据流中写入一个半字(16位)数据, 大端模式(高字节先写，低字节后写)
** 参数:       [in]  Sp:              数据流
**             [in]  writeword:       写入的数据
** 返回:       无
********************************************************************/
void OUR_WriteHWORD_Strm(STREAM_T *Sp, INT16U writeword)
{
    HWORD_UNION temp;
    
    temp.hword = writeword;
    OUR_WriteBYTE_Strm(Sp, temp.bytes.high);
    OUR_WriteBYTE_Strm(Sp, temp.bytes.low);
}

/*******************************************************************
** 函数名:     OUR_LE_WriteHWORD_Strm
** 函数描述:   往数据流中写入一个半字(16位)数据, 小端模式(低字节先写，高字节后写)
** 参数:       [in]  Sp:              数据流
**             [in]  writeword:       写入的数据
** 返回:       无
********************************************************************/
void OUR_LE_WriteHWORD_Strm(STREAM_T *Sp, INT16U writeword)
{
    HWORD_UNION temp;
    
    temp.hword = writeword;
    OUR_WriteBYTE_Strm(Sp, temp.bytes.low);     
    OUR_WriteBYTE_Strm(Sp, temp.bytes.high);
}

/*******************************************************************
** 函数名:     OUR_WriteLONG_Strm
** 函数描述:   往数据流中写入一个半字(32位)数据, 大端模式(高字节先写，低字节后写)
** 参数:       [in]  Sp:              数据流
**             [in]  writeword:       写入的数据
** 返回:       无
********************************************************************/
void OUR_WriteLONG_Strm(STREAM_T *sp, INT32U writelong)
{
    OUR_WriteHWORD_Strm(sp, writelong >> 16);
    OUR_WriteHWORD_Strm(sp, writelong);
}

/*******************************************************************
** 函数名:     OUR_LE_WriteLONG_Strm
** 函数描述:   往数据流中写入一个半字(32位)数据, 小端模式(低字节先写，高字节后写)
** 参数:       [in]  Sp:              数据流
**             [in]  writeword:       写入的数据
** 返回:       无
********************************************************************/
void OUR_LE_WriteLONG_Strm(STREAM_T *sp, INT32U writelong)
{
    OUR_LE_WriteHWORD_Strm(sp, writelong);
    OUR_LE_WriteHWORD_Strm(sp, writelong >> 16);    //高16位
}

/*******************************************************************
** 函数名:     OUR_WriteLF_Strm
** 函数描述:   往数据流中写入换行符, 即写入'\r'和'\n'
** 参数:       [in]  Sp:              数据流
** 返回:       无
********************************************************************/
void OUR_WriteLF_Strm(STREAM_T *Sp)
{
    OUR_WriteBYTE_Strm(Sp, CR);
    OUR_WriteBYTE_Strm(Sp, LF);
}

/*******************************************************************
** 函数名:     GpsStrm_WriteLF
** 函数描述:   往数据流中写入回车符, 即写入'\r''
** 参数:       [in]  Sp:              数据流
** 返回:       无
********************************************************************/
void OUR_WriteCR_Strm(STREAM_T *Sp)
{
    OUR_WriteBYTE_Strm(Sp, CR);
}

/*******************************************************************
** 函数名:     OUR_WriteSTR_Strm
** 函数描述:   往数据流中写入字符串
** 参数:       [in]  Sp:              数据流
**             [in]  Ptr:             写入的字符串指针
** 返回:       无
********************************************************************/
void OUR_WriteSTR_Strm(STREAM_T *Sp, char *Ptr)
{
    while(*Ptr) {
        OUR_WriteBYTE_Strm(Sp, *Ptr++);
    }
}

/*******************************************************************
** 函数名:     OUR_WriteDATA_Strm
** 函数描述:   往数据流中写入一块内存数据
** 参数:       [in]  Sp:              数据流
**             [in]  Ptr:             写入的数据块地址
**             [in]  Len:             写入的数据块字节数
** 返回:       无
********************************************************************/
void OUR_WriteDATA_Strm(STREAM_T *Sp, INT8U *Ptr, INT32U Len)
{
    while(Len--) {
        OUR_WriteBYTE_Strm(Sp, *Ptr++);
    }
}

/*******************************************************************
** 函数名:     OUR_ReadBYTE_Strm
** 函数描述:   从数据流中读取一个字节
** 参数:       [in]  Sp:              数据流
** 返回:       读取到的字节
********************************************************************/
INT8U OUR_ReadBYTE_Strm(STREAM_T *Sp)
{
    Sp->Len++;
    return (*Sp->CurPtr++);
}

/*******************************************************************
** 函数名:     OUR_ReadHWORD_Strm
** 函数描述:   从数据流中读取一个半字(16位)数据,大端模式(先读为高字节，后读为低字节)
** 参数:       [in]  Sp:              数据流
** 返回:       读取到的字
********************************************************************/
INT16U OUR_ReadHWORD_Strm(STREAM_T *Sp)
{
    HWORD_UNION temp;
	
    temp.bytes.high = OUR_ReadBYTE_Strm(Sp);
    temp.bytes.low  = OUR_ReadBYTE_Strm(Sp);
    return temp.hword;
}

/*******************************************************************
** 函数名:     OUR_LE_ReadHWORD_Strm
** 函数描述:   从数据流中读取一个半字(16位)数据,小端模式(先读为低字节，后读为高字节)
** 参数:       [in]  Sp:              数据流
** 返回:       读取到的字
********************************************************************/
INT16U OUR_LE_ReadHWORD_Strm(STREAM_T *Sp)
{
    HWORD_UNION temp;
	
    temp.bytes.low   = OUR_ReadBYTE_Strm(Sp);
    temp.bytes.high  = OUR_ReadBYTE_Strm(Sp);
    return temp.hword;
}

/*******************************************************************
** 函数名:     OUR_ReadLONG_Strm
** 函数描述:   从数据流中读取一个半字(32位)数据,大端模式(先读为高字节，后读为低字节)
** 参数:       [in]  Sp:              数据流
** 返回:       读取到的字
********************************************************************/
INT32U OUR_ReadLONG_Strm(STREAM_T *Sp)
{
    INT32U temp;
	
	temp = (OUR_ReadHWORD_Strm(Sp) << 16);
	temp += OUR_ReadHWORD_Strm(Sp);
    
    return temp;
}

/*******************************************************************
** 函数名:     OUR_LE_ReadLONG_Strm
** 函数描述:   从数据流中读取一个半字(32位)数据,小端模式(先读为低字节，后读为高字节)
** 参数:       [in]  Sp:              数据流
** 返回:       读取到的字
********************************************************************/
INT32U OUR_LE_ReadLONG_Strm(STREAM_T *Sp)
{
    INT32U temp;
	
	temp = OUR_LE_ReadHWORD_Strm(Sp);
	temp += (OUR_LE_ReadHWORD_Strm(Sp) << 16);
    
    return temp;
}

/*******************************************************************
** 函数名:     OUR_ReadDATA_Strm
** 函数描述:   从数据流中读取指定长度的数据内容
** 参数:       [in]  Sp:              数据流
**             [out] Ptr:             读取到的数据存放的内存地址
**             [in]  Len:             读取的数据长度
** 返回:       无
********************************************************************/
void OUR_ReadDATA_Strm(STREAM_T *Sp, INT8U *Ptr, INT32U Len)
{
    while(Len--)
    {
        *Ptr++ = OUR_ReadBYTE_Strm(Sp);
    }
}





