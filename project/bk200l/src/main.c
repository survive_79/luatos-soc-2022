/*
 * Copyright (c) 2022 OpenLuat & AirM2M
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "bk_os.h"
#include "bk_port_hardware.h"
#include "bk_port_public.h"
#include "bk_port_gnss.h"
#include "bk_port_gprs.h"
#include "bk_dal_gpsdrv.h"
#include "bk_dal_public.h"
#include "bk_dal_systime.h"
#include "bk_app_picwork.h"
#include "bk_app_gprssend.h"
#include "bk_app_gprswork.h"
#include "bk_app_bkwork.h"
#include "bk_our_tools.h"
#if BL_EN > 0
#include "bk_dal_blind.h"
#endif
#if APP_OTA_EN > 0
#include "bk_app_otawork.h"
#endif


void app_bk_work_init(void);



luat_rtos_task_handle task_handle;

static BOOLEAN s_m_sleep = FALSE;
// 目的是让main线程不再工作
void sleep_mainTask(void)
{
    s_m_sleep = TRUE;
}
/*
#include "luat_fs.h"

static int recur_fs(const char* dir_path)
{
    luat_fs_dirent_t *fs_dirent = LUAT_MEM_MALLOC(sizeof(luat_fs_dirent_t)*100);
    memset(fs_dirent, 0, sizeof(luat_fs_dirent_t)*100);

    int lsdir_cnt = luat_fs_lsdir(dir_path, fs_dirent, 0, 100);

    if (lsdir_cnt > 0)
    {
        char path[255] = {0};

        LUAT_DEBUG_PRINT("dir_path=%s, lsdir_cnt=%d", dir_path, lsdir_cnt);

        for (size_t i = 0; i < lsdir_cnt; i++)
        {
            memset(path, 0, sizeof(path));            

            switch ((fs_dirent+i)->d_type)
            {
            // 文件类型
            case 0:   
                snprintf(path, sizeof(path)-1, "%s%s", dir_path, (fs_dirent+i)->d_name);             
                LUAT_DEBUG_PRINT("\tfile=%s, size=%d", path, luat_fs_fsize(path));
                break;
            case 1:
                snprintf(path, sizeof(path)-1, "%s/%s/", dir_path, (fs_dirent+i)->d_name);
                recur_fs(path);
                break;

            default:
                break;
            }
        }        
    }

    LUAT_MEM_FREE(fs_dirent);
    fs_dirent = NULL;
    
    return lsdir_cnt;
}
*/


/***

static void hw_demoB_init(void)
{
	
}

static void dr_demoC_init(void)
{
	LUAT_DEBUG_PRINT("this dr demo1");
}

static void dr_demoD_init(void)
{
	LUAT_DEBUG_PRINT("this dr demo2");
}
*/

static void _led_100ms(void)
{
    static INT32U tick = 0;
    INT32U tmp;

    
    // BLUE灯的闪灯流程
    if (!PORT_GSM_IS_REG()) {// 找不到SIM卡，没网就长亮
        PORT_LED_BLUE(TRUE);
    } else {
        tmp = tick % 20;
        
        if ((tmp > 10) && (tmp < 12)) {
            PORT_LED_BLUE(TRUE);
        } else if ((tmp > 15) && (tmp < 27)) {
            if (PORT_GPRS_IS_ON()) {// 已经注册LTE了则2s内快闪2次
                PORT_LED_BLUE(TRUE);
            } else {                // 未注册LTE, 则2s快闪1次
                PORT_LED_BLUE(FALSE);
            }
        } else {
            PORT_LED_BLUE(FALSE);
        }
    }
    // GREEN灯表示定位
    if (PORT_Gnss_IsPowerOn()) {           // GPS闪灯流程
        tmp = tick % 20;
        if (DAL_GPS_POS_IsOK()) {
            if (tmp < 3) {              // 定位则2s快闪1次
                PORT_LED_GREEN(TRUE);
            } else {
                PORT_LED_GREEN(FALSE);
            }
        } else {                        // GPS未定位，则2s间隔亮灭
            if (tmp < 10) {
                PORT_LED_GREEN(TRUE);
            } else {
                PORT_LED_GREEN(FALSE);
            }
        }
    } else {                            // LBS闪灯流程, 有几个基站就闪几下 0.1s闪一下
        tmp = tick % 30;
        if (tmp < g_cell.cell_num*2) {
            if (tmp % 2) {
                PORT_LED_GREEN(TRUE);
            } else {
                PORT_LED_GREEN(FALSE);
            }
        } else {
            PORT_LED_GREEN(FALSE);
        }
    }
    tick++;
}

static void bk_main(void *param)
{
    INT32U tick = 0;
    
    /* 
		出现异常后默认为死机重启
		demo这里设置为LUAT_DEBUG_FAULT_HANG_RESET出现异常后尝试上传死机信息给PC工具，上传成功或者超时后重启
		如果为了方便调试，可以设置为LUAT_DEBUG_FAULT_HANG，出现异常后死机不重启
		但量产出货一定要设置为出现异常重启！！！！！！！！！
	*/
	luat_debug_set_fault_mode(LUAT_DEBUG_FAULT_RESET);
    s_m_sleep = FALSE;
    
    OURAPP_InitTrace(1);
    DAL_PP_Init();

    #if BL_EN > 0
    DAL_BlindInit();
    //OURAPP_trace("剩余空间:%d", ql_internal_fs_free_size_get());
    #endif
    //luat_fs_init(); // 必须先初始化    
    //print_fs_info();
    //recur_fs("/");

    DAL_InitSysTime();
    InitPic_work();
    app_bk_work_init();// 放这边初始化，让打印慢一些

    OURAPP_trace("[%s],SN=%s", PORT_GetVersion(), PORT_GetIMEI());
    
	while(1) {
        App_Pic_PollFunc_100ms();
        tick++;
        if (s_m_sleep) {
            PORT_LED_GREEN(0);
            PORT_LED_BLUE(0);
        } else {
            _led_100ms();
            APP_GPRSSendTmrProc();
            APP_socket_scan_recv_func();
            s2_scan_recv_func();
            if ((tick % 10) == 0) {
                app_bk_work_poll_1s();
                #if APP_OTA_EN > 0
                OTA_tmr_func();
                #endif
            }
        }
        BK_Sleep(100);
        /*
        PORT_LED_GREEN(TRUE);
        PORT_LED_BLUE(FALSE);
		luat_rtos_task_sleep(500);
		//LUAT_DEBUG_PRINT("task loop");

		// 去掉下面的注释, 可以关闭日志打印
		// luat_debug_print_onoff(0);
		PORT_LED_GREEN(FALSE);
        PORT_LED_BLUE(TRUE);
		luat_rtos_task_sleep(1500);
        PORT_GetVoltage();
        */
	}
}

static void bk_main_init(void)
{
	luat_rtos_task_create(&task_handle, 4*1024, 10, "BMain", bk_main, NULL, 0);
}

/*
    加入头文件common_api.h
    类似于void main(void)，但是可以任意名字，不能有入口参数和返回参数，同时允许多个入口
    通过INIT_HW_EXPORT INIT_DRV_EXPORT INIT_TASK_EXPORT这3个宏修饰后，系统初始化的时候会调用function，不同的宏涉及到调用顺序不同
    大优先级上INIT_HW_EXPORT > INIT_DRV_EXPORT > INIT_TASK_EXPORT
    这3个宏有统一用法INIT_HW_EXPORT(function, level)，其中function就是入口函数，level是小优先级，填"0","1","2"...(引号要写的)，引号里数字越小，优先级越高。
    INIT_HW_EXPORT一般用于硬件初始化，GPIO之类的，可以没有
    INIT_DRV_EXPORT一般用于外设驱动初始化，初始化外部器件之类的，打开电源，提供时钟之类的，可以没有
    INIT_TASK_EXPORT一般用于初始化任务，用户代码基本上都是跑在任务里，原则上必须有

*/

// 初始化硬件，例如串口、GPIO，启动位置硬件初始1级
INIT_HW_EXPORT(PORT_GPIO_Init, "1");
//启动hw_demoB_init，启动位置硬件初始2级
//INIT_HW_EXPORT(hw_demoB_init, "2");
//启动dr_demoC_init，启动位置驱动1级
//INIT_DRV_EXPORT(dr_demoC_init, "1");
//启动dr_demoD_init，启动位置驱动2级
//INIT_DRV_EXPORT(dr_demoD_init, "2");
//启动bk_main_init，启动位置任务1级
INIT_TASK_EXPORT(bk_main_init, "0");
//INIT_TASK_EXPORT(app_bk_work_init, "2");
//INIT_TASK_EXPORT(PORT_Gnss_MspInit, "3");

