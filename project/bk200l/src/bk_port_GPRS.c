/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : port_gprs.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2022年12月21日, 星期三
  最近修改   :
  功能描述   : 网络服务
  函数列表   :
              PORT_GPRS_Init
              PORT_GPRS_Resume
              PORT_GPRS_Suspend
  修改历史   :
  1.日    期   : 2022年12月21日, 星期三
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/


/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#define GLOBAL_PORT_GPRS 
#include "luat_wifiscan.h"
#include "luat_mobile.h"
#include "luat_network_adapter.h"
#include "net_lwip.h"

#include "bk_our_tools.h"
#include "bk_port_hardware.h"
#include "bk_port_gprs.h"
#include "bk_dal_public.h"
#include "bk_dal_systime.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/
static void _wifi_scan_func(void);
/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define MAX_GPRS_INIT_CNT   360     // 180秒

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef enum {
    GPRS_STATE_REGISTER = 0,
    ////GPRS_STATE_CONFIG,
    GPRS_STATE_ACTIVATE,
    GPRS_STATE_ACTIVATING,
    //GPRS_STATE_GET_DNSADDRESS, // 把这2步合并算了
    //GPRS_STATE_GET_LOCALIP,
    //GPRS_STATE_SOC_REG,         // 注册soc回调接口
    GPRS_STATE_OK,
    GPRS_STATE_ERR,             // 新增一个出错处理入口
    GPRS_STATE_NULL,            // 新增一个NULL状态，用来睡眠
} GPRS_STATE_E;
/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
luat_rtos_task_handle netlink_task_ref;
static BOOLEAN  s_cfun = 0;

static GPRS_STATE_E s_gprs_state = GPRS_STATE_NULL;

static int s_pdp_OK;  // 在_datacall_indicate_cb的ctx形参，应该可以作为cgreg的判断

static int s_gprs_init_cnt;

static int s_csq = 0;
static BOOLEAN s_creg = FALSE;
static BOOLEAN s_wifi_opened = FALSE;
static WIFI_AP_RESULT_T s_wifi_ap;  // 最终给的wifi值
static WIFI_AP_RESULT_T curAp;      // 当前wifi状态

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/


// 网络的一些回调
static void _mobile_event_cb(LUAT_MOBILE_EVENT_E event, uint8_t index, uint8_t status)
{
	switch(event)
	{
	case LUAT_MOBILE_EVENT_CFUN:
		GPRS_TRACE("cfun status=%d", status);
		break;
	case LUAT_MOBILE_EVENT_SIM:
		switch(status)
		{
		case LUAT_MOBILE_SIM_READY:
			GPRS_TRACE("sim ready");
			break;
		case LUAT_MOBILE_NO_SIM:
			GPRS_TRACE("sim not insert");
			break;
		case LUAT_MOBILE_SIM_NEED_PIN:
			GPRS_TRACE("sim need pin");
			break;
		}
		break;
	case LUAT_MOBILE_EVENT_REGISTER_STATUS:
		if (status == LUAT_MOBILE_STATUS_REGISTERED_ROAMING || status == LUAT_MOBILE_STATUS_REGISTERED) {
		    s_creg = TRUE;
		} else {
		    s_creg = FALSE;
		}
		GPRS_TRACE("CREG=%d", status);
		break;
	case LUAT_MOBILE_EVENT_CELL_INFO:
		switch(status)
		{
		case LUAT_MOBILE_CELL_INFO_UPDATE:
			break;
		case LUAT_MOBILE_SIGNAL_UPDATE:
            PORT_GSM_Get_SNR(); // 更新一下csq
			break;
		}
		break;
	case LUAT_MOBILE_EVENT_PDP:
        GPRS_TRACE("pdp event, cid %d, status %d", index, status);
        ////g_s_cid = index;
		break;
	case LUAT_MOBILE_EVENT_NETIF:
		switch (status)
		{
		case LUAT_MOBILE_NETIF_LINK_ON:
			GPRS_TRACE("netif link on!");
			//g_s_is_link_up = 1;
			s_pdp_OK = 1;
            s_gprs_state = GPRS_STATE_OK;
			break;
		default:
			GPRS_TRACE("netif link off!");
			//g_s_is_link_up = 0;
			s_pdp_OK = 0;
            s_gprs_state = GPRS_STATE_REGISTER;
			break;
		}
		break;
	case LUAT_MOBILE_EVENT_TIME_SYNC:
		GPRS_TRACE("time sync event");
        app_set_pic_time();    // 这里判断是否需要休眠
		break;
	case LUAT_MOBILE_EVENT_CSCON:
		GPRS_TRACE("rrc event, status %d", status);
		break;
	default:
		break;
	}
}
#if 0
static void _network_indicate_cb(uint8_t nSim, unsigned int ind_type, void *ctx)
{
	char csq = 99;
    unsigned char qlCsq;
	ql_nw_common_reg_status_info_s *quec_nw_msg = NULL;
    ql_nw_errcode_e ret;
    ql_nw_signal_strength_info_s signal_info = {0};
	
	////GPRS_TRACE("nSim=%d,ind_type=%x", nSim, ind_type);
	switch(ind_type)
	{

		case QUEC_NW_NITZ_TIME_UPDATE_IND:
            if (ctx) {
                ql_nw_nitz_time_info_s *quec_nw_nitz_time_info = NULL;
                SYSTIME_T tm;
                int y,m,d, a,b,c;
    			quec_nw_nitz_time_info = (ql_nw_nitz_time_info_s *)ctx;
	    		//GPRS_TRACE("nitz_time=%s,abs_time=%ld", quec_nw_nitz_time_info->nitz_time, quec_nw_nitz_time_info->abs_time);
                // 网络授时OK
                /*真实打印
                05-19_15:44:52->网络时间:23/05/19 15:44:53 +32 0
                05-19_15:44:52->tm=23-5-19 15:44:53
                */
                //GPRS_TRACE("网络时间:%s", quec_nw_nitz_time_info->nitz_time);
                sscanf(quec_nw_nitz_time_info->nitz_time, "%d/%d/%d %d:%d:%d", &y,&m,&d,&a,&b,&c);
                GPRS_TRACE("ntm=%d-%d-%d %d:%d", y,m,d,a,b);
                tm.date.year    = y;
                tm.date.month   = m;
                tm.date.day     = d;
                tm.time.hour    = a;
                tm.time.minute  = b;
                tm.time.second  = c;
                DAL_SetSysTime(&tm, TRUE);
            }
            
			break;
	}
	
}

/* 目前尝试发现回调无效*/
static void _datacall_indicate_cb(uint8_t nSim, unsigned int ind_type, int profile_idx, bool result, void *ctx)
{
	int *active_state = ctx;
    
////	GPRS_TRACE("Sim=%d, profile_idx=%d, ind_type=0x%x, result=%d", nSim, profile_idx, ind_type, result);
	if(QUEC_DATACALL_ACT_RSP_IND == ind_type && result == TRUE) {
		*active_state = 1;  // 网络拨号成功
	} else {
	    *active_state = 0;  // 网络断开？
	}
}
#endif
// 探询基站啥的
void _net_poll_func(void)
{
    int i;
	luat_mobile_cell_info_t cell_info;
    
    luat_mobile_get_cell_info(&cell_info);

    if (cell_info.lte_info_valid && cell_info.lte_service_info.cid) {
		/*GPRS_TRACE("4G! mcc=%x mnc=%x cid=%u band %d tac %u pci %u earfcn %u is_tdd %d rsrp %d rsrq %d snr %d rssi %d",
					cell_info.lte_service_info.mcc, cell_info.lte_service_info.mnc, cell_info.lte_service_info.cid,
					cell_info.lte_service_info.band, cell_info.lte_service_info.tac, cell_info.lte_service_info.pci, cell_info.lte_service_info.earfcn,
					cell_info.lte_service_info.is_tdd, cell_info.lte_service_info.rsrp, cell_info.lte_service_info.rsrq,
					cell_info.lte_service_info.snr, cell_info.lte_service_info.rssi);*/
        if (cell_info.lte_neighbor_info_num > MAX_CELL_CNT-1) {
            g_cell.cell_num = MAX_CELL_CNT;
        } else {
            g_cell.cell_num = cell_info.lte_neighbor_info_num;
        }
        //GPRS_TRACE("个数=%d,%d", g_cell.cell_num, cell_info.lte_neighbor_info_num);
        g_cell.mcc = cell_info.lte_service_info.mcc;  // 一般0是serving基站，其他是相邻基站
        g_cell.mnc = cell_info.lte_service_info.mnc;
        g_cell.cell[0].cellid = cell_info.lte_service_info.cid;
        g_cell.cell[0].lac = cell_info.lte_service_info.tac;
        g_cell.cell[0].signal = cell_info.lte_service_info.rssi+140;
        GPRS_TRACE("4G:%d个mcc=%x,mnc=%x,cid=%d tac=%d snr=%d", g_cell.cell_num, g_cell.mcc, g_cell.mnc,
            g_cell.cell[0].cellid, g_cell.cell[0].lac, g_cell.cell[0].signal);
/*
4G! mcc=460 mnc=0 cid=108225834 band 40 tac 24815 pci 186 earfcn 38950 is_tdd 1 rsrp -97 rsrq -8 snr 28 rssi -69!
个数=4,3!
4G[1]cid=108225834 tac=24815 snr=28!
4G[2]cid=108225834 tac=24815 snr=28!
4G[3]cid=99664947 tac=24337 snr=0!*/
        for (i = 1; i < g_cell.cell_num; i++) {//1~7放邻居基站信息, 0从打印看，和主基站一致
            g_cell.cell[i].cellid = cell_info.lte_info[i].cid;
            g_cell.cell[i].lac = cell_info.lte_info[i].tac;
            g_cell.cell[i].signal = cell_info.lte_info[i].rsrp+169; // rsrp和rssi是小29的关系

            //GPRS_TRACE("4G[%d]cid=%d tac=%d snr=%d", i, 
            //    g_cell.cell[i].cellid, g_cell.cell[i].lac, g_cell.cell[i].signal);
        }
    } else if (cell_info.gsm_info_valid && cell_info.gsm_service_info.cid) {
        
        if (cell_info.gsm_neighbor_info_num > MAX_CELL_CNT-1) {
            g_cell.cell_num = MAX_CELL_CNT;
        } else {
            g_cell.cell_num = cell_info.gsm_neighbor_info_num+1;
        }
        GPRS_TRACE("2G!%d个 mcc=%x mnc=%x cid=%u lac=%u arfcn %u bsic %d rssi %d",g_cell.cell_num,
					cell_info.gsm_service_info.mcc, cell_info.gsm_service_info.mnc, cell_info.gsm_service_info.cid,
					cell_info.gsm_service_info.lac, cell_info.gsm_service_info.arfcn,
					cell_info.gsm_service_info.bsic, cell_info.gsm_service_info.rssi);
        
        g_cell.mcc = cell_info.gsm_service_info.mcc;  // 一般0是serving基站，其他是相邻基站
        g_cell.mnc = cell_info.gsm_service_info.mnc;
        g_cell.cell[0].cellid = cell_info.gsm_service_info.cid;
        g_cell.cell[0].lac = cell_info.gsm_service_info.lac;
        g_cell.cell[0].signal = cell_info.gsm_service_info.rssi+111;
        for (i = 1; i < g_cell.cell_num; i++) {//1~7放邻居基站信息
            g_cell.cell[i].cellid = cell_info.gsm_info[i-1].cid;
            g_cell.cell[i].lac = cell_info.gsm_info[i-1].lac;
            g_cell.cell[i].signal = cell_info.gsm_info[i-1].rssi+111;
            //GPRS_TRACE("2G[%d]cid=%d lac=%d rssi=%d",
            //    cell_info.gsm_info[i-1].cid,cell_info.gsm_info[i-1].lac, cell_info.gsm_info[i-1].rssi);
        }
    }
}

// 500MS左右执行一次
static BOOLEAN _GPRS_Monitor_Func(void)
{
    static GPRS_STATE_E lastSt = GPRS_STATE_NULL;

    #if 1
    if (lastSt != s_gprs_state) {
        if (s_gprs_state == GPRS_STATE_OK) {
            ip_addr_t ipv4;
        	ip_addr_t ipv6;
            luat_mobile_get_local_ip(0, BK_NW_IDX, &ipv4, &ipv6);
            GPRS_TRACE("找网OK!");
			if (ipv4.type != 0xff)
			{
				GPRS_TRACE("IPV4 %s", ip4addr_ntoa(&ipv4.u_addr.ip4));
			}
			if (ipv6.type != 0xff)
			{
				GPRS_TRACE("IPV6 %s", ip6addr_ntoa(&ipv4.u_addr.ip6));
			}
            
        }
        lastSt = s_gprs_state;
    }
    if (lastSt == GPRS_STATE_OK) return TRUE;
    else return FALSE;
    
    #else
    ql_datacall_errcode_e ret;
    static int net_tick;
    // TODO: 超时要重新实现了
    if ((s_gprs_state != GPRS_STATE_OK) && (s_gprs_state != GPRS_STATE_NULL)) {
        s_gprs_init_cnt++;
        if (s_gprs_init_cnt > MAX_GPRS_INIT_CNT) {
            GPRS_TRACE("网络超时"); // TODO: 超时出错，应该重拨
            s_gprs_state = GPRS_STATE_ERR;
        }
    }
    
    switch (s_gprs_state) {
    case GPRS_STATE_REGISTER:// 注册
        GPRS_TRACE("网络注册");
        ql_nw_register_cb(_network_indicate_cb);
    
        s_pdp_OK = 0;
        //ql_datacall_register_cb(QL_DATACALL_REGISTER_ALL_SIM, QL_DATACALL_REGISTER_ALL_PDP, _datacall_indicate_cb, &s_pdp_OK);
        ret = ql_datacall_register_cb(0, BK_NW_IDX, _datacall_indicate_cb, &s_pdp_OK);// 仅对sim0的pdp1进行注册。目前就用到1个链接
        if (ret != QL_DATACALL_SUCCESS) {
            GPRS_TRACE("注册回调失败:%d", ret);
            goto _GPRS_ERROR;
        }
        s_gprs_state = GPRS_STATE_ACTIVATE;
        break;
    case GPRS_STATE_ACTIVATE:// 激活上下文
        ql_set_data_call_asyn_mode(0, BK_NW_IDX, 1);
        GPRS_TRACE("激活PDP");
        if (1) {
            APN_PARA_T apn;
            memset(&apn, 0, sizeof(apn));
        
            if (!DAL_PP_Read(APN_, (void*)&apn)) {
                strcpy(apn.apn, "CMIOT");
            }
            // 231128 印尼的APN需求。因为国内都是可能不需要APN，故这样试一试
            ////ret = ql_start_data_call(0, BK_NW_IDX, QL_DATA_TYPE_IP, "m2minternet", "", "", QL_DATA_AUTH_TYPE_NONE);
            ret = ql_start_data_call(0, BK_NW_IDX, QL_DATA_TYPE_IP, apn.apn, apn.user, apn.pswd, QL_DATA_AUTH_TYPE_NONE);
            GPRS_TRACE("APN=%s %s %s", apn.apn, apn.user, apn.pswd);
        }
        
        if (ret != QL_DATACALL_SUCCESS) {
            GPRS_TRACE("激活PDP失败:%d", ret);
            goto _GPRS_ERROR;
        }
        s_gprs_state = GPRS_STATE_ACTIVATING;
        s_gprs_init_cnt = 0;    // 给足时间
        break;
    case GPRS_STATE_ACTIVATING:
        /*ret = ql_get_data_call_info(0, BK_NW_IDX, &local_dc_info);
        if (local_dc_info.ip_version == QL_DATA_TYPE_IPV6) {//ipv6
            GPRS_TRACE("IPv6=%s", ql_ip6addr_ntoa(&local_dc_info.v6.addr.ip));
            if (local_dc_info.v6.state == 1) {
                s_pdp_OK = TRUE;
            }
        } else if (local_dc_info.ip_version == QL_DATA_TYPE_IP) {// ipv4
            GPRS_TRACE("IPv4=%s", ql_ip4addr_ntoa(&local_dc_info.v4.addr.ip));
            if (local_dc_info.v4.state == 1) {
                s_pdp_OK = TRUE;
            }
        } else {
            GPRS_TRACE("cid没激活");
            ql_rtos_task_sleep_ms(1000);
            break;
        }*/
        if (s_pdp_OK) {
            s_gprs_state = GPRS_STATE_OK;
            net_tick = 0;
        }
        GPRS_TRACE("激活=%d", s_pdp_OK);
        break;
    case GPRS_STATE_OK:
        s_gprs_init_cnt = 0;
        if (net_tick++ > 20) {  // 每隔20s探询一下datacall状态
            net_tick = 0;
            ret = ql_get_data_call_info(0, BK_NW_IDX, &local_dc_info);
            if (local_dc_info.ip_version == QL_DATA_TYPE_IPV6) {//ipv6
                GPRS_TRACE("IPv6:%s,%d", ql_ip4addr_ntoa(&local_dc_info.v4.addr.ip), local_dc_info.v4.state);
                if (local_dc_info.v6.state == 1) {
                    s_pdp_OK = TRUE;
                } else {
                    s_pdp_OK = FALSE;
                }
            } else if (local_dc_info.ip_version == QL_DATA_TYPE_IP) {// ipv4
                GPRS_TRACE("IP:%s,%d", ql_ip4addr_ntoa(&local_dc_info.v4.addr.ip), local_dc_info.v4.state);
                if (local_dc_info.v4.state == 1) {
                    s_pdp_OK = TRUE;
                } else {
                    s_pdp_OK = FALSE;
                }
            } else {
                GPRS_TRACE("cid异常");
                s_pdp_OK = FALSE;
            }
        }
        if (!s_pdp_OK) {
            GPRS_TRACE("掉线%d", s_pdp_OK);
            s_gprs_state = GPRS_STATE_ERR;
        } else {
            return TRUE;    
        }
        break;
    case GPRS_STATE_ERR:
_GPRS_ERROR:
        GPRS_TRACE("net err");
        PORT_GPRS_Deattach(); // TODO: 超时出错，应该重拨
        ql_rtos_task_sleep_ms(2000);
        s_gprs_state = GPRS_STATE_REGISTER;
        s_gprs_init_cnt = 0;
        break;
    case GPRS_STATE_NULL:
    default:
        // 啥也不干
        break;
    }
    return FALSE;
    #endif
}

static void _netst_task(void* para)
{
    INT32U cnt = 0;
    s_cfun = 1; // 默认是开机

    PORT_GPRS_Attach();
    
    while (1) {
        if (!PORT_GPRS_IsPowerOn()) {// 关机了则不必响应
            luat_rtos_task_sleep(1000);
            continue;
        }
        cnt++;
        if (cnt % 6 == 0) {// 6秒左右执行一次
            _net_poll_func();
        }

        if ((cnt % 8 == 0) && (s_wifi_opened)) {    // 每8s扫描一次wifi
            _wifi_scan_func();
        }
        if (!_GPRS_Monitor_Func()) {
            luat_rtos_task_sleep(500);
        } else {
            luat_rtos_task_sleep(1000);
        }
    }
}

BOOLEAN PORT_GPRS_IS_ON(void)
{
    if (s_gprs_state == GPRS_STATE_OK || s_pdp_OK == 1) return TRUE;
    else return FALSE;
}

/*****************************************************************************
 函 数 名  : PORT_GPRS_IsPowerOn
 功能描述  : 获取开关机的状态
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2022年12月27日, 星期二
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN PORT_GPRS_IsPowerOn(void)
{
    return s_cfun;
}

void PORT_GPRS_Init(void)
{
    APN_PARA_T apn;
    memset(&apn, 0, sizeof(apn));
    if (!DAL_PP_Read(APN_, (void*)&apn)) {
        strcpy(apn.apn, "CMIOT");
    }
    s_wifi_opened = FALSE;
    s_creg = FALSE;
    s_csq = 0;
    s_gprs_init_cnt = 0;
    s_pdp_OK = FALSE;
    curAp.cnt = 0;
    s_wifi_ap.cnt = 0;

    /*  设置专网卡APN信息时，如需要设置在第一路承载（CID 1），则必须使用自动模式去激活
		如果是其他路承载，自动模式还是手动模式都可以
		如果设置过APN信息的模块需要换回普通卡用，需要把apn清空
		如果APN需要设置密码，根据APN信息和加密协议设置
		apn信息需要在开机注网前配置好	*/
	//如果之前用过特殊APN的卡，现在要转回普通卡，建议删除原先设置的APN信息，调用下面的del接口即可
    //	luat_mobile_del_apn(0,1,0);
    luat_mobile_event_register_handler(_mobile_event_cb);   // 注册网络通知函数

	////luat_mobile_user_apn_auto_active(0, BK_NW_IDX, 3, 0, "CMIOT", 5, NULL, 0, NULL, 0);
	luat_mobile_user_apn_auto_active(0, BK_NW_IDX, 1, 0, 
	            (uint8_t*)apn.apn, strlen(apn.apn), 
	            (uint8_t*)apn.user, strlen(apn.user), 
	            (uint8_t*)apn.pswd, strlen(apn.pswd));// 只拨号IPV4
    luat_mobile_set_period_work(0, 5000, 0);   //// 手动设置找cell、找sim卡的间隔
    GPRS_TRACE("apn=%s %s %s", apn.apn, apn.user, apn.pswd);
    net_lwip_init();
	net_lwip_register_adapter(NW_ADAPTER_INDEX_LWIP_GPRS);
	network_register_set_default(NW_ADAPTER_INDEX_LWIP_GPRS);
    
    // INIT阶段好像不能加延时，否则挂掉 luat_rtos_task_sleep(1000);
    PORT_wifi_ap_enable(1); // 直接打开wifi，不关了

    // 合宙这个可以自动拨号，就改为监测即可
    ////luat_rtos_task_create(&netlink_task_ref,  4*1024, 30, "NETst", _netst_task, NULL, 16);
    luat_rtos_task_create(&netlink_task_ref, 12*1024, 60, "NETst", _netst_task, NULL, 16);// WIFISCAN需要大堆栈
    
}

void PORT_GPRS_Attach(void)
{
    /* 目前啥也不干，靠自动
    GPRS_TRACE("NET attach%d", s_gprs_state);
    
    if ((s_gprs_state >= GPRS_STATE_ACTIVATE) && (s_gprs_state <= GPRS_STATE_OK)) {
        PORT_GPRS_Deattach();
    }
    s_gprs_state = GPRS_STATE_REGISTER;*/
}

void PORT_GPRS_Deattach(void)
{
    /* 目前啥也不干，靠自动
    GPRS_TRACE("NET Deattach");
    
    ql_stop_data_call(0, BK_NW_IDX);
    // ql_datacall_unregister_cb(0, BK_NW_IDX, _datacall_indicate_cb, &s_pdp_OK);
    s_pdp_OK = 0;
    s_gprs_state = GPRS_STATE_NULL;*/
}

// 暂停线程，并且CFUN=0. 必须由其他任务调用，不可自己暂停自己
void PORT_GPRS_Suspend(void)
{
    int ret;
    
    PORT_GPRS_Deattach();
    
    ret = luat_mobile_set_flymode(0, 1);    // 进入飞行模式
    GPRS_TRACE("进飞行=%d", ret);
    if (ret == 0) {
        s_cfun = 0;
    }
}

// CFUN=1, 并恢复线程
void PORT_GPRS_Resume(void)
{
    int ret;
    
    ret = luat_mobile_set_flymode(0, 0);    // 退出飞行模式
    GPRS_TRACE("退飞行=%d", ret);
    if (ret == 0) {
        s_cfun = 1;
    }
    s_gprs_state = GPRS_STATE_REGISTER;
}

char* PORT_GPRS_GetIMEI(void)
{
    static char tmpImei[20] = {0};
    luat_mobile_get_imei(0, tmpImei, 20);
    
    return tmpImei;
}

// 获取信号强度 OK
INT8U PORT_GSM_Get_SNR(void)
{
    uint8_t csq = 0;

    luat_mobile_get_last_notify_signal_strength(&csq);

    if (s_csq < csq) {
        s_csq = csq;
    }
    GPRS_TRACE("报Csq=%d,%d", s_csq,csq);
    if (s_csq > 31) s_csq = 31;
    return s_csq;
}

// 确认GSM网络是否注册成功  OK
BOOLEAN PORT_GSM_IS_REG(void)
{
    // 210126 ADD, 增加用CPSI去判断
    if (s_creg) {
        return TRUE;
    } else {
        LUAT_MOBILE_REGISTER_STATUS_E st;
        st = luat_mobile_get_register_status();
        if (st == LUAT_MOBILE_STATUS_REGISTERED_ROAMING || st == LUAT_MOBILE_STATUS_REGISTERED) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

char* PORT_Get_iccid(void)
{
    static char iccid[22] = {0};
    int ret;
    
    ret = luat_mobile_get_iccid(0, iccid, 21);
    if (ret > 0) {
        iccid[20] = 0;  // 字符串结束符
        return iccid;
    } else {
        return NULL;
    }
}

//////////////////WIFI API/////////////////////
static void _wifi_scan_func(void)
{
    uint8_t i;
    ////char Wifiscan_req[100] = {0};
    
    luat_wifiscan_set_info_t wifiscan_info = {0};
    luat_wifisacn_get_info_t wifiscan_getinfo;

    /*if (!s_pdp_OK) {
        GPRS_TRACE("先不扫wifi");
        return;
    }*/
    
    wifiscan_info.maxTimeOut = 10000;
    wifiscan_info.round = 1;
    wifiscan_info.maxBssidNum = 10;
    wifiscan_info.scanTimeOut = 3;
    wifiscan_info.wifiPriority = LUAT_WIFISCAN_DATA_PERFERRD;
    wifiscan_info.channelCount=1;
    wifiscan_info.channelRecLen=280;
    wifiscan_info.channelId[0]=0;

    if (0 != luat_get_wifiscan_cell_info(&wifiscan_info, &wifiscan_getinfo)) return;

    if (wifiscan_getinfo.bssidNum > MAX_WIFI_AP) {
        curAp.cnt = MAX_WIFI_AP;
    } else {
        curAp.cnt = wifiscan_getinfo.bssidNum;
    }
    GPRS_TRACE("wifi=%d,%d个", curAp.cnt,wifiscan_getinfo.bssidNum);

    for (i = 0; i < curAp.cnt; i++) {
        curAp.ap[i].mac[0] = wifiscan_getinfo.bssid[i][0];
        curAp.ap[i].mac[1] = wifiscan_getinfo.bssid[i][1];
        curAp.ap[i].mac[2] = wifiscan_getinfo.bssid[i][2];
        curAp.ap[i].mac[3] = wifiscan_getinfo.bssid[i][3];
        curAp.ap[i].mac[4] = wifiscan_getinfo.bssid[i][4];
        curAp.ap[i].mac[5] = wifiscan_getinfo.bssid[i][5];
        curAp.ap[i].rssi   = wifiscan_getinfo.rssi[i];
        /*memset(Wifiscan_req, 0, sizeof(Wifiscan_req));
        snprintf_(Wifiscan_req, 64, "%.*s,\"%02x:%02x:%02x:%02x:%02x:%02x\",%d,%d", wifiscan_getinfo.ssidHexLen[i], wifiscan_getinfo.ssidHex[i], wifiscan_getinfo.bssid[i][0], wifiscan_getinfo.bssid[i][1], wifiscan_getinfo.bssid[i][2],
                 wifiscan_getinfo.bssid[i][3], wifiscan_getinfo.bssid[i][4], wifiscan_getinfo.bssid[i][5], wifiscan_getinfo.rssi[i], wifiscan_getinfo.channel[i]);
        GPRS_TRACE("wifi[%d]:%s", i, Wifiscan_req);*/
    }
    if (curAp.cnt >= s_wifi_ap.cnt) {
        memcpy(&s_wifi_ap, &curAp, sizeof(WIFI_AP_RESULT_T));
    }
}

// 启动wifi扫描，结果就等待回调
void PORT_wifi_ap_enable(BOOLEAN en)
{
    s_wifi_opened = en;
    if (!en) {
        curAp.cnt = 0;
        s_wifi_ap.cnt = 0;
    }
}

WIFI_AP_RESULT_T* PORT_get_cur_ap(void)
{
    return &curAp;
}

WIFI_AP_RESULT_T* PORT_get_wifi_ap(void)
{
    return &s_wifi_ap;
}

INT8U PORT_has_wifi_ap(void)
{
    // 210114 del, 没必要这么严格。if (!s_wifi_opened) return 0;

//    if (s_wifi_ap.cnt < 2) return 0;
    return s_wifi_ap.cnt;
}

//////////////////WIFI END/////////////////////


