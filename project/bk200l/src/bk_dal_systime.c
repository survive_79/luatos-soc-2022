/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : dal_systime.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年3月20日
  最近修改   :
  功能描述   : 时间接口。MC20内部有时钟，故不再开timer了
  函数列表   :
              DAL_GetDayOfWeek
              DAL_GetSysTime
              DAL_InitSysTime
              DAL_SetSysTime
  修改历史   :
  1.日    期   : 2018年3月20日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_task.h"
#include "bk_port_hardware.h"
#include "bk_dal_systime.h"
#include "bk_dal_gpsdrv.h"
//#include "dal_public.h"
#include "bk_app_bkwork.h"
#include "bk_app_picwork.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/
time_t PORT_GetSec(void);
/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
// 2018-1-1 0:0:0
#define DEFAULT_YEAR         18
#define DEFAULT_MONTH        1
#define DEFAULT_DAY          1
#define DEFAULT_WEEK         1// 礼拜一
#define DEFAULT_HOUR         0
#define DEFAULT_MINUTE       0
#define DEFAULT_SECOND       0
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static BOOLEAN s_gps_calibrated;    //GPS已经校准过
/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



/*****************************************************************************
 函 数 名  : DAL_GetSysTime
 功能描述  : 获取系统时间
 输入参数  : SYSTIME_T *ptr  
 输出参数  : 无
 返 回 值  : 获取成功返回结构体长度，失败返回0
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年3月20日
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
INT8U DAL_GetSysTime(SYSTIME_T *ptr)
{
    if(PORT_GetSysTime(ptr)) return 6;
    else return 0;
}

/*****************************************************************************
 函 数 名  : DAL_GetDayOfWeek
 功能描述  : 获取当前星期
 输入参数  : DATE_T *st  系统日期
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年3月20日
    作    者   : Rocky
    修改内容   : 新生成函数.利用改良后的基姆拉尔森计算公式

*****************************************************************************/
INT8U DAL_GetDayOfWeek(DATE_T *st)
{
    INT16U y, m, d;
    INT16U t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    
    y = st->year + 2000;   // 换算到20xx年
    y -= st->month < 3;    // 改良后的基姆拉尔森计算公式

    m = st->month;
    d = st->day;
    return ((y + y/4 - y/100 + y/400 + t[m-1] + d) % 7);
}

/*****************************************************************************
 函 数 名  : DAL_SetSysTime
 功能描述  : 设置系统时间
 输入参数  : SYSTIME_T *dt  系统时间
             BOOLEAN force  如果为false，则要看系统时间是否被GPS修正过。
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年3月20日
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_SetSysTime(SYSTIME_T *dt, BOOLEAN force)
{
    UP_PARA_NEW_T* para;
    SYSTIME_T lTime;

    #if 0  // SYSTIME_DEBUG > 0
    OURAPP_trace("DAL设时:%d/%d/%d %d:%d:%d", 
                dt->date.year,dt->date.month,dt->date.day,dt->time.hour,dt->time.minute,dt->time.second);
    #endif
    PORT_GetSysTime(&lTime);

    if (lTime.date.year  < 23) {
        PORT_SetSysTime(dt); // 系统时间异常，必须校时
        return;
    }

    if (!force) {
        if (!DAL_GPS_POS_IsOK() && !DAL_GPS_time_valid()) return;// 如果不定位，则不用GPS时间
        if (s_gps_calibrated) return;
        else s_gps_calibrated = TRUE;
    }
    PORT_SetSysTime(dt);
    para = APP_GetUploadPara();

    Requset_PIC_BaseSet(dt, para->max_worktime.hword, para->eint_en);
}

/*****************************************************************************
 函 数 名  : DAL_InitSysTime
 功能描述  : 系统时间模块初始化接口
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年3月20日
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_InitSysTime(void)
{
    SYSTIME_T defaultTime;
    defaultTime.date.year   = DEFAULT_YEAR;
    defaultTime.date.month  = DEFAULT_MONTH;
    defaultTime.date.day    = DEFAULT_DAY;
    
    defaultTime.time.hour   = DEFAULT_HOUR;
    defaultTime.time.minute = DEFAULT_MINUTE;
    defaultTime.time.second = DEFAULT_SECOND;
    PORT_SetSysTime(&defaultTime);
    s_gps_calibrated = FALSE;
}


BOOLEAN ST_RTC_GetSystime(DATE_T *date, TIME_T *time, INT32U *subsecond)
{
    SYSTIME_T lTime;
    
    if (!PORT_GetSysTime(&lTime)) {
        OURAPP_trace("RTC_get ERR");
        return FALSE;
    }

    if (time != NULL) {
        time->hour      = lTime.time.hour;
        time->minute    = lTime.time.minute;
        time->second    = lTime.time.second;
    }
    if (date != NULL) {
        date->year      = lTime.date.year;
        date->month     = lTime.date.month;
        date->day       = lTime.date.day;
    }
    // 此秒为大致估算，非UTC秒，与BK_get_sysSec一致
    if (subsecond != NULL) {
        //*subsecond = date->year*86400*365 + date->month*86400*31 + date->day*86400 + \
          //  time->hour*3600 + time->minute*60 + time->second;
        *subsecond = PORT_GetSec();
    }
    
    return TRUE;
}



