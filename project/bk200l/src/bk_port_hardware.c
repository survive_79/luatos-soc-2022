/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_port_hardware.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年12月26日, 星期二
  最近修改   :
  功能描述   : 硬件相关接口位置
  函数列表   :
  修改历史   :
  1.日    期   : 2023年12月26日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os_task.h"

#include "common_api.h"
#include "luat_rtos.h"
#include "luat_debug.h"

#include "luat_uart.h"
#include "plat_config.h"

#include "driver_gpio.h"
#include "gpr_common.h"
#include "bk_port_hardware.h"
#include "bk_our_tools.h"
#include "luat_rtc.h"


/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
/*#define NET_LED_PIN HAL_GPIO_27
#define LCD_RST_PIN HAL_GPIO_1
#define LCD_RS_PIN HAL_GPIO_10
#define LCD_CS_PIN HAL_GPIO_8
#define DTR_PIN HAL_GPIO_22
#define LCD_DATA_PIN HAL_GPIO_9
#define DEMO_IRQ_PIN HAL_GPIO_20*/

#define BK_POUT_ADC_POW     HAL_GPIO_4  //  13// ADC供电
#define BK_POUT_GREEN       HAL_GPIO_6  //  14// GPS定位情况,   1灯灭，0亮
#define BK_POUT_GPS_POW     HAL_GPIO_7  //  15// GPS上电
#define BK_POUT_BLUE        HAL_GPIO_5  //  16// net网络情况    1灯灭，0亮

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



void PORT_GPIO_Init(void)
{
    luat_gpio_cfg_t gpio_cfg;
// TODO: 注意，目前这个执行比BK调试打印要早，故这里不能用bk打印接口    
	luat_gpio_set_default_cfg(&gpio_cfg);// mode为0，默认就是output
    gpio_cfg.pull   = LUAT_GPIO_PULLUP;
    
	gpio_cfg.pin    = BK_POUT_GREEN;
	luat_gpio_open(&gpio_cfg);
    luat_gpio_set(BK_POUT_GREEN, 1);
    
    gpio_cfg.pin = BK_POUT_BLUE;
	luat_gpio_open(&gpio_cfg);
    luat_gpio_set(BK_POUT_BLUE, 1);
    
    gpio_cfg.pin = BK_POUT_ADC_POW;
	luat_gpio_open(&gpio_cfg);
    luat_gpio_set(BK_POUT_ADC_POW, 1);  // 默认ADC供电

    gpio_cfg.pin = BK_POUT_GPS_POW;
	luat_gpio_open(&gpio_cfg);
    luat_gpio_set(BK_POUT_GPS_POW, 0);  // GPS关机
    

// 除非你已经非常清楚uart0作为普通串口给用户使用所带来的的后果，否则不要打开以下注释掉的代码
    BSP_SetPlatConfigItemValue(PLAT_CONFIG_ITEM_LOG_PORT_SEL,PLAT_CFG_ULG_PORT_USB);
// 除非你已经非常清楚uart0作为普通串口给用户使用所带来的的后果，否则不要打开以下注释掉的代码
    soc_uart0_set_log_off(1);   // 这句打开，让uart0作为普通串口使用
    // uart
    GPIO_IomuxEC618(GPIO_ToPadEC618(HAL_GPIO_16, 0), 3, 0, 0);  // uart0启用，设置到alt3
	GPIO_IomuxEC618(GPIO_ToPadEC618(HAL_GPIO_17, 0), 3, 0, 0);
	GPIO_PullConfig(GPIO_ToPadEC618(HAL_GPIO_16, 0), 1, 1);
	GPIO_IomuxEC618(GPIO_ToPadEC618(HAL_GPIO_14, 0), 0, 0, 0);	//原先的UART0 TXRX变回GPIO功能
	GPIO_IomuxEC618(GPIO_ToPadEC618(HAL_GPIO_15, 0), 0, 0, 0);

    GPIO_IomuxEC618(GPIO_ToPadEC618(HAL_GPIO_18, 0), 1, 0, 0);	// uart1启用，设置到alt1
	GPIO_IomuxEC618(GPIO_ToPadEC618(HAL_GPIO_19, 0), 1, 0, 0);

    // ADC
    luat_adc_open(0, NULL);     // 使能ADC0

    LUAT_DEBUG_PRINT("BK HW INIT OK");
}
//硬件上是BK200L的黄灯
void PORT_LED_GREEN(BOOLEAN level)
{
    int lvl;
    if (level) { 
        lvl = 0;
    } else {
        lvl = 1;
    }
    luat_gpio_set(BK_POUT_GREEN, lvl);
}
//硬件上是BK200L的红灯
void PORT_LED_BLUE(BOOLEAN level)
{
    int lvl;
    if (level) { 
        lvl = 0;
    } else {
        lvl = 1;
    }
    luat_gpio_set(BK_POUT_BLUE, lvl);
}

void PORT_GPS_power(BOOLEAN level)
{
    luat_gpio_set(BK_POUT_GPS_POW, (int)level);
}

void PORT_ADC_power(BOOLEAN level)
{
    luat_gpio_set(BK_POUT_ADC_POW, (int)level);
}

/*******************************************************************
** 函数名:     PORT_GetVoltage
** 函数描述:   获取电源电压值
** 参数:       [in]  void
**               
** 返回:       电压值mv
********************************************************************/
static INT32U raw_volt = 3600;
INT32U PORT_GetVoltage(void)
{
    int val, val2;// val2貌似是经过某个校准后的值
    
    luat_adc_read(0 , &val, &val2);

    raw_volt = val2 / 253;// val2 * 4 / 1000
    
    //OURAPP_trace("batt=%dmV,RAW=%d微伏", raw_volt, val2);  // 从ADC0读值

    return raw_volt;
}

/*******************************************************************
** 函数名:     PORT_GetSysTime
** 函数描述:   从平台中读取当前时间
** 参数:       [out] dt:   时间
** 返回:       成功返回true，失败返回false
********************************************************************/
BOOLEAN PORT_GetSysTime(SYSTIME_T *dt)
{
    struct tm rtc;
    //int ret;

    luat_rtc_get(&rtc);
    
    /*if ((ret != 0) || (rtc.tm_year < 2023) || (rtc.tm_year >2100) ||
        (rtc.tm_sec > 59) || (rtc.tm_min > 59) || (rtc.tm_hour > 23) ||
        (rtc.tm_mday == 0) || (rtc.tm_mday > 31) || (rtc.tm_mon == 0) || (rtc.tm_mon > 12)) {
        return FALSE;
    }
    #if SYSTIME_DEBUG > 0
    OURAPP_trace("get rtc=%d/%d/%d %d:%d:%d", 
                rtc.tm_year,rtc.tm_mon,rtc.tm_mday,rtc.tm_hour, rtc.tm_min,rtc.tm_sec);
    #endif*/

    dt->date.year      = rtc.tm_year-100;  // 2000是0, 合宙返回的是从1900起
    dt->date.month     = rtc.tm_mon+1;      // 1~12
    dt->date.day       = rtc.tm_mday;
    dt->time.hour      = rtc.tm_hour;
    dt->time.minute    = rtc.tm_min;
    dt->time.second    = rtc.tm_sec;

    OUR_ConvertGmtToLocaltime(&dt->date, &dt->time, 8); // 都是8时区的
    #if SYSTIME_DEBUG > 0
    OURAPP_trace("RD rtc %d/%d/%d %d:%d:%d", 
                dt->date.year,dt->date.month,dt->date.day,dt->time.hour,dt->time.minute,dt->time.second);
    #endif
        
    return TRUE;
}

/*******************************************************************
**  函数名:     PORT_SetSysTime
**  函数描述:   设置系统时间
**  参数:       [in] st: 系统时间
**  返回:       成功返回true，失败返回false
********************************************************************/
BOOLEAN PORT_SetSysTime(SYSTIME_T *dt)
{
    struct tm rtc;
    int result;
    SYSTIME_T utc;

    if (!DateValid(&dt->date) || !TimeValid(&dt->time)) return FALSE;  // 不去设错误时间
    
    // 看代码，时区是写死32的  //ql_rtc_set_timezone(32);    //UTC+8
    memcpy(&utc, dt, sizeof(SYSTIME_T));  // dt会被改掉
    OUR_ConvertLocaltimeToGmt(&utc.date, &utc.time, 8);     // 转回GMT

    rtc.tm_year = 100 + utc.date.year;    // tm_year是以1900为0点，我们是2000为0点
    rtc.tm_mon  = utc.date.month-1;
    rtc.tm_mday = utc.date.day;
    rtc.tm_hour = utc.time.hour;
    rtc.tm_min  = utc.time.minute;
    rtc.tm_sec  = utc.time.second;
    
    result = luat_rtc_set(&rtc);

    #if SYSTIME_DEBUG > 0
    OURAPP_trace("RTC%d=%d/%d/%d %d:%d:%d", result, 
                dt->date.year,dt->date.month,dt->date.day,dt->time.hour,dt->time.minute,dt->time.second);
    #endif
    
    if (result != 0) {
        return FALSE;
    } else {
        return TRUE;
    }
}

time_t PORT_GetSec(void)
{
    struct tm rtc;
    time_t sec;

    luat_rtc_get(&rtc);
    sec = mktime(&rtc);
    return sec;
}
#if 0 // 用不上，这个可能是500q的接口
/*******************************************************************
** 函数名:     PORT_GetVoltagePercentage
** 函数描述:   获取电源电量百分比
** 参数:       [in]  void
**               
** 返回:       百分比
********************************************************************/
INT8U PORT_GetVoltagePercentage(void)
{
    INT32U tmpdata = 0;
    
    if(raw_volt) {
    	tmpdata = raw_volt;
        // TODO: 190714 算法再次更，分3段线性化:4.1以上是100，4.1~3.75, 3.75~3.6, 3.6~3.45阶段线性
    	if (tmpdata >= 4100) tmpdata = 100; 				// 超出4.1都是100%
    	else if (tmpdata >= 3750) {
    		tmpdata = 58 + ((tmpdata - 3750) * 12) / 100;	// 3.7~4.1伏之间线性化，范围是100~58%
    	} else if (tmpdata >= 3600) {
    		tmpdata = 8 + ((tmpdata - 3600) * 333) / 1000;	// 3.75~3.6伏之间线性化，范围是58~8%
    	} else if (tmpdata >= 3450) {
    		tmpdata = 1 + ((tmpdata - 3450) * 46) / 1000;	// 3.6~3.45伏之间线性化，范围是8~1%
    	} else {
    		tmpdata = 1;
    	}
    }
    //OURAPP_trace("电量百分比:%d",tmpdata);
    return ((INT8U)tmpdata);
}
/*******************************************************************
** 函数名:     PORT_PowerPercentage
** 函数描述:   获取电源电量百分比
** 参数:       [in]  void
**               
** 返回:       百分比
********************************************************************/
INT8U PORT_PowerPercentage(void)
{
    return PORT_GetVoltagePercentage();
}
INT16U PORT_Get_humidity(void)
{
    return g_our_status.humidity;
}

INT8U PORT_Get_chargestatus(void)
{
    return g_our_status.charge_sta;
}

#endif

INT16U PORT_Get_temperature(void)
{
    return g_our_status.temperature;
}


