/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : dal_blind.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年11月14日, 星期二
  最近修改   :
  功能描述   : 盲区补传的dal层接口
  函数列表   :
              DAL_BlindDeinit
              DAL_BlindErase
              DAL_BlindGetData
              DAL_BlindInit
              DAL_BlindReset
              DAL_Blind_AddNode
              _get_blind_cnt
              _set_blind_cnt
  修改历史   :
  1.日    期   : 2023年11月14日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#if BL_EN > 0
#include "bk_port_uart.h"
#include "bk_dal_blind.h"
#include "bk_our_tools.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define MAX_BL_SIZE   200
static INT8U s_data[MAX_BL_SIZE];
//static char s_cur_name[50];    // 相当于st151里面的s_curpage
static INT16U s_bl_read_len = 0;    // bl读取到的LEN

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/






#if PRINT_BLIND_CNT > 0
#include "bk_dal_public.h"
static INT8U s_read_blcnt = 0;
static INT32U _get_blind_cnt(void)
{
    INT32U cnt;
    DAL_PP_Read(BLIND_CNT_, (void *)&cnt);
    return cnt;
}

static void _set_blind_cnt(INT32U para)
{
    INT32U cnt = para;
    DAL_PP_Store(BLIND_CNT_, (void *)&cnt);
}
#endif


void DAL_BlindInit(void)
{
    //memset(&s_cur_name, 0, sizeof(s_cur_name));
    s_bl_read_len = 0;
    memset(&s_data, 0, sizeof(s_data));

    PORT_BL_Init();

#if PRINT_BLIND_CNT > 0
    s_read_blcnt = 0;
    if (1) {
        INT32U cnt, total, freed;
        cnt = _get_blind_cnt();
        #if BL_DEBUG > 0
        total = 307200;
        freed = total - cnt * 20;
        OURAPP_trace("盲区总空间=%d,已使用%d[%d]\r\n盲区剩余%d[%d]", total, cnt*BLIND_NODE_SIZE, cnt, freed, freed / BLIND_NODE_SIZE);
        #endif
    }
#endif
//    BL_TEST();
}

void DAL_BlindDeinit(void)
{
    
}

// 复位清空整个盲区
void DAL_BlindReset(void)
{
    PORT_BL_DeleteAll();
#if PRINT_BLIND_CNT > 0
    if (1) {
        _set_blind_cnt(0);
        #if BL_DEBUG > 0
        OURAPP_trace("清盲点=0");
        #endif
    }
#endif
}

// 获取一包盲区点. 如果获取到了，则返回s_data的指针，dlen是长度
INT8U* DAL_BlindGetData(INT8U* dlen)
{
    s_bl_read_len = PORT_BL_ReadData(s_data, MAX_BL_SIZE);
#if PRINT_BLIND_CNT > 0
    s_read_blcnt = s_bl_read_len / BLIND_NODE_SIZE;
#endif
    if (s_bl_read_len == 0) return NULL;

    *dlen = s_bl_read_len;
    return s_data;
}

// 和上面的函数成对用，当确定发送成功后，要把该page删了
BOOLEAN DAL_BlindErase(void)
{
    BOOLEAN result;
        
    result = PORT_BL_DeleteData(s_bl_read_len);

    if (result) {// 221011
#if PRINT_BLIND_CNT > 0
        INT32U cnt;
        cnt = _get_blind_cnt();
        if (cnt > s_read_blcnt) {
            cnt -= s_read_blcnt;
        } else {
            cnt = 0;
        }
        _set_blind_cnt(cnt);
        #if BL_DEBUG > 0
        OURAPP_trace("删盲点=%d", cnt);
        #endif
#endif
        s_bl_read_len = 0;  // 240115 清零，才不怕重复删
    }

    return result;
}



// 存一个盲区点
BOOLEAN DAL_Blind_AddNode(BLIND_STRUCT_UNION* node)
{
    BOOLEAN result = PORT_BL_InsertNode(node->data, 20);
#if PRINT_BLIND_CNT > 0
    if (result) {// 221011
        INT32U cnt;
        cnt = _get_blind_cnt();
        cnt++;
        _set_blind_cnt(cnt);
        #if BL_DEBUG > 0
        OURAPP_trace("存盲点=%d", cnt);
        #endif
        
    }
#endif
    return result;
}
#if 0
static void pre_BL_TEST(void)
{
    BLIND_STRUCT_UNION data;
    int i;
    INT8U cnt;

    data.bl.t_local.date.year = 22;
    data.bl.t_local.date.month = 2;
    data.bl.t_local.date.day = 2;
    data.bl.t_local.time.hour = 22;
    data.bl.t_local.time.minute = 22;
    data.bl.t_local.time.second = 22;
    data.bl.status_flag = 0;
    data.bl.spd = 0;
    data.bl.D_lat = 0;
    data.bl.D_long = 0;
    data.bl.voltage = 100;
    
    for (i=0; i<12; i++) {
        DAL_Blind_AddNode(&data);
    }
    // cnt = PORT_BL_ReadFile(s_cur_name, s_data, MAX_BL_SIZE);
    
}

#include "ql_type.h"
#include "ql_fs.h"
// 这边是读取并删除
void BL_TEST(void)
{
    INT8U* dataPtr;
    INT8U dataLen, i;
    BLIND_STRUCT_UNION* bl;
    QFILE *fp = NULL;

    pre_BL_TEST();// 插入数据

    while (1) {
        
        dataPtr = DAL_BlindGetData(&dataLen);
        if ((dataPtr == NULL) || (dataLen < BLIND_NODE_SIZE)) {
            OURAPP_trace("test读空");
            return;
        }
        OURAPP_trace("读到%d,个数%d", dataLen, dataLen / 20);
        DAL_BlindErase();
    }
}
#endif

#endif




