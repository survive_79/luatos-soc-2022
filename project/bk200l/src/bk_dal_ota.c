

/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : dal_sock_ota.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年4月16日
  最近修改   :
  功能描述   : OTA升级的socket数据接收及具体升级动作
  函数列表   :
              DAL_OTA_Connect
              DAL_OTA_DeInit
              DAL_OTA_DownloadData
              DAL_OTA_GetState
              DAL_OTA_SendRequest
              DAL_OTA_SetState
              _ota_close_cb
              _ota_link_cb
              _OTA_RecvFromServer
              _ota_recv_cb
              _ota_sent_cb
  修改历史   :
  1.日    期   : 2018年4月16日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#if APP_OTA_EN > 0
#include "luat_full_ota.h"

#include "bk_port_socket.h"
#include "bk_port_hardware.h"
#include "bk_port_gprs.h"
#include "bk_port_public.h"
#include "bk_dal_ota.h"

#include "bk_our_tools.h"
#include "bk_app_picwork.h"

#if OTA_DEBUG > 0
#define OSOCK_TRACE OURAPP_trace
#define OSOCK_LOGH  OURAPP_printhexbuf
#else
#define OSOCK_TRACE(FORMAT,...) 
#define OSOCK_LOGH(A, B) 
#endif

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define OTA_WRITE_SIZE      1024

#define MAX_OTA_DATA_SEND  100
//#define OTA_RBUFLEN        5000 // TODO: 可能要改成动态分配的会比较好

#define FOTA_NAME       "BKOTA.bin"  // 存储的文件名

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

luat_full_ota_ctrl_t *s_ota_fd = NULL;  // 句柄

//static char s_ota_rbuf[OTA_RBUFLEN];
//static BOOLEAN s_got_head;      // 是否收到头部
static OTA_STATE_E s_ota_state = OTA_ATTACH_IDLE;
//// 不需要用fd跟踪 static int s_ota_sockfd = SOCKET_FD_INVALID;
static struct {// OTA REQ发送缓存
    INT8U data[MAX_OTA_DATA_SEND];
    INT8U len;
} s_ota_sbuf;

static BOOLEAN s_got_head;      // 是否手动HTTP头
static INT32U s_crc_result;
static INT32U s_crc_expect;
static INT32U s_ota_rlen;       // 接收长度
static INT32U s_ota_expectedLen;   // 预期长度
//static INT32U s_ota_payloadLen;    // HTTP载体长度, 改大了，原来是INT16U
//static char* s_ota_pPayload = NULL;
// static INT8U  s_ota_tmp_rbuf[OTA_WRITE_SIZE];

//#define FILE_MAX_SIZE       120000       // 120KB
//static INT8U* s_file_save_p = NULL;     // 文件存储的缓存指针，动态分配的


//static unsigned short s_file_len = 0;   // 文件长度
//static unsigned char s_rcvbuf[2048];
//static unsigned short s_rlen = 1;

/*
其实用不上，屏蔽省空间算了。在port_GPRS那边有根据下标维护了
static struct BK_SOCKET_OBJ s_ota_sock = {
        SOCK_NONE,          // state
        SOCKET_FD_INVALID,  // fd
        0,                  // port
        {0},                // ip_str
        _ota_link_cb,
        _ota_sent_cb,
        _ota_recv_cb,
        _ota_close_cb,
        FALSE
    };
*/

void DAL_OTA_FsInit(void)
{
    // TODO: 是否要考虑FS内是否够用？
    s_ota_fd = luat_full_ota_init(0, 0, NULL, NULL, 0);
    if (s_ota_fd == NULL) {
        OSOCK_TRACE("OTA open err:%d", s_ota_fd);
    }
    s_ota_rlen = 0;
    s_ota_expectedLen = 0;
    CRC_32_StepInit();
    s_crc_result = 0;
}

void DAL_OTA_FsWriteData(INT8U* data, INT32U len)
{
    int ret;
    if (s_ota_fd == NULL) return;

    ret = luat_full_ota_write(s_ota_fd, data, len);
    //ret = ql_fwrite(data, 1, len, s_ota_fd);
    if (ret < 0) {
        OSOCK_TRACE("O写入err:%d", ret);
    } else {
        s_ota_rlen += len;
    }
    // TODO: 这里考虑计算CRC校验
    s_crc_result = CRC_32_step(data, len);
}
// 检验
void DAL_OTA_Check(void)
{
    int ret;
    OSOCK_TRACE("O-CRC:%d,算=%d", s_crc_expect, s_crc_result);
    // TODO: 要不要进行crc校验，看情况再说

    ret = luat_full_ota_is_done(s_ota_fd);    
    OSOCK_TRACE("OTA校验=%d", ret);
    
    if (!ret) {
        if (s_ota_fd) {
            free(s_ota_fd);
        }
        s_ota_fd = NULL;
        
        BK_Reset();
    } else {
        luat_full_ota_end(s_ota_fd, 0);
        if (s_ota_fd) {
            free(s_ota_fd);
        }
        s_ota_fd = NULL;
        Request_PIC_Sleep(0);
    }
}

/* 小洪给的格式
OTA DEST VER:
OTA Length:
OTA CRC32:

OTA DEST VER:NULL
OTA Length:11

NO NEED OTA	
*/
// TODO: 要改写，字段不一样. 还要考虑避免睡眠导致升级失败!!!
static void _OTA_RecvFromServer(INT8U* data, INT32U len)
{
    if (s_ota_state < OTA_ATTACH_SUCC) {
        OSOCK_TRACE("OTA-R:state错%d", s_ota_state);
        return;
    }
    
    if (!s_got_head) {
        INT8U *rPtr;
        char *pos = NULL;
        rPtr = BK_MEM_Alloc(len+1);
        BK_ASSERT(rPtr);
        memcpy(rPtr, data, len);
        rPtr[len] = 0x00;

        pos = strstr((const char*)rPtr, "OTA DEST VER");
        if (pos) {
            INT32U contentLength = 0;
            
            if (pos[13] == 'N' && pos[14] == 'U') {// OTA DEST VER:填的是NULL
                OSOCK_TRACE("OTA无需升级");
                BK_MEM_Free(rPtr);
                goto _OTA_ERR_SLEEP;
            }
            // OSOCK_TRACE("OTA-ver:%s", strBuf);// 版本号
            pos = strstr((char*)rPtr, "OTA Length");
            if (sscanf(pos, "OTA Length:%u",&contentLength) && (contentLength > 0)) {
                if (contentLength < 1000) { // 表示不必升级，或者错误
            	    OSOCK_TRACE("OTA过短不升%d", contentLength);
                    BK_MEM_Free(rPtr);
            		goto _OTA_ERR_SLEEP;
                }
                // TODO: 是否要把DAL_OTA_FsInit放这边执行？
                OSOCK_TRACE("OTA Len=%d", contentLength);
                s_ota_expectedLen = contentLength;
                s_got_head = TRUE;
                
                pos=strstr((const char*)data, "\r\n\r\n");
                if (((INT8U*)pos > data) && (((INT8U*)(pos) - data + 4) < len)) { // 考虑包合并的可能性, 需要把这些数据写入到
                    OSOCK_TRACE("OTA粘包%d", len-((INT8U*)pos - data + 4));
                    DAL_OTA_FsWriteData((INT8U*)pos+4, len-((INT8U*)pos - data + 4));   // (pos - data + 4)是头的长度，该包减去头长度，还有数据，就视为有包合并了
                }
            }

            pos = strstr((char*)rPtr, "OTA CRC32");
            // 校验和
            if (pos && sscanf((const char *)pos, "OTA CRC32:%x", &s_crc_expect)) {
                OSOCK_TRACE("OTA-CRC=%x", s_crc_expect);
    		} else {
    		    OSOCK_TRACE("OTA-CRC获取err");
                s_crc_expect = 0;
    		}
        }
        BK_MEM_Free(rPtr);
        return;
    } else {
        if (s_ota_expectedLen && (s_ota_rlen < s_ota_expectedLen)) { // 有值说明可以接收了
            DAL_OTA_FsWriteData(data, len);
            ////OSOCK_TRACE("O收:%d", s_ota_rlen);
        }
    }
    
    if (s_ota_rlen && s_ota_expectedLen && (s_ota_rlen >= s_ota_expectedLen)) {
        OSOCK_TRACE("OTA收齐,len=%d[%d]", s_ota_rlen, s_ota_expectedLen);
        s_ota_state = OTA_GOT_DATA; // 收齐了
        return; // 可以进行升级校验了
    }
    return;

_OTA_ERR_SLEEP:
    // TODO: 出错，要关机. 改为检查pic OTA
    //Request_PIC_Sleep(0);
    s_ota_state = OTA_NONEED;
}

// 发送升级请求。命令格式<IMEI15位@版本号字符串>
BOOLEAN DAL_OTA_SendRequest(void)
{
    int ret = 0;
    
    if ((PORT_GPRS_sock_state(SOCK_FD_OTA) != SOCK_OK) || (s_ota_state < OTA_ATTACH_SUCC)) {
        return FALSE;
    }
    s_ota_rlen = 0;
    s_ota_expectedLen = 0;
    
    //Ql_memset((void*)&s_ota_sbuf, 0x00, sizeof(s_ota_sbuf));
    s_ota_sbuf.data[0] = '<';
    memcpy(&s_ota_sbuf.data[1], (const void*)PORT_GetIMEI(), 15);
    s_ota_sbuf.data[16] = '@';
    strcpy((char*)&s_ota_sbuf.data[17], PORT_GetVersion());
    s_ota_sbuf.len= 17+strlen(PORT_GetVersion());
    s_ota_sbuf.data[s_ota_sbuf.len++] = '>';
    s_ota_sbuf.data[s_ota_sbuf.len++] = 0x00;
    OSOCK_TRACE("O备发:%d,%s", s_ota_sbuf.len, s_ota_sbuf.data);
    
    ret = PORT_GPRS_sock_send(SOCK_FD_OTA, s_ota_sbuf.data, (int)s_ota_sbuf.len);
    //OSOCK_TRACE("OTA请求:%d,%d", s_ota_sbuf.len, ret);
    if (ret > 0) {
        /*//OSOCK_LOGH(data, ret);
        if (ret < s_ota_sbuf.len) {    // 说明还有数据没发完
            memmove(s_ota_sbuf.data, s_ota_sbuf.data+ret, ret);
            s_ota_sbuf.len -= ret;
        } else {
            s_ota_sbuf.len = 0;
        }*/
        s_ota_sbuf.len = 0;
        s_ota_state = OTA_ATTACH_REQUESTING;
        return TRUE;
    } else {
        // TODO: 这里要考虑是否需要重新建立socket
        return FALSE;
    }
}

// socket链接已创建的处理回调
static void _ota_link_cb(SOCK_INDEX_E index, s32 errCode, void* customParam)
{
/*    OSOCK_TRACE("OTA链接OK%d", fd);

    s_ota_state = OTA_ATTACH_SUCC;
    s_ota_sockfd = fd;*/
    if (index != SOCK_FD_OTA){
        OSOCK_TRACE("O链:err=%d", index);
        return;
    }

    if (errCode == 0) { // 用于port_gprs来通知connect出错
        BK_LOG("O链OK");
        s_ota_state = OTA_ATTACH_SUCC;
    } else {
        BK_LOG("O链ERR");
        s_ota_state = OTA_ATTACH_FAIL;
        PORT_GPRS_sock_close(index);
    }
}

static void _ota_sent_cb(SOCK_INDEX_E index, s32 errCode, void* customParam)
{
    /* INT8U* sptr = s_ota_sbuf.data;
    int ret = 0;
    if (index != SOCK_FD_OTA){
        OSOCK_TRACE("O发:err=%d", index);
        return;
    }
    测试发现EC600E这样写不行，PORT_GPRS_sock_send成同步调用，里面会直接调用到sent_cb，导致发送死循环
    while (s_ota_sbuf.len) {
        ret = PORT_GPRS_sock_send(SOCK_FD_OTA, sptr, s_ota_sbuf.len);
        OSOCK_TRACE("OTA cb-send=%d,sent%d", s_ota_sbuf.len, ret);
        if (ret > 0) {
            if (ret >= s_ota_sbuf.len) {// 发送完毕
                s_ota_sbuf.len = 0;
            } else {
                sptr += ret;
                s_ota_sbuf.len -= ret;
            }
        } else {
            OSOCK_TRACE("OTA cb-send err.%d", ret);
            break;// 跳出while循环
        }
    }*/
}

static void _ota_recv_cb(SOCK_INDEX_E index, s32 rlen, void* rbuf)
{
    if (index != SOCK_FD_OTA){
        OSOCK_TRACE("O收:err=%d", index);
        return;
    }
    _OTA_RecvFromServer(rbuf, rlen);
}

static void _ota_close_cb(SOCK_INDEX_E index, s32 errCode, void* customParam)
{
    if (index != SOCK_FD_OTA){
        OSOCK_TRACE("O关:err=%d", index);
        return;
    }
    DAL_OTA_DeInit();
}


OTA_STATE_E DAL_OTA_GetState(void)
{
    return s_ota_state;
}

void DAL_OTA_SetState(OTA_STATE_E state)
{
    s_ota_state = state;
}

// 初始化OTA, 在DAL_OTA_Connect前调用
void DAL_OTA_DeInit(void)
{
    // PORT_GPRS_Sock_Init(SOCK_FD_OTA);
    s_ota_state = OTA_ATTACH_IDLE;

    OSOCK_TRACE("OTA deinit");
    s_got_head = FALSE;
}

// ota开启
BOOLEAN DAL_OTA_Connect(void)
{
    GPRS_GROUP_T addr = {TRUE, 10001, "ydzc.borcom.net"};
    int ret;
    if (!PORT_GPRS_IS_ON()) {
        OSOCK_TRACE("OTA:GPRS未妥");
        return false;
    }
    if (s_ota_state != OTA_ATTACH_IDLE) {
        OSOCK_TRACE("OTA:reinit");
        DAL_OTA_DeInit();
        s_ota_state = OTA_ATTACH_IDLE;
    }
    ret = PORT_GPRS_sock_new(SOCK_FD_OTA, TRUE, addr.ip, strlen(addr.ip), addr.port, 
        _ota_link_cb, _ota_sent_cb, _ota_recv_cb, _ota_close_cb);
    if (ret < 0) {
        OSOCK_TRACE("OTA:new败%d", ret);
        return false;
    }

    //OSOCK_TRACE("O连%s:%d", addr.ip, addr.port);
    s_ota_state = OTA_ATTACH_ING;
    return true;
}

#endif



