/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : bk_port_socket.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2024年1月9日, 星期二
  最近修改   :
  功能描述   : socket通讯适配层
  函数列表   :
              _conn_task_proc
              PORT_GPRS_sock_close
              PORT_GPRS_sock_new
              PORT_GPRS_sock_send
              PORT_GPRS_sock_state
              PORT_Socket_MspInit
              _recv_task_proc
              _send_task_proc
              _sock_close
              _sock_connect
              _sock_event_CB
              _sock_exist
              _sock_reconnect
              _sock_send
  修改历史   :
  1.日    期   : 2024年1月9日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "sockets.h"
#include "dns.h"
#include "lwip/ip4_addr.h"
#include "netdb.h"
#include "luat_debug.h"
#include "luat_rtos.h"
#include "luat_mem.h"

#include "bk_os.h"
#include "bk_our_tools.h"
#include "bk_port_socket.h"
#include "bk_port_hardware.h"
#include "bk_port_gprs.h"
#include "bk_port_uart.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/
static int _sock_connect(int app_id, int is_tcp, char *address, int port, socket_service_event_callback_t callback);
/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define SOCK_TRACE(X, Y...) GPRS_TRACE("S%d=>"X, app_id, ##Y)

#define SOCK_CB(evt, param); do {  \
										{ \
											if(s_sockets[app_id].callback) \
											{ \
												s_sockets[app_id].callback(app_id, evt, param); \
											}\
										} \
									} while(0) 

//#define USER_SOCKET_MAX_COUNT SOCK_FD_MAX
//socket数据发送task mailbox长度
#define SEND_TASK_MAILBOX_EVENT_MAX     50
#define SOCKET_TASK_NAME_MAX_LEN        12

#define RECV_BUF_LEN                    1024
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/
typedef struct sock_item_info
{
	int app_id;         //// 对应 SOCK_INDEX_E
	int socket_id;      //// sock fd句柄
	int is_tcp;       //1:tcp 0:udp
	char *address;      //// 网址
	int port;
	int is_connected;
	int connect_task_exist;
	socket_service_event_callback_t callback;
	char connect_task_name[SOCKET_TASK_NAME_MAX_LEN+1];
	luat_rtos_task_handle connect_task_handle;
	char send_task_name[SOCKET_TASK_NAME_MAX_LEN+1];
	luat_rtos_task_handle send_task_handle;
	char recv_task_name[SOCKET_TASK_NAME_MAX_LEN+1];
	luat_rtos_task_handle recv_task_handle;
	luat_rtos_semaphore_t connect_ok_semaphore;
} SOCK_ITEM_INFO_T;

typedef struct sock_send_data
{
    INT8U *data;
    uint32_t len;
    int user_param;
} SOCK_SEND_T;

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static SOCK_ITEM_INFO_T s_sockets[SOCK_FD_MAX];
static SOCKET_FD_T s_sockfd_list[SOCK_FD_MAX];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



static BOOLEAN _sock_exist(int id) 
{
    return (id >= 0 && id < SOCK_FD_MAX) ? TRUE : FALSE;
}

static void _sock_close(int app_id)
{
	SOCK_TRACE("关s[%d]", app_id);
	close(s_sockets[app_id].socket_id);
	s_sockets[app_id].socket_id = -1;
	s_sockets[app_id].is_connected = 0;

// TODO: 加这里，看看行不行
    if (s_sockfd_list[app_id].close_cb) {
        s_sockfd_list[app_id].close_cb(app_id, 0, NULL);
    }
    s_sockfd_list[app_id].sock_fd = SOCKET_FD_INVALID;
    s_sockfd_list[app_id].state = SOCK_IDLE;
}

static void _sock_reconnect(int app_id)
{
    SOCK_TRACE("重连s[%d]", app_id);
	_sock_connect(app_id, 
		s_sockets[app_id].is_tcp, 
		s_sockets[app_id].address, 
		s_sockets[app_id].port, 
		s_sockets[app_id].callback);
}

static void _send_task_proc(void *arg)
{
	uint32_t message_id;
	SOCK_SEND_T *data_item;
	int ret = -1;
	uint32_t sent_len = 0;
	int app_id = *(int*)(arg);
	socket_event_param_t event_param;

	while(1)
	{
		if(luat_rtos_message_recv(s_sockets[app_id].send_task_handle, &message_id, (void **)&data_item, LUAT_WAIT_FOREVER) == 0)
		{
			if(s_sockets[app_id].is_connected)
			{
				sent_len = 0;
				// SOCK_TRACE("total len %d, sent len %d", data_item->len, sent_len);
				while (sent_len < data_item->len)
				{
					ret = send(s_sockets[app_id].socket_id, data_item->data+sent_len, data_item->len-sent_len, 0);
					if (ret >= 0)
					{
						SOCK_TRACE("s[%d]发=%d", app_id, ret);
						sent_len += ret;
						if (sent_len >= data_item->len)
						{
							event_param.send_cnf_t.result = sent_len;
							event_param.send_cnf_t.user_param = data_item->user_param;
							SOCK_CB(SOCKET_EVENT_SEND, event_param);
						}						
					}
					else
					{
						if (errno == EWOULDBLOCK)
						{
							SOCK_TRACE("block, wait send buffer ok");
							luat_rtos_task_sleep(1000);
						}
						else
						{
							_sock_close(app_id);

							event_param.send_cnf_t.result = ret;
							event_param.send_cnf_t.user_param = data_item->user_param;
							SOCK_CB(SOCKET_EVENT_SEND, event_param);

							luat_rtos_task_sleep(5000);
							_sock_reconnect(app_id);

							break;
						}
					}					
				}				
			}
			else
			{
				ret = -1;
				event_param.send_cnf_t.result = ret;
				event_param.send_cnf_t.user_param = data_item->user_param;
				SOCK_CB(SOCKET_EVENT_SEND, event_param);
			}

			LUAT_MEM_FREE(data_item->data);
			data_item->data = NULL;
			LUAT_MEM_FREE(data_item);

			data_item = NULL;
		}
	}
}

static void _recv_task_proc(void *arg)
{	
	fd_set read_set, error_set;
	struct timeval timeout;
	int ret, read_len;
	char * recv_buf = NULL;
	int app_id = *(int*)(arg);
	socket_event_param_t event_param;

	SOCK_TRACE("s%d收GO", app_id);

	while(1) {
		if(s_sockets[app_id].is_connected) {
			luat_rtos_semaphore_take(s_sockets[app_id].connect_ok_semaphore, LUAT_NO_WAIT);

			if(NULL == recv_buf)
			{
				recv_buf = (char *)LUAT_MEM_MALLOC(RECV_BUF_LEN);
				LUAT_DEBUG_ASSERT(recv_buf != NULL,"malloc recv_buf fail");
			}

			timeout.tv_sec = 60;
			timeout.tv_usec = 0;

			while(1)
			{
				FD_ZERO(&read_set);
				FD_ZERO(&error_set);
				FD_SET(s_sockets[app_id].socket_id,&read_set);
				FD_SET(s_sockets[app_id].socket_id,&error_set);

				SOCK_TRACE("before select");

				ret = select(s_sockets[app_id].socket_id+1, &read_set, NULL, &error_set, &timeout);

				if(ret < 0)
				{
					//失败
					SOCK_TRACE("select fail, ret %d",ret);
					break;
				}
				else if(ret == 0)
				{
					//超时
					SOCK_TRACE("select timeout");
				}
				else
				{
					if(FD_ISSET(s_sockets[app_id].socket_id, &error_set))
					{
						//出错
						SOCK_TRACE("select error event");
						break;
					}
					else if(FD_ISSET(s_sockets[app_id].socket_id, &read_set))
					{
						SOCK_TRACE("select read event");
						read_len = recv(s_sockets[app_id].socket_id, recv_buf, RECV_BUF_LEN, 0);
						//read event后，第一次读不到数据，表示出错
						//实测，在select时，服务器主动断开连接，会走到这里
						if(read_len <= 0)
						{
							SOCK_TRACE("select read event error");
							break;
						}
						else
						{
							do
							{
								// SOCK_TRACE("s[%d]读=%d", app_id, read_len);
								if(read_len > 0)
								{
									//读到了数据，在recv_buf中，长度为read_len
									event_param.recv_ind_t.len = read_len;
									event_param.recv_ind_t.data = recv_buf;
									SOCK_CB(SOCKET_EVENT_RECEIVE, event_param);
								}

								read_len = recv(s_sockets[app_id].socket_id, recv_buf, RECV_BUF_LEN, 0);
							}while(read_len > 0);
						}					

					}
					else
					{
						SOCK_TRACE("select other socket event");
					}
				}
			}

			_sock_close(app_id);			

			luat_rtos_task_sleep(5000);
			_sock_reconnect(app_id);					
		}
		else
		{
			//等待connect ok
			SOCK_TRACE("wait conn ok sem");
			luat_rtos_semaphore_take(s_sockets[app_id].connect_ok_semaphore, 1000);
		}
	}
	
}
/*
//// 怀疑同时被调用，select有问题，故拆开
static void _recv_bakc_task_proc(void *arg)
{	
	fd_set read_set, error_set;
	struct timeval timeout;
	int ret, read_len;
	char * recv_buf = NULL;
	int app_id = *(int*)(arg);
	socket_event_param_t event_param;

	SOCK_TRACE("副收GO!s%d", app_id);

	while(1) {
		if(s_sockets[app_id].is_connected) {
			luat_rtos_semaphore_take(s_sockets[app_id].connect_ok_semaphore, LUAT_NO_WAIT);

			if(NULL == recv_buf)
			{
				recv_buf = (char *)LUAT_MEM_MALLOC(RECV_BUF_LEN);
				LUAT_DEBUG_ASSERT(recv_buf != NULL,"malloc recv_buf fail");
			}

			timeout.tv_sec = 60;
			timeout.tv_usec = 0;

			while(1)
			{
				FD_ZERO(&read_set);
				FD_ZERO(&error_set);
				FD_SET(s_sockets[app_id].socket_id,&read_set);
				FD_SET(s_sockets[app_id].socket_id,&error_set);

				SOCK_TRACE("before select");

				ret = select(s_sockets[app_id].socket_id+1, &read_set, NULL, &error_set, &timeout);

				if(ret < 0)
				{
					//失败
					SOCK_TRACE("select fail, ret %d",ret);
					break;
				}
				else if(ret == 0)
				{
					//超时
					SOCK_TRACE("select timeout");
				}
				else
				{
					if(FD_ISSET(s_sockets[app_id].socket_id, &error_set))
					{
						//出错
						SOCK_TRACE("select error event");
						break;
					}
					else if(FD_ISSET(s_sockets[app_id].socket_id, &read_set))
					{
						SOCK_TRACE("select read event");
						read_len = recv(s_sockets[app_id].socket_id, recv_buf, RECV_BUF_LEN, 0);
						//read event后，第一次读不到数据，表示出错
						//实测，在select时，服务器主动断开连接，会走到这里
						if(read_len <= 0)
						{
							SOCK_TRACE("select read event error");
							break;
						}
						else
						{
							do
							{
								SOCK_TRACE("s[%d]读=%d", app_id, read_len);
								if(read_len > 0)
								{
									//读到了数据，在recv_buf中，长度为read_len
									event_param.recv_ind_t.len = read_len;
									event_param.recv_ind_t.data = recv_buf;
									SOCK_CB(SOCKET_EVENT_RECEIVE, event_param);
								}

								read_len = recv(s_sockets[app_id].socket_id, recv_buf, RECV_BUF_LEN, 0);
							}while(read_len > 0);
						}					

					}
					else
					{
						SOCK_TRACE("select other socket event");
					}
				}
			}

			_sock_close(app_id);			

			luat_rtos_task_sleep(5000);
			_sock_reconnect(app_id);					
		}
		else
		{
			//等待connect ok
			SOCK_TRACE("wait conn ok sem");
			luat_rtos_semaphore_take(s_sockets[app_id].connect_ok_semaphore, 1000);
		}
	}
}*/


static void _conn_task_proc(void *arg)
{
	ip_addr_t remote_ip;
    struct sockaddr_in name;
    socklen_t sockaddr_t_size = sizeof(name);
    int ret, h_errnop;
    struct hostent dns_result;
    struct hostent *p_result;
	int app_id = *(int*)(arg);
	socket_event_param_t event_param;

    luat_rtos_task_sleep(6000); //// 不着急联网，反正一开始还在定位
	SOCK_TRACE("联网:%d", app_id);

	while(1) {
		//检查网络是否准备就绪，如果尚未就绪，等待1秒后循环重试
		while(!PORT_GPRS_IS_ON()) {
			SOCK_TRACE("wait net");
			luat_rtos_task_sleep(1000);
		}

		//执行DNS，如果失败，等待1秒后，返回检查网络逻辑，重试
		char buf[128] = {0};// TODO: 这个大小不能调小，调小就会出现dns fail。。。
		ret = lwip_gethostbyname_r(s_sockets[app_id].address, &dns_result, buf, sizeof(buf), &p_result, &h_errnop);
		if(ret == 0) {
			remote_ip = *((ip_addr_t *)dns_result.h_addr_list[0]);
		} else {
			luat_rtos_task_sleep(1000);
			SOCK_TRACE("dns fail:%s %d", s_sockets[app_id].address, ret);
			continue;
		}

		//创建套接字，如果创建失败，等待3秒后循环重试
		int socket_id = socket(AF_INET, 
							   (s_sockets[app_id].is_tcp)==1 ? SOCK_STREAM : SOCK_DGRAM, 
							   (s_sockets[app_id].is_tcp)==1 ? IPPROTO_TCP : IPPROTO_UDP);
		if(socket_id < 0)
		{
			SOCK_TRACE("new sock err%d", socket_id);
			luat_rtos_task_sleep(3000);
			continue;
		}
		s_sockets[app_id].socket_id = socket_id;
		
		//连接服务器，如果失败，关闭套接字，等待5秒后，返回检查网络逻辑，重试
		name.sin_family = AF_INET;
		name.sin_addr.s_addr = remote_ip.u_addr.ip4.addr;
		name.sin_port = htons(s_sockets[app_id].port);

        ret = connect(socket_id, (const struct sockaddr *)&name, sockaddr_t_size);
		if(ret < 0)
		{
			SOCK_TRACE("connect fail, ret %d",ret);
			_sock_close(app_id);
			event_param.connect_result = ret;
			SOCK_CB(SOCKET_EVENT_CONNECT, event_param);			
			luat_rtos_task_sleep(5000);
			continue;
		}
		SOCK_TRACE("connect ok");
		s_sockets[app_id].is_connected = 1;
		
		fcntl(socket_id, F_SETFL, O_NONBLOCK);

		if (NULL == s_sockets[app_id].connect_ok_semaphore)
		{
			luat_rtos_semaphore_create(&s_sockets[app_id].connect_ok_semaphore, 1);
		}
		luat_rtos_semaphore_release(s_sockets[app_id].connect_ok_semaphore);
		   

		if(s_sockets[app_id].send_task_handle == NULL)
		{
			snprintf(s_sockets[app_id].send_task_name, SOCKET_TASK_NAME_MAX_LEN, "%s_%d", "sock_snd", app_id);
			luat_rtos_task_create(&s_sockets[app_id].send_task_handle, 2048, 20, s_sockets[app_id].send_task_name, _send_task_proc, &s_sockets[app_id].app_id, SEND_TASK_MAILBOX_EVENT_MAX);
		}
		if(s_sockets[app_id].recv_task_handle == NULL)
		{
			snprintf(s_sockets[app_id].recv_task_name, SOCKET_TASK_NAME_MAX_LEN, "%s_%d", "sock_rcv", app_id);
            //if (app_id == 0) {
		        luat_rtos_task_create(&s_sockets[app_id].recv_task_handle, 2048, 20, s_sockets[app_id].recv_task_name, _recv_task_proc, &s_sockets[app_id].app_id, 0);
            //} else {
            //    luat_rtos_task_create(&s_sockets[app_id].recv_task_handle, 2048, 30, s_sockets[app_id].recv_task_name, _recv_bakc_task_proc, &s_sockets[app_id].app_id, 0);
            //}
		}

		event_param.connect_result = ret;
		SOCK_CB(SOCKET_EVENT_CONNECT, event_param);	

		break;//// 连接完了，可以delete此任务了
    }

	// 打印出来该任务自启动起来最小剩余栈空间大小
	//然后我们就可以计算出最大使用的大小，一般可以再乘以1.5左右作为最终分配的值，必须是4的倍数
	// LUAT_DEBUG_PRINT("before luat_rtos_task_delete, %d", luat_rtos_task_get_high_water_mark());

	SOCK_TRACE("exit");
	s_sockets[app_id].connect_task_exist = 0;
	luat_rtos_task_delete(s_sockets[app_id].connect_task_handle);	
}



static int _sock_connect(int app_id, int is_tcp, char *address, int port, socket_service_event_callback_t callback)
{
	if (!_sock_exist(app_id)) {
		return -1;
	}

	if(!s_sockets[app_id].connect_task_exist) {
		s_sockets[app_id].app_id = app_id;
		s_sockets[app_id].is_tcp = is_tcp;

		if (!s_sockets[app_id].address)
		{
			LUAT_MEM_FREE(s_sockets[app_id].address);
			s_sockets[app_id].address = NULL;
		}
		s_sockets[app_id].address = LUAT_MEM_MALLOC(strlen(address)+1);
		memset(s_sockets[app_id].address, 0, strlen(address)+1);
		memcpy(s_sockets[app_id].address, address, strlen(address));
		
		s_sockets[app_id].port = port;
		s_sockets[app_id].callback = callback;

		////snprintf(s_sockets[app_id].connect_task_name, SOCKET_TASK_NAME_MAX_LEN, "%s_%d", "socket_connect", app_id);
		snprintf(s_sockets[app_id].connect_task_name, SOCKET_TASK_NAME_MAX_LEN, "%s_%d", "sock", app_id);
		s_sockets[app_id].connect_task_exist = (luat_rtos_task_create(&s_sockets[app_id].connect_task_handle, 2560, 30, s_sockets[app_id].connect_task_name, _conn_task_proc, &s_sockets[app_id].app_id , 0)==0);
	}

	return 	s_sockets[app_id].connect_task_exist ? 0 : -1;
}

//同步插入队列结果通过返回值判断：0成功，其他失败
//插入成功后，异步发送结果通过callback通知
int _sock_send(int app_id, uint32_t len, const INT8U *data, int user_param)
{
	if (!_sock_exist(app_id)) {
		return -1;
	}

	if(data==NULL || len==0) {
		return -1;
	}
	SOCK_SEND_T *data_item = (SOCK_SEND_T *)LUAT_MEM_MALLOC(sizeof(SOCK_SEND_T));
	LUAT_DEBUG_ASSERT(data_item != NULL,"malloc data_item fail");

	data_item->data = LUAT_MEM_MALLOC(len);
	LUAT_DEBUG_ASSERT(data_item->data != NULL, "malloc data_item.data fail");
	memcpy(data_item->data, data, len);
	data_item->len = len;
	data_item->user_param = user_param;

	int ret = luat_rtos_message_send(s_sockets[app_id].send_task_handle, 0, data_item);

	if(ret != 0)
	{
		LUAT_MEM_FREE(data_item->data);
		data_item->data = NULL;
		LUAT_MEM_FREE(data_item);
		data_item = NULL;
	}

	return ret;
}

///////////////////////////////博琨对上层的接口////////////////////////////////////////

static void _sock_event_CB(int app_id, SOCKET_EVENT_E event, socket_event_param_t param)
{
	// LUAT_DEBUG_PRINT("[app_id %d]: %d", app_id, event);

	switch (event)
	{
	case SOCKET_EVENT_CONNECT:
		if (param.connect_result >= 0) {//// 连成功
            ////LUAT_DEBUG_PRINT("[app_id %d]: connect ok", app_id);
			////socket_send(app_id, 13, "socket0 login", DEV_LOGIN);
			s_sockfd_list[app_id].sock_fd = s_sockets[app_id].socket_id;
            s_sockfd_list[app_id].state = SOCK_OK;
            s_sockfd_list[app_id].need_run_sentCB = FALSE;
            GPRS_TRACE("s[%d]conn OK%d", app_id, s_sockfd_list[app_id].sock_fd);
            s_sockfd_list[app_id].link_cb(app_id, 0, NULL);
 		} else {
			// LUAT_DEBUG_PRINT("[app_id %d]: connect fail, result %d", param.connect_result);
			GPRS_TRACE("s[%d]conn ERR%d", app_id, param.connect_result);
            if (s_sockfd_list[app_id].close_cb) {
                s_sockfd_list[app_id].close_cb(app_id, 0, NULL);
            }
            s_sockfd_list[app_id].sock_fd = SOCKET_FD_INVALID;
            s_sockfd_list[app_id].state = SOCK_IDLE;
		}
		break;
	case SOCKET_EVENT_SEND://// 发送结果，也可以通过param.send_cnf_t.user_param来做相应的关联
        if (param.send_cnf_t.result >= 0) {
				s_sockfd_list[app_id].need_run_sentCB = FALSE;
                if (s_sockfd_list[app_id].sent_cb) {
                    s_sockfd_list[app_id].sent_cb(app_id, 0, NULL);
                }
			} else {// 发送失败，但是底层会自己关闭sock，重连
                GPRS_TRACE("s[%d]send ERR%d", app_id, param.send_cnf_t.result);
			}
		/*if (DEV_LOGIN == param.send_cnf_t.user_param){
			LUAT_DEBUG_PRINT("[app_id %d]: send DEV_LOGIN %s", app_id, (param.send_cnf_t.result > 0) ? "OK" : "FAIL");

			if (param.send_cnf_t.result > 0){
				socket0_send_heart();
			}else{

			}
		}else if (DEV_HEART == param.send_cnf_t.user_param){
			LUAT_DEBUG_PRINT("[app_id %d]: send DEV_HEART %s", app_id, (param.send_cnf_t.result > 0) ? "OK" : "FAIL");

			if (param.send_cnf_t.result > 0){
				socket0_send_heart();
			}else{

			}
		}	*/
		break;
	case SOCKET_EVENT_RECEIVE://// 收到数据
		//GPRS_TRACE("s[%d]收:%d", app_id, param.recv_ind_t.len);
        if (s_sockfd_list[app_id].recv_cb) {
            s_sockfd_list[app_id].recv_cb(app_id, param.recv_ind_t.len, param.recv_ind_t.data);
        }
		break;
	default:
		break;
	}	
}


// 目前的做法是new和connect用同一个函数，new完毕就自动去连接了
// 创建成功返回0或者原有的sock，失败返回-1
int PORT_GPRS_sock_new(SOCK_INDEX_E index, bool is_tcp, char* url, INT8U u_len, INT16U port,
                        sock_cb l_cb, sock_cb s_cb, sock_cb r_cb, sock_cb c_cb)
{
    int ret;
    if (index >= SOCK_FD_MAX) return -1;
    if (u_len >= URL_LEN) {
        GPRS_TRACE("[%d]URL过长%d", index, u_len);
        return -1;
    }

    // if (PORT_GPRS_IS_ON() == FALSE) return -1;
    if (s_sockfd_list[index].sock_fd != -1) return  s_sockfd_list[index].sock_fd;

    s_sockfd_list[index].state      = SOCK_IDLE;
    memcpy(s_sockfd_list[index].server_str, url, u_len);
    s_sockfd_list[index].server_str[u_len] = '\0';
    s_sockfd_list[index].port       = port;
    
    s_sockfd_list[index].link_cb    = l_cb;
    s_sockfd_list[index].sent_cb    = s_cb;
    s_sockfd_list[index].recv_cb    = r_cb;
    s_sockfd_list[index].close_cb   = c_cb;
    s_sockfd_list[index].is_tcp     = is_tcp;
    // 这句多余，如果是-1前面就返回了。s_sockfd_list[index].sock_fd = -1;
    // s_sockfd_list[index].is_linked = FALSE;
    GPRS_TRACE("[%d]连%s:%d", index, s_sockfd_list[index].server_str, s_sockfd_list[index].port);

    ret = _sock_connect(index, 
                    s_sockfd_list[index].is_tcp, 
                    s_sockfd_list[index].server_str, 
                    s_sockfd_list[index].port, 
                    _sock_event_CB);
    return ret;
}

SOCK_STATE_E PORT_GPRS_sock_state(SOCK_INDEX_E index)
{
    if (index >= SOCK_FD_MAX) return SOCK_NONE;
    return (s_sockfd_list[index].state);
}

// 发送接口
int PORT_GPRS_sock_send(SOCK_INDEX_E index, const INT8U *sbuf, int len)
{
    int ret;
    if (index >= SOCK_FD_MAX) return -1;
    if (s_sockfd_list[index].state != SOCK_OK) return -2;

    ret = _sock_send(index, len, sbuf, 0);
    // 目前是放task里处理，是否发送成功，不管，返回0就当成功
    GPRS_TRACE("sock%d[%d]发=%d", index, s_sockfd_list[index].sock_fd, ret);
    if (ret == 0) {   // 已发完，调用通知
        s_sockfd_list[index].sent_cb(index, 0, NULL);
        return len;
    } else {            // 没发完
        s_sockfd_list[index].need_run_sentCB = true;
        return -3;
    }
}

int PORT_GPRS_sock_close(SOCK_INDEX_E index)
{
    if (index >= SOCK_FD_MAX) return -1;

    _sock_close(index);
    GPRS_TRACE("关sock%d[%d]", index, s_sockfd_list[index].sock_fd);
    
    s_sockfd_list[index].sock_fd = SOCKET_FD_INVALID;
    s_sockfd_list[index].state = SOCK_IDLE; //// 改，这样能重连
    return 0;
}

void PORT_Socket_MspInit(void)
{
    INT8U i;
    // 初始化socket
    memset(&s_sockfd_list, 0, sizeof(s_sockfd_list));
    for (i=0; i<SOCK_FD_MAX; i++) {
        s_sockfd_list[i].state  = SOCK_NONE;
        s_sockfd_list[i].sock_fd= SOCKET_FD_INVALID;
    }
}


