/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : dal_public.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2018年3月7日
  最近修改   :
  功能描述   : dal_public,公共数据存储管理
  函数列表   :
              DAL_PP_Init
              DAL_PP_Read
              DAL_PP_Ready
              DAL_PP_RestoreDefault
              DAL_PP_Store
              DAL_PP_Store_RAM
              DAL_PP_Update_NVRAM
              DAL_PP_Valid
              _fill_with_default_value
              _get_pubpara_addr
              _pp_check_nvram
              _pp_delay_tmr_func
              _pp_init_delay_tmr_func
  修改历史   :
  1.日    期   : 2018年3月7日
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/
/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#define  PUBLIC_C

#include "bk_os_task.h"
#include "bk_our_tools.h"
#include "bk_port_public.h"
#include "bk_dal_public.h"
#include "bk_port_hardware.h"


#if PP_DEBUG > 0
#define PP_TRACE OURAPP_trace
#else
#define PP_TRACE(FORMAT,...) 
#endif

#define MAX_RETRY_PP    3   // ec200重试3次

#define USE_SECURE_PP   1   // 使用flash来作为PP的存储接口


// static INT8U s_pp_init_tmr = INVALID_TMR;
static INT8U s_pp_ok_flag;// 160605 add

#if USE_SECURE_PP == 0
#define DONOT_USE_TIMER_TO_SAVE_BACK        1   // 不使用timer来存储back区. 

#if DONOT_USE_TIMER_TO_SAVE_BACK == 0
static INT8U s_pp_saveback_tmr = INVALID_TMR;
#endif

// 延时存储的处理，存储back分区
static void _pp_delay_tmr_func(void)
{
    //PP_TRACE("pp tmr:%d", s_pp_saveback_tmr);
    PORT_PP_Write(FALSE);
}

#endif

static void* _get_pubpara_addr(INT8U ParaID)
{
    INT8U  i;
    INT16U offset = 0;

    INT8U* ptr = PORT_PP_Get_RamPtr();

    for (i = 0; i < ParaID; i++) {
        offset += (PubTbl[i].len + 1);
    }
    offset++;   // 跳过校验和
    return (ptr+offset);
}

/*****************************************************************************
 函 数 名  : _fill_with_default_value
 功能描述  : 把默认值赋值给ram中，并计算校验和
 输入参数  : INT8U ParaID  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
static BOOLEAN _fill_with_default_value(INT8U ParaID)
{
    INT8U *ptr;
    
    if (ParaID >= sizeof(PubTbl)/sizeof(PubTbl[0])) return FALSE;
    
    if (PubTbl[ParaID].i_ptr != 0) {
        ptr = _get_pubpara_addr(ParaID);
        memcpy(ptr, PubTbl[ParaID].i_ptr, PubTbl[ParaID].len);
        *(ptr - 1) = ChkSum_N(ptr, PubTbl[ParaID].len);
        return TRUE;
    } else {
        return FALSE;
    }
}

/*****************************************************************************
 函 数 名  : DAL_PP_Valid
 功能描述  : 判断校验和来确定para是否有效
 输入参数  : INT8U ParaID  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN DAL_PP_Valid(INT8U ParaID)
{
    INT8U *ptr;

    if (ParaID >= sizeof(PubTbl)/sizeof(PubTbl[0])) return FALSE;

    ptr = _get_pubpara_addr(ParaID);
    if (*(ptr - 1) == ChkSum_N(ptr, PubTbl[ParaID].len))
        return TRUE;
    else 
        return FALSE;
}

/*****************************************************************************
 函 数 名  : DAL_PP_Read
 功能描述  : 读取PP
 输入参数  : INT8U ParaID  
             void *ptr     
 输出参数  : 无
 返 回 值  : 长度
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
INT8U DAL_PP_Read(INT8U ParaID, void *ptr)
{
    if (!DAL_PP_Valid(ParaID)) {// 200826 change, 对读取的参数做有效判断，无效先尝试恢复默认值
        if(!_fill_with_default_value(ParaID)) return 0;
    }
    
    memcpy(ptr, _get_pubpara_addr(ParaID), PubTbl[ParaID].len);
    return PubTbl[ParaID].len;
}

/*****************************************************************************
 函 数 名  : DAL_PP_Store_RAM
 功能描述  : 存储某个pp，但不立即写到NVRAM
 输入参数  : INT8U ParaID  
             void *ptr     
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Store_RAM(INT8U ParaID, void *ptr)
{
    INT8U *imageptr;
    
    if (ParaID >= sizeof(PubTbl)/sizeof(PubTbl[0])) return;

    imageptr  = _get_pubpara_addr(ParaID);
    memcpy(imageptr, ptr, PubTbl[ParaID].len);
    *(imageptr - 1) = ChkSum_N(imageptr, PubTbl[ParaID].len);
}

/*****************************************************************************
 函 数 名  : DAL_PP_Update_NVRAM
 功能描述  : 立即更新到NVRAM
 输入参数  : immi, true 表示立即更新，falsh表示走定时器    
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Update_NVRAM(BOOLEAN immi)
{
    if (s_pp_ok_flag != 0xaa) {
        return;   // 160605 容错处理
    }
    PORT_PP_Write(TRUE);
#if USE_SECURE_PP == 0
    if (immi) {
        PORT_PP_Write(FALSE);   // 如果立即就马上写back分区
    } else {
        #if DONOT_USE_TIMER_TO_SAVE_BACK == 0
        StartTmr(s_pp_saveback_tmr, _HALFSECOND, FALSE);
        #else
        _pp_delay_tmr_func();
        #endif
    }
#endif
}
/*****************************************************************************
 函 数 名  : DAL_PP_Store
 功能描述  : 存储某个PP，并立即更新到NVRAM
 输入参数  : INT8U ParaID  
             void *ptr     
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Store(INT8U ParaID, void *ptr)
{
    DAL_PP_Store_RAM(ParaID, ptr);
#if PP_DEBUG > 0
    OURAPP_trace("PP_Save:%d", ParaID);
#endif
    DAL_PP_Update_NVRAM(FALSE);
}

/*****************************************************************************
 函 数 名  : DAL_PP_RestoreDefault
 功能描述  : 恢复所有pp默认值，并更新到NVRAM
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_RestoreDefault(BOOLEAN update)// 160413 add 增加不强制写文件的接口
{
    INT8U i;

    for (i = 0; i < sizeof(PubTbl)/sizeof(PubTbl[0]); i++) {
        _fill_with_default_value(i);
    }
    if (update)
        DAL_PP_Update_NVRAM(TRUE);
}
// 160605 add
static void _pp_check_nvram(void)
{
    INT8U i;
    
    if (!PORT_PP_RamValid()) {
        DAL_PP_RestoreDefault(TRUE);
        PP_TRACE("DAL_PP_RestoreDefault!");
    } else {
        BOOLEAN needSave = FALSE; //160413 change, 仅用默认的覆盖，不存入文件
        PP_TRACE("PP Valid:%d/%d", sizeof(PubTbl), sizeof(PubTbl[0]));
        for (i = 0; i < sizeof(PubTbl)/sizeof(PubTbl[0]); i++) {
            if ((PubTbl[i].i_ptr != NULL) && !DAL_PP_Valid(i)) { // 逐项检测，如果无效就用默认值替代
                needSave = TRUE;//160413 change, 仅用默认的覆盖，不存入文件
                PP_TRACE("%d not valid", i);
                _fill_with_default_value(i);
                
            }
        }
        if (needSave) {
            DAL_PP_Update_NVRAM(TRUE);
            PP_TRACE("update PP");
        }
    }
}

static void _PP_init_delay_hdl(void)
{
    if (PORT_PP_Read()) {
        s_pp_ok_flag = 0xaa;    // 有效
        //_pp_check_nvram(); // 怕用pp的用到00数据导致错误，每次还是都去初始化一下nvram
    } else {
        PP_TRACE("ppi_fail%d", s_pp_ok_flag);

        /*if (s_pp_ok_flag < MAX_RETRY_PP) {
            s_pp_ok_flag++;
            StartTmr(s_pp_init_tmr, _SECOND, FALSE);// 500ms试一次
        } else {
            s_pp_ok_flag = 0xaa; // 无效, 同样也要初始化到nvram
            //_pp_check_nvram();
        }*/
        s_pp_ok_flag = 0xaa; // 无效, 同样也要初始化到nvram
    }
    _pp_check_nvram();
}

BOOLEAN DAL_PP_Ready(void)
{
    if (s_pp_ok_flag < MAX_RETRY_PP) 
        return false;
    else
        return true;
}
// 160605 add end
/*****************************************************************************
 函 数 名  : DAL_PP_Init
 功能描述  : pp的初始化入口, 隐含对PP的nvram写操作(在判断pp无效时)
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2014年6月9日
    作    者   : rocky
    修改内容   : 新生成函数

*****************************************************************************/
void DAL_PP_Init(void)
{
    //INT8U i;
    //BOOLEAN needSave;
    BOOLEAN ret;

    PORT_PP_Init();
#if USE_SECURE_PP == 0 // 用securedata来实现的话，就没有back操作了
    #if DONOT_USE_TIMER_TO_SAVE_BACK == 0
    PP_TRACE("%s", __FUNCTION__);
    s_pp_saveback_tmr = CreateTmr(_pp_delay_tmr_func);
    #endif
#endif

#if 1 // 160605 change，改个定时初始化，5s
    s_pp_ok_flag = 0;
    ret = PORT_PP_Read();
    // TODO: 测试屏蔽
    if (!ret) {
        //PP_TRACE("PP读错,重试!");
        //s_pp_init_tmr = CreateTmr(_PP_init_delay_hdl);
        //StartTmr(s_pp_init_tmr, _HALFSECOND, FALSE);// 500ms试一次
        PP_TRACE("PP读错,恢复!");
        _PP_init_delay_hdl();   // 尝试不用延时，那是sim800的bug
    } else {
        s_pp_ok_flag = 0xaa;    // 有效
    }
    _pp_check_nvram();  // 这边会马上恢复默认参数到ram
#else
    ret = PORT_PP_Read();
    if (!ret) {
#if PP_DEBUG > 0
        OURAPP_trace("PORT_PP_Read Fail!, set Default!");
#endif
        // TODO: 暂时先这么处理，不返回 return;
    }
    
    if (!PORT_PP_RamValid()) {
        DAL_PP_RestoreDefault(FALSE); //160413 change, 仅用默认的覆盖，不存入文件
        
#if PP_DEBUG > 0
        OURAPP_trace("DAL_PP_RestoreDefault!");
#endif
    } else {
        //needSave = FALSE; //160413 change, 仅用默认的覆盖，不存入文件
        for (i = 0; i < sizeof(PubTbl)/sizeof(PubTbl[0]); i++) {
            if ((PubTbl[i].i_ptr != NULL) && !DAL_PP_Valid(i)) { // 逐项检测，如果无效就用默认值替代
                //needSave = TRUE;//160413 change, 仅用默认的覆盖，不存入文件
#if PP_DEBUG > 0
                OURAPP_trace("[%d] not valid", i);
#endif
                _fill_with_default_value(i);
            }
        }
        /*if (needSave) {
            DAL_PP_Update_NVRAM(TRUE);
#if PP_DEBUG > 0
            OURAPP_trace("update PP");
#endif
        }*///160413 change, 仅用默认的覆盖，不存入文件
    }
#endif
}

