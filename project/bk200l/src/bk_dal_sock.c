

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#include "bk_our_tools.h"
#include "bk_our_stream.h"
#include "bk_port_gprs.h"

#include <string.h>
#include "bk_dal_sock.h"
////#include "dal_public.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/
//INT8U  get_BatteryVoltage(void);

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define GSM_STACK_SIZE          4096
#define GSM_THREAD_PRIORITY     110

//#define GSM_UART_PORT           "uart1"
//#define GSM_POWER_PIN   GET_PIN(C, 5)////37
//#define GSM_MODEM_PIN   GET_PIN(A, 7)
////#define GSM_WAKEUP_PIN -1
#define GSM_RECV_BUFF_LEN       1024

#define SOCKET_NUM_MAX 	3   // 231208 加入副中心的支持   // 目前仅供主链路用，OTA另外维护 // SOCKET_CH_MAX   // 2

#define SOCKET_SEND_LEN     1024
#define SOCKET_RECV_LEN     1024

/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
struct SOCK_LINK {
    SOCK_STATE_E    state;
//    int             fd;
    GPRS_GROUP_T    host;
//    INT8U           s_buf[SOCKET_SEND_LEN];    // 发送缓存,为了防止malloc异常，用静态
    INT8U           r_buf[SOCKET_RECV_LEN];    // 接收缓存,为了防止malloc异常，用静态
	ROUND_T         r_round;
    INT32U          lastConnectSec;   // 最近一次连接时间
    INT32U          reConntCnt;      // 重连次数
};

static struct SOCK_LINK s_sock[SOCKET_NUM_MAX];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/


#ifdef EN_UDP
bool dal_connectUdpLog(void);
#endif


// socket链接已创建的处理回调
static void _sock_link_cb(SOCK_INDEX_E index, s32 errCode, void* customParam)
{
    if (index >= SOCK_FD_MAX){
        BK_LOG("link-cb:err i=%d", index);
        return;
    }

    if (errCode == 0) { // 用于port_gprs来通知connect出错
        BK_LOG("m链OK!");
        s_sock[index].state = SOCK_OK;
    } else {
        BK_LOG("m链失败");
        s_sock[index].state = SOCK_IDLE;
        PORT_GPRS_sock_close(index);
    }
}

static void _sock_sent_cb(SOCK_INDEX_E index, s32 errCode, void* customParam)
{
    // TODO: 发送完毕通知回调。目前没啥操作
}

static void _sock_recv_cb(SOCK_INDEX_E index, s32 rlen, void* rbuf)
{
    if (index >= SOCK_FD_MAX){
        BK_LOG("r-cb:err[%d]", index);
        return;
    }
    if (rlen > 0) {
        //BK_LOG("sock[%d]收成功[%d]", index, rlen);
        //BK_LOG_H(rbuf, rlen);
        _Round_WriteBlock(&s_sock[index].r_round, (INT8U*)rbuf, rlen);
    }
}

static void _sock_close_cb(SOCK_INDEX_E index, s32 errCode, void* customParam)// socket意外断开?
{
    if (index >= SOCK_FD_MAX){
        BK_LOG("close-cb:err[%d]", index);
        return;
    }
    
    BK_LOG("m%d close-cb:[%d]",index, errCode);
    
    s_sock[index].state = SOCK_IDLE;    // 感觉也没啥用
    //// 240111 del, 感觉用不上 屏蔽 PORT_GPRS_sock_close(index);
}


void Dal_SockEnable(SOCK_INDEX_E index, GPRS_GROUP_T* para)
{
    //int ret;
    if (index >= SOCKET_NUM_MAX) return;
    if ((index > 0) && !para->isvalid) return;    // 副ip才判断是否valid
    
    memcpy((void*)&s_sock[index].host, (void*)para, sizeof(GPRS_GROUP_T));
    
    BK_LOG("EN s_%d", index);
    s_sock[index].state = SOCK_IDLE;
    ////BK_InitLoopBuffer(&s_sock[index].s_round, s_sock[index].s_buf, SOCKET_SEND_LEN);
    _Round_Init(&s_sock[index].r_round, s_sock[index].r_buf, SOCKET_RECV_LEN);
    PORT_GPRS_sock_new(index, TRUE, s_sock[index].host.ip, strlen(s_sock[index].host.ip),
                s_sock[index].host.port, _sock_link_cb, _sock_sent_cb, _sock_recv_cb, _sock_close_cb);
}

void Dal_SockDisable(SOCK_INDEX_E  index)
{
    if (index >= SOCKET_NUM_MAX) return;

    BK_LOG("关闭sock%d,state[%d]", index,s_sock[index].state);
    if (0 <= PORT_GPRS_sock_close(index)) {
        BK_LOG("关闭OK");
        s_sock[index].state = SOCK_NONE;
        s_sock[index].reConntCnt = 0;
    }
}
#if (BL_EN > 0) && (BL_DEBUG > 0)
BOOLEAN g_fake_fail = FALSE;
#endif
BOOLEAN Dal_SockSend(SOCK_INDEX_E index, INT8U* sptr, INT16U slen)
{
    #if (BL_EN > 0) && (BL_DEBUG > 0)
    if (g_fake_fail) return TRUE;   //// 231111 add, 模拟盲区点
    #endif
    
    if (index >= SOCKET_NUM_MAX) return FALSE;
    //if (index == 0) {
        BK_LOG("S%d发%d",index, slen);
        BK_LOG_H(sptr, slen);
    //}
    if ((s_sock[index].state != SOCK_OK)) {
        BK_LOG("S%d状态错%d,%d", index,s_sock[index].state);
        return FALSE;
    }

    if (PORT_GPRS_sock_send(index, sptr, slen)) {
        //BK_LOG("SSend%d加OK", index);
        return TRUE;
    } else {
        BK_LOG("S%d发败", index);
        Dal_SockDisable(index);
        s_sock[index].state = SOCK_IDLE;
        return FALSE;
    }
}

INT16S DAL_SockReadByte(SOCK_INDEX_E index)
{
    if (index >= SOCKET_NUM_MAX) return -1;

    return _Round_ReadByte(&s_sock[index].r_round);
}

SOCK_STATE_E Dal_SockGetState(SOCK_INDEX_E index)
{
    if (index >= SOCKET_NUM_MAX) return SOCK_NONE;

    return PORT_GPRS_sock_state(index);
}

void DAL_GPRSInit(void)
{
    INT8U i;
    //GPRS_GROUP_T para = {TRUE, 9999, "ydzc.borcom.net"};//// for test

    // 初始化socket
    for (i=0; i<SOCKET_NUM_MAX; i++) {
        s_sock[i].state = SOCK_NONE;
        s_sock[i].host.isvalid = FALSE;
    }
    PORT_GPRS_Init();
    PORT_Socket_MspInit();

    //Dal_SockEnable(SOCK_FD_MAIN, &para);// for test
}

void DAL_GSMThreadDeInit(void)
{
    // TODO: 这边需要关闭模块等处理
   // rt_thread_delete(s_gsm_tid);
}


