/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : port_blind.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年5月22日, 星期一
  最近修改   :
  功能描述   : 盲区实现的底层适配文件
  函数列表   :
              PORT_BL_DeleteAll
              PORT_BL_DeleteFile
              PORT_BL_Init
              PORT_BL_InsertNode
              PORT_BL_ReadFile
  修改历史   :
  1.日    期   : 2023年5月22日, 星期一
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#if BL_EN > 0
#include "luat_fs.h"
#include "bk_our_tools.h"
#include "bk_port_blind.h"
#include "bk_port_uart.h"
#include "bk_dal_systime.h"

// TODO: 这边给盲区的接口
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#if BL_DEBUG > 0
#define BLP_TRACE OURAPP_trace
#define BLP_LOGH  OURAPP_printhexbuf
#else
#define BLP_TRACE(FORMAT,...) 
#define BLP_LOGH(A, B) 
#endif

// LFS没有目录太多操作，改为只存取1个文件好了

#define BL_CUR_FILE     "/BBlind.bin"   // 存当前
// #define BL_SAVE_DIR     "/BKBlind"
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

static BOOLEAN s_bl_valid;

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/



#if 0
typedef struct {
    INT8U valid_flag1;      // 有效标志位, 0x55
    INT8U cur_cnt;          // 当前已经存储的个数, 最多10个点
    INT8U res[5];           // 预留5字节，用于扩展
    INT8U valid_flag2;      // 有效标志位2,0xaa
} BLIND_HEAD_T;

#define VALID_FLAG1 0x55
#define VALID_FLAG2 0xAA

static INT8U s_file_cache[MAX_BLIND_CNT*BLIND_NODE_SIZE+sizeof(BLIND_HEAD_T)];
/*  设计思路:
    类似小渔船版本的，但是一个文件而不是一个block存一包数据。文件内部都是head+data的方式。
    head目前比较简单，就标志位，和内部包括的节点数.
    data固定存20字节，具体20字节如何分配，由dal层决定。

    具体实现方式: 文件分为2种名称:BL180520是一种，这种都是放置已经存满BL的文件；
    另外一种是当前正在操作的文件，名称是CurBlind.
    当CurBlind里存满后，用rename接口改成BLxxxxxx的格式，时间取改名时的时间。如果xxxxxx有重名的，在后面加1

    这样读写就简单了，读取bl就先读取BLxxxxxx，有就成功，否则读取CurBlind
    
*/
// TODO: 如果时间不准，可能会导致重名，届时在write时会覆盖旧节点
static void _new_fname(char* dname)
{
    char sname[50];
    SYSTIME_T tt;

    DAL_GetSysTime(&tt);

    // 年月日时分，没有秒
    sprintf(sname, ""BL_SAVE_DIR"%02d%02d%02d%02d%02d.bin", tt.date.year, tt.date.month, tt.date.day, tt.time.hour, tt.time.minute);
    strcpy(dname, sname);
}

// 删除指定目录内10个文件。返回删除个数
static int _del_old_file(int total)
{
    int lsdir_cnt, i, ret, cnt;
    char path[255] = {0};
    luat_fs_dirent_t *fs_dirent = LUAT_MEM_MALLOC(sizeof(luat_fs_dirent_t) * total);

    if (!fs_dirent) {
        BLP_TRACE("malloc err");
        return -1;
    }
    memset(fs_dirent, 0, sizeof(luat_fs_dirent_t) * total);

    lsdir_cnt = luat_fs_lsdir(BL_SAVE_DIR, fs_dirent, 0, total);

    cnt = 0;
    for (i=0; i<lsdir_cnt; i++) {
        if (((fs_dirent+i)->d_type) == 0) {//0是文件，1是目录. 理论上，本目录内不会有子目录
            memset(path, 0, sizeof(path));
            snprintf(path, sizeof(path)-1, "%s/%s", BL_SAVE_DIR, (fs_dirent+i)->d_name);
            ret = luat_fs_remove(path);
            BLP_TRACE("删[%s]=%d", path, ret);
            if (!ret) cnt++;
        } else if (((fs_dirent+i)->d_type) == 1) {
            BLP_TRACE("目录![%s]", (fs_dirent+i)->d_name);
        }
    }
    LUAT_MEM_FREE(fs_dirent);
    fs_dirent = NULL;
    return cnt;
}

/*****************************************************************************
 函 数 名  : PORT_BL_ReadFile
 功能描述  : 读取最旧的一个BLxxxxxx文件，如果读取成功，则返回读取到的NODE个
             数。读不到，则去读CurBlind
 输出参数  : char* destname     out，用于存放读取到的文件的文件名
             INT8U* destbuf      out，用于存放读取到的文件内容所存放的内存指针
 输入参数  : INT16U max_destlen  in，对应内存的大小限制
             
 返 回 值  : 0表示没读取到，非0表示读取到的文件内的node个数
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年5月22日, 星期一
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
/*
INT8U PORT_BL_ReadFile(char* destname, INT8U* destbuf, INT16U max_destlen)
{
    int ret;

    QDIR *dp = NULL;
    QFILE fp = -1;
    char fName[60];
    int rLen;
    BLIND_HEAD_T* head;
    qdirent *info = NULL;


    int lsdir_cnt, i, ret, cnt;
    char path[255] = {0};
    luat_fs_dirent_t *fs_dirent = LUAT_MEM_MALLOC(sizeof(luat_fs_dirent_t) * 1);

    if (!fs_dirent) {
        BLP_TRACE("malloc err");
        return -1;
    }
// STEP1: 先找最旧的BL开头的文件
    memset(fs_dirent, 0, sizeof(luat_fs_dirent_t) * 1);
    lsdir_cnt = luat_fs_lsdir(BL_SAVE_DIR, fs_dirent, 0, 1);    // 就读1个文件

    if (lsdir_cnt == 1) {
        if ((fs_dirent[0]->d_type) == 0) {//0是文件，1是目录. 理论上，本目录内不会有子目录
            memset(path, 0, sizeof(path));
            snprintf(path, sizeof(path)-1, "%s/%s", BL_SAVE_DIR, fs_dirent[0]->d_name);
            ret = luat_fs_remove(path);
            BLP_TRACE("删[%s]=%d", path, ret);
            if (!ret) cnt++;
        } else if (((fs_dirent+i)->d_type) == 1) {
            BLP_TRACE("啊目录![%s]", fs_dirent[0]->d_name);
        }
    }
    // TODO: 待完善，写到这里，先不用此方法
    
    dp = ql_opendir(BL_SAVE_DIR);
	if(dp == NULL) {
	    BLP_TRACE("BLdir不存在");
	} else {
	    while(1) {
	        info = ql_readdir(dp);
            if (info == NULL) {
                BLP_TRACE("BLdir读败or读完");
                ret = ql_closedir(dp);
                dp = NULL;
                BLP_TRACE("BLdir关闭=%d", ret);
                ret = ql_remove(BL_SAVE_DIR);
                BLP_TRACE("BLdir删=%d", ret);    // 测试，去掉删除目录看正常否
                goto _BL_READ_FAULT;
            }
            if ((info->d_type == QL_FS_TYPE_FILE) ||
                ((info->d_name[0] != '.') && (strlen(info->d_name)>3))) {
                sprintf(fName, ""BL_SAVE_DIR"/%s", info->d_name);
                goto _BL_READ_OK;
	        }
	    }
	}
// 读不到BL开头的文件
_BL_READ_FAULT: // 读取目录出错，读当前文件
    strcpy(fName, BL_CUR_FILE);
_BL_READ_OK:
    // BLP_TRACE("BL读文件=%s", fName);
// STEP2: 打开文件
    fp = ql_fopen(fName, "rb"); // 只读方式打开
    BLP_TRACE("BL读:%s=%d", fName, fp);
    if (fp < 0) {
        BLP_TRACE("BL打开错%d", fp);
        goto _ERR_READ; // 出错处理        //return 0;
    }
// STEP3: 读取文件，先判断文件合法性及长度是否匹配
    ret = ql_fseek(fp, 0, QL_SEEK_SET);
    // BLP_TRACE("BK-FSEEK=%d", ret);
    rLen = ql_fread((void*)s_file_cache, 1, sizeof(s_file_cache), fp);

    if (rLen < 0) {
        BLP_TRACE("读LB错%d", rLen);
        goto _ERR_READ; // 出错处理
    }
    // BLP_TRACE("Fread=%d", rLen);
    //BLP_LOGH(s_file_cache, rLen);
    head = (BLIND_HEAD_T*)s_file_cache;// head合法性判断
    if ((rLen < BLIND_NODE_SIZE) || (head->cur_cnt > MAX_BLIND_CNT) || (head->cur_cnt == 0) ||
        (head->valid_flag1 != VALID_FLAG1) || (head->valid_flag2 != VALID_FLAG2)) {
        BLP_TRACE("BL头错%d", rLen);
        goto _ERR_READ; // 出错处理
    }
    
    // 判断数据合法性
    if ((rLen < (sizeof(BLIND_HEAD_T) + head->cur_cnt*BLIND_NODE_SIZE)) || (rLen > (sizeof(BLIND_HEAD_T) + max_destlen))) {
        BLP_TRACE("BL长度错%d,%d", rLen, head->cur_cnt);
    } else {
        // 到这里视为读取正常, 可以返回了
        strcpy(destname, fName);
        memcpy(destbuf, s_file_cache+sizeof(BLIND_HEAD_T), head->cur_cnt*BLIND_NODE_SIZE);
        ql_fclose(fp);
        if (dp) ql_closedir(dp);
        return head->cur_cnt;
    }
_ERR_READ:
    if (fp>=0) ql_fclose(fp);
    if (dp) ql_closedir(dp);
    return 0;
}*/

/*****************************************************************************
 函 数 名  : PORT_BL_DeleteFile
 功能描述  : 删除指定文件名的文件
 输入参数  : char* fn  待删除文件名
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年5月22日, 星期一
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN PORT_BL_DeleteFile(char* fn)
{
    int ret;

    ret = luat_fs_remove(fn);
    if (ret != 0) {
        BLP_TRACE("BL删错%d", ret);
        return FALSE;
    } else {
        BLP_TRACE("BL删OK:%s", fn);
        return TRUE;
    }
}
/**
BOOLEAN PORT_BL_DeleteAll(void)
{
	int ret = 0;
    
    do {
        ret = _del_old_file(5);
    } while (ret > 0) ;
    
    // 最后把BL_CUR_FILE删掉
    ret = luat_fs_remove(BL_CUR_FILE);
    BLP_TRACE("删cur=%d", ret);
    
    return TRUE;
}

// 存一个满包数据到新文件中
static BOOLEAN _save_new(INT8U* sptr, int slen)
{
    // 另存为一个新的文件
    char newName[60];
    BLIND_HEAD_T* head;
    int wLen;
    QFILE newHdl = -1;

    if (sptr == NULL) return FALSE;
    if (slen != (MAX_BLIND_CNT* BLIND_NODE_SIZE+sizeof(BLIND_HEAD_T))) return FALSE;

    head = (BLIND_HEAD_T*)sptr;
    head->valid_flag1 = VALID_FLAG1;
    head->valid_flag2 = VALID_FLAG2;
    head->cur_cnt = MAX_BLIND_CNT;
    _new_fname(newName);

    newHdl = ql_fopen(newName, "wb");// 强行创建文件
    if (newHdl < 0) {// 这样写错就放弃掉本文件
        BLP_TRACE("存新错");
        return FALSE;
    } else {
        wLen = ql_fwrite((void*)sptr, 1, slen, newHdl);
        if (wLen != slen) {// 写出错就放弃本文件
            BLP_TRACE("存新写错%d", wLen);
            ql_fclose(newHdl);
            ql_remove(newName);
            return FALSE;
        }
        ql_fclose(newHdl);
        BLP_TRACE("存新OK-%s", newName);
        return TRUE;
    }
}**/

#endif

/*****************************************************************************
 函 数 名  : PORT_BL_ReadData
 功能描述  : 从盲区文件的末尾读取一定长度的内容
 输入参数  : INT8U* destbuf      
             INT16U max_destlen  
 输出参数  : 无
 返 回 值  : 实际读到的长度，不会超出max_destlen.  0表示没读到
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2024年1月12日, 星期五
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
INT16U PORT_BL_ReadData(INT8U* destbuf, INT16U max_destlen)
{
    FILE* fp = NULL;
    int fileLen, ret;
    INT16U readLen = 0;

    if (!destbuf || (max_destlen % BLIND_NODE_SIZE)) {
        BLP_TRACE("Br参数错:%d,%d", destbuf, max_destlen);
        return 0;
    }
    fp = luat_fs_fopen(BL_CUR_FILE, "rb");
    if (!fp) {
        BLP_TRACE("BL打开err%d", fp);
        return 0;
    }
    luat_fs_fseek(fp, 0, SEEK_END);
    fileLen = luat_fs_ftell(fp);  // 获取大小
    BLP_TRACE("Rftell=%d", fileLen);
    if (fileLen <= 0) {// 获取失败, 或者长度为0
        goto _ERR_READFILE;
    } else if (fileLen % BLIND_NODE_SIZE) {
        BLP_TRACE("读长度err:%d", fileLen);
        goto _ERR_READFILE;
    }
    if (fileLen >= max_destlen) {
        ret = luat_fs_fseek(fp, (fileLen - max_destlen), SEEK_SET);
    } else {// 文件不够读，有多少读多少
        max_destlen = fileLen;
        ret = luat_fs_fseek(fp, 0, SEEK_SET);
    }
    if (ret < 0) {
        BLP_TRACE("读长度err_2:%d", fileLen);
        goto _ERR_READFILE;
    }
    ret = luat_fs_fread(destbuf, max_destlen, 1, fp);
    if (ret > 0) {
        BLP_TRACE("读BL OK:%d", ret);
        readLen = ret;
    } else {
        BLP_TRACE("读BL err:%d", ret);
    }

_ERR_READFILE:
    luat_fs_fclose(fp);
    return readLen;
}

BOOLEAN PORT_BL_DeleteData(INT16U del_len)
{
    FILE* fp = NULL;
    int ret;
    size_t fileLen;

    if ((del_len == 0) || (del_len % BLIND_NODE_SIZE)) {
        BLP_TRACE("dl err:%d", del_len);
        return FALSE;
    }
    fileLen = luat_fs_fsize(BL_CUR_FILE);
    if (fileLen <= 0) {
        BLP_TRACE("fsize错:%d", fileLen);
        return FALSE;
    }
    if (fileLen <=  del_len) {
        fileLen = 0;
    } else {
        fileLen -= del_len;
        if (fileLen % BLIND_NODE_SIZE) {
            BLP_TRACE("dl错2:%d", fileLen);
            fileLen = (fileLen / BLIND_NODE_SIZE) * BLIND_NODE_SIZE;
        }
    }
    
    ret = luat_fs_truncate(BL_CUR_FILE, fileLen);
    
    if (ret != fileLen) {
        BLP_TRACE("BL删错%d, fileLen", ret, fileLen);
        return FALSE;
    } else {
        BLP_TRACE("BL删OK:%d", ret);
        return TRUE;
    }
}

/*****************************************************************************
 函 数 名  : PORT_BL_DeleteAll
 功能描述  : 删除所有盲区
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年5月22日, 星期一
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN PORT_BL_DeleteAll(void)
{
	int ret = 0;
    
    ret = luat_fs_remove(BL_CUR_FILE);
    BLP_TRACE("删cur=%d", ret);
    if (ret) 
        return FALSE;
    else
        return TRUE;
}

/*****************************************************************************
 函 数 名  : PORT_BL_InsertNode
 功能描述  : 插入一个node到BL中。
 输入参数  : INT8U* sptr    
             INT8U nodelen  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年5月22日, 星期一
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
BOOLEAN PORT_BL_InsertNode(INT8U* sptr, INT8U nodelen)
{
    FILE* fp = NULL;
    int fsRet;
    //BLIND_HEAD_T* head;
    //int rLen = 0, wLen = 0;

    // 首先判断一下参数合法性
    if ((sptr == NULL) || (nodelen != BLIND_NODE_SIZE)) {
        BLP_TRACE("BLIN参错");
        return FALSE;
    }
// step1: 打开文件
    if (luat_fs_fexist(BL_CUR_FILE)) {// 文件存在
        fp = luat_fs_fopen(BL_CUR_FILE, "ab+");  // "a+": 追加更新模式，所有之前的数据都保留，只允许在文件尾部做写入。
    } else {
        fp = luat_fs_fopen(BL_CUR_FILE, "wb");   // "w": 写模式
    }
    if (!fp) {
        BLP_TRACE("fopen err:%d", fp);
        return FALSE;
    }
    
    fsRet = luat_fs_ftell(fp);
    BLP_TRACE("ftell=%d", fsRet);
    if (fsRet < 0) {// 获取失败
        luat_fs_fseek(fp, 0, SEEK_SET); // 移动到头部
    } else if (fsRet % BLIND_NODE_SIZE) {
        luat_fs_fseek(fp, (fsRet / BLIND_NODE_SIZE)*BLIND_NODE_SIZE, SEEK_SET);// 修正成整20的倍数
    }
    fsRet = luat_fs_fwrite(sptr, nodelen, 1, fp);
    BLP_TRACE("fwrite=%d", fsRet);
    luat_fs_fclose(fp);
    
    if (fsRet >= 0) return TRUE;
    else return FALSE;
    /*
    if (fp < 0) {
        BLP_TRACE("BLIN错1");
        //ql_remove(BL_CUR_FILE);// 以防万一，删除一下
        fp = ql_fopen(BL_CUR_FILE, "wb+");// 创建空文件
        if (fp < 0) {
            BLP_TRACE("BLIN错2");
            return FALSE;
        }
    }
    
// step2: 读取文件，判断合法性
    fsRet = ql_fseek(fp, 0, QL_SEEK_SET);// 移到头部
	if (fsRet < 0) {
		BLP_TRACE("没头ERR");
        goto _ERR_INSERT;
	}
    rLen = ql_fread((void*)s_file_cache, 1, sizeof(s_file_cache), fp);
    if (rLen < 0) {
        BLP_TRACE("BLIN读错%d", fsRet);
        goto _ERR_INSERT; // 出错处理
    }
    head = (BLIND_HEAD_T*)s_file_cache;// head合法性判断
    BLP_TRACE("BLIN头%d,%d,%x,%x", rLen, head->cur_cnt, head->valid_flag1,head->valid_flag2);
    if ((rLen < BLIND_NODE_SIZE) || (head->cur_cnt > MAX_BLIND_CNT) || (head->cur_cnt == 0) ||
        (head->valid_flag1 != VALID_FLAG1) || (head->valid_flag2 != VALID_FLAG2)) {
        BLP_TRACE("BLIN头错%d,%d,%x,%x", rLen, head->cur_cnt, head->valid_flag1,head->valid_flag2);
        // 出错就新建一个头, 最好把rLen也清空一下，因为如果文件读取失败，rLen会是随机数
        head->cur_cnt = 0;
        rLen = 0;
    } else {    
        if (rLen < (head->cur_cnt* BLIND_NODE_SIZE+sizeof(BLIND_HEAD_T))) {// 容错1
            BLP_TRACE("BLIN容错1");
            head->cur_cnt = (rLen - sizeof(BLIND_HEAD_T)) / BLIND_NODE_SIZE;
        }
    }
    if ((head->cur_cnt == MAX_BLIND_CNT) || (rLen == (MAX_BLIND_CNT* BLIND_NODE_SIZE+sizeof(BLIND_HEAD_T)))) {
        // 容错2, 把cur文件另存为一个BL文件
        BLP_TRACE("CUR已满,容错");
        _save_new(s_file_cache, rLen);
        head->cur_cnt = 0;// 再把cur清零
    }

// step3: 写入一包数据
    head->cur_cnt++;
    head->valid_flag1 = VALID_FLAG1;
    head->valid_flag2 = VALID_FLAG2;
    memcpy(s_file_cache + sizeof(BLIND_HEAD_T) + BLIND_NODE_SIZE*head->cur_cnt, sptr, BLIND_NODE_SIZE);
    
    fsRet = ql_fseek(fp, 0, QL_SEEK_SET);// 移到头位置
    if (fsRet < 0) {
		BLP_TRACE("移头ERR");
        goto _ERR_INSERT;
	}
    wLen = ql_fwrite((void*)s_file_cache, 1, sizeof(BLIND_HEAD_T) + BLIND_NODE_SIZE*head->cur_cnt, fp);
    if (wLen != (sizeof(BLIND_HEAD_T) + BLIND_NODE_SIZE*head->cur_cnt)) {// 写出错
        BLP_TRACE("BLIN写错%d", wLen);
        ql_fclose(fp);
        ql_remove(BL_CUR_FILE);
        return false;
    } else {
        BLP_TRACE("BLIN写OK%d", head->cur_cnt);
        ql_ftruncate(fp, wLen); // 为防止文件内容原先比较长，用truncate确保长度
        ql_fclose(fp);

        if (head->cur_cnt == MAX_BLIND_CNT) {// cur文件已满，需要改名
            char newName[50];
            _new_fname(newName);
            fsRet = ql_mkdir(BL_SAVE_DIR, 1);//// 这里是1，看手册是为了兼容C接口，无用。fsRet = ql_mkdir(BL_SAVE_DIR, 0x777);
            BLP_TRACE("mkdir=%d", fsRet);
            fsRet = ql_rename(BL_CUR_FILE, newName);
            BLP_TRACE("BLIN改名%s=%d", newName, fsRet);
        }
        return TRUE;
    }

_ERR_INSERT:
    if (fp >= 0) ql_fclose(fp);
    return FALSE;*/
}

/*****************************************************************************
 函 数 名  : PORT_BL_Init
 功能描述  : 初始化，判断剩余空间，如果不足，则删掉若干文件，
            文件大小已知，目录已知
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年5月22日, 星期一
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void PORT_BL_Init(void)
{
    int ret, newLen;

    luat_fs_info_t fs_info = {0};

    s_bl_valid = false;
// step1: 判断是否有足够空间
    luat_fs_init(); // 必须先初始化    
    luat_fs_info("/", &fs_info);
    
    if (fs_info.total_block > fs_info.block_used) {
        ret = (fs_info.total_block - fs_info.block_used) * fs_info.block_size;
    } else {
        ret = 0;
    }
    // 打印文件系统空间信息  print_fs_info 93:fs_info lfs 1 72 14 4096
    LUAT_DEBUG_PRINT("LFS:%s %d %d %d %d,剩%d", 
        fs_info.filesystem, 
        fs_info.type, 
        fs_info.total_block, 
        fs_info.block_used, 
        fs_info.block_size, ret);
    
#if 1// 240111 换个写法，就1个文件操作
    if (ret > 20000) { ////预留
        //BLP_TRACE("fs剩%d", ret);
        return;
    }
    // 说明空间不足，要瘦身
    ret = luat_fs_fsize(BL_CUR_FILE);
    if (ret % BLIND_NODE_SIZE) {
        BLP_TRACE("文件有误!不是20倍数%d", ret);
    }
    if (ret > (BLIND_NODE_SIZE*MAX_BLIND_CNT)) {// 1个点20字节，200就是10个点
        newLen = luat_fs_truncate(BL_CUR_FILE, ret-(BLIND_NODE_SIZE*MAX_BLIND_CNT));
        if (newLen % BLIND_NODE_SIZE) {
            BLP_TRACE("文件err!不是20倍数%d", newLen);
        }
        if (newLen == (ret-(BLIND_NODE_SIZE*MAX_BLIND_CNT))) {
            BLP_TRACE("裁剪OK");
            return;
        } else {
            BLP_TRACE("裁剪err");
        }
    }
#else
    if (ret > 20480) { ////预留
        BLP_TRACE("fs剩%d", ret);
        if (luat_fs_dexist(BL_SAVE_DIR)) {
            BLP_TRACE("创建BL OK!");
        } else {
            ret = luat_fs_mkdir(BL_SAVE_DIR);
            if (!ret) {
                BLP_TRACE("创建BL OK!");
            } else {
                BLP_TRACE("创建%s ERR!", BL_SAVE_DIR);
                return;
            }
        }
        s_bl_valid = true;
        return;
    }
    
// step2: 没空间则随意删除最旧的1个盲区文件
    BLP_TRACE("BL空间不足");
    _del_old_file(3);
    s_bl_valid = true;
    
/*    dp = ql_opendir(BL_SAVE_DIR);
    if (!dp) {
        BLP_TRACE("BLdir错");
        goto _BL_INIT_FAULT;
    }
    // 空间不足，则删掉一个最旧的文件
    
    if (1) {
        info = ql_readdir(dp);
        if (info == NULL) {
            BLP_TRACE("BLdir败or读完");
            ql_closedir(dp);
            //// ret = ql_remove(BL_SAVE_DIR);   // 尝试删除目录
            goto _BL_INIT_FAULT;
        }
        if ((info->d_type == QL_FS_TYPE_FILE) ||
            ((info->d_name[0] != '.') && (strlen(info->d_name)>3))) {    // 待验证
            BLP_TRACE("读到%s", info->d_name);
            sprintf(oldPath, ""BL_SAVE_DIR"/%s", info->d_name);
            BLP_TRACE("要删%s", oldPath);
            ret = ql_remove((const char*)oldPath);
            if (ret == QL_FS_OK) {
                BLP_TRACE("%s删OK", oldPath);
                s_bl_valid = true;
                ql_closedir(dp);
                return;
            }
        }
    }
    
// TODO: 严重失败的处理, 要删掉其他无关的文件? 先空着
_BL_INIT_FAULT:
     s_bl_valid = false;    // 暂时就是不给操作
    return;
*/
#endif
}
#endif

