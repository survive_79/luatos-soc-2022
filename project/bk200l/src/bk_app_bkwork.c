/******************************************************************************

                  版权所有 (C), 2010-2015, 厦门博琨信息技术有限公司

 ******************************************************************************
  文 件 名   : app_bk_work.c
  版 本 号   : 初稿
  作    者   : Rocky
  生成日期   : 2023年5月23日, 星期二
  最近修改   :
  功能描述   : 整个业务逻辑处理单元
  函数列表   :
              APP_GetUploadPara
              APP_Store_UploadPara
  修改历史   :
  1.日    期   : 2023年5月23日, 星期二
    作    者   : Rocky
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "bk_os.h"
#include "bk_dal_public.h"
#include "bk_port_uart.h"
#include "bk_port_hardware.h"
#include "bk_port_gprs.h"
#include "bk_port_gnss.h"
#include "bk_dal_gpsdrv.h"

#include "bk_app_picwork.h"
#include "bk_app_bkwork.h"
#include "bk_app_gprswork.h"
#include "bk_app_gprssend.h"
#if APP_OTA_EN > 0
#include "bk_app_otawork.h"
#endif
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/
void sleep_mainTask(void);
/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
#define MAX_LBS_WORK_TIME   45
/*----------------------------------------------*
 * type/enum定义                                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
static UP_PARA_NEW_T s_upload_para; // 定位模式参数
static CELL_ID_T    s_cell_info;    // cellid。注意，在这里主要缓存的是MAX_LBS_WORK_TIME时间内基站数最多的那个点
static INT16U   s_gps_ok_2d     = 0; // 统计2D定位次数
static INT32U   s_bk_work_sec   = 0;
static INT16U   s_gps_work_sec  = 0; // GPS工作时长
static INT16U   s_lbs_work_sec  = 0; // LBS工作时长
static INT16U   s_lbs_filter    = 0; // 150325 add lbs
static BOOLEAN  s_report_is_OK  = FALSE; // 新增变量，当为true时，1s的report任务自己要跳过

static INT16U s_cur_volt= 1800; // 电压瞬时值
static INT16U s_voltage = 1800; // 当前电压最大值，单位毫伏

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
INT32U app_get_WorkSec(void)
{
    return s_bk_work_sec;
}

INT16U APP_GetGPSWorkSec(void)
{
    return s_gps_work_sec;
}


INT16U GetVolt_Max(void)
{
    return s_voltage;
}
INT16U GetVolt_Cur(void)
{
    return s_cur_volt;
}
static void _query_batt(void)
{
    INT32U tmpWord;
    tmpWord = PORT_GetVoltage();
    // EC600E: 目前粗测是电压3610,读数是903。先3610/903这么修正,约分后为4/1
    // 这个是1601的修正值 s_cur_volt = tmpWord * 4;
    //BK_LOG("vtg=%d", tmpWord);
    if (tmpWord > 4000) return;   // 1603 偶发性读值超大，宁可丢掉

    //s_cur_volt = (tmpWord * 43) / 10;
    s_cur_volt = tmpWord;//ec600e的修正 s_cur_volt = tmpWord * 4;
    if (s_cur_volt > 3600) s_cur_volt = 3600;   // 做个限制

    BK_LOG("v=%d", s_cur_volt);
    if (s_cur_volt > s_voltage) {
        s_voltage = s_cur_volt;
    }
}

static INT32U s_track_cur   = 0;
static INT32U s_track_total = 0;
static INT16U s_track_period= 0;
BOOLEAN APP_IsInTrack(void)
{
    if (s_track_period) return TRUE;
    else return FALSE;
}
/*****************************************************************************
 函 数 名  : APP_TrackStart
 功能描述  : 追踪开始
 输入参数  : INT32U total_sec   0表示永久追踪
             INT16U period_sec  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2023年11月15日, 星期三
    作    者   : Rocky
    修改内容   : 新生成函数

*****************************************************************************/
void APP_TrackStart(INT32U total_sec, INT16U period_sec)
{
    INT16U tmpWord;

    if (!period_sec || (total_sec < (period_sec*3))) {// 参数合法性检测
        return;
    }
    BK_LOG("T-Go:%d,%d", total_sec, period_sec);
    PORT_Gnss_open();
    // 暂时限制就最大65535 秒
    if (total_sec > 0xffff) {
        tmpWord = 0xffff;
    } else {
        tmpWord = total_sec;
    }
    s_track_total   = total_sec;
    s_track_period  = period_sec;
    s_track_cur     = 0;
    Request_PIC_TmpWork(tmpWord);
}
// 追踪部分////////////////
void APP_TrackStop(BOOLEAN need_sleep)
{
    BK_LOG("T-Stop");

    PORT_Gnss_close();    

    s_track_total   = 0;
    s_track_period  = 0;
    s_track_cur = 0;
    if (need_sleep) {
        // APP_Stop_ConnectGPRS(); 不能关闭，还得发应答呢
        BK_LOG("@T-slp@");
        #if APP_OTA_EN > 0
        OTA_works();
        #else
        Request_PIC_Sleep(0);
        #endif
    }
}

static void _TRACK_report_func(void)
{
    INT8U ex_bit = GetPicAlarmStatus();

    if (DAL_GPS_POS_IsOK()) {   // GPS已定位
        APP_UploadPos(FALSE, &g_gps_data, NULL, TRUE, ex_bit);
        APP_UploadPos(TRUE, &g_gps_data, NULL, TRUE, ex_bit);
    } else {
        APP_UploadPos(FALSE, NULL, &g_cell, TRUE, ex_bit);
        APP_UploadPos(TRUE, NULL, &g_cell, TRUE, ex_bit);
    }
}

// 获取LBS信息的动作。取MAX_LBS_WORK_TIME内最多ID时刻的基站信息。
static void _LBS_report_func(void)
{
    if (!PORT_GSM_IS_REG() || s_report_is_OK) {
        return;
    }
    #if 0
    netTime = PORT_GetGSMTime_Nitz();
    if (netTime) {
        // TODO: 这里可以加条件，如果GPS不定位、系统时间无效、pic也校时不准的时候
        if (/*!DAL_GpsIsValid()*/1) {
            /*SYSTIME_T rSystime;
            if (6 == DAL_GetSysTime(&rSystime)) {
                if (DateValid(&rSystime.date) && TimeValid(&rSystime.time)) {
                    goto _SKIP_NETTIME;
                }
            }*/
            DAL_SetSysTime(netTime, SOURCE_NET);
        }
    }
    #endif
// _SKIP_NETTIME:
    if (s_lbs_work_sec == 2) {   // 简化处理，开1次wifi扫描
        PORT_wifi_ap_enable(1);
    }
    if (s_lbs_work_sec++ >= MAX_LBS_WORK_TIME) {
        if (s_cell_info.cell_num) {
            goto __REPORT_LBS;
        } else {
            // TODO: 这边要进入盲区处理, 一般而言，不太可能，因为在app_gprs_work的mon那边会作为盲区处理
            return;
        }
    }
    BK_LOG("LBS:%d,%d,%d", s_lbs_work_sec, s_cell_info.cell_num, PORT_has_wifi_ap());
    // 取最多的基站
    if (g_cell.cell_num >= s_cell_info.cell_num) {
        memcpy((void*)&s_cell_info, &g_cell, sizeof(s_cell_info));
    }
    if (s_cell_info.cell_num) {
        // cat1模块只查出1个基站 if ((s_cell_info.cell_num >= MAX_CELL_IN_APP) || (s_cell_info.type == CELL_TYPE_LTE)) {// 达到6个则提前结束
        // TODO: 已查明，port_gsm那边g_cell需要对type进行设置
            s_lbs_filter++;
            if (PORT_has_wifi_ap() && (s_lbs_filter > 3)) {// 等一下NITZ, 加入wifi判断
                goto __REPORT_LBS;
            }
        // }
    }
    return;
__REPORT_LBS:
    //StopTmr(s_lbs_tmr);
    if ((s_upload_para.mode & 0x03) == MODE_POS_LBS) {
        INT8U ex_bit = GetPicAlarmStatus();
        BK_LOG("准备LBS上报");
        APP_UploadPos(FALSE, NULL, &s_cell_info, FALSE, ex_bit);
        APP_UploadPos(TRUE, NULL, &s_cell_info, FALSE, ex_bit);
        s_report_is_OK = TRUE;
    }
}
// 获取位置信息的动作
static void _GPS_report_func(void)
{
    INT8U ex_bit;
    RESET_RETRY_T gt;
    
    if (s_report_is_OK) return;
    if ((s_upload_para.mode & 0x03) != MODE_POS_GPS) {
        return;
    }
    ex_bit = GetPicAlarmStatus();        

    DAL_PP_Read(GPS_CHEATER_, (void *)&gt);
    if (gt.rst_retry < 5 || gt.rst_retry > 180) {
        gt.rst_retry = GPS_CHEATER_DEFAULT;// 确保数据有效性
    }

    s_gps_work_sec++;

    if (DAL_GPS_POS_IsOK()) {   // GPS已定位
        BK_LOG("G定:(%d)%d", s_gps_work_sec, gt.rst_retry);
        s_gps_ok_2d++;  //// 150325 add
        if (DAL_GPS_POS_Is_3DFIX()) {   //// 150325 add
            //BK_LOG("3D");
            s_report_is_OK = TRUE;
            APP_UploadPos(FALSE, &g_gps_data, NULL, FALSE, ex_bit);
            APP_UploadPos(TRUE, &g_gps_data, NULL, FALSE, ex_bit);
            PORT_Gnss_close();
            return;
        } else if ((s_gps_work_sec > gt.rst_retry) || (s_gps_ok_2d > 10)) {
            //BK_LOG("3D失败");
            s_report_is_OK = TRUE;  //StopTmr(s_loc_tmr);
            s_gps_ok_2d = 0;
            APP_UploadPos(FALSE, &g_gps_data, NULL, FALSE, ex_bit);
            APP_UploadPos(TRUE, &g_gps_data, NULL, FALSE, ex_bit);
            PORT_Gnss_close();  // 231213 放后面，否则报点会被清零
        }
    } else if (s_gps_work_sec >= gt.rst_retry) {
        // 这里也要触发上报, 但是是上报LBS
        BK_LOG("GPS不定%d,上报LBS", gt.rst_retry);
        PORT_Gnss_close();
        if (s_cell_info.cell_num) {
            s_report_is_OK = TRUE;  //StopTmr(s_loc_tmr); // 141130 这里才关闭定时器
            APP_UploadPos(FALSE, NULL, &s_cell_info, FALSE, ex_bit);
            APP_UploadPos(TRUE, NULL, &s_cell_info, FALSE, ex_bit);
        } else {
            BK_LOG("暂无LBS");
            return; // 141130 没有基站就工作到有基站id为止
        }
    } else {
        BK_LOG("LOC:%d", s_gps_work_sec);
    }
}

void app_bk_work_poll_1s(void)
{
    s_bk_work_sec++;
    // 追踪处理逻辑
    if ((s_bk_work_sec % 5) == 2) {
        _query_batt();
    }
    if (APP_IsInTrack()) {
        s_track_cur++;
        if (s_track_cur < s_track_total) {
            if ((s_track_cur % s_track_period) == 0) {
                _TRACK_report_func();
            }
            return;
        } else {
            goto _FIN_SLEEP;
        }
    }
    // BK_LOG("S%d", s_bk_work_sec);
    // 下面是非追踪处理逻辑，就是等待报点条件是否满足，报点成功后，就睡眠
    _LBS_report_func();
    _GPS_report_func();

    if (s_upload_para.max_worktime.hword < app_get_WorkSec()) {
_FIN_SLEEP:
        sleep_mainTask();
        BK_LOG("耗尽睡!");
        Request_PIC_Sleep(0);
    }
}

void app_bk_work_init(void)
{
    app_gprswork_init();
    APP_GPRS_InitSend();
    
    APP_GetUploadPara();
    #if APP_OTA_EN > 0
    OTA_Init();
    #endif
    
    #if ALMFAIL_HDL_EN > 0  // 160605 add
    DAL_PP_Read(ALARM_HDL_, (void*)&almHdl);
    if ((almHdl.cur < almHdl.cnt) && (almHdl.cnt <= 100) && 
            (almHdl.period < 721) && (almHdl.period > 4)) {
        // 认为还处于报警处理状态
        almHdl.cur++;
        s_upload_para.mode = MODE_POS_GPS;
        BK_LOG("ALMHDL:%d-%d, p=%d", almHdl.cnt, almHdl.cur, almHdl.period);
        DAL_PP_Store(ALARM_HDL_, (void*)&almHdl);
    }
    #endif
    s_report_is_OK  = FALSE;
    s_lbs_work_sec  = 0;
    s_lbs_filter    = 0;
    s_bk_work_sec   = 0;
    s_gps_work_sec  = 0;
    s_gps_ok_2d     = 0;
    // LBS初始化
    memset((void*)&s_cell_info, 0, sizeof(s_cell_info));
    // GPS初始化
    if ((s_upload_para.mode & 0x03) == MODE_POS_GPS) {
        ////PORT_Gnss_open();
        PORT_Gnss_MspInit();
    }
}

void APP_Store_UploadPara(UP_PARA_NEW_T* para)
{
    CALI_PARA_T  cali = {{{15, 1, 1}, {0, 0, 0}}, 0};
    
    if (memcmp(&s_upload_para.alm, &para->alm, 3)) {// 间隔发生变更，则清掉补偿参数
        
        BK_LOG("Del Cali Para!");
        DAL_PP_Store_RAM(CALI_MINUTE_, (void *)&cali);
    }
    memcpy(&s_upload_para.mode, &para->mode, sizeof(UP_PARA_NEW_T));
    BK_LOG("UP_save");
    //OURAPP_printhexbuf((void*)&s_upload_para, sizeof(s_upload_para));
    DAL_PP_Store(UPLOAD_PARA_, (void *)para);
}

// 获取当前模式
UP_PARA_NEW_T* APP_GetUploadPara(void)
{
    DAL_PP_Read(UPLOAD_PARA_, (void*)&s_upload_para);// 160605 add

    BK_LOG("UP_para");
    OURAPP_printhexbuf((void*)&s_upload_para, 10);
    return &s_upload_para;
}

